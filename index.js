require('dotenv').load();
const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const compression = require('compression');
const Prismic = require('prismic-javascript');
const PrismicDOM = require('prismic-dom');
const expressStaticGzip = require('express-static-gzip');
const swig = require('swig');
const app = express().use(expressStaticGzip(__dirname + '/client/public/'));
app.use(compression());
app.engine('swig', swig.renderFile);
app.set('view engine', 'swig');
const http = require('http').Server(app);
const checkRedirectFile = require('./utils/redirect');

app.set('views', __dirname + '/views');
app.set('view cache', false);
swig.setDefaults({ cache: false });

const prerender = require('prerender-node').set(
  'prerenderToken',
  process.env.PRERENDERTOKEN
);
prerender.crawlerUserAgents.push('googlebot');
prerender.crawlerUserAgents.push('bingbot');
prerender.crawlerUserAgents.push('yandex');
app.use(prerender);

const apiEndpoint = 'https://fletcher-repo.prismic.io/api/v2';

// Middleware to inject prismic context
app.use(function(req, res, next) {
  res.locals.ctx = {
    endpoint: apiEndpoint
  };
  // add PrismicDOM in locals to access them in templates.
  res.locals.PrismicDOM = PrismicDOM;
  next();
});

// Initialize the prismic.io api
function initApi(req) {
  return Prismic.getApi(apiEndpoint, {
    accessToken: process.env.ACCESSTOKEN,
    req: req
  });
}

app.use(checkRedirectFile);

app.get('*', function(req, res) {
  initApi(req).then(function(api) {
    api
      .query(Prismic.Predicates.at('document.type', 'accent_colour'))
      .then(function(response) {
        let color;
        response.results.forEach(color => {
          color = color.data.hasOwnProperty('accent_colour')
            ? color.data.accent_colour
            : '#FDD927';

          // response is the response object. Render your views here.
          res.render('index', {
            accesstoken: process.env.ACCESSTOKEN,
            color
          });
        });
      });
  });
});

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

app.post('/api/form', (req, res) => {
  const htmlEmail = `
            <h3>Hard Copy Contact Details</h3>
            <ul>
                <li>First Name: ${req.body.firstname}</li>
                <li>Second Name: ${req.body.secondname}</li>
                <li>Address Line 1: ${req.body.address1}</li>
                <li>Address Line 2: ${req.body.address2}</li>
                <li>Town: ${req.body.addresstown}</li>
                <li>Postcode: ${req.body.postcode}</li>
                <li>Hard Copy Requested: ${req.body.hardCopyTitle}</li>
            </ul>
        `;

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.USERNAME,
      pass: process.env.PASSWORD
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  });

  let mailOptions = {
    from: process.env.USERNAME,
    to: process.env.EMAILTO,
    replyTo: process.env.USERNAME,
    subject: 'New Hard Copy Request 💪',
    text: '',
    html: htmlEmail
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      return console.log(err);
    }

    res.send(info);
  });
});

app.post('/api/application', (req, res) => {
  const htmlEmail = `
            <h3>Job Application for ${req.body.applicationName}</h3>
            <ul>
                <li>First name: ${req.body.firstname}</li>
                <li>Second name: ${req.body.secondname}</li>
                <li>Email: ${req.body.email}</li>
                <li>Number: ${req.body.number}</li>
            </ul>
        `;

  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.USERNAME,
      pass: process.env.PASSWORD
    },
    tls: {
      // do not fail on invalid certs
      rejectUnauthorized: false
    }
  });

  function createAttachmentObject(files, callback) {
    const fileArray = [];
    files.forEach(file => {
      fileArray.push({
        path: file
      });
    });

    return fileArray;
  }

  let mailOptions = {
    from: process.env.USERNAME,
    to: process.env.EMAILTO,
    replyTo: req.body.email,
    subject: `New Application for ${req.body.applicationName}`,
    text: '',
    html: htmlEmail,
    attachments: createAttachmentObject(req.body.droppedFiles)
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      return console.log(err);
    }

    res.send(info);
  });
});

const PORT = process.env.PORT || 3001;

http.listen(PORT, () => {
  console.log(`Server listen on port ${PORT}`);
});
