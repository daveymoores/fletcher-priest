// flow-typed signature: 1631d6f5b9a65d2ce0d9a3100c514061
// flow-typed version: <<STUB>>/html-webpack-partials-plugin_v^0.1.4/flow_v0.79.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'html-webpack-partials-plugin'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'html-webpack-partials-plugin' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'html-webpack-partials-plugin/examples/basic/dist/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/basic/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/basic/webpack.config' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/high-priority-head/dist/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/high-priority-head/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/high-priority-head/webpack.config' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/react-root/dist/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/react-root/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/react-root/webpack.config' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/variables/dist/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/variables/main' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/examples/variables/webpack.config' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/lib/partial' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/lib/util' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/test/basic.test' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/test/high-priority-head.test' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/test/react-root.test' {
  declare module.exports: any;
}

declare module 'html-webpack-partials-plugin/test/variables.test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'html-webpack-partials-plugin/examples/basic/dist/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/basic/dist/main'>;
}
declare module 'html-webpack-partials-plugin/examples/basic/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/basic/main'>;
}
declare module 'html-webpack-partials-plugin/examples/basic/webpack.config.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/basic/webpack.config'>;
}
declare module 'html-webpack-partials-plugin/examples/high-priority-head/dist/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/high-priority-head/dist/main'>;
}
declare module 'html-webpack-partials-plugin/examples/high-priority-head/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/high-priority-head/main'>;
}
declare module 'html-webpack-partials-plugin/examples/high-priority-head/webpack.config.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/high-priority-head/webpack.config'>;
}
declare module 'html-webpack-partials-plugin/examples/react-root/dist/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/react-root/dist/main'>;
}
declare module 'html-webpack-partials-plugin/examples/react-root/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/react-root/main'>;
}
declare module 'html-webpack-partials-plugin/examples/react-root/webpack.config.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/react-root/webpack.config'>;
}
declare module 'html-webpack-partials-plugin/examples/variables/dist/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/variables/dist/main'>;
}
declare module 'html-webpack-partials-plugin/examples/variables/main.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/variables/main'>;
}
declare module 'html-webpack-partials-plugin/examples/variables/webpack.config.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/examples/variables/webpack.config'>;
}
declare module 'html-webpack-partials-plugin/index' {
  declare module.exports: $Exports<'html-webpack-partials-plugin'>;
}
declare module 'html-webpack-partials-plugin/index.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin'>;
}
declare module 'html-webpack-partials-plugin/lib/partial.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/lib/partial'>;
}
declare module 'html-webpack-partials-plugin/lib/util.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/lib/util'>;
}
declare module 'html-webpack-partials-plugin/test/basic.test.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/test/basic.test'>;
}
declare module 'html-webpack-partials-plugin/test/high-priority-head.test.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/test/high-priority-head.test'>;
}
declare module 'html-webpack-partials-plugin/test/react-root.test.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/test/react-root.test'>;
}
declare module 'html-webpack-partials-plugin/test/variables.test.js' {
  declare module.exports: $Exports<'html-webpack-partials-plugin/test/variables.test'>;
}
