// flow-typed signature: 68b6bb00a19d2d12202d08b1255b1797
// flow-typed version: <<STUB>>/scrollmagic_v^2.0.5/flow_v0.79.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'scrollmagic'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'scrollmagic' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'scrollmagic/scrollmagic/minified/plugins/animation.gsap.min' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/minified/plugins/animation.velocity.min' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/minified/plugins/jquery.ScrollMagic.min' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/minified/ScrollMagic.min' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/uncompressed/plugins/animation.velocity' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/uncompressed/plugins/jquery.ScrollMagic' {
  declare module.exports: any;
}

declare module 'scrollmagic/scrollmagic/uncompressed/ScrollMagic' {
  declare module.exports: any;
}

// Filename aliases
declare module 'scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/minified/plugins/animation.gsap.min'>;
}
declare module 'scrollmagic/scrollmagic/minified/plugins/animation.velocity.min.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/minified/plugins/animation.velocity.min'>;
}
declare module 'scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min'>;
}
declare module 'scrollmagic/scrollmagic/minified/plugins/jquery.ScrollMagic.min.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/minified/plugins/jquery.ScrollMagic.min'>;
}
declare module 'scrollmagic/scrollmagic/minified/ScrollMagic.min.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/minified/ScrollMagic.min'>;
}
declare module 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'>;
}
declare module 'scrollmagic/scrollmagic/uncompressed/plugins/animation.velocity.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/uncompressed/plugins/animation.velocity'>;
}
declare module 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators'>;
}
declare module 'scrollmagic/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/uncompressed/plugins/jquery.ScrollMagic'>;
}
declare module 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js' {
  declare module.exports: $Exports<'scrollmagic/scrollmagic/uncompressed/ScrollMagic'>;
}
