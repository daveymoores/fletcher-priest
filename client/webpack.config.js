const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
// const CleanWebpackPlugin = require('clean-webpack-plugin');

const modeConfig = env => require(`./build-utils/webpack.${env}`)(env);
const presetConfig = require('./build-utils/loadPresets');

module.exports = ({ mode, presets } = { mode: 'production', presets: [] }) =>
  webpackMerge(
    {
      mode,
      entry: ['./src/index.jsx'], // "@babel/polyfill",
      devtool: 'inline-source-map',
      devServer: {
        historyApiFallback: true,
        contentBase: './dist',
        host: '0.0.0.0',
        hot: true,
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      },
      module: {
        rules: [
          {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
          },
          {
            test: /\.(png|jpg|gif)$/i,
            use: [
              {
                loader: 'url-loader',
                options: {
                  limit: 5000
                }
              }
            ]
          },
          {
            test: /\.svg$/,
            use: [
              {
                loader: 'babel-loader'
              },
              {
                loader: 'react-svg-loader',
                options: {
                  jsx: true // true outputs JSX tags
                }
              }
            ]
          },
          {
            enforce: 'pre',
            test: /\.jsx?$/,
            loader: 'eslint-loader',
            exclude: /node_modules/
          },
          {
            test: /\.jsx?$/,
            loader: 'babel-loader',
            exclude: /node_modules/
          }
        ]
      },
      resolve: {
        extensions: ['.js', '.jsx', '.json']
      },
      externals: {
        react: 'React',
        'react-dom': 'ReactDOM'
      },
      output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/'
      },
      stats: {
        colors: true,
        reasons: true,
        chunks: true
      },
      plugins: [
        // new CleanWebpackPlugin(['public']),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new webpack.ProgressPlugin()
      ]
    },
    modeConfig(mode),
    presetConfig({ mode, presets })
  );
