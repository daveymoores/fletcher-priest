const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");

module.exports = () => ({
    devtool: false,
    output: {
        publicPath: "./",
        filename: "bundle.js"
    },
    plugins: [
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new MiniCssExtractPlugin(),
        new CopyWebpackPlugin([{ from: "dist/", to: "", toType: "dir" }])
    ]
});
