import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import LazyLoad from 'react-lazyload';
import { Date } from 'prismic-reactjs';
import Moment from 'moment';

const GenericCont = styled.div`
  padding: 0 10px;
  height: ${props => (props.medium ? '75px' : '450px')};
  display: flex;
  justify-content: ${props =>
    props.medium ? 'justify-content' : 'space-between'};
  flex-direction: ${props => (props.medium ? 'row' : 'column')};
  border: ${props =>
    props.medium
      ? `${`1px solid ${
          props.searchResult ? props.theme.darkGrey7 : props.theme.tertiary
        }`}`
      : 'none'};
  margin-bottom: ${props => (props.medium ? '20px' : 0)};

  img {
    width: 100%;
  }

  h3,
  h4,
  h5 {
    margin: 0px;
    color: ${props => (props.searchResult ? 'white' : 'black')};
  }
`;

const Thumbnail = styled(Link)`
  height: 100%;
  width: 120px;
  min-width: 120px;
  max-width: 120px;
  display: block;
  margin-right: 10px;

  > div {
    width: 100%;
    height: 100%;
    background-size: cover;
    background-position: center center;
  }
`;

const ThumbnailText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 0;
`;

const Name = styled.h3`
  font-size: 12px !important;

  &::after {
    display: none;
  }
`;

const Title = styled.h4`
  font-size: 8px !important;
  margin: 0px !important;
`;

const GenericTeaser = ({ result, searchResult, PrismicReact }) => {
  const title =
    result.type === 'job_listing'
      ? result.data.job_listing_title
      : result.data.page_title;

  const url =
    result.type === 'job_listing' ? `/careers/${result.uid}` : `/${result.uid}`;
  return (
    <GenericCont medium searchResult={searchResult}>
      <Link to={url}>
        <ThumbnailText>
          <Name>{PrismicReact.RichText.asText(title)}</Name>
        </ThumbnailText>
      </Link>
    </GenericCont>
  );
};

export default GenericTeaser;
