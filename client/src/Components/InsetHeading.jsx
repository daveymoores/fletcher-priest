// @flow

import React from "react";
import styled from "styled-components";
import media from "../utils/style-utils";

const SubHeadingContainer = styled.header`
    margin: 0 0 30px 20px;

    ${media.tablet`
      margin: 0 0 0 40px;
    `};
`;

const SubHeading = styled.h3`
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 20px;

    ${media.tablet`
      font-size: 30px;
    `};

    ${media.desktop`
      font-size: 36px;
    `};
`;

type Props = {
    text: string
};

const InsetHeader = (props: Props) => {
    const { text } = props;
    return (
        <SubHeadingContainer>
            <SubHeading>{text}</SubHeading>
        </SubHeadingContainer>
    );
};

export default InsetHeader;
