// @flow

import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Btn = styled.a`
    padding: 15px 20px;
    background-color: ${props => props.theme.primary};
    font-family: ${props => props.theme.FFDINWebProMedium};
    display: table;
    opacity: ${props => (props.type === "inactive" ? 0.3 : 1)};
`;

type Props = {
    content: string,
    destination: string,
    type: string
};

const Button = (props: Props) => {
    const { content, destination, type } = props;

    return (
        <Btn href={destination} type={type} target="_blank" rel="noopener">
            {content} &rarr;
        </Btn>
    );
};

export default Button;
