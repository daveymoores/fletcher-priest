import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled, { css } from "styled-components";
import { connect } from "react-redux";

import LoaderSearchBar from "../Loaders/LoaderSearchBar";
import GridContainer from "../Common/GridContainer";
import Small from "./icons/small.svg";
import Large from "./icons/large.svg";
import {
    OpenDropdown,
    OpenSearchDropdown
} from "../Animations/SearchAnimation";
import GridSlider from "./GridSlider";
import {
    SearchParent,
    SearchDropdown,
    SearchDropdownOptions,
    SearchItem,
    SearchResize,
    SearchTextInput,
    SearchContainer,
    SearchIcon,
    DownChevron,
    LoadMore
} from "./search-bar-styles";
import { setFilterState } from "../redux/actionCreators";
import SearchBarAnimation from "../Animations/SearchBarAnimation";

function setSelectState(element) {
    const parent = element.parentNode.parentNode;
    const currentSelected = parent.querySelectorAll(".selected");

    // does target element contain selected
    function checkChildrenForSelected() {
        if (element.nextSibling) {
            const selected =
                element.nextSibling.querySelector(".selected") || false;
            return selected;
        }
        return false;
    }

    if (element.classList.contains("selected")) {
        element.classList.remove("selected");
    } else if (currentSelected && !checkChildrenForSelected()) {
        [].forEach.call(currentSelected, e => {
            e.classList.remove("selected");
        });
        element.classList.add("selected");
    } else {
        element.classList.add("selected");
    }
    return true;
}

class SearchBarProjects extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: null,
            dropdownData: null,
            projectLandingResults: null,
            singleProjectResults: null,
            textSearch: "",
            filterTerm: ["document.type", "single_project_page"],
            sortTerm: "[my.single_project_page.sort_number desc]",
            gridSize: 100,
            response: null,
            totalRendered: null, // total visible on the page
            resultsSize: null, // number of total results
            increment: 6, // number to increment by
            revealed: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.toggleSearchDropdown = this.toggleSearchDropdown.bind(this);
        this.handleSortClick = this.handleSortClick.bind(this);
        this.toggleDropdown = this.toggleDropdown.bind(this);
        this.handleFilterClick = this.handleFilterClick.bind(this);
    }

    componentWillMount() {
        this.fetchPage(this.props);
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            results,
            dropdownData,
            textSearch,
            filterTerm,
            sortTerm,
            gridSize,
            projectLandingResults,
            singleProjectResults,
            revealed
        } = this.state;
        const { callback, history } = this.props;

        if (
            prevState.sortTerm !== sortTerm ||
            prevState.filterTerm !== filterTerm ||
            prevState.textSearch !== textSearch
        ) {
            history.push({
                pathname: "/projects",
                search: `?sortTerm=${sortTerm}&filterTerm=${filterTerm}&textSearch=${textSearch}`
            });
        }

        if (this.searchParentRef && !revealed) {
            this.setState({ revealed: true }, () => {
                this.animations = new SearchBarAnimation(this.searchParentRef);
            });
        }

        if (prevState.gridSize !== gridSize || prevState.results !== results) {
            callback([results, textSearch, gridSize]);
        }

        // if they haven't been combined yet
        if (
            projectLandingResults !== null &&
            singleProjectResults !== null &&
            dropdownData == null
        ) {
            // remove carousel from deep link results
            const setDeepLinkResults =
                history.location.search === ""
                    ? projectLandingResults.concat(singleProjectResults)
                    : singleProjectResults;
            /*eslint-disable */
            this.setState(
                {
                    dropdownData: projectLandingResults.concat(
                        singleProjectResults
                    ),
                    results: setDeepLinkResults
                },
                () => {
                    this.fetchDeepLink();
                }
            );
            /* eslint-enable */
        }
    }

    gridSizeCallback = data => {
        this.updateState(data);
    };

    fetchDeepLink() {
        const { history } = this.props;

        if (history.location.search !== "") {
            const searchTerms = history.location.search.split("&");
            let s = searchTerms[0]
                .replace("?sortTerm=", "")
                .replace("%20", " ");
            let f = searchTerms[1]
                .replace("filterTerm=", "")
                .replace("[", "")
                .replace("]", "")
                .replace("%20", " ")
                .split(",");
            // const t = searchTerms[2].replace("textSearch=", "");

            s = s[0] === "" ? "" : s;
            f = f[0] === "" ? ["document.type", "single_project_page"] : f;

            this.setState(
                {
                    filterTerm: f,
                    sortTerm: s
                },
                () => {
                    this.buildPredicate();
                }
            );
        }
    }

    fetchPage(props) {
        const { filterTerm, sortTerm, totalRendered } = this.state;
        const paginationTotal = totalRendered || 12;

        if (props.prismicCtx) {
            props.prismicCtx.api
                .query(
                    Prismic.Predicates.any("document.type", [
                        "project_landing"
                    ]),
                    {
                        orderings: sortTerm,
                        pageSize: paginationTotal
                    }
                )
                .then(res => {
                    const { results } = res;

                    this.setState({
                        projectLandingResults: results
                    });
                });

            props.prismicCtx.api
                .query(
                    Prismic.Predicates.any("document.type", [
                        "single_project_page"
                    ]),
                    {
                        orderings: sortTerm,
                        pageSize: paginationTotal
                    }
                )
                .then(res => {
                    const {
                        results,
                        total_results_size: totalResultsSize
                    } = res;
                    this.setState({
                        singleProjectResults: results,
                        totalRendered: results.length,
                        resultsSize: totalResultsSize
                    });
                });
        }
        return null;
    }

    handleChange(event) {
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "complete") handleFilterChange("loading");
        this.searchParentRef.style.marginBottom = "60px";

        this.setState(
            { textSearch: event.target.value, totalRendered: null },
            () => {
                this.buildPredicate();
            }
        );
    }

    handleFilterClick(e) {
        const target = e.currentTarget;
        setSelectState(target);
        const { filterTerm } = this.state;
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "complete") handleFilterChange("loading");
        const parent = target.parentNode.parentNode;
        let term;

        function getSearchTerm(element) {
            return element.getAttribute("data-search-term");
        }

        if (!target.classList.contains("selected")) {
            term = ["document.type", "single_project_page"];
        } else if (parent.getAttribute("data-search-predicate") === "service") {
            term = [
                `my.single_project_page.service_group.service`,
                getSearchTerm(target)
            ];
        } else {
            term = [
                `my.single_project_page.sector_group.sector`,
                getSearchTerm(target)
            ];
        }

        this.setState({ filterTerm: term, totalRendered: null }, () => {
            this.buildPredicate();
        });
    }

    toggleDropdown(e) {
        const target = e.currentTarget;
        setSelectState(target);
        const dropdown = target.parentNode.querySelector("ul");
        const dropdownItems = dropdown.querySelectorAll("li");

        OpenDropdown(dropdownItems);
        this.searchParentRef.classList.add("dropdown-open");
        const currentActiveDropdown = this.searchParentRef.querySelector(
            ".visible"
        );

        if (currentActiveDropdown) {
            this.searchInputRef.style = "";
            currentActiveDropdown.classList.remove("visible");
        }

        dropdown.classList.add("visible");
    }

    toggleSearchDropdown(e) {
        e.preventDefault();
        const target = e.currentTarget;
        setSelectState(target);
        const input = target.querySelector("input");
        const currentActiveDropdown = this.searchParentRef.querySelector(
            ".visible"
        );

        if (currentActiveDropdown) {
            currentActiveDropdown.classList.remove("visible");
            currentActiveDropdown.classList.remove("filter-open");
        }

        target.classList.add("visible");

        if (window.innerWidth < 576) {
            OpenSearchDropdown(input);
        }
    }

    handleSortClick(e) {
        const target = e.currentTarget;
        const { sortTerm } = this.state;
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "complete") handleFilterChange("loading");
        setSelectState(target);
        let s;

        function findSortParam(value) {
            let x;
            switch (value) {
                case "latest":
                    x = "[my.single_project_page.completion_date desc]";
                    break;
                case "alphabetical":
                    x = "[my.single_project_page.project_name]";
                    break;
                default:
                    x = "";
            }

            return x;
        }

        if (!target.classList.contains("selected")) {
            s = "";
        } else {
            s = target.getAttribute("data-sort-term")
                ? findSortParam(target.getAttribute("data-sort-term"))
                : sortTerm;
        }

        this.setState({ sortTerm: s }, () => {
            this.buildPredicate();
        });
    }

    updateState(data) {
        this.setState(
            prevState => {
                if (prevState.gridSize !== data) {
                    return { gridSize: data };
                }
                return null;
            },
            () => {
                this.buildPredicate();
            }
        );
    }

    loadMore(e) {
        e.preventDefault();
        const { response, totalRendered, increment, resultsSize } = this.state;
        const { filterState, handleFilterChange } = this.props;
        const newPageSize = totalRendered + increment;
        const newTotal = newPageSize < resultsSize ? newPageSize : resultsSize;
        if (filterState === "complete") handleFilterChange("loading");

        this.setState({ totalRendered: newTotal }, () => {
            this.buildPredicate();
        });
    }

    buildPredicate() {
        const { prismicCtx, filterState, handleFilterChange } = this.props;
        const {
            textSearch,
            filterTerm,
            sortTerm,
            gridSize,
            totalRendered
        } = this.state;

        const paginationTotal = totalRendered || 12;

        if (prismicCtx) {
            return prismicCtx.api
                .query(
                    [
                        Prismic.Predicates.at(
                            "document.type",
                            "single_project_page"
                        ),
                        Prismic.Predicates.at(filterTerm[0], filterTerm[1]),
                        Prismic.Predicates.fulltext("document", textSearch)
                    ],
                    {
                        orderings: sortTerm,
                        pageSize: paginationTotal
                    }
                )
                .then(response => {
                    const { results } = response;
                    this.setState(
                        {
                            results,
                            response,
                            totalRendered: paginationTotal,
                            resultsSize: response.total_results_size
                        },
                        () => {
                            if (filterState === "loading")
                                handleFilterChange("complete");
                        }
                    );
                });
        }
        return null;
    }

    render() {
        const { value, dropdownData, totalRendered, resultsSize } = this.state;
        const { children } = this.props;
        const serviceArr = [];
        const sectorArr = [];
        const serviceDuplicateCheck = [];
        const sectorDuplicateCheck = [];
        const ctx = this;

        function renderLoadMore() {
            if (totalRendered < resultsSize) {
                return (
                    <LoadMore href="#" onClick={ctx.loadMore.bind(ctx)}>
                        Load More
                    </LoadMore>
                );
            }

            return null;
        }

        if (dropdownData) {
            const LoadMoreButton = renderLoadMore();
            return (
                <div>
                    <GridContainer type="pullRight">
                        <SearchParent
                            innerRef={e => {
                                this.searchParentRef = e;
                            }}
                        >
                            <SearchDropdown>
                                <SearchItem onClick={this.toggleDropdown}>
                                    <span>Service</span> <DownChevron />
                                </SearchItem>
                                <SearchDropdownOptions data-search-predicate="service">
                                    {dropdownData
                                        .filter(
                                            item =>
                                                item.type ===
                                                "single_project_page"
                                        )
                                        .map(item => {
                                            item.data.service_group.map(x => {
                                                if (
                                                    serviceArr.indexOf(
                                                        x.service.uid
                                                    ) === -1
                                                ) {
                                                    serviceArr.push([
                                                        x.service.uid,
                                                        x.service.id
                                                    ]);
                                                }
                                            });

                                            return serviceArr.map(x => {
                                                if (
                                                    serviceDuplicateCheck.indexOf(
                                                        x[0]
                                                    ) === -1
                                                ) {
                                                    serviceDuplicateCheck.push(
                                                        x[0]
                                                    );
                                                    const service = x[0];
                                                    let serviceLabel = service.replace(
                                                        /-/g,
                                                        " "
                                                    );
                                                    serviceLabel =
                                                        serviceLabel
                                                            .charAt(0)
                                                            .toUpperCase() +
                                                        serviceLabel.slice(1);

                                                    return (
                                                        <li key={x[1]}>
                                                            <SearchItem
                                                                data-search-term={
                                                                    x[1]
                                                                }
                                                                onClick={
                                                                    this
                                                                        .handleFilterClick
                                                                }
                                                            >
                                                                <span>
                                                                    {
                                                                        serviceLabel
                                                                    }
                                                                </span>
                                                            </SearchItem>
                                                        </li>
                                                    );
                                                }

                                                return null;
                                            });
                                        })}
                                </SearchDropdownOptions>
                            </SearchDropdown>

                            <SearchDropdown>
                                <SearchItem onClick={this.toggleDropdown}>
                                    <span>Sector</span> <DownChevron />
                                </SearchItem>
                                <SearchDropdownOptions data-search-predicate="sector">
                                    {dropdownData
                                        .filter(
                                            item =>
                                                item.type ===
                                                "single_project_page"
                                        )
                                        .map(item => {
                                            item.data.sector_group.map(x => {
                                                if (
                                                    sectorArr.indexOf(
                                                        x.sector.uid
                                                    ) === -1
                                                ) {
                                                    sectorArr.push([
                                                        x.sector.uid,
                                                        x.sector.id
                                                    ]);
                                                }
                                            });

                                            return sectorArr.map(x => {
                                                if (
                                                    sectorDuplicateCheck.indexOf(
                                                        x[0]
                                                    ) === -1
                                                ) {
                                                    sectorDuplicateCheck.push(
                                                        x[0]
                                                    );
                                                    const sector = x[0];
                                                    let sectorLabel = sector.replace(
                                                        /-/g,
                                                        " "
                                                    );

                                                    switch (sectorLabel) {
                                                        case "retail hospitality":
                                                            sectorLabel =
                                                                "Retail and Hospitality";
                                                            break;
                                                        case "culture sport leisure":
                                                            sectorLabel =
                                                                "Culture, Sport and Leisure";
                                                            break;
                                                        default:
                                                            sectorLabel =
                                                                sectorLabel
                                                                    .charAt(0)
                                                                    .toUpperCase() +
                                                                sectorLabel.slice(
                                                                    1
                                                                );
                                                    }

                                                    return (
                                                        <li key={x[1]}>
                                                            <SearchItem
                                                                data-search-term={
                                                                    x[1]
                                                                }
                                                                onClick={
                                                                    this
                                                                        .handleFilterClick
                                                                }
                                                            >
                                                                <span>
                                                                    {
                                                                        sectorLabel
                                                                    }
                                                                </span>
                                                            </SearchItem>
                                                        </li>
                                                    );
                                                }

                                                return null;
                                            });
                                        })}
                                </SearchDropdownOptions>
                            </SearchDropdown>

                            <SearchDropdown>
                                <SearchItem onClick={this.toggleDropdown}>
                                    <span>Sort</span> <DownChevron />
                                </SearchItem>
                                <SearchDropdownOptions data-search-predicate="sort">
                                    <li>
                                        <SearchItem
                                            data-sort-term="latest"
                                            onClick={this.handleSortClick}
                                        >
                                            <span>Latest</span>
                                        </SearchItem>
                                    </li>
                                    <li>
                                        <SearchItem
                                            data-sort-term="alphabetical"
                                            onClick={this.handleSortClick}
                                        >
                                            <span>Alphabetical</span>
                                        </SearchItem>
                                    </li>
                                </SearchDropdownOptions>
                            </SearchDropdown>
                            <SearchResize>
                                <GridSlider callback={this.gridSizeCallback} />
                            </SearchResize>
                            <SearchContainer
                                onClick={this.toggleSearchDropdown}
                            >
                                <SearchTextInput
                                    value={value}
                                    innerRef={el => (this.searchInputRef = el)}
                                    onChange={this.handleChange}
                                    onFocus={this.handleChange}
                                    placeholder="Search"
                                    type="search"
                                />
                                <SearchIcon />
                            </SearchContainer>
                        </SearchParent>
                    </GridContainer>

                    {children}
                    {LoadMoreButton}
                </div>
            );
        }
        return <LoaderSearchBar />;
    }
}

const mapStateToProps = state => ({
    filterState: state.filterState
});

const mapDispatchToProps = dispatch => ({
    handleFilterChange(value) {
        dispatch(setFilterState(value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchBarProjects);
