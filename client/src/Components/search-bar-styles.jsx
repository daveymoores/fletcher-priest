import styled, { css } from "styled-components";
import media from "../utils/style-utils";
import Chevron from "./icons/chevron.svg";
import Search from "./icons/search.svg";
import Spots from "../Common/textures/spots.png";

const SelectStyle = css`
    border: 1px solid ${props => props.theme.darkGrey};
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 18px;
    line-height: 18px;
    padding: 20px 35px 20px 15px;
    background-color: white;
    height: 60px;
    display: block;
    position: relative;
    margin-right: -1px;

    ${media.mobile`
      padding: 20px 45px 20px 20px;
    `} ${media.tablet`
      min-width: 180px;
    `};
`;

export const SearchParent = styled.div`
    display: flex;
    align-items: center;
    width: calc(100% + 1px);
    margin: 40px 0 60px;
    height: 60px;
    position: relative;
    z-index: 2;
    background: url(${Spots}) repeat center center;
`;

export const SearchDropdown = styled.div`
    flex: 1;

    ${media.mobile`
      flex: none;
    `};
`;

export const SearchDropdownOptions = styled.ul`
    position: absolute;
    top: calc(100% - 1px);
    width: 100%;
    left: 0;
    list-style: none;
    display: flex;
    margin: 0;
    padding: 0;
    z-index: -1;
    margin-left: 1px;
    flex-wrap: wrap;
    opacity: 0;
    pointer-events: none;

    &.filter-open {
        opacity: 1;
    }

    li {
        width: calc(100% + 1px);
        margin-top: -1px;
        margin-left: -1px;

        a {
            border-right: 1px solid ${props => props.theme.darkGrey};
        }

        ${media.mobile`
          width: calc(50% + 1px);
        `};

        ${media.desktop`
        width: auto;
      `};
    }

    li:last-child > a {
        width: calc(100% + 1px);

        ${media.mobile`
          border-right: 1px solid ${props => props.theme.darkGrey};
          width: auto;
        `};
    }

    &.visible {
        pointer-events: auto;
        opacity: 1;

        ${media.desktop`
          flex-wrap: no-wrap;
        `};
    }
`;

export const SearchItem = styled.a`
    ${SelectStyle};
    cursor: pointer;

    > span {
        position: relative;

        &::before {
            content: "";
            position: absolute;
            left: 0;
            bottom: -2px;
            height: 2px;
            width: 0;
            background-color: ${props => props.theme.primary};
            display: block;
            transition: width 0.3s cubic-bezier(0.77, 0, 0.175, 1);
        }
    }

    &.selected {
        > span::before {
            width: 100%;
        }
    }
`;

export const SearchResize = styled.div`
    ${SelectStyle};
    width: 300px;
    display: none;
    padding: 20px;

    ${media.desktop`
      display: block;
      padding: 20px;
    `};
`;

export const SearchTextInput = styled.input`
    ${SelectStyle};
    background-color: ${props => props.theme.lightGrey};
    -webkit-appearance: none;
    width: 100%;
    position: absolute;
    left: 0;
    top: calc(100% - 1px);
    opacity: 0;
    visibility: hidden;
    z-index: -1;
    pointer-events: ${props => (props.disable ? "none" : "auto")};

    ${media.mobile`
      position: relative;
      top: inherit;
      left: inherit;
      opacity: 1;
      visibility: visible;
      z-index: 0;
    `};
`;

export const SearchContainer = styled.div`
    border: 1px solid ${props => props.theme.darkGrey};
    border-right: none;
    cursor: pointer;
    width: 60px;
    height: 100%;
    background-color: ${props => props.theme.lightGrey};

    ${media.mobile`
      border: none;
      position: relative;
      cursor: default;
      width: auto;
      flex: 1;
    `};
`;

export const SearchIcon = styled(Search)`
    position: absolute;
    right: 19px;
    height: 20px;
    width: 20px;
    top: 50%;
    transform: translateY(-50%);

    ${media.mobile`
      right: 20px;
      height: 15px;
      width: 15px;
    `};
`;

export const DownChevron = styled(Chevron)`
    height: 6px;
    width: 11px;
    right: 20px;
    top: 50%;
    position: absolute;
    transform: translateY(-50%);

    ${media.mobile`
      right: 20px;
      top: 50%;
      position: absolute;
    `};
`;

export const LoadMore = styled.a`
    display: table;
    margin: 40px auto;
    padding: 15px 34px;
    background-color: ${props => props.theme.primary};
    font-family: ${props => props.theme.FFDINWebProMedium};
`;
