// @flow

import React from 'react';
import styled from 'styled-components';
import uniqid from 'uniqid';
import { Link } from 'react-router-dom';
import get from 'lodash/get';
import PrismicConfig from '../prismic-configuration';

const LinkWrapper = styled.div`
  + div {
    margin-top: -30px;
  }

  > div {
    display: inline-block;
  }

  span {
    font-family: ${props => props.theme.FFDINWebProBlack};
    font-size: 20px;
    color: ${props => props.theme.primary};
    display: inline-block;
  }

  a {
    font-family: ${props => props.theme.FFDINWebProBlack};
    font-size: 20px;
    margin-top: 5px;
    color: ${props => props.theme.primary};
    display: inline-block;
    padding-right: 10px;
  }
`;

type Props = {
  link: Array<Object>,
  PrismicReact: () => mixed,
  type?: string
};

function returnString(link, PrismicReact, type) {
  if (type === 'careers') {
    return [
      <div key={uniqid.process()}>
        <Link
          to={{
            pathname: '/careers',
            hash: '#vacancies'
          }}
        >
          {PrismicReact.RichText.asText(link)}
        </Link>
      </div>,
      <span key={uniqid.process()}>&rarr;</span>
    ];
  }
  return [
    <div key={uniqid.process()}>
      {PrismicReact.RichText.render(link, PrismicConfig.linkResolver)}
    </div>,
    <span key={uniqid.process()}>&rarr;</span>
  ];
}

const InternalLink = (props: Props) => {
  const { link, PrismicReact, type } = props;

  if (link && link.length > 0) {
    if (link[0].text.length > 0) {
      return (
        <LinkWrapper key={uniqid.process()}>
          {returnString(link, PrismicReact, type)}
        </LinkWrapper>
      );
    }
  }

  return null;
};

InternalLink.defaultProps = {
  type: ''
};

export default InternalLink;
