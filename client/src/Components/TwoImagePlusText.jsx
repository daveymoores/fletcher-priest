import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import InternalLink from "./InternalLink";
import media from "../utils/style-utils";

const maxHeight = "800px";

const ImageWrapper = styled.div`
  position: relative;
  margin-bottom: 40px;

  ${media.tablet`
      margin-bottom: 100px;
    `};
`;

const ImageTrack = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  position: relative;
  overflow: hidden;
  max-height: ${maxHeight};

  > div {
    &:first-child {
      ${media.tablet`
            padding-right: 10px;
          `};
    }

    &:last-child {
      display: none;

      ${media.tablet`
              padding-left: 10px;
              display: block;
            `};
    }

    > img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
`;

const TextContainer = styled.aside`
  background-color: white;
  padding: 20px 60px 10px 30px;
  position: relative;
  bottom: 0;
  width: calc(100% - 100px);
  left: ${props => (props.type === "Left" ? "15px" : "85px")};
  margin-left: 0px;
  margin-top: -50px;

  ${media.mobile`
      max-width: 550px;
    `};

  ${media.tablet`
      margin-left: ${props => (props.type === "Left" ? "-15%" : "15%")};
      position: absolute;
      margin-top: 0px;
      left: 50%;
      transform: translateX(-50%);
    `};

  > h3 {
    margin: 0;
    font-size: 28px;
    line-height: 38px;
    font-family: ${props => props.theme.FFDINWebProMedium};
  }

  > p {
    margin-bottom: 0px;
    font-family: ${props => props.theme.FFDINWebProMedium};
  }
`;

const TwoImagePlusText = props => {
  const { slice, prismicCtx, PrismicReact, location } = props;

  return (
    <ImageWrapper>
      <ImageTrack>
        <div>
          <img src={slice.primary.image_left.url} alt="" />
        </div>
        <div>
          <img src={slice.primary.image_right.url} alt="" />
        </div>
      </ImageTrack>
      <TextContainer type={slice.primary.text_positioning}>
        <h3>{PrismicReact.RichText.asText(slice.primary.section_heading)}</h3>
        <p>{PrismicReact.RichText.asText(slice.primary.section_description)}</p>
        <InternalLink
          PrismicReact={PrismicReact}
          link={slice.primary.internal_link}
        />
      </TextContainer>
    </ImageWrapper>
  );
};

export default TwoImagePlusText;
