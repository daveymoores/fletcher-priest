import styled, { css } from "styled-components";
import { Link } from "react-router-dom";
import media from "../utils/style-utils";

export const AwardRow = styled.div`
  display: table;
  width: 100%;
  font-size: 12px;
  font-family: ${props => props.theme.FFDINWebProMedium};
  border: 1px solid ${props => props.theme.tertiary};
  padding: 15px 40px 15px 15px;
  margin-bottom: 5px;
  position: relative;

  > div {
    display: flex;
    text-align: left;
    width: 100%;
    padding-bottom: 3px;

    @media (min-width: 550px) {
      display: table-cell;
      width: 22%;
      padding-right: 3%;
    }
  }
`;

export const AwardWrapper = styled.div``;

export const ProjectArrow = styled(Link)`
  position: absolute;
  right: 15px;
  top: 50%;
  transform: translateY(-50%);
  width: 18px;
  height: 18px;
  border-radius: 50%;
  overflow: hidden;
  opacity: ${props => (props.type === "disabled" ? 0.3 : 1)};
  pointer-events: ${props => (props.type === "disabled" ? "none" : "auto")};

  > svg {
    width: 100%;
    height: 100%;
  }
`;

export const YearHeading = styled.h3`
  font-size: 36px;
  margin-top: 0;
  margin-left: ${props => (props.inset ? `40px` : `0px`)};
`;

export const YearGroup = styled.div`
  margin-bottom: 40px;
`;
