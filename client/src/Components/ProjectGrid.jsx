import React, { Component } from "react";
import Loadable from "react-loadable";
import styled from "styled-components";
import debounce from "lodash/debounce";
import VisibilitySensor from "react-visibility-sensor";

import { Grid, GridItem } from "styled-grid-responsive";
import GridContainer from "../Common/GridContainer";
import { FluidGrid, FluidRow, FluidGridParent } from "../Common/FluidGrid";
import SearchBarProjects from "./SearchBarProjects";
import KeyProjectsCarousel from "./KeyProjectsCarousel";
import LoaderGridItem from "../Loaders/LoaderGridItem";
import LoaderSearchBar from "../Loaders/LoaderSearchBar";
import LoaderPageContent from "../Loaders/LoaderPageContent";
import ProjectTeaser from "./ProjectTeaser";
import GridAnimation from "../Animations/GridAnimation";

const LoadableSearchBarProjects = Loadable({
    loader: () => import("../Components/SearchBarProjects"),
    loading: () => <LoaderSearchBar />
});

const LoadableLoadingStateWrapper = Loadable({
    loader: () => import("./LoadingStateWrapper"),
    loading: () => <LoaderPageContent />
});

const NoResults = styled.h3`
    text-align: center;
    display: block;
    margin: 60px auto 200px;
    max-width: 400px;

    strong {
        font-family: ${props => props.theme.FFDINWebProBold};
    }
`;

const CarouselBorderWrapper = styled.div`
    border-bottom: 1px solid ${props => props.theme.darkGrey2};
    padding: 0;
    margin-bottom: 60px;
`;

class ProjectGrid extends Component {
    constructor() {
        super();
        this.state = {
            results: null,
            textSearch: "",
            gridSize: 100,
            rowSize: 3
        };
        this.handleResize = debounce(this.handleResize, 100);
    }

    componentWillMount() {
        const { results } = this.props;
        this.setState({ results });
        this.handleResize();
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleResize, false);

        this.domRefs = {
            wrapper: this.wrapperRef
        };
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize, false);
    }

    handleResize = () => {
        const ww = window.innerWidth;
        let rowSize;

        if (ww > 768) {
            rowSize = 3;
        } else if (ww <= 768 && ww >= 576) {
            rowSize = 2;
        } else {
            rowSize = 1;
        }

        this.setState(() => ({
            viewport: { x: window.innerWidth, y: window.innerHeight },
            rowSize
        }));
    };

    searchCallback = data => {
        this.updateState(data);
    };

    updateState(data) {
        this.setState(prevState => {
            if (
                prevState.results !== data[0] ||
                prevState.gridSize !== data[2]
            ) {
                return {
                    results: data[0],
                    textSearch: data[1],
                    gridSize: data[2]
                };
            }
            return null;
        });
    }

    render() {
        const { results, textSearch, gridSize, rowSize } = this.state;
        const { prismicCtx, location, PrismicReact, history } = this.props;
        let rowArray = [];

        function generateRows(i, totalRows, remainder, element) {
            if (i < totalRows * rowSize) {
                rowArray.push(element);

                if (rowArray.length === rowSize) {
                    const newRow = rowArray;
                    rowArray = [];
                    return newRow;
                }

                return null;
            }

            rowArray.push(element);

            if (rowArray.length === remainder) {
                const newRow = rowArray;
                rowArray = [];
                return newRow;
            }

            return null;
        }

        function returnBoxStyle(item, i, arr) {
            const remainder = arr.length % rowSize;
            const totalRows = Math.floor(arr.length / rowSize);
            const TeaserComponent = (
                <ProjectTeaser
                    gridSize={gridSize}
                    key={item.id}
                    project={item}
                    prismicCtx={prismicCtx}
                    PrismicReact={PrismicReact}
                    location={location}
                />
            );

            if (gridSize === 100) {
                const element = (
                    <FluidGrid key={item.id}>{TeaserComponent}</FluidGrid>
                );
                return generateRows(i, totalRows, remainder, element);
            }

            if (gridSize === 50) {
                const element = (
                    <GridItem col={1 / rowSize} key={item.id}>
                        {TeaserComponent}
                    </GridItem>
                );
                return generateRows(i, totalRows, remainder, element);
            }

            return (
                <GridItem col={1 / 1} key={item.id}>
                    {TeaserComponent}
                </GridItem>
            );
        }

        function checkResults(res) {
            if (res) {
                if (res.length > 0) {
                    if (gridSize === 100) {
                        return (
                            <div>
                                {results.map((item, index) => {
                                    if (item.type === "project_landing") {
                                        return (
                                            <CarouselBorderWrapper
                                                key={item.id}
                                            >
                                                <GridContainer type="pullRight">
                                                    <KeyProjectsCarousel
                                                        prismicCtx={prismicCtx}
                                                        location={location}
                                                        gridSize={gridSize}
                                                    />
                                                </GridContainer>
                                            </CarouselBorderWrapper>
                                        );
                                    }
                                    return null;
                                })}

                                <GridContainer type="pullRight">
                                    <FluidGridParent>
                                        {results
                                            .filter(
                                                item =>
                                                    item.type ===
                                                    "single_project_page"
                                            )
                                            .map((item, index, array) => {
                                                const boxStyle = returnBoxStyle(
                                                    item,
                                                    index,
                                                    array
                                                );

                                                if (boxStyle !== null) {
                                                    return (
                                                        <FluidRow key={item.id}>
                                                            {boxStyle}
                                                        </FluidRow>
                                                    );
                                                }
                                                return null;
                                            })}
                                    </FluidGridParent>
                                </GridContainer>
                            </div>
                        );
                    }
                    if (gridSize === 50) {
                        return (
                            <GridContainer>
                                {results.map((item, index) => {
                                    if (item.type === "project_landing") {
                                        return (
                                            <Grid key={item.id}>
                                                <KeyProjectsCarousel
                                                    prismicCtx={prismicCtx}
                                                    location={location}
                                                    gridSize={gridSize}
                                                />
                                            </Grid>
                                        );
                                    }
                                    return null;
                                })}

                                {results
                                    .filter(
                                        item =>
                                            item.type === "single_project_page"
                                    )
                                    .map((item, index, array) => {
                                        const boxStyle = returnBoxStyle(
                                            item,
                                            index,
                                            array
                                        );

                                        if (boxStyle !== null) {
                                            return (
                                                <Grid key={item.id}>
                                                    {boxStyle}
                                                </Grid>
                                            );
                                        }
                                        return null;
                                    })}
                            </GridContainer>
                        );
                    }

                    return (
                        <GridContainer>
                            {results.map((item, index) => {
                                if (item.type === "project_landing") {
                                    return (
                                        <Grid key={item.id}>
                                            <KeyProjectsCarousel
                                                prismicCtx={prismicCtx}
                                                location={location}
                                                gridSize={gridSize}
                                            />
                                        </Grid>
                                    );
                                }
                                return null;
                            })}

                            {results
                                .filter(
                                    item => item.type === "single_project_page"
                                )
                                .map((item, index, array) => {
                                    const boxStyle = returnBoxStyle(
                                        item,
                                        index,
                                        array
                                    );

                                    if (boxStyle !== null) {
                                        return (
                                            <Grid key={item.id}>
                                                {boxStyle}
                                            </Grid>
                                        );
                                    }
                                    return null;
                                })}
                        </GridContainer>
                    );
                }
                return (
                    <GridContainer>
                        <NoResults>
                            No results found for
                            <br /> search term <strong>{textSearch}</strong>
                        </NoResults>
                    </GridContainer>
                );
            }
            return null;
        }

        return (
            <div>
                <LoadableSearchBarProjects
                    history={history}
                    prismicCtx={prismicCtx}
                    location={location}
                    callback={this.searchCallback}
                >
                    <LoadableLoadingStateWrapper>
                        {checkResults(results)}
                    </LoadableLoadingStateWrapper>
                </LoadableSearchBarProjects>
            </div>
        );
    }
}

export default ProjectGrid;
