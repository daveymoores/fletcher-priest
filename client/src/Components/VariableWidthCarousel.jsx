import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import Loadable from "react-loadable";
import Slider from "react-slick";
import media from "../utils/style-utils";
import SlickCarouselArrow from "../Common/SlickCarouselArrow";

const CarouselWrapper = styled.div`
  position: relative;
  margin-top: 40px;
  display: block;
  margin-bottom: 40px;
  max-height: 70vw;
  height: 70vw;
  overflow: hidden;

  ${media.tablet`
  max-height: 900px;
  height: 900px;
    `};

  div {
    height: 100%;
  }

  div[type="prev"],
  div[type="next"] {
    height: 40px;
  }

  ${media.tablet`
          margin-bottom: 80px;
    `};

  .slick-track {
    height: 100%;
    display: flex;
  }

  .slick-slide {
    padding-right: 40px;

    @media all and (min-width: 600px) {
      padding-right: 120px;
    }
  }

  .slick-list {
    overflow: visible;
    margin: 0;
    padding: 0;
    width: 80%;
    height: 100%;

    @media all and (min-width: 600px) {
      width: auto;
      overflow: hidden;
    }
  }
`;

const Slide = styled.div`
  position: relative;
  overflow: hidden;

  &:focus {
    outline: 0;
  }

  img {
    display: block;
    height: 100%;
  }
`;

class VariableWidthCarousel extends Component {
  constructor(props) {
    super();

    this.keyCount = 0;
    this.getKey = this.getKey.bind(this);
  }

  getKey() {
    return this.keyCount++;
  }

  render() {
    const { content, prismic } = this.props;
    let slides = [];
    let carousel = null;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      nextArrow: <SlickCarouselArrow type="next" />,
      prevArrow: <SlickCarouselArrow type="prev" />,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            infinite: true,
            variableWidth: true,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            infinite: true,
            variableWidth: true,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    slides = content.map(img => (
      <Slide key={this.getKey()}>
        <img
          src={img.variable_width_image.url}
          alt={img.variable_width_image.alt}
        />
      </Slide>
    ));

    if (slides.length > 1) {
      carousel = <Slider {...settings}>{slides}</Slider>;
    } else {
      carousel = slides;
    }

    return <CarouselWrapper>{carousel}</CarouselWrapper>;
  }
}

export default VariableWidthCarousel;
