import React from "react";
import styled from "styled-components";
import media from "../utils/style-utils";

const Container = styled.div`
  padding: 0 15px;
  ${media.tablet`padding: 0 30px;`}
  ${media.desktop`padding: 0 60px;`}
  ${props => (props.type ? "display: none;" : "display: block;")}
`;

const Heading = styled.div`
  color: white;
  margin: 0;
  padding-top: 90px;

  ${media.tabletLrg`
    padding-top: 110px;
  `};

  h1 {
    font-size: 60px;
    line-height: auto;
    margin-top: 0;
    position: relative;
    z-index: 2;
    width: 95%;
    color: ${props => props.theme.secondary};
    margin-bottom: 0px;
    display: inline;

    ${media.tablet`
      font-size: 80px;
      line-height: auto;
      padding-bottom: 10px;
  `};

    ${media.desktop`
        font-size: 90px;
        line-height: auto;
        padding-bottom: 20px;
      `};
  }
`;

const PageHeading = ({ text, color, type }) => (
  <Container type={type}>
    <Heading>{text}</Heading>
  </Container>
);

export default PageHeading;
