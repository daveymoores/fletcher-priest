import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import Loadable from "react-loadable";
import Slider from "react-slick";
import uniqid from "uniqid";

import media from "../utils/style-utils";
import PrismicConfig from "../prismic-configuration";
import SlickCarouselArrow from "../Common/SlickCarouselArrow";

const CarouselWrapper = styled.div`
    position: relative;
    display: block;
    margin: 0 0 100px;
`;

const Slide = styled.div`
    height: auto;
    width: 100%;
    display: flex;
    align-items: center;
    padding: ${props => (!props.type ? "0 120px 0 45px" : "0 45px")};
    position: relative;
    margin-top: 20px;

    ${media.mobile`
      padding: ${props => (!props.type ? "0 130px 0 45px" : "0 45px")};
    `};

    ${media.desktop`
      padding: ${props => (!props.type ? "0 170px 0 45px" : "0 45px")};
    `};

    img {
        width: auto;
        height: 100%;
        margin-right: 100px;
    }
`;

const Quote = styled.div`
    position: relative;

    &::before {
        content: "“";
        color: ${props => props.theme.primary};
        font-family: ${props => props.theme.FFDINWebProMedium};
        font-size: 75px;
        line-height: 75px;
        position: absolute;
        top: -7px;
        left: -50px;
    }

    p {
        font-size: 20px;
        line-height: 24px;
        font-family: ${props => props.theme.FFDINWebProMedium};
        color: black;
    }

    span p {
        color: black;
        font-family: ${props => props.theme.FFDINWebProMedium};
        font-size: 18px;
        line-height: 18px;
        margin: 0;
    }

    h4 {
        font-size: 25px;
        line-height: 30px;
        padding-bottom: 10px;
        color: black;
        font-family: ${props => props.theme.FFDINWebProMedium};
        margin: 0;
    }
`;

const QuoteCarousel = props => {
    const { content, prismic } = props;
    let slides = [];
    let carousel = null;

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <SlickCarouselArrow type="next" />,
        prevArrow: <SlickCarouselArrow type="prev" />
    };

    const type = content.length > 1 ? null : "single";

    slides = content
        .filter(item => {
            if (item.name.length > 0) return true;
            return false;
        })
        .map(quote => (
            <Slide type={type} key={uniqid()}>
                <Quote>
                    {PrismicReact.RichText.render(
                        quote.quote_text,
                        PrismicConfig.linkResolver
                    )}
                    {PrismicReact.RichText.render(
                        quote.name,
                        PrismicConfig.linkResolver
                    )}
                    <span>
                        {PrismicReact.RichText.render(
                            quote.staff_job_title,
                            PrismicConfig.linkResolver
                        )}
                    </span>
                </Quote>
            </Slide>
        ));

    if (slides.length > 1) {
        carousel = <Slider {...settings}>{slides}</Slider>;
        return <CarouselWrapper>{carousel}</CarouselWrapper>;
    }

    carousel = slides;
    return <CarouselWrapper>{carousel}</CarouselWrapper>;
};

export default QuoteCarousel;
