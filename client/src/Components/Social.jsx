import React from "react";
import styled from "styled-components";
import FacebookIcon from "./icons/facebook.svg";
import LinkedInIcon from "./icons/linkedin.svg";
import MailIcon from "./icons/mail.svg";
import TwitterIcon from "./icons/twitter.svg";
import media from "../utils/style-utils";

const SocialListWrapper = styled.div`
  position: relative;
  left: 0;
  width: 38px;
  margin-top: 20px;

  ${media.tablet`
      margin-top: 0px;
    `};
`;

const SocialList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  display: flex;

  ${media.tablet`
      display: block;
    `};

  li {
    height: 38px;
    width: 38px;
    min-width: 38px;
    margin-right: 10px;
    margin-bottom: 15px;

    ${media.tablet`
          margin-right: 0;
        `};

    > a {
      width: 100%;
      display: block;
    }

    svg {
      width: 100%;
      height: 100%;
      display: block;
    }
  }
`;

const SocialTitle = styled.p`
  text-align: center;
  font-family: ${props => props.theme.FFDINWebProMedium};
  line-height: 16px;
  color: black;
  margin-top: 0;
  width: 100%;
`;

const Social = ({ title }) => {
  const location = window.location.href;
  return (
    <SocialListWrapper>
      <SocialTitle>Share</SocialTitle>
      <SocialList>
        <li>
          <a
            href={`https://www.facebook.com/sharer/sharer.php?u=${location}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <FacebookIcon />
          </a>
        </li>
        <li>
          <a
            href={`https://www.linkedin.com/shareArticle?mini=true&url=${location}&title=${title}&summary=&source=`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <LinkedInIcon />
          </a>
        </li>
        <li>
          <a
            href={`https://twitter.com/home?status=${location}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <TwitterIcon />
          </a>
        </li>
        <li>
          <a
            href={`mailto:?subject=${title ||
              `Fletcher Priest`}&body=I found this on fletcherpriest.com - ${location}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <MailIcon />
          </a>
        </li>
      </SocialList>
    </SocialListWrapper>
  );
};

export default Social;
