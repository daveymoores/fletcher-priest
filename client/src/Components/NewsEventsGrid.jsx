import React, { Component } from "react";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import { Grid } from "styled-grid-responsive";
import Loadable from "react-loadable";
import debounce from "lodash/debounce";
import uniqid from "uniqid";

import EventTeaser from "./EventTeaser";
import NewsTeaser from "./NewsTeaser";
import Footer from "../Common/Footer";
import Hero from "../Common/Hero";
import GridContainer from "../Common/GridContainer";
import FullWidthLink from "./FullWidthLink";
import LoaderGridItem from "../Loaders/LoaderGridItem";
import LoaderSearchBar from "../Loaders/LoaderSearchBar";
import LoaderPageContent from "../Loaders/LoaderPageContent";

const LoadableSearchBarNewsEvents = Loadable({
    loader: () => import("./SearchBarNewsEvents"),
    loading: () => <LoaderSearchBar />
});

const LoadableTextOnlyPageTitle = Loadable({
    loader: () => import("../Components/TextOnlyPageTitle"),
    loading: () => <LoaderGridItem />
});

const LoadableEventTeaser = Loadable({
    loader: () => import("../Components/EventTeaser"),
    loading: () => <LoaderGridItem />
});

const LoadableNewsTeaser = Loadable({
    loader: () => import("../Components/NewsTeaser"),
    loading: () => <LoaderGridItem />
});

const LoadableLoadingStateWrapper = Loadable({
    loader: () => import("./LoadingStateWrapper"),
    loading: () => <LoaderPageContent />
});

const NoResults = styled.h3`
    text-align: center;
    display: block;
    margin: 60px auto 200px;
    max-width: 400px;

    strong {
        font-family: ${props => props.theme.FFDINWebProBold};
    }
`;

export default class NewsEvents extends Component {
    constructor(props) {
        super();
        this.state = {
            results: null,
            textSearch: ""
        };
        this.handleResize = debounce(this.handleResize, 100);
    }

    componentWillMount() {
        const { results } = this.props;
        this.setState({ results });
        this.handleResize();
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleResize, false);
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     const { results } = this.state;
    //     if (nextState.results !== results) {
    //         return true;
    //     }
    //     return false;
    // }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize, false);
    }

    searchCallback = data => {
        this.updateState(data);
    };

    handleResize = () => {
        this.setState(() => ({
            viewport: { x: window.innerWidth, y: window.innerHeight }
        }));
    };

    updateState(data) {
        this.setState(prevState => {
            if (prevState.results !== data[0]) {
                return { results: data[0], textSearch: data[1] };
            }
            return null;
        });
    }

    render() {
        const { results, textSearch, viewport } = this.state;
        const { prismicCtx, location, PrismicReact, history } = this.props;
        let gridNumber = 0;
        let gridFlag = true;
        const teaserCount = 0;

        function findSpaceForSignUp(item) {
            gridNumber =
                item.data.featured_item === "Featured"
                    ? gridNumber + 2
                    : gridNumber + 1;
            if (gridNumber === 4 && gridFlag) {
                gridFlag = false;
                return (
                    <FullWidthLink
                        destination="https://fletcherpriest.us13.list-manage.com/subscribe/post?u=0f797f2a2950b4bd1c97f0d27&amp;id=b22beb9ffb"
                        linktext="Sign up to our newsletter"
                    />
                );
            }
            return null;
        }

        function returnTeaser(item) {
            if (item.type === `single_events_page`) {
                return (
                    <React.Fragment key={uniqid()}>
                        <LoadableEventTeaser
                            key={item.id}
                            event={item}
                            windowSize={viewport}
                            prismicCtx={prismicCtx}
                            PrismicReact={PrismicReact}
                        />
                        {findSpaceForSignUp(item)}
                    </React.Fragment>
                );
            }
            return (
                <React.Fragment key={uniqid()}>
                    <LoadableNewsTeaser
                        key={item.id}
                        news={item}
                        windowSize={viewport}
                        prismicCtx={prismicCtx}
                        PrismicReact={PrismicReact}
                    />
                    {findSpaceForSignUp(item)}
                </React.Fragment>
            );
        }

        function checkResults(res) {
            if (res) {
                if (res.length > 0) {
                    return (
                        <GridContainer>
                            <Grid>
                                {res
                                    .filter(item => {
                                        const values = Object.values(item);
                                        return (
                                            values.indexOf(
                                                "single_news_item"
                                            ) >= 0 ||
                                            values.indexOf(
                                                "single_events_page"
                                            ) >= 0
                                        );
                                    })
                                    .map((item, index) => returnTeaser(item))}
                            </Grid>
                        </GridContainer>
                    );
                }
                return (
                    <GridContainer>
                        <NoResults>
                            No results found for
                            <br /> search term <strong>{textSearch}</strong>
                        </NoResults>
                    </GridContainer>
                );
            }
            return <LoaderPageContent />;
        }

        return (
            <div>
                <LoadableSearchBarNewsEvents
                    prismicCtx={prismicCtx}
                    location={location}
                    history={history}
                    callback={this.searchCallback}
                >
                    <LoadableLoadingStateWrapper>
                        {checkResults(results)}
                    </LoadableLoadingStateWrapper>
                </LoadableSearchBarNewsEvents>
            </div>
        );
    }
}
