// @flow

import * as React from "react";
import styled from "styled-components";
import media from "../utils/style-utils";

const Wrapper = styled.article`
    margin-top: 90px;

    ${media.tablet`
      margin-top: 125px;
    `};
`;

type Props = {
    children: React.Node
};

const ArticleWrapper = (props: Props) => {
    const { children } = props;
    return <Wrapper>{children}</Wrapper>;
};

export default ArticleWrapper;
