import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import Loadable from 'react-loadable';
import { Grid } from 'styled-grid-responsive';
import Slider from 'react-slick';
import media from '../utils/style-utils';

import EventTeaser from './EventTeaser';
import NewsTeaser from './NewsTeaser';
import GridContainer from '../Common/GridContainer';
import InsetHeading from './InsetHeading';
import SlickCarouselArrow from '../Common/SlickCarouselArrow';
import LoaderGridItemCarousel from '../Loaders/LoaderGridItemCarousel';

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const CarouselWrapper = styled.div`
  position: relative;
  display: block;

  div[type='prev'],
  div[type='next'] {
    top: 0;
    margin-top: -60px;

    ${media.tablet`
          margin-top: -70px;
        `};
  }

  .slick-list {
    width: calc(100% + 20px);
  }

  .slick-slide {
    display: block;
    position: relative;
    padding-right: 20px;

    div {
      width: 100% !important;
    }
  }
`;

class NewsEventsCarousel extends Component {
  state = {
    gridSize: null,
    results: null,
    domLoaded: false
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(
          Prismic.Predicates.any('document.type', [
            'single_news_item',
            'single_events_page',
            'news__events_landing'
          ]),
          {
            fetchLinks: [
              'single_events_page.start_time_date',
              'single_events_page.venue_name'
            ],
            orderings: '[my.single_news_item.publication_date desc]',
            pageSize: 12
          }
        )
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results, gridSize, domLoaded } = this.state;
    const { prismicCtx, location, headingText } = this.props;
    let slides = [];
    let carousel = null;
    const ctx = this;

    const settings = {
      dots: false,
      infinite: true,
      draggable: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      nextArrow: <SlickCarouselArrow type='next' />,
      prevArrow: <SlickCarouselArrow type='prev' />,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            draggable: true
          }
        },
        {
          breakpoint: 576,

          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            draggable: true
          }
        }
      ]
    };

    if (results) {
      const text = headingText || `More News & Events`;

      slides = results
        .filter(item => {
          const values = Object.values(item);
          return (
            values.indexOf('single_news_item') >= 0 ||
            values.indexOf('single_events_page') >= 0
          );
        })
        .map((item, index) => {
          if (item.type === `single_events_page`) {
            return (
              <EventTeaser
                key={item.id}
                event={item}
                prismicCtx={prismicCtx}
                PrismicReact={PrismicReact}
              />
            );
          }
          return (
            <NewsTeaser
              key={item.id}
              news={item}
              prismicCtx={prismicCtx}
              PrismicReact={PrismicReact}
            />
          );
        });

      if (slides.length > 4) {
        carousel = (
          <CarouselWrapper innerRef={node => (this.wrapperRef = node)}>
            <InsetHeading text={text} />
            <Grid>
              <Slider {...settings}>{slides}</Slider>
            </Grid>
          </CarouselWrapper>
        );
      } else {
        return (
          <CarouselWrapper innerRef={node => (this.wrapperRef = node)}>
            <InsetHeading text={text} />
            <Grid>{(carousel = slides)}</Grid>
          </CarouselWrapper>
        );
      }

      return <section>{carousel}</section>;
    }
    return <LoaderGridItemCarousel />;
  }
}

export default NewsEventsCarousel;
