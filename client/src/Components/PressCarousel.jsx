import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import Loadable from 'react-loadable';
import { Grid } from 'styled-grid-responsive';
import Slider from 'react-slick';
import uniqid from 'uniqid';
import LoaderGridItem from '../Loaders/LoaderGridItem';
import media from '../utils/style-utils';

import EventTeaser from './EventTeaser';
import NewsTeaser from './NewsTeaser';
import GridContainer from '../Common/GridContainer';
import InsetHeading from './InsetHeading';
import SlickCarouselArrow from '../Common/SlickCarouselArrow';

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => <div>Loading...</div>
});

const LoadableEventTeaser = Loadable({
  loader: () => import('../Components/EventTeaser'),
  loading: () => <LoaderGridItem />
});

const LoadableNewsTeaser = Loadable({
  loader: () => import('../Components/NewsTeaser'),
  loading: () => <LoaderGridItem />
});

const CarouselWrapper = styled.div`
  position: relative;
  display: block;

  div[type='prev'],
  div[type='next'] {
    top: 0;
    margin-top: -60px;

    ${media.tablet`
          margin-top: -70px;
        `};
  }

  .slick-list {
    width: calc(100% + 20px);
  }

  .slick-slide {
    display: block;
    position: relative;
    padding-right: 20px;

    div {
      width: 100% !important;
    }
  }
`;

const PressCarousel = props => {
  const { prismicCtx, location, headingText, results, windowSize } = props;
  let slides = [];
  let carousel = null;

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SlickCarouselArrow type='next' />,
    prevArrow: <SlickCarouselArrow type='prev' />,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  if (results) {
    const text = headingText || `More News & Events`;

    slides = results.items
      .filter(item => {
        if (item.internal_press_link.id) return true;
        return false;
      })
      .map((item, index) => (
        <LoadableNewsTeaser
          key={uniqid()}
          windowSize={windowSize}
          news={item.internal_press_link}
          prismicCtx={prismicCtx}
          PrismicReact={PrismicReact}
        />
      ));

    if (slides.length > 4) {
      carousel = (
        <CarouselWrapper>
          <InsetHeading text={text} />
          <Grid>
            <Slider {...settings}>{slides}</Slider>
          </Grid>
        </CarouselWrapper>
      );
    } else {
      return (
        <CarouselWrapper>
          <InsetHeading text={text} />
          <Grid>{(carousel = slides)}</Grid>
        </CarouselWrapper>
      );
    }

    return <section>{carousel}</section>;
  }
  return <h1>Loading</h1>;
};

export default PressCarousel;
