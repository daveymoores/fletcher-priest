import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import LazyLoad from 'react-lazyload';
import { Date } from 'prismic-reactjs';
import Moment from 'moment';

const NewsTeaserCont = styled.div`
  padding: ${props => (props.medium ? '0px' : '20px')};
  height: ${props => (props.medium ? '75px' : '450px')};
  display: flex;
  justify-content: ${props =>
    props.medium ? 'justify-content' : 'space-between'};
  flex-direction: ${props => (props.medium ? 'row' : 'column')};
  border: ${props =>
    props.medium
      ? `${`1px solid ${
          props.searchResult ? props.theme.darkGrey7 : props.theme.tertiary
        }`}`
      : 'none'};
  margin-bottom: ${props => (props.medium ? '20px' : 0)};

  img {
    width: 100%;
  }

  h3,
  h4,
  h5 {
    margin: 0px;
    color: ${props => (props.searchResult ? 'white' : 'black')};
  }
`;

const Thumbnail = styled(Link)`
  height: 100%;
  width: 120px;
  min-width: 120px;
  max-width: 120px;
  display: block;
  margin-right: 10px;

  > div {
    width: 100%;
    height: 100%;
    background-size: cover;
    background-position: center center;
  }
`;

const ThumbnailText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 0;
`;

const NewsSearchTitle = styled.h3`
  font-size: 12px !important;

  > a {
    color: inherit;
  }

  &::after {
    display: none;
  }
`;

const Title = styled.h4`
  font-size: 8px !important;
  margin: 0px !important;
`;

function formatDate(dd) {
  const date = Date(dd.data.publication_date);
  const newDate = Moment(date).format('LL');
  return newDate;
}

const NewsSearchTeaser = ({ result, searchResult, PrismicReact }) => {
  const loc = '/news-and-events';
  const formattedDate = formatDate(result);

  const url = Object.prototype.hasOwnProperty.call(
    result.data.featured_image,
    'Thumbnail'
  )
    ? result.data.featured_image.Thumbnail.url
    : result.data.featured_image.url;
  return (
    <NewsTeaserCont medium searchResult={searchResult}>
      <Thumbnail to={`${loc}/${result.uid}`}>
        <LazyLoad offset={100}>
          <div
            style={{
              backgroundImage: `url(${url})`
            }}
          />
        </LazyLoad>
      </Thumbnail>
      <ThumbnailText>
        <NewsSearchTitle>
          <Link to={`${loc}/${result.uid}`}>
            {PrismicReact.RichText.asText(result.data.title)}
          </Link>
        </NewsSearchTitle>
        <Title>{formattedDate}</Title>
      </ThumbnailText>
    </NewsTeaserCont>
  );
};

export default NewsSearchTeaser;
