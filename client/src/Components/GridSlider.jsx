// @flow

import React, { Component } from "react";
import Slider from "react-rangeslider";
import styled from "styled-components";
import "react-rangeslider/lib/index.css";
import SmallIcon from "./icons/small.svg";
import LargeIcon from "./icons/large.svg";

const SliderParent = styled.div`
    display: flex;
    align-items: center;

    > svg {
        width: 20px;
        height: 20px;
    }
`;

const SliderWrapper = styled.div`
    width: 100%;

    .rangeslider-horizontal {
        margin: 0 10px;
        height: 6px;
        background-color: ${props => props.theme.lightGrey};
        border-radius: none;
        box-shadow: none;

        .rangeslider__fill {
            background-color: ${props => props.theme.lightGrey};
            border-radius: none;
            box-shadow: none;
        }

        .rangeslider__handle {
            background-color: black;
            height: 15px;
            width: 15px;
            box-shadow: none;
            border: none;
            outline: none;

            &::after {
                display: none;
            }
        }
    }
`;

type State = {
    size: number | null
};

type Props = {
    callback: (a: ?number) => mixed
};

class GridSlider extends Component<Props, State> {
    state = {
        size: 100
    };

    componentDidUpdate(prevProps: ?Props, prevState: State) {
        const { size } = this.state;
        const { callback } = this.props;

        if (prevState.size !== size) {
            callback(size);
        }
    }

    handleOnChange = (value: number) => {
        this.setState({
            size: value
        });
    };

    render() {
        const { size } = this.state;
        const ctx = this;

        return (
            <SliderParent>
                <SmallIcon />
                <SliderWrapper>
                    <Slider
                        min={0}
                        max={100}
                        step={50}
                        value={size}
                        tooltip={false}
                        orientation="horizontal"
                        onChange={this.handleOnChange}
                    />
                </SliderWrapper>
                <LargeIcon />
            </SliderParent>
        );
    }
}

export default GridSlider;
