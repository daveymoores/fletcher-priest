import React, { Component } from 'react';
import Prismic from 'prismic-javascript';
import { Link } from 'react-router-dom';
import PrismicReact from 'prismic-reactjs';
import BackgroundImage from 'react-background-image-loader';
import styled, { css } from 'styled-components';
import media from '../utils/style-utils';
import Spots from '../Common/textures/spots.png';

const CarouselTypeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: stretch;
  positon: relative;
  margin: 80px 0 20px;
  background-image: url(${Spots});

  ${media.tablet`
      flex-direction: row;
    `};

  > div {
    &:first-child {
      order: 0;
      width: 100%;
      min-width: 100%;
      height: 100%;

      ${media.tablet`
              width: 50%;
              min-width: 50%;
            `};
    }

    &:last-child {
      order: 1;
      display: flex;
      align-items: center;
    }
  }

  ${props =>
    props.type === 'Left' &&
    css`
      > div {
        &:first-child {
          order: 1;

          ${media.tablet`
                    order: 1;
                  `};
        }

        &:last-child {
          order: 0;

          ${media.tablet`
                    order: 0;
                  `};
        }
      }
    `};
`;

const Slide = styled.div`
  height: 650px;
  width: 100%;
  position: relative;

  > div {
    background-position: center center;
    background-size: cover;
    height: 100vh;
    min-height: auto;
    max-height: 650px;
  }

  img {
    position: absolute;
    max-width: calc(100% - 120px);
    width: auto;
    max-height: 65%;
    top: 50%;
    left: 50%;
    -webkit-transform: translate3d(-50%, -50%, 0);
    -ms-transform: translate3d(-50%, -50%, 0);
    transform: translate3d(-50%, -50%, 0);
  }
`;

const TwoColumnTextured = props => {
  const { content, prismic } = props;
  const slides = [];
  const carousel = null;

  return (
    <CarouselTypeWrapper type={content.primary.variable_size_image_side}>
      <Slide>
        <BackgroundImage
          placeholder='./images/grey-placeholder.png'
          className='slide'
          src={content.primary.image_one.url}
        />
      </Slide>
      <Slide>
        <img src={content.primary.image_two.url} alt='' />
      </Slide>
    </CarouselTypeWrapper>
  );
};

export default TwoColumnTextured;
