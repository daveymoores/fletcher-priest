// @flow

import * as React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import LoadingPulse from "../Common/LoadingPulse";

const LoadingWrapper = styled.div`
    position: relative;
`;

const LoadingWrapperChildren = styled.div`
    transition: opacity 0.2s ease;
    opacity: ${props => (props.type === "loading" ? "0.2" : "1")};
`;

type Props = {
    children: React.Node,
    filterState: string
};

const LoadingStateWrapper = (props: Props) => {
    const { children, filterState } = props;
    return (
        <LoadingWrapper>
            <LoadingPulse loaderVisibility={filterState} />
            <LoadingWrapperChildren type={filterState}>
                {children}
            </LoadingWrapperChildren>
        </LoadingWrapper>
    );
};

const mapStateToProps = state => ({ filterState: state.filterState });

export default connect(mapStateToProps)(LoadingStateWrapper);
