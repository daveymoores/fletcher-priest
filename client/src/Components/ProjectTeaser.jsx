import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled, { withTheme } from 'styled-components';

import AwardArrow from '../Common/icons/award-arrow';

const AwardRow = styled.div`
  display: table;
  width: 100%;
  font-size: 12px;
  font-family: ${props => props.theme.FFDINWebProMedium};
  border: 1px solid ${props => props.theme.tertiary};
  margin-bottom: 5px;
  position: relative;
  background-color: white;

  > div {
    display: table-cell;
    text-align: left;
    padding-left: 10px;
    width: 25%;
  }
`;

const ProjectArrow = styled(Link)`
  position: absolute;
  right: 15px;
  top: 50%;
  transform: translateY(-50%);
  width: 18px;
  height: 18px;
  border-radius: 50%;
  overflow: hidden;

  > svg {
    width: 100%;
    height: 100%;
  }
`;

const ProjectTeaserCont = styled.div`
  padding: ${props => (props.medium ? '0px' : '20px')};
  height: ${props => (props.medium ? '75px' : '450px')};
  display: flex;
  justify-content: ${props =>
    props.medium ? 'justify-content' : 'space-between'};
  flex-direction: ${props => (props.medium ? 'row' : 'column')};
  border: ${props =>
    props.medium
      ? `${`1px solid ${
          props.searchResult ? props.theme.darkGrey7 : props.theme.tertiary
        }`}`
      : 'none'};
  margin-bottom: ${props => (props.medium ? '20px' : 0)};

  img {
    width: 100%;
  }

  h3,
  h4,
  h5 {
    margin: 0px;
    color: ${props => (props.searchResult ? 'white' : 'black')};

    > a {
      color: inherit;
    }
  }

  &.animating {
    h3::after {
      transition: width 0.3s cubic-bezier(0.77, 0, 0.175, 1);
      width: 0;
    }

    h3.completed::after {
      width: 40px;
    }
  }
`;

const Thumbnail = styled(Link)`
  height: 100%;
  width: 120px;
  min-width: 120px;
  max-width: 120px;
  display: block;
  margin-right: 10px;

  > div {
    width: 100%;
    height: 100%;
    background-size: cover;
    background-position: center center;
  }
`;

const ThumbnailText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 0;
`;

const Name = styled.h3`
  font-size: 12px !important;

  &::after {
    display: none;
  }
`;

const Title = styled.h4`
  font-size: 8px !important;
  margin: 0px !important;
`;

class ProjectTeaser extends Component {
  componentDidMount() {}

  render() {
    const {
      project,
      prismicCtx,
      PrismicReact,
      location,
      gridSize,
      searchResult,
      theme
    } = this.props;

    if (!project.isBroken) {
      if (gridSize === 100) {
        return (
          <ProjectTeaserCont innerRef={node => (this.projectTeaserRef = node)}>
            <div>
              <h3>
                <Link to={`/projects/${project.uid}`}>
                  {Object.prototype.hasOwnProperty.call(
                    project.data,
                    'project_name'
                  ) && PrismicReact.RichText.asText(project.data.project_name)}
                </Link>
              </h3>
              <h4>
                {Object.prototype.hasOwnProperty.call(project.data, 'client') &&
                  PrismicReact.RichText.asText(project.data.client)}
              </h4>
            </div>
            <Link to={`/projects/${project.uid}`}>
              <span className='mask' />
              <img alt='cover' src={project.data.image.thumbnail.url} />
            </Link>
            <h5>
              {Object.prototype.hasOwnProperty.call(project.data, 'location') &&
                PrismicReact.RichText.asText(project.data.location)}
            </h5>
          </ProjectTeaserCont>
        );
      }

      if (gridSize === 50) {
        return (
          <ProjectTeaserCont
            medium
            innerRef={node => (this.projectTeaserRef = node)}
            searchResult={searchResult}
          >
            <Thumbnail to={`/projects/${project.uid}`}>
              <div
                style={{
                  backgroundImage: `url(${project.data.image.thumbnail.url})`
                }}
              />
            </Thumbnail>
            <ThumbnailText>
              <Name>
                <Link to={`/projects/${project.uid}`}>
                  {Object.prototype.hasOwnProperty.call(
                    project.data,
                    'project_name'
                  ) && PrismicReact.RichText.asText(project.data.project_name)}
                </Link>
              </Name>
              <Title>
                {Object.prototype.hasOwnProperty.call(
                  project.data,
                  'location'
                ) && PrismicReact.RichText.asText(project.data.location)}
              </Title>
            </ThumbnailText>
          </ProjectTeaserCont>
        );
      }

      if (gridSize === 0) {
        return (
          <AwardRow innerRef={node => (this.projectTeaserRef = node)}>
            <div>
              <Name>
                <Link to={`/projects/${project.uid}`}>
                  {Object.prototype.hasOwnProperty.call(
                    project.data,
                    'project_name'
                  ) && PrismicReact.RichText.asText(project.data.project_name)}
                </Link>
              </Name>
            </div>
            <div>
              <Title>
                {Object.prototype.hasOwnProperty.call(project.data, 'client') &&
                  PrismicReact.RichText.asText(project.data.client)}
              </Title>
            </div>
            <div>
              <Title>
                {Object.prototype.hasOwnProperty.call(
                  project.data,
                  'location'
                ) && PrismicReact.RichText.asText(project.data.location)}
              </Title>
            </div>
            <ProjectArrow to={`/projects/${project.uid}`}>
              <AwardArrow color={theme.primary} />
            </ProjectArrow>
          </AwardRow>
        );
      }
    }

    return null;
  }
}

export default withTheme(ProjectTeaser);
