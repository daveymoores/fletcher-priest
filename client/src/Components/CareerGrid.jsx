// @flow

import React from "react";
import styled from "styled-components";
import PrismicReact from "prismic-reactjs";
import media from "../utils/style-utils";

import PrismicConfig from "../prismic-configuration";

const CareerGridItem = styled.div`
  width: 100%;
  max-width: 100%;
  min-width: 100%;
  max-height: 500px;
  min-height: 350px;
  height: 75vw;
  background-size: cover;
  margin-bottom: 20px;
  position: relative;

  &:nth-child(odd) > div {
    left: 15px;

    ${media.tablet`
          left: inherit;
        `};
  }

  &:nth-child(even) > div {
    right: 15px;

    ${media.tablet`
          right: 0;
        `};
  }

  ${media.tablet`
      width: calc(50% - 10px);
      max-width: calc(50% - 10px);
      min-width: calc(50% - 10px);
      transform: translateY(${props => (props.type === "true" ? 100 : 0)}px);
    `};

  ${media.desktop`
      max-height: 600px;
    `};
`;

const CareerGridText = styled.div`
  padding: 10px 30px;
  width: 60%;
  min-width: 310px;
  background-color: white;
  position: absolute;
  bottom: -1px;
  right: 0;

  @media (min-width: 400px) {
    min-width: 350px;
  }

  ${media.mobile`
      min-width: 400px;
    `};

  h3 {
    font-size: 27px;
    line-height: 37px;
    margin-top: 10px;
  }

  p {
    font-size: 12px;
    line-height: 16px;
  }
`;

type Props = {
  item: {
    title: string,
    description: string,
    reason_background_image: {
      url: string
    }
  },
  index: number
};

const CareerGrid = (props: Props) => {
  const { item, index } = props;
  let type = "false";

  if ((index + 1) % 2 === 0) {
    type = "true";
  }

  return (
    <CareerGridItem
      style={{
        backgroundImage: `url(${item.reason_background_image.url})`
      }}
      type={type}
    >
      <CareerGridText>
        {PrismicReact.RichText.render(item.title, PrismicConfig.linkResolver)}
        {PrismicReact.RichText.render(
          item.description,
          PrismicConfig.linkResolver
        )}
      </CareerGridText>
    </CareerGridItem>
  );
};

export default CareerGrid;
