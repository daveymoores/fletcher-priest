import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled, { css } from "styled-components";
import { connect } from "react-redux";

import GridContainer from "../Common/GridContainer";
import Small from "./icons/small.svg";
import Large from "./icons/large.svg";
import { OpenDropdown } from "../Animations/SearchAnimation";
import GridSlider from "./GridSlider";
import {
    SearchParent,
    SearchDropdown,
    SearchDropdownOptions,
    SearchItem,
    SearchResize,
    SearchTextInput,
    SearchContainer,
    SearchIcon,
    DownChevron,
    LoadMore
} from "./search-bar-styles";
import { setFilterState } from "../redux/actionCreators";
import SearchBarAnimation from "../Animations/SearchBarAnimation";

function setSelectState(element) {
    const parent = element.parentNode.parentNode;
    const currentSelected = parent.querySelectorAll(".selected");

    // does target element contain selected
    function checkChildrenForSelected() {
        if (element.nextSibling) {
            const selected =
                element.nextSibling.querySelector(".selected") || false;
            return selected;
        }
        return false;
    }

    if (element.classList.contains("selected")) {
        element.classList.remove("selected");
    } else if (currentSelected && !checkChildrenForSelected()) {
        [].forEach.call(currentSelected, e => {
            e.classList.remove("selected");
        });
        element.classList.add("selected");
    } else {
        element.classList.add("selected");
    }
    return true;
}

class SearchBarAwards extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: null,
            revealed: false,
            sortTerm: "[my.awards.award_date desc]",
            totalRendered: null, // total visible on the page
            resultsSize: null, // number of total results
            increment: 6 // number to increment by
        };
    }

    componentWillMount() {
        this.buildPredicate(this.props);
    }

    componentWillReceiveProps(props) {
        this.buildPredicate(props);
    }

    componentDidUpdate(prevProps, prevState) {
        const { results, sortTerm, revealed } = this.state;
        const { callback } = this.props;

        if (this.searchParentRef && !revealed) {
            this.setState({ revealed: true }, () => {
                this.animations = new SearchBarAnimation(this.searchParentRef);
            });
        }

        if (prevState.results !== results) {
            callback([results, sortTerm]);
        }
    }

    toggleDropdown(e) {
        const target = e.currentTarget;
        setSelectState(target);
        const dropdown = target.parentNode.querySelector("ul");
        const dropdownItems = dropdown.querySelectorAll("li");

        OpenDropdown(dropdownItems);
        this.searchParentRef.classList.add("dropdown-open");
        const currentActiveDropdown = this.searchParentRef.querySelector(
            ".visible"
        );

        if (currentActiveDropdown)
            currentActiveDropdown.classList.remove("visible");
        dropdown.classList.add("visible");
    }

    handleSortClick(e) {
        const target = e.currentTarget;
        const { sortTerm } = this.state;
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "complete") handleFilterChange("loading");
        setSelectState(target);
        let s;

        function findSortParam(value) {
            let x;
            switch (value) {
                case "date":
                    x = "[my.awards.award_date desc]";
                    break;
                case "award":
                    x = "[my.awards.award_name desc]";
                    break;
                case "project":
                    x = "[my.awards.single_project_page.project_name desc]";
                    break;
                default:
                    x = "[my.awards.award_date desc]";
            }

            return x;
        }

        if (!target.classList.contains("selected")) {
            s = "[my.awards.award_date desc]";
        } else {
            s = target.getAttribute("data-sort-term")
                ? findSortParam(target.getAttribute("data-sort-term"))
                : sortTerm;
        }

        this.setState({ sortTerm: s }, () => {
            this.buildPredicate();
        });
    }

    loadMore(e) {
        e.preventDefault();
        const { totalRendered, increment, resultsSize } = this.state;
        const { filterState, handleFilterChange } = this.props;
        const newPageSize = totalRendered + increment;
        const newTotal = newPageSize < resultsSize ? newPageSize : resultsSize;
        if (filterState === "complete") handleFilterChange("loading");
        this.setState({ totalRendered: newTotal }, () => {
            this.buildPredicate();
        });
    }

    buildPredicate() {
        const { prismicCtx, filterState, handleFilterChange } = this.props;
        const { sortTerm, totalRendered } = this.state;

        const paginationTotal = totalRendered || 12;

        if (prismicCtx) {
            return prismicCtx.api
                .query(Prismic.Predicates.any("document.type", ["awards"]), {
                    fetchLinks: "single_project_page.project_name",
                    orderings: sortTerm,
                    pageSize: paginationTotal
                })
                .then(response => {
                    const {
                        results,
                        total_results_size: totalResultsSize
                    } = response;

                    this.setState(
                        {
                            results,
                            totalRendered: results.length,
                            resultsSize: totalResultsSize
                        },
                        () => {
                            if (filterState === "loading")
                                handleFilterChange("complete");
                        }
                    );
                });
        }
        return null;
    }

    render() {
        const { results, value, totalRendered, resultsSize } = this.state;
        const { children } = this.props;
        const ctx = this;

        function renderLoadMore() {
            if (totalRendered < resultsSize) {
                return (
                    <LoadMore href="#" onClick={ctx.loadMore.bind(ctx)}>
                        Load More
                    </LoadMore>
                );
            }

            return null;
        }

        const LoadMoreButton = renderLoadMore();

        return (
            <div>
                <GridContainer type="pullRight">
                    <SearchParent
                        innerRef={e => {
                            this.searchParentRef = e;
                        }}
                    >
                        <SearchDropdown>
                            <SearchItem
                                onClick={this.toggleDropdown.bind(this)}
                            >
                                <span>Sort by</span> <DownChevron />
                            </SearchItem>
                            <SearchDropdownOptions data-search-predicate="sort">
                                <li>
                                    <SearchItem
                                        data-sort-term="date"
                                        onClick={this.handleSortClick.bind(
                                            this
                                        )}
                                    >
                                        <span>Date</span>
                                    </SearchItem>
                                </li>
                                <li>
                                    <SearchItem
                                        data-sort-term="award"
                                        onClick={this.handleSortClick.bind(
                                            this
                                        )}
                                    >
                                        <span>Award</span>
                                    </SearchItem>
                                </li>
                                {/* <li>
                                    <SearchItem
                                        data-sort-term="project"
                                        onClick={this.handleSortClick.bind(
                                            this
                                        )}
                                    >
                                        <span>Project</span>
                                    </SearchItem>
                                </li> */}
                            </SearchDropdownOptions>
                        </SearchDropdown>
                        <SearchContainer>
                            <SearchTextInput disable />
                        </SearchContainer>
                    </SearchParent>
                </GridContainer>
                {children}
                {LoadMoreButton}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    filterState: state.filterState
});

const mapDispatchToProps = dispatch => ({
    handleFilterChange(value) {
        dispatch(setFilterState(value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchBarAwards);
