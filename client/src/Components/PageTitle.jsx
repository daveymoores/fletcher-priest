import React from "react";
import styled from "styled-components";

const Title = styled.h1`
    font-family: FFDINWebProMedium;
    font-size: 38px;
    margin-top: 0;

    span {
    }
`;

const PageTitle = props => {
    const { content } = props;
    return <Title>{content}</Title>;
};

export default PageTitle;
