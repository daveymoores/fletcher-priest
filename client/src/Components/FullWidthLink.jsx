// @flow

import React, { Component } from "react";
import styled from "styled-components";
import Scroll from "react-scroll-to-element";
import GridContainer from "../Common/GridContainer";

const BigLink = styled.a`
    padding: 50px;
    border: 1px solid ${props => props.theme.darkGrey};
    width: calc(100 + 1px);
    margin: 30px 1px 60px;
    color: ${props => props.theme.primary};
    font-family: ${props => props.theme.FFDINWebProBlack};
    font-size: 21px;
    width: 100%;
    display: block;
`;

type Props = {
    linktext: string,
    destination: string,
    type: string
};

const FullWidthLink = (props: Props) => {
    const { linktext, destination, type } = props;

    if (type) {
        return (
            <GridContainer type="pullRight">
                <Scroll type="id" element={destination}>
                    <BigLink href={destination}>{linktext} &rarr;</BigLink>
                </Scroll>
            </GridContainer>
        );
    }

    return (
        <GridContainer type="pullRight">
            <BigLink href={destination}>{linktext} &rarr;</BigLink>
        </GridContainer>
    );
};

export default FullWidthLink;
