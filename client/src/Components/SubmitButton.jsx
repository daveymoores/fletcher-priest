import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Btn = styled.input`
    padding: 15px 20px;
    background-color: ${props => props.theme.primary};
    font-family: ${props => props.theme.FFDINWebProMedium};
    border: none;
    cursor: pointer;
`;

const SubmitButton = props => {
    const { content } = props;
    const text = `${content} →`;

    return <Btn type="submit" value={text} />;
};

export default SubmitButton;
