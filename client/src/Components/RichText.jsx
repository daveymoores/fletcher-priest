import React from "react";
import styled from "styled-components";
import { Grid, GridItem } from "styled-grid-responsive";
import GridContainer from "../Common/GridContainer";

const RichTextWrapper = styled.section`
    p {
        margin-bottom: 30px;
    }

    img {
        width: 100%;
        max-width: 100%;
        margin: 30px 0;
    }
`;

const RichText = ({ PrismicReact, slice, PrismicConfig }) => (
    <GridContainer>
        <Grid>
            <GridItem
                media={{
                    smallPhone: 0,
                    mediumPhone: 1 / 12
                }}
                col={1 / 6}
            />
            <GridItem
                media={{
                    smallPhone: 1,
                    mediumPhone: 10 / 12
                }}
                col={2 / 3}
            >
                <RichTextWrapper>
                    {PrismicReact.RichText.render(
                        slice.primary.rich_text,
                        PrismicConfig.linkResolver
                    )}
                </RichTextWrapper>
            </GridItem>
        </Grid>
    </GridContainer>
);

export default RichText;
