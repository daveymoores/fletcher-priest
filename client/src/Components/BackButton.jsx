// @flow

import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Back = styled(Link)`
    font-family: ${props => props.theme.FFDINWebProBlack};
    font-size: 20px;
    margin-top: 40px;
    color: ${props => props.theme.primary};
    display: block;
`;

type Props = {
    slug: string,
    destination: string
};

const BackButton = (props: Props) => {
    const { slug, destination } = props;
    return <Back to={slug}>&larr; Back to {destination}</Back>;
};

export default BackButton;
