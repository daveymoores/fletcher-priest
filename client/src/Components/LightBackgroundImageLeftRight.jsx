// @flow

import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import PrismicReact from "prismic-reactjs";
import "../Vendor/carousel-styles.css";
import styled, { css } from "styled-components";
import BackgroundImage from "react-background-image-loader";
import Loadable from "react-loadable";
import Slider from "react-slick";
import media from "../utils/style-utils";

import PrismicConfig from "../prismic-configuration";
import SlickCarouselArrow from "../Common/SlickCarouselArrow";
import GridContainer from "../Common/GridContainer";
import InternalLink from "./InternalLink";

const CarouselWrapper = styled.div`
  position: relative;
  display: block;

  .BrainhubCarousel__customArrows > div {
    margin-top: 40px;
  }
`;

const Slide = styled.div`
  height: 42vw;
  max-height: 75vh;
  min-height: 350px;
  width: 100%;

  > div {
    background-position: center center;
    background-size: cover;
    height: 100vh;
    min-height: auto;
    height: 42vw;
    max-height: 75vh;
    min-height: 350px;
  }
`;

const CarouselTypeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  positon: relative;
  margin: 80px 0 20px;

  ${media.tablet`
      flex-direction: row;
    `};

  > div {
    &:first-child {
      order: 0;
      width: 100%;
      min-width: 100%;
      height: 100%;

      ${media.tablet`
              width: 50%;
              min-width: 50%;
            `};
    }

    &:last-child {
      order: 1;
    }
  }

  ${props =>
    props.type === "Right" &&
    css`
      > div {
        &:first-child {
          order: 0;

          ${media.tablet`
                      order: 1;
                    `};
        }

        &:last-child {
          order: 1;

          ${media.tablet`
                      order: 0;
                    `};
        }
      }
    `};
`;

const CarouselText = styled.div`
  background-color: ${props => props.theme.lightGrey};
  width: 100%;
  min-height: ${props => (props.height ? `${props.height}px` : `auto`)};
  padding: 30px 20px 20px;
  position: relative;
  display: flex;

  ${media.mobile`
      padding: 40px;
    `};

  ${media.tablet`
      width: 50%;
      padding: 60px;
    `};

  > div {
    max-width: 500px;
    margin: auto;
  }

  h3 {
    font-size: 28px;
    line-height: 28px;
    color: black;
    margin-top: 0;

    ${media.tablet`
          font-size: 45px;
          line-height: 45px;
        `};
  }

  p {
    color: black;
  }
`;

type Props = {
  content: {
    items: Array<Object>,
    primary: {
      image_alignment: string,
      section_title: Array<Object>,
      section_text: Array<Object>,
      internal_link: Array<Object>,
      internal_link_2: Array<Object>
    }
  },
  prismic: () => mixed
};

type State = {
  domLoaded: boolean,
  height: ?number
};

class LightBackgroundImageLeftright extends Component<Props, State> {
  keyCount: number;

  carouselRef: HTMLElement;

  constructor() {
    super();

    this.state = {
      domLoaded: false,
      height: null
    };

    this.keyCount = 0;
    (this: any).getKey = this.getKey.bind(this);
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  getKey() {
    return this.keyCount++;
  }

  handleResize = () => {
    const c = this.carouselRef.getBoundingClientRect().height;
    this.setState({
      height: c - 80
    });
  };

  render() {
    const { domLoaded, height } = this.state;
    const { content, prismic } = this.props;
    let slides = [];
    let carousel = null;
    const ctx = this;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SlickCarouselArrow type="next" />,
      prevArrow: <SlickCarouselArrow type="prev" />
    };

    slides = content.items.map(img => {
      if ("dimensions" in img.image) {
        return (
          <Slide key={this.getKey()}>
            <BackgroundImage
              key={this.getKey()}
              placeholder="./images/grey-placeholder.png"
              className="slide"
              src={img.image.url}
            />
          </Slide>
        );
      }
      return null;
    });

    if (slides.length > 1) {
      carousel = <Slider {...settings}>{slides}</Slider>;
    } else {
      carousel = slides;
    }

    return (
      <CarouselTypeWrapper type={content.primary.image_alignment}>
        <CarouselWrapper innerRef={el => (this.carouselRef = el)}>
          {carousel}
        </CarouselWrapper>
        <CarouselText height={height}>
          <div>
            <div>
              {PrismicReact.RichText.render(
                content.primary.section_title,
                PrismicConfig.linkResolver
              )}
              {PrismicReact.RichText.render(
                content.primary.section_text,
                PrismicConfig.linkResolver
              )}
              <InternalLink
                PrismicReact={PrismicReact}
                link={content.primary.internal_link}
              />
              <InternalLink
                PrismicReact={PrismicReact}
                link={content.primary.internal_link_2}
              />
            </div>
          </div>
        </CarouselText>
      </CarouselTypeWrapper>
    );
  }
}

export default LightBackgroundImageLeftright;
