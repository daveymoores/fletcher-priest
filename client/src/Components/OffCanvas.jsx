import React, { Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import debounce from "lodash/debounce";
import axios from "axios";
import { TimelineMax as Timeline, Power2 } from "gsap/TweenMax";
import SimpleReactValidator from "simple-react-validator";
import media from "../utils/style-utils";

import OffCanvasAnimation from "../Animations/OffCanvasAnimation";
import Cross from "../Common/icons/offcanvas-cross.svg";
import { setOffCanvasState } from "../redux/actionCreators";

const animOffset = 450;
const animOffsetMobile = 325;

const OffCanvasWrapper = styled.div`
    position: relative;
    width: 100vw;
    height: auto;
    top: 0;
    overflow-y: initial;
`;

const OffCanvasInnerWrapper = styled.div`
    position: relative;
    width: auto;
    height: auto;
    top: 0px;
`;

const OffCanvasContent = styled.div`
    background-color: #f2f2f2;
    border-left: 1px solid ${props => props.theme.darkGrey};
    padding: 30px;
    width: ${props =>
        props.type === "desktop" ? `${animOffset}px` : `${animOffsetMobile}px`};
    height: 100vh;
    position: absolute;
    top: 0;
    right: ${props =>
        props.type === "desktop"
            ? `-${animOffset}px`
            : `-${animOffsetMobile}px`};
    margin-top: ${props => (props.type === "desktop" ? `-20px` : `0`)};

    h3 {
        font-size: 28px;
        margin: 0 0 38px;

        ${media.mobile`
          font-size: 45px;
        `};
    }

    input {
        background-color: white;
        padding: 15px 10px;
        font-family: ${props => props.theme.FFDINWebProMedium};
        width: 100%;
        margin-bottom: 10px;
        border: none;

        &::placeholder {
            color: black;
        }

        &[type="checkbox"] {
            max-width: 20px;
            max-height: 20px;
            border-radius: 50%;
            background-color: ${props => props.theme.darkGrey6};
            padding: 0px;
            position: relative;

            &:checked {
                background-color: ${props => props.theme.darkGrey3};
            }
        }
    }

    button {
        width: 100%;
        border: none;
        padding: 15px 10px;
        background-color: ${props => props.theme.primary};
        font-family: ${props => props.theme.FFDINWebProMedium};
        text-align: center;
        cursor: pointer;
    }

    label {
        display: flex;
        margin: 20px 0;
        font-family: ${props => props.theme.FFDINWebProMedium};

        span {
            display: flex;
            flex-direction: column;
            margin-left: 20px;
        }

        small {
            padding-top: 5px;
        }
    }
`;

const FixedContent = styled.div`
    position: relative;
`;

const OffCanvasChildren = styled.div``;

const DownloadMessage = styled.div`
    position: absolute;
    top: 100px;
`;

const RequestHardCopy = styled.div`
    position: absolute;
    top: 50px;
`;

const Close = styled.a`
    position: absolute;
    top: 0px;
    right: 0px;
    width: 25px;
    height: 25px;
    z-index: 2;

    > svg {
        width: 100%;
        height: 100%;
    }
`;

const FormWrapper = styled.div`
    position: relative;
`;

const Confirmation = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    width: 70%;
    transform: translate3d(-50%, -50%, 0);
    opacity: 0;
    pointer-events: none;

    h3 {
        font-size: 28px;
        text-align: center;
    }
`;

class OffCanvas extends Component {
    constructor(props) {
        super();

        this.state = {
            viewport: { x: window.innerWidth, y: window.innerHeight },
            firstname: "",
            secondname: "",
            address1: "",
            address2: "",
            addresstown: "",
            postcode: ""
        };

        this.handleResize = debounce(this.handleResize, 100);
        this.handleClickEvent = this.handleClickEvent.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleHardCopySubmit = this.handleHardCopySubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.validator = new SimpleReactValidator();
    }

    componentWillMount() {
        this.handleResize();
    }

    componentDidMount() {
        this.domRefs = {
            wrapper: this.offCanvasWrapperRef,
            content: this.offCanvasContentRef,
            download: this.downloadRef,
            hardCopy: this.hardcopyRef,
            fixedContent: this.fixedContentRef
        };

        this.scrollVars = {
            latestKnownScrollY: 0,
            ticking: false
        };

        this.animation = new OffCanvasAnimation(this.domRefs, animOffset);

        window.addEventListener("scroll", this.onScroll.bind(this), false);
        window.addEventListener("resize", this.handleResize, false);
    }

    componentDidUpdate() {
        const { offCanvas, downloadMessage } = this.props;
        const { viewport } = this.state;

        if (downloadMessage === "download") {
            this.downloadRef.style.display = "block";
            this.hardcopyRef.style.display = "none";
        } else {
            this.downloadRef.style.display = "none";
            this.hardcopyRef.style.display = "block";
        }

        if (offCanvas === "open") {
            return this.animation.openCanvas();
        }

        return this.animation.closeCanvas();
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.onScroll.bind(this), false);
        window.removeEventListener("resize", this.handleResize, false);
    }

    onScroll() {
        this.scrollVars.latestKnownScrollY = window.scrollY;
        this.requestTick();
    }

    handleResize = () => {
        const ww = window.innerWidth;

        this.setState(() => ({
            viewport: { x: window.innerWidth, y: window.innerHeight }
        }));
    };

    update() {
        this.scrollVars.ticking = false;

        const offset = this.scrollVars.latestKnownScrollY;
        const wrapperDims = this.domRefs.wrapper.getBoundingClientRect().height;
        const hardCopyHeight = this.domRefs.hardCopy.getBoundingClientRect()
            .height;
        if (offset < document.body.scrollHeight) {
            this.domRefs.fixedContent.parentNode.style.transform = `translate3d(0, ${offset}px, 0)`;
        }
    }

    requestTick() {
        if (!this.scrollVars.ticking) {
            requestAnimationFrame(this.update.bind(this));
        }
        this.scrollVars.ticking = true;
    }

    handleClickEvent(e) {
        e.preventDefault();

        const { offCanvas, handleOffCanvasChange } = this.props;
        if (offCanvas === "open") handleOffCanvasChange("closed");
    }

    handleSubmit(e) {
        const { offCanvas, handleOffCanvasChange } = this.props;
        const formParent = e.currentTarget;
        const child = formParent.firstChild;

        child.style.display = "none";
        const node = document.createElement("h4");
        const text = document.createTextNode(
            "Thanks for joining our mailing list."
        );
        node.setAttribute(
            "style",
            "position: absolute; text-align: center; margin: 20px auto; width: 100%;"
        );
        node.appendChild(text);
        formParent.appendChild(node);
        if (offCanvas === "open") handleOffCanvasChange("closed");
    }

    handleInput(e) {
        if (e.target.checked) {
            this.setState({ [e.target.name]: e.target.checked });
        } else {
            this.setState({ [e.target.name]: e.target.value });
        }
    }

    async handleHardCopySubmit(e) {
        e.preventDefault();
        const {
            offCanvas,
            handleOffCanvasChange,
            hardCopyTitle,
            history
        } = this.props;
        const tl = new Timeline();

        const {
            firstname,
            secondname,
            address1,
            address2,
            addresstown,
            postcode
        } = this.state;

        if (this.validator.allValid()) {
            const formPost = axios
                .post("/api/form", {
                    firstname,
                    secondname,
                    address1,
                    address2,
                    addresstown,
                    postcode,
                    hardCopyTitle
                })
                .then(response => {
                    if (response.status === 200) {
                        tl.to(this.formRef, 0.3, { autoAlpha: 0.1 }).to(
                            this.confirmationRef,
                            0.3,
                            {
                                autoAlpha: 1,
                                onComplete: () => {
                                    setTimeout(() => {
                                        if (offCanvas === "open")
                                            handleOffCanvasChange("closed");
                                    }, 1500);
                                }
                            }
                        );
                    }
                })
                .catch(error => {
                    console.log(error); // eslint-disable-line no-console
                    history.push("/error");
                });
        } else {
            this.validator.showMessages();
            // rerender to show messages for the first time
            this.forceUpdate();
        }
    }

    render() {
        const { children } = this.props;
        const {
            viewport,
            firstname,
            secondname,
            address1,
            address2,
            addresstown,
            postcode
        } = this.state;
        const type = viewport.x >= 576 ? "desktop" : "mobile";

        return (
            <OffCanvasWrapper>
                <OffCanvasInnerWrapper
                    innerRef={el => {
                        this.offCanvasWrapperRef = el;
                    }}
                >
                    <OffCanvasContent
                        type={type}
                        innerRef={el => {
                            this.offCanvasContentRef = el;
                        }}
                    >
                        <FixedContent
                            innerRef={el => {
                                this.fixedContentRef = el;
                            }}
                        >
                            <Close href="#" onClick={this.handleClickEvent}>
                                <Cross />
                            </Close>
                            <DownloadMessage
                                innerRef={el => {
                                    this.downloadRef = el;
                                }}
                            >
                                <h3>Your download should start shortly</h3>

                                <p>
                                    Get the latest insights, straight to your
                                    inbox.
                                </p>

                                <p>
                                    Sign up to the Fletcher Priest Architects
                                    newsletter.
                                </p>

                                <p>
                                    We’ll send you a newsletter once per month.
                                    No spam, ever.
                                </p>

                                <form
                                    action="https://fletcherpriest.us13.list-manage.com/subscribe/post?u=0f797f2a2950b4bd1c97f0d27&amp;id=b22beb9ffb"
                                    method="post"
                                    name="mc-embedded-subscribe-form"
                                    className="validate"
                                    target="_blank"
                                    noValidate
                                    onSubmit={this.handleSubmit}
                                >
                                    <FormWrapper>
                                        <div
                                            style={{
                                                position: "absolute",
                                                left: "-5000px"
                                            }}
                                            aria-hidden="true"
                                        >
                                            <input
                                                type="text"
                                                name="b_0f797f2a2950b4bd1c97f0d27_b22beb9ffb"
                                                tabIndex="-1"
                                                value=""
                                            />
                                        </div>
                                        <button
                                            type="submit"
                                            value="Subscribe"
                                            name="subscribe"
                                            id="mc-embedded-subscribe"
                                            className="button"
                                        >
                                            Sign up
                                        </button>
                                    </FormWrapper>
                                </form>
                            </DownloadMessage>
                            <RequestHardCopy
                                innerRef={el => {
                                    this.hardcopyRef = el;
                                }}
                            >
                                <h3>Request a hard copy</h3>

                                <Confirmation
                                    innerRef={el => {
                                        this.confirmationRef = el;
                                    }}
                                >
                                    <h3>Thanks for your request</h3>
                                </Confirmation>

                                <form
                                    id="hardCopyForm"
                                    encType="multipart/form-data"
                                    onSubmit={this.handleHardCopySubmit}
                                    ref={el => {
                                        this.formRef = el;
                                    }}
                                >
                                    <input
                                        type="text"
                                        name="firstname"
                                        value={firstname}
                                        placeholder="First name"
                                        onChange={this.handleInput}
                                    />
                                    {this.validator.message(
                                        "first name",
                                        firstname,
                                        "required"
                                    )}
                                    <input
                                        type="text"
                                        name="secondname"
                                        value={secondname}
                                        placeholder="Second name"
                                        onChange={this.handleInput}
                                    />
                                    {this.validator.message(
                                        "second name",
                                        secondname,
                                        "required"
                                    )}
                                    <input
                                        type="text"
                                        name="address1"
                                        value={address1}
                                        placeholder="Address line 1"
                                        onChange={this.handleInput}
                                    />
                                    {this.validator.message(
                                        "address",
                                        address1,
                                        "required"
                                    )}
                                    <input
                                        type="text"
                                        name="address2"
                                        value={address2}
                                        placeholder="Address line 2"
                                        onChange={this.handleInput}
                                    />
                                    <input
                                        type="text"
                                        name="addresstown"
                                        value={addresstown}
                                        placeholder="Town / city"
                                        onChange={this.handleInput}
                                    />
                                    <input
                                        type="text"
                                        name="postcode"
                                        value={postcode}
                                        placeholder="Postcode"
                                        onChange={this.handleInput}
                                    />
                                    {this.validator.message(
                                        "postcode",
                                        postcode,
                                        "required"
                                    )}
                                    <input
                                        type="hidden"
                                        name="_honeypot"
                                        defaultValue
                                        onChange={this.handleInput}
                                    />
                                    <input
                                        type="hidden"
                                        name="_subject"
                                        value="Hard Copy Request"
                                        onChange={this.handleInput}
                                    />
                                    <button type="submit">
                                        Request a copy
                                    </button>
                                </form>
                            </RequestHardCopy>
                        </FixedContent>
                    </OffCanvasContent>
                    <OffCanvasChildren>{children}</OffCanvasChildren>
                </OffCanvasInnerWrapper>
            </OffCanvasWrapper>
        );
    }
}

const mapStateToProps = state => ({
    offCanvas: state.offCanvas,
    downloadMessage: state.downloadMessage,
    hardCopyTitle: state.hardCopyTitle
});

const mapDispatchToProps = dispatch => ({
    handleOffCanvasChange(value) {
        dispatch(setOffCanvasState(value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OffCanvas);
