import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import Slider from "react-slick";
import debounce from "lodash/debounce";
import media from "../utils/style-utils";

import PrismicConfig from "../prismic-configuration";
import SlickCarouselArrow from "../Common/SlickCarouselArrow";

const CarouselWrapper = styled.div`
  position: relative;
  display: block;
  padding-bottom: 40px;

  div[type="prev"],
  div[type="next"] {
    top: -60px;
    margin-right: 20px;

    ${media.tablet`
          top: 80%;
          margin-right: 8vw;
        `};
  }

  .slick-list {
    display: flex;
    align-items: flex-start;

    > .slick-track {
      display: flex;
    }
  }
`;

const Slide = styled.div`
  width: 100%;
  display: flex !important;
  align-items: flex-start;
  padding-right: 30px;
  position: relative;
  min-width: 100vw;
  max-width: 100vw;
  width: 100vw;
  flex-direction: column;

  @media (min-width: 340px) {
    flex-direction: row;
  }

  &:focus {
    outline: 0;
  }

  ${media.tablet`
        padding-right: 80px;
          min-height: 340px;
          min-width: 90vw;
          max-width: 90vw;
          width: 100vw;
    `};

  ${media.desktop`
      align-items: center;
    `};

  img {
    min-width: 120px;
    max-width: 340px;
    width: 100%;
    height: 100%;
    margin-right: 60px;

    @media all and (-ms-high-contrast: none) {
      min-width: 340px;
    }

    ${media.tablet`
          margin-right: 100px;
        `};
  }
`;

const Quote = styled.div`
  position: relative;
  margin-top: 30px;
  margin-left: 40px;

  @media (min-width: 340px) {
    margin-top: 0;
    margin-left: 0;
  }

  &::before {
    content: "“";
    color: ${props => props.theme.primary};
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 75px;
    line-height: 75px;
    position: absolute;
    top: -10px;
    left: -45px;
  }

  p {
    font-size: 16px;
    line-height: 20px;
    margin-top: 0;

    ${media.tablet`
          font-size: 22px;
          line-height: 30px;
        `};
  }

  span p {
    color: black;
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 18px;
    line-height: 18px;
    margin: 0;
  }

  h4 {
    font-size: 25px;
    line-height: 50px;
    color: black;
    font-family: ${props => props.theme.FFDINWebProMedium};
    margin: 0;
  }
`;

class QuoteCarousel extends Component {
  constructor(props) {
    super();

    this.counter = 0;
    this.state = {
      viewport: { x: window.innerWidth, y: window.innerHeight }
    };
    this.handleResize = debounce(this.handleResize, 100);
    this.getKey = this.getKey.bind(this);
  }

  componentWillMount() {
    this.handleResize();
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize, false);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize, false);
  }

  getKey() {
    return this.counter++;
  }

  handleResize = () => {
    this.setState(() => ({
      viewport: { x: window.innerWidth, y: window.innerHeight }
    }));
  };

  render() {
    const { content, prismic } = this.props;
    const { viewport } = this.state;
    let slides = [];
    let carousel = null;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      nextArrow: <SlickCarouselArrow type="next" />,
      prevArrow: <SlickCarouselArrow type="prev" />
    };

    slides = content.map(quote => (
      <Slide key={this.getKey()}>
        <img src={quote.staff_profile_photo.url} alt="placeholder" />
        <Quote>
          {PrismicReact.RichText.render(
            quote.quote_text,
            PrismicConfig.linkResolver
          )}
          {PrismicReact.RichText.render(
            quote.staff_name,
            PrismicConfig.linkResolver
          )}
          <span>
            {PrismicReact.RichText.render(
              quote.staff_job_title,
              PrismicConfig.linkResolver
            )}
          </span>
        </Quote>
      </Slide>
    ));

    if (slides.length > 1) {
      carousel = <Slider {...settings}>{slides}</Slider>;
      return <CarouselWrapper>{carousel}</CarouselWrapper>;
    }

    carousel = slides;
    return <CarouselWrapper>{carousel}</CarouselWrapper>;
  }
}

export default QuoteCarousel;
