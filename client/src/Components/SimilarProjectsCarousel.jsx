import React, { Component } from 'react';
import Prismic from 'prismic-javascript';
import { Link } from 'react-router-dom';
import PrismicReact from 'prismic-reactjs';
import '../Vendor/carousel-styles.css';
import styled from 'styled-components';
import Loadable from 'react-loadable';
import Slider from 'react-slick';
import media from '../utils/style-utils';

import SlickCarouselArrow from '../Common/SlickCarouselArrow';
import InsetHeading from './InsetHeading';
import { FluidGrid, FluidRow, FluidGridParent } from '../Common/FluidGrid';
import Spots from '../Common/textures/spots.png';
import LoaderGridItem from '../Loaders/LoaderGridItem';
import LoaderGridItemCarousel from '../Loaders/LoaderGridItemCarousel';
import ProjectTeaser from './ProjectTeaser';

const CarouselWrapper = styled.div`
  position: relative;
  margin-bottom: 60px;
  display: block;

  div[type='prev'],
  div[type='next'] {
    top: 0;
    margin-top: -60px;

    ${media.tablet`
          margin-top: -70px;
        `};
  }

  .slick-track {
    background-image: url(${Spots});
  }

  .slick-slide {
    display: block;
    border: 1px solid ${props => props.theme.darkGrey};
    border-right: none;
    background-color: white;
    position: relative;

    &.slick-active {
      .animating {
        border-right: 1px solid #868989;
        width: calc(100% + 1px);
      }
    }

    > div {
      h3 {
        font-size: 21px;
        position: relative;

        &:: after {
          content: '';
          width: 40px;
          height: 4px;
          position: absolute;
          bottom: -0.8em;
          left: 0;
          background-color: ${props => props.theme.primary};
        }
      }

      h4 {
        font-size: 14px;
        margin: 25px 0 10px;
      }
    }

    h5 {
      font-size: 15px;
    }

    a > img {
      position: absolute;
      max-width: calc(100% - 40px);
      width: auto;
      max-height: 65%;
      top: 50%;
      left: 50%;
      transform: translate3d(-50%, -50%, 0);
      margin-top: 25px;
    }
  }
`;

export default class SimilarProjectsCarousel extends Component {
  constructor() {
    super();

    this.state = {
      domLoaded: false
    };
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {
    const { domLoaded } = this.state;
    const { prismicCtx, location, headingText, results } = this.props;
    let slides = [];
    let carousel = null;
    const ctx = this;

    const settings = {
      dots: false,
      infinite: true,
      draggable: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      nextArrow: <SlickCarouselArrow type='next' />,
      prevArrow: <SlickCarouselArrow type='prev' />,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            draggable: true
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            draggable: true
          }
        }
      ]
    };

    if (results) {
      const text = headingText || `Key projects`;

      slides = results.items
        .filter(item => {
          if (item.project_link.id && !item.isBroken) return true;
          return false;
        })
        .map((project, index, array) => {
          if (array.length > 3) {
            return (
              <ProjectTeaser
                key={project.project_link.id}
                project={project.project_link}
                prismicCtx={prismicCtx}
                PrismicReact={PrismicReact}
                location={location}
                gridSize={100}
              />
            );
          }
          return (
            <FluidGrid key={project.project_link.id}>
              <ProjectTeaser
                project={project.project_link}
                prismicCtx={prismicCtx}
                PrismicReact={PrismicReact}
                location={location}
                gridSize={100}
              />
            </FluidGrid>
          );
        });

      if (slides.length > 3) {
        carousel = (
          <CarouselWrapper innerRef={node => (this.wrapperRef = node)}>
            <InsetHeading text={text} />
            <Slider {...settings}>{slides}</Slider>
          </CarouselWrapper>
        );
      } else {
        carousel = (
          <CarouselWrapper innerRef={node => (this.wrapperRef = node)}>
            <InsetHeading text={text} />
            <FluidGridParent>
              <FluidRow>{slides}</FluidRow>
            </FluidGridParent>
          </CarouselWrapper>
        );
      }

      return <section>{carousel}</section>;
    }
    return <LoaderGridItemCarousel />;
  }
}
