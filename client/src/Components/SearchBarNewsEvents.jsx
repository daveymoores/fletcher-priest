import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled, { css } from "styled-components";
import Moment from "moment";
import { connect } from "react-redux";

import LoaderSearchBar from "../Loaders/LoaderSearchBar";
import GridContainer from "../Common/GridContainer";
import Search from "./icons/search.svg";
import Small from "./icons/small.svg";
import Large from "./icons/large.svg";
import {
  OpenDropdown,
  OpenSearchDropdown
} from "../Animations/SearchAnimation";
import {
  SearchParent,
  SearchDropdown,
  SearchDropdownOptions,
  SearchItem,
  SearchResize,
  SearchTextInput,
  SearchContainer,
  SearchIcon,
  DownChevron
} from "./search-bar-styles";
import { setFilterState } from "../redux/actionCreators";
import SearchBarAnimation from "../Animations/SearchBarAnimation";

function setSelectState(element) {
  const parent = element.parentNode.parentNode;
  const currentSelected = parent.querySelectorAll(".selected");

  // does target element contain selected
  function checkChildrenForSelected() {
    if (element.nextSibling) {
      const selected = element.nextSibling.querySelector(".selected") || false;
      return selected;
    }
    return false;
  }

  if (element.classList.contains("selected")) {
    element.classList.remove("selected");
  } else if (currentSelected && !checkChildrenForSelected()) {
    [].forEach.call(currentSelected, e => {
      e.classList.remove("selected");
    });
    element.classList.add("selected");
  } else {
    element.classList.add("selected");
  }
  return true;
}

function arraysEqual(_arr1, _arr2) {
  function returnArrayFromIds(arr) {
    const a = [];

    arr.map(i => {
      a.push(i.id);
    });

    return a;
  }

  if (
    !Array.isArray(_arr1) ||
    !Array.isArray(_arr2) ||
    _arr1.length !== _arr2.length
  )
    return false;

  const arr1 = returnArrayFromIds(_arr1)
    .concat()
    .sort();
  const arr2 = returnArrayFromIds(_arr2)
    .concat()
    .sort();

  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) return false;
  }

  return true;
}

function constructQuery(filter, yearTerm, sortTerm, textSearch) {
  if (!sortTerm || sortTerm === "") {
    return [
      Prismic.Predicates.any(filter[0], filter[1]),
      Prismic.Predicates.fulltext("document", textSearch)
    ];
  }
  return [
    Prismic.Predicates.any(filter[0], filter[1]),
    Prismic.Predicates.fulltext("document", textSearch),
    Prismic.Predicates.year(yearTerm, sortTerm)
  ];
}

class SearchBarNewsEvents extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: null,
      revealed: false,
      dropdownData: null,
      textSearch: "",
      filterTerm: ["document.type", ["single_news_item", "single_events_page"]],
      sortTerm: "",
      totalRendered: null, // total visible on the page
      resultsSize: null // number of total results
    };

    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.toggleSearchDropdown = this.toggleSearchDropdown.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFilterClick = this.handleFilterClick.bind(this);
    this.handleSortClick = this.handleSortClick.bind(this);
  }

  componentWillMount() {
    this.fetchPage(1, this.props, [], allContent => {
      // allContent contains all the documents
      this.setState({
        dropdownData: allContent
      });
    });

    // get all event data
    // get all news data
    // combine data
    this.getCombinedPredicate();
  }

  componentDidMount() {
    this.mounted = true;
    const { history } = this.props;

    if (history.location.search !== "") {
      const searchTerms = history.location.search.split("&");
      let s = searchTerms[0].replace("?sortTerm=", "").replace("%20", " ");
      let f = searchTerms[1]
        .replace("filterTerm=", "")
        .replace("[", "")
        .replace("]", "")
        .replace("%20", " ")
        .split(",");

      s = s === "" ? "" : s;

      if (f[0] === "") {
        f = ["document.type", ["single_news_item", "single_events_page"]];
      } else {
        const arr = [];
        arr.push(f[0]);
        arr.push([f[1]]);
        f = arr;
      }

      this.setState(
        {
          filterTerm: f,
          sortTerm: s,
          totalRendered: null
        },
        () => {
          this.getCombinedPredicate();
        }
      );
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { dropdownData } = prevState;
    const {
      results,
      textSearch,
      sortTerm,
      filterTerm,
      dropdownData: data,
      revealed
    } = this.state;
    const { callback, history } = this.props;

    if (this.searchParentRef && !revealed) {
      this.setState({ revealed: true }, () => {
        this.animations = new SearchBarAnimation(this.searchParentRef);
      });
    }

    if (prevState.results !== results) {
      if (!arraysEqual(prevState.results, results)) {
        callback([results, textSearch]);
      }
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  getCombinedPredicate() {
    const { filterState, handleFilterChange } = this.props;
    const { filterTerm, results: oldResults } = this.state;

    this.buildNewsPredicate(1, this.props, [], newsContent => {
      // allContent contains all the documents
      this.buildEventsPredicate(1, this.props, [], eventsContent => {
        // allContent contains all the documents
        const newsResults = newsContent;
        const eventResults = eventsContent;

        // check we're looking for events and news
        // also check we're not only looking at events
        const newArray =
          filterTerm[1].indexOf("single_events_page") >= 0 &&
          filterTerm[1].length > 1
            ? newsResults.concat(eventResults)
            : newsResults;
        const uniqueArray = newArray.filter(
          (item, index, self) =>
            index === self.indexOf(self.filter(t => t.id === item.id)[0])
        );

        uniqueArray.sort((a, b) => {
          function getDate(x) {
            if (x.type === "single_events_page") {
              return x.data.event_dates_and_times[0].start_time_date;
            }
            return x.data.publication_date;
          }
          const dateA = new Date(getDate(a));
          const dateB = new Date(getDate(b));
          return dateB - dateA;
        });

        if (!arraysEqual(oldResults, uniqueArray)) {
          this.setState(
            {
              results: uniqueArray
            },
            () => {
              if (filterState === "loading") handleFilterChange("complete");
            }
          );
        }
      });
    });
  }

  buildNewsPredicate(currentPage, props, currentContent, callback) {
    const { prismicCtx } = this.props;
    const { textSearch, filterTerm, sortTerm } = this.state;

    if (prismicCtx) {
      return prismicCtx.api
        .query(
          constructQuery(
            filterTerm,
            "my.single_news_item.publication_date",
            sortTerm,
            textSearch
          ),
          {
            orderings: "[my.single_news_item.publication_date desc]",
            pageSize: 100,
            fetchLinks: [
              "single_events_page.start_time_date",
              "single_events_page.event_dates_and_times",
              "single_events_page.venue_name"
            ]
          }
        )
        .then(response => {
          const oldContent = currentContent.concat(response.results);
          if (response.page < response.total_pages) {
            this.fetchPage(response.page + 1, props, oldContent, newContent => {
              callback(newContent);
            });
          } else {
            callback(oldContent);
          }
        });
    }
    return null;
  }

  buildEventsPredicate(currentPage, props, currentContent, callback) {
    const { prismicCtx } = this.props;
    const { textSearch, filterTerm, sortTerm } = this.state;

    if (prismicCtx) {
      prismicCtx.api
        .query(
          constructQuery(
            ["document.type", ["single_events_page"]],
            "my.single_events_page.event_dates_and_times.start_time_date",
            sortTerm,
            textSearch
          ),
          {
            orderings:
              "[my.single_events_page.event_dates_and_times.start_time_date desc]",
            pageSize: 100,
            fetchLinks: [
              "single_events_page.start_time_date",
              "single_events_page.venue_name"
            ]
          }
        )
        .then(response => {
          const oldContent = currentContent.concat(response.results);
          if (response.page < response.total_pages) {
            this.fetchPage(response.page + 1, props, oldContent, newContent => {
              callback(newContent);
            });
          } else {
            callback(oldContent);
          }
        });
    }
  }

  fetchPage(currentPage, props, currentContent, callback) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(
          Prismic.Predicates.any("document.type", [
            "single_events_page",
            "single_news_item"
          ]),
          {
            pageSize: 100,
            page: currentPage,
            orderings: "[my.single_news_item.publication_date desc]"
          }
        )
        .then(response => {
          const oldContent = currentContent.concat(response.results);
          if (response.page < response.total_pages) {
            this.fetchPage(response.page + 1, props, oldContent, newContent => {
              callback(newContent);
            });
          } else {
            callback(oldContent);
          }
        });
    }
    return null;
  }

  handleChange(event) {
    const { filterState, handleFilterChange } = this.props;
    if (filterState === "complete") handleFilterChange("loading");
    this.searchParentRef.style.marginBottom = "60px";
    this.setState(
      { textSearch: event.target.value, totalRendered: null },
      () => {
        this.getCombinedPredicate();
      }
    );
  }

  handleFilterClick(e) {
    const target = e.currentTarget;
    setSelectState(target);
    const { filterTerm } = this.state;
    const { filterState, handleFilterChange } = this.props;
    if (filterState === "complete") handleFilterChange("loading");
    let f;

    function findFilterParam(value) {
      let x;
      switch (value) {
        case "Event":
          x = ["document.type", ["single_events_page"]];
          break;
        case "News":
          x = ["my.single_news_item.category", ["News"]];
          break;
        case "Press":
          x = ["my.single_news_item.category", ["Press"]];
          break;
        case "Recap":
          x = ["my.single_news_item.category", ["Recap"]];
          break;
        default:
          x = ["document.type", ["single_news_item", "single_events_page"]];
      }

      return x;
    }

    if (!target.classList.contains("selected")) {
      f = ["document.type", ["single_news_item", "single_events_page"]];
    } else {
      f = target.getAttribute("data-search-term")
        ? findFilterParam(target.getAttribute("data-search-term"))
        : filterTerm;
    }

    this.setState({ filterTerm: f, totalRendered: null }, () => {
      this.getCombinedPredicate();
    });
  }

  toggleDropdown(e) {
    const target = e.currentTarget;
    setSelectState(target);
    const dropdown = target.parentNode.querySelector("ul");
    const dropdownItems = dropdown.querySelectorAll("li");

    OpenDropdown(dropdownItems);
    this.searchParentRef.classList.add("dropdown-open");
    const currentActiveDropdown = this.searchParentRef.querySelector(
      ".visible"
    );

    if (currentActiveDropdown) {
      this.searchInputRef.style = "";
      currentActiveDropdown.classList.remove("visible");
    }

    dropdown.classList.add("visible");
  }

  toggleSearchDropdown(e) {
    e.preventDefault();
    const target = e.currentTarget;
    setSelectState(target);
    const input = target.querySelector("input");
    const currentActiveDropdown = this.searchParentRef.querySelector(
      ".visible"
    );
    if (currentActiveDropdown) {
      currentActiveDropdown.classList.remove("filter-open");
      currentActiveDropdown.classList.remove("visible");
    }

    target.classList.add("visible");

    if (window.innerWidth < 576) {
      OpenSearchDropdown(input);
    }
  }

  handleSortClick(e) {
    const target = e.currentTarget;
    const { sortTerm } = this.state;
    const { filterState, handleFilterChange } = this.props;
    if (filterState === "complete") handleFilterChange("loading");
    setSelectState(target);
    let s;

    if (!target.classList.contains("selected")) {
      s = "";
    } else {
      s = target.getAttribute("data-sort-term")
        ? target.getAttribute("data-sort-term")
        : sortTerm;
    }

    this.setState({ sortTerm: s }, () => {
      this.getCombinedPredicate();
    });
  }

  render() {
    const {
      results,
      value,
      dropdownData,
      totalRendered,
      resultsSize
    } = this.state;
    const { children } = this.props;
    const categoryArr = [];
    const yearArr = [];
    const ctx = this;

    function formatDate(dd) {
      const date = PrismicReact.Date(dd);
      const newDate = Moment(date, "ddd MMM DD YYYY HH:mm:ss ZZ").format(
        "YYYY"
      );
      return newDate;
    }

    function onlyUniqueYears(item, index, self) {
      const date = formatDate(item.data.publication_date);
      if (categoryArr.indexOf(date) === -1) {
        categoryArr.push(date);
        return item;
      }
      return null;
    }

    function returnYearDropDown(ddd) {
      if (ddd) {
        return dropdownData.filter(onlyUniqueYears).map(item => {
          if (item.data.publication_date) {
            const date = formatDate(item.data.publication_date);

            return (
              <li key={item.id}>
                <SearchItem data-sort-term={date} onClick={ctx.handleSortClick}>
                  <span>{date}</span>
                </SearchItem>
              </li>
            );
          }
          return null;
        });
      }
      return null;
    }

    function returnChildren(res) {
      if (res) {
        return <React.Fragment>{children}</React.Fragment>;
      }

      return null;
    }

    if (dropdownData || results) {
      return (
        <div>
          <GridContainer type="pullRight">
            <SearchParent
              innerRef={e => {
                this.searchParentRef = e;
              }}
            >
              <SearchDropdown>
                <SearchItem onClick={this.toggleDropdown}>
                  <span>Type</span> <DownChevron />
                </SearchItem>
                <SearchDropdownOptions data-search-predicate="filter">
                  <li>
                    <SearchItem
                      data-search-term="News"
                      onClick={this.handleFilterClick}
                    >
                      <span>News</span>
                    </SearchItem>
                  </li>
                  <li>
                    <SearchItem
                      data-search-term="Press"
                      onClick={this.handleFilterClick}
                    >
                      <span>Press</span>
                    </SearchItem>
                  </li>
                  <li>
                    <SearchItem
                      data-search-term="Recap"
                      onClick={this.handleFilterClick}
                    >
                      <span>Recap</span>
                    </SearchItem>
                  </li>
                  <li>
                    <SearchItem
                      data-search-term="Event"
                      onClick={this.handleFilterClick}
                    >
                      <span>Event</span>
                    </SearchItem>
                  </li>
                </SearchDropdownOptions>
              </SearchDropdown>
              <SearchDropdown>
                <SearchItem onClick={this.toggleDropdown}>
                  <span>Year</span> <DownChevron />
                </SearchItem>
                <SearchDropdownOptions data-search-predicate="sort">
                  {returnYearDropDown(dropdownData)}
                </SearchDropdownOptions>
              </SearchDropdown>
              <SearchContainer onClick={this.toggleSearchDropdown}>
                <SearchTextInput
                  value={value}
                  innerRef={el => (this.searchInputRef = el)}
                  onChange={this.handleChange}
                  onFocus={this.handleChange}
                  placeholder="Search"
                  type="search"
                />
                <SearchIcon />
              </SearchContainer>
            </SearchParent>
          </GridContainer>
          {returnChildren(results)}
        </div>
      );
    }
    return <LoaderSearchBar />;
  }
}

const mapStateToProps = state => ({
  filterState: state.filterState
});

const mapDispatchToProps = dispatch => ({
  handleFilterChange(value) {
    dispatch(setFilterState(value));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBarNewsEvents);
