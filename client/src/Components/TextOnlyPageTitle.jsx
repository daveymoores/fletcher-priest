// @flow

import * as React from 'react';
import styled from 'styled-components';
import media from '../utils/style-utils';

import GridContainer from '../Common/GridContainer';

const Title = styled.div`
  h1 {
    font-size: 16vw;
    line-height: 14vw;
    padding-top: 100px;
    margin-bottom: 50px;
    margin-top: 0;

    ${media.mobile`
          font-size: 14vw;
          line-height: 12vw;
      `};

    ${media.tablet`
          margin-top: 20px;
          font-size: 12vw;
          line-height: 10vw;
      `};

    ${media.desktop`
          font-size: 125px;
          line-height: 110px;
      `};
  }
`;

type Props = {
  pageTitle: React.Element<'h1'> | string
};

const TextOnlyPageTitle = (props: Props) => {
  const { pageTitle } = props;

  return (
    <GridContainer>
      <Title>{pageTitle}</Title>
    </GridContainer>
  );
};

export default TextOnlyPageTitle;
