import React from 'react';
import { Link } from 'react-router-dom';
import { GridItem } from 'styled-grid-responsive';
import styled from 'styled-components';
import { Date } from 'prismic-reactjs';
import Moment from 'moment';
import LazyLoad from 'react-lazyload';
import media from '../utils/style-utils';
import LoaderImage from '../Loaders/LoaderImage';

import PrismicConfig from '../prismic-configuration';

const NewsTeaserCont = styled.div`
  margin-bottom: 40px;

  img {
    width: 100%;
  }

  p {
    line-height: 20px;
    font-size: 16px;

    ${media.mobile`
        font-size: 12px;
        color: ${props => props.theme.darkGrey};
        line-height: 18px;
        `};
  }

  dl {
    display: flex;
    margin: 0 0 10px;
    width: 100%;
  }

  dd {
    margin: 0px;

    > span {
      color: ${props => props.theme.primary};
      padding-right: 10px;
    }
  }

  dt {
    visibility: hidden;
    font-size: 0px;
    text-indent: -9999px;
    position: absolute;
  }
`;

const ReadMore = styled(Link)`
  color: ${props => props.theme.primary};
  font-family: FFDINWebProBlack;
  font-weight: normal;
  font-style: normal;
`;

const Heading = styled.h3`
  font-family: FFDINWebProBold;
  margin-top: 0px;
`;

const EventTeaserCont = styled.div`
  background-color: ${props => props.theme.secondary};
  min-height: 400px;
  margin-bottom: 40px;
  width: 100%;
  position: relative;

  dl {
    display: flex;
    flex-direction: column;
    align-items: space-between;
    justify-content: space-between;
    margin: 10px;
    height: calc(100% - 20px);
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
  }

  dt {
    font-size: 0px;
    visibility: hidden;
    position: absolute;
  }

  dd {
    margin: 0px;
    color: ${props => props.theme.primary};
    width: calc(100% - 20px);

    &.date {
      font-size: 24px;
    }

    span {
      display: block;
      font-size: 16px;
      padding-bottom: 5px;
    }

    h3 {
      margin: 0px;
      font-size: 35px;
      line-height: 40px;
      color: ${props => props.theme.primary};
      font-family: FFDINWebProMedium;
      width: 100%;
      align-self: stretch;
      flex-basis: 100%;
    }
  }
`;

function formatDate(dd) {
  const date = Date(dd);
  const newDate = Moment(date).format('LL');
  return newDate;
}

function checkImage(n) {
  if (n.data.featured_image.url) {
    return (
      <Link to={`/news-and-events/${n.uid}`}>
        <LazyLoad
          height={300}
          debounce={false}
          placeholder={<LoaderImage />}
          offset={400}
        >
          <img alt='cover' src={n.data.featured_image.url} />
        </LazyLoad>
      </Link>
    );
  }

  return null;
}

const NewsTeaser = props => {
  const { news, PrismicReact, windowSize } = props;
  let formattedDate;
  let newsElement;
  let recapElement;
  const ww = windowSize ? windowSize.x : window.innerWidth;
  let rowSize = 0;
  let featuredRowSize = 0;

  if (ww >= 992) {
    rowSize = 4;
    featuredRowSize = 2;
  } else if (ww < 992 && ww > 768) {
    rowSize = 3;
    featuredRowSize = 3;
  } else if (ww <= 768 && ww >= 576) {
    rowSize = 2;
    featuredRowSize = 2;
  } else {
    rowSize = 1;
    featuredRowSize = 1;
  }

  if (news.data.category === 'Recap') {
    let venueName;
    if (typeof news.data.event_recap_link.data !== 'undefined') {
      venueName = PrismicReact.RichText.asText(
        news.data.event_recap_link.data.venue_name
      );
    } else {
      venueName = '';
    }

    recapElement = (
      <Link to={`/news-and-events/${news.uid}`}>
        <EventTeaserCont>
          <dl>
            <dt>Title</dt>
            <dd>
              <h3>
                {Object.prototype.hasOwnProperty.call(news.data, 'title') &&
                  PrismicReact.RichText.asText(news.data.title)}
              </h3>
            </dd>
            <dt>Date</dt>
            <dd className='date'>
              <span>Recap</span>
            </dd>
            <dt>Venue</dt>
            <dd>{venueName}</dd>
          </dl>
        </EventTeaserCont>
      </Link>
    );
  } else {
    formattedDate = formatDate(news.data.publication_date);

    newsElement = (
      <NewsTeaserCont>
        <Heading>
          <Link to={`/news-and-events/${news.uid}`}>
            {Object.prototype.hasOwnProperty.call(news.data, 'title') &&
              PrismicReact.RichText.asText(news.data.title)}
          </Link>
        </Heading>
        <dl>
          <dt>Tags</dt>
          <dd>
            <span>{news.data.category}</span>
          </dd>
          <dt>Date</dt>
          <dd>{formattedDate}</dd>
        </dl>
        {checkImage(news)}
        {Object.prototype.hasOwnProperty.call(news.data, 'text_contents') &&
          PrismicReact.RichText.render(
            news.data.text_contents,
            PrismicConfig.linkResolver
          )}
        <ReadMore to={`/news-and-events/${news.uid}`}>
          Read more &rarr;
        </ReadMore>
      </NewsTeaserCont>
    );
  }

  if (news.data.category === 'Recap') {
    if (news.data.featured_item === 'Featured') {
      return <GridItem col={1 / featuredRowSize}>{recapElement}</GridItem>;
    }
    return <GridItem col={1 / rowSize}>{recapElement}</GridItem>;
  }

  if (news.data.featured_item === 'Featured') {
    return <GridItem col={1 / featuredRowSize}>{newsElement}</GridItem>;
  }

  return <GridItem col={1 / rowSize}>{newsElement}</GridItem>;
};

export default NewsTeaser;
