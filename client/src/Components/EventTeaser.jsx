// @flow

import React from "react";
import { Link } from "react-router-dom";
import { GridItem } from "styled-grid-responsive";
import styled from "styled-components";
import Moment from "moment";

const EventTeaserCont = styled(Link)`
    background-color: ${props => props.theme.primary};
    min-height: 400px;
    width: 100%;
    position: relative;
    display: block;
    margin-bottom: 40px;

    dl {
        display: flex;
        flex-direction: column;
        align-items: space-between;
        justify-content: space-between;
        margin: 10px;
        height: calc(100% - 20px);
        position: absolute;
        top: 0;
        left: 0;
        align-self: stretch;
        flex-basis: 100%;

        @media all and (-ms-high-contrast: none) {
            width: 90%;
        }
    }

    dt {
        font-size: 0px;
        visibility: hidden;
        position: absolute;
    }

    dd {
        margin: 0px;

        &.date {
            font-size: 24px;
        }

        span {
            display: block;
            font-size: 16px;
            padding-bottom: 5px;
        }

        h3 {
            margin: 0px;
            font-size: 35px;
            line-height: 40px;
            font-family: FFDINWebProMedium;
        }
    }
`;

type Props = {
    event: {
        uid: string,
        data: {
            event_dates_and_times: Array<Object>,
            event_name: string,
            venue_name: string
        }
    },
    PrismicReact: {
        Date: (a: string) => {},
        RichText: (a: string) => {}
    }
};

const EventTeaser = (props: Props) => {
    const { event, PrismicReact } = props;
    const date = PrismicReact.Date(
        event.data.event_dates_and_times[0].start_time_date
    );
    const formattedDate = Moment(date).format("LL");
    const ww = window.innerWidth;
    let rowSize = 0;

    if (ww >= 992) {
        rowSize = 4;
    } else if (ww < 992 && ww > 768) {
        rowSize = 3;
    } else if (ww <= 768 && ww >= 576) {
        rowSize = 2;
    } else {
        rowSize = 1;
    }

    return (
        <GridItem col={1 / rowSize}>
            <EventTeaserCont to={`/news-and-events/${event.uid}`}>
                <dl>
                    <dt>Title</dt>
                    <dd>
                        <h3>
                            {PrismicReact.RichText.asText(
                                event.data.event_name
                            )}
                        </h3>
                    </dd>
                    <dt>Date</dt>
                    <dd className="date">
                        <span>Event</span>
                        {formattedDate}
                    </dd>
                    <dt>Venue</dt>
                    <dd>
                        {PrismicReact.RichText.asText(event.data.venue_name)}
                    </dd>
                </dl>
            </EventTeaserCont>
        </GridItem>
    );
};

export default EventTeaser;
