import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import styled, { css } from 'styled-components';
import { connect } from 'react-redux';

import GridContainer from '../Common/GridContainer';
import Small from './icons/small.svg';
import Large from './icons/large.svg';
import {
  OpenDropdown,
  OpenSearchDropdown
} from '../Animations/SearchAnimation';
import GridSlider from './GridSlider';
import {
  SearchParent,
  SearchDropdown,
  SearchDropdownOptions,
  SearchItem,
  SearchResize,
  SearchTextInput,
  SearchContainer,
  SearchIcon,
  DownChevron,
  LoadMore
} from './search-bar-styles';
import { setFilterState } from '../redux/actionCreators';
import SearchBarAnimation from '../Animations/SearchBarAnimation';

function setSelectState(element) {
  const parent = element.parentNode.parentNode;
  const currentSelected = parent.querySelectorAll('.selected');

  // does target element contain selected
  function checkChildrenForSelected() {
    if (element.nextSibling) {
      const selected = element.nextSibling.querySelector('.selected') || false;
      return selected;
    }
    return false;
  }

  if (element.classList.contains('selected')) {
    element.classList.remove('selected');
  } else if (currentSelected && !checkChildrenForSelected()) {
    [].forEach.call(currentSelected, e => {
      e.classList.remove('selected');
    });
    element.classList.add('selected');
  } else {
    element.classList.add('selected');
  }
  return true;
}

class SearchBarPeople extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: null,
      revealed: false,
      textSearch: '',
      filterTerm: [
        'Founding Partner',
        'Partner',
        'Interior Design Partner',
        'Urban Design Partner',
        'Finance Partner',
        'Associate Partner',
        'Associate',
        'Senior Associate'
      ],
      sortTerm: '[my.person.published_date desc]',
      gridSize: 100,
      totalRendered: null, // total visible on the page
      resultsSize: null, // number of total results
      increment: 6 // number to increment by
    };

    this.handleChange = this.handleChange.bind(this);
    this.toggleSearchDropdown = this.toggleSearchDropdown.bind(this);
    this.handleSortClick = this.handleSortClick.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.handleFilterClick = this.handleFilterClick.bind(this);
  }

  componentWillMount() {
    this.buildPredicate(this.props);
  }

  componentDidMount() {
    const { history } = this.props;

    if (history.location.search !== '') {
      const searchTerms = history.location.search.split('&');
      let s = searchTerms[0].replace('?sortTerm=', '').replace('%20', ' ');
      let f = searchTerms[1]
        .replace('filterTerm=', '')
        .replace('[', '')
        .replace(']', '')
        .replace('%20', ' ')
        .split(',');
      // const t = searchTerms[2].replace("textSearch=", "");

      s = s[0] === '' ? '[my.person.publication_date desc]' : s;
      f =
        f[0] === ''
          ? [
              'Founding Partner',
              'Partner',
              'Interior Design Partner',
              'Urban Design Partner',
              'Finance Partner',
              'Associate Partner',
              'Associate',
              'Senior Associate'
            ]
          : [f[0].replace('%20', ' ')];

      this.setState(
        {
          filterTerm: f,
          sortTerm: s
        },
        () => {
          this.buildPredicate();
        }
      );
    }
  }

  componentWillReceiveProps(props) {
    this.buildPredicate(props);
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      results,
      textSearch,
      gridSize,
      filterTerm,
      sortTerm,
      revealed
    } = this.state;
    const { callback, history } = this.props;

    if (
      prevState.sortTerm !== sortTerm ||
      prevState.filterTerm !== filterTerm ||
      prevState.textSearch !== textSearch
    ) {
      history.push({
        pathname: '/people',
        search: `?sortTerm=${sortTerm}&filterTerm=${filterTerm}&textSearch=${textSearch}`
      });
    }

    if (this.searchParentRef && !revealed) {
      this.setState({ revealed: true }, () => {
        this.animations = new SearchBarAnimation(this.searchParentRef);
      });
    }

    if (
      prevState.gridSize !== gridSize ||
      (prevState.results !== results && results !== 'undefined')
    ) {
      callback([results, textSearch, gridSize]);
    }
  }

  gridSizeCallback = data => {
    this.updateState(data);
  };

  handleChange(event) {
    const { filterState, handleFilterChange } = this.props;
    if (filterState === 'complete') handleFilterChange('loading');
    this.searchParentRef.style.marginBottom = '60px';
    this.setState(
      { textSearch: event.target.value, totalRendered: null },
      () => {
        this.buildPredicate();
      }
    );
  }

  handleFilterClick(e) {
    const target = e.currentTarget;
    setSelectState(target);

    const { filterTerm } = this.state;
    const { filterState, handleFilterChange } = this.props;
    if (filterState === 'complete') handleFilterChange('loading');
    let f;

    if (!target.classList.contains('selected')) {
      f = [
        'Founding Partner',
        'Partner',
        'Interior Design Partner',
        'Urban Design Partner',
        'Finance Partner',
        'Associate Partner',
        'Associate',
        'Senior Associate'
      ];
    } else {
      f = target.getAttribute('data-search-term')
        ? target.getAttribute('data-search-term').split(',')
        : filterTerm;

      if (f[0] === 'Partner') {
        f = [
          'Founding Partner',
          'Partner',
          'Interior Design Partner',
          'Urban Design Partner',
          'Finance Partner',
          'Associate Partner'
        ];
      }
    }

    this.setState({ filterTerm: f, totalRendered: null }, () => {
      this.buildPredicate();
    });
  }

  toggleDropdown(e) {
    const target = e.currentTarget;

    setSelectState(target);
    const dropdown = target.parentNode.querySelector('ul');
    const dropdownItems = dropdown.querySelectorAll('li');

    OpenDropdown(dropdownItems);
    this.searchParentRef.classList.add('dropdown-open');
    const currentActiveDropdown = this.searchParentRef.querySelector(
      '.visible'
    );

    dropdown.classList.add('visible');

    if (currentActiveDropdown) {
      this.searchInputRef.style = '';
      currentActiveDropdown.classList.remove('visible');
    }
  }

  toggleSearchDropdown(e) {
    e.preventDefault();
    const target = e.currentTarget;

    setSelectState(target);
    const input = target.querySelector('input');
    const currentActiveDropdown = this.searchParentRef.querySelector(
      '.visible'
    );

    if (currentActiveDropdown) {
      currentActiveDropdown.classList.remove('filter-open');
      currentActiveDropdown.classList.remove('visible');
    }

    target.classList.add('visible');

    if (window.innerWidth < 576) {
      OpenSearchDropdown(input);
    }
  }

  handleSortClick(e) {
    const target = e.currentTarget;
    const { sortTerm } = this.state;
    const { filterState, handleFilterChange } = this.props;
    if (filterState === 'complete') handleFilterChange('loading');
    setSelectState(target);
    let s;

    function findSortParam(value) {
      let x;
      switch (value) {
        case 'forename_ascending':
          x = '[my.person.first_name]';
          break;
        case 'surname_ascending':
          x = '[my.person.last_name]';
          break;
        case 'forename_descending':
          x = '[my.person.first_name desc]';
          break;
        case 'surname_descending':
          x = '[my.person.last_name desc]';
          break;
        default:
          x = '[my.person.first_name]';
      }

      return x;
    }

    if (!target.classList.contains('selected')) {
      s = '[my.person.first_name]';
    } else {
      s = target.getAttribute('data-sort-term')
        ? findSortParam(target.getAttribute('data-sort-term'))
        : sortTerm;
    }

    this.setState({ sortTerm: s }, () => {
      this.buildPredicate();
    });
  }

  updateState(data) {
    this.setState(
      prevState => {
        if (prevState.gridSize !== data) {
          return { gridSize: data };
        }
        return null;
      },
      () => {
        this.buildPredicate();
      }
    );
  }

  loadMore(e) {
    e.preventDefault();
    const { totalRendered, increment, resultsSize } = this.state;
    const { filterState, handleFilterChange } = this.props;
    const newPageSize = totalRendered + increment;
    const newTotal = newPageSize < resultsSize ? newPageSize : resultsSize;
    if (filterState === 'complete') handleFilterChange('loading');
    this.setState({ totalRendered: newTotal }, () => {
      this.buildPredicate();
    });
  }

  buildPredicate() {
    const { prismicCtx, filterState, handleFilterChange } = this.props;
    const {
      textSearch,
      filterTerm,
      sortTerm,
      gridSize,
      totalRendered
    } = this.state;

    const paginationTotal = totalRendered || 25;

    if (prismicCtx) {
      return prismicCtx.api
        .query(
          [
            Prismic.Predicates.any('my.person.job_title', filterTerm),
            Prismic.Predicates.fulltext('document', textSearch)
          ],
          {
            orderings: sortTerm,
            pageSize: paginationTotal
          }
        )
        .then(response => {
          const { results, total_results_size: totalResultsSize } = response;

          this.setState(
            {
              results,
              totalRendered: results.length,
              resultsSize: totalResultsSize
            },
            () => {
              if (filterState === 'loading') handleFilterChange('complete');
            }
          );
        });
    }
    return null;
  }

  render() {
    const { results, value, totalRendered, resultsSize } = this.state;
    const { children } = this.props;
    const ctx = this;

    function renderLoadMore() {
      if (totalRendered < resultsSize) {
        return (
          <LoadMore href='#' onClick={ctx.loadMore.bind(ctx)}>
            Load More
          </LoadMore>
        );
      }

      return null;
    }

    const LoadMoreButton = renderLoadMore();

    return (
      <div>
        <GridContainer type='pullRight'>
          <SearchParent
            innerRef={e => {
              this.searchParentRef = e;
            }}
          >
            <SearchDropdown>
              <SearchItem onClick={this.toggleDropdown}>
                <span>Type</span> <DownChevron />
              </SearchItem>
              <SearchDropdownOptions data-search-predicate='job_title'>
                <li>
                  <SearchItem
                    data-search-term='Founding Partner'
                    onClick={this.handleFilterClick}
                  >
                    <span>Founding Partner</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Partner'
                    onClick={this.handleFilterClick}
                  >
                    <span>Partner</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Associate Partner'
                    onClick={this.handleFilterClick}
                  >
                    <span>Associate Partner</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Interior Design Partner'
                    onClick={this.handleFilterClick}
                  >
                    <span>Interior Design Partner</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Urban Design Partner'
                    onClick={this.handleFilterClick}
                  >
                    <span>Urban Design Partner</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Finance Partner'
                    onClick={this.handleFilterClick}
                  >
                    <span>Finance Partner</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Senior Associate'
                    onClick={this.handleFilterClick}
                  >
                    <span>Senior Associate</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-search-term='Associate'
                    onClick={this.handleFilterClick}
                  >
                    <span>Associate</span>
                  </SearchItem>
                </li>
              </SearchDropdownOptions>
            </SearchDropdown>
            <SearchDropdown>
              <SearchItem onClick={this.toggleDropdown}>
                <span>Sort by</span> <DownChevron />
              </SearchItem>
              <SearchDropdownOptions data-search-predicate='sort'>
                <li>
                  <SearchItem
                    data-sort-term='forename_ascending'
                    onClick={this.handleSortClick}
                  >
                    <span>Forename ascending</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-sort-term='surname_ascending'
                    onClick={this.handleSortClick}
                  >
                    <span>Surname ascending</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-sort-term='forename_descending'
                    onClick={this.handleSortClick}
                  >
                    <span>Forename descending</span>
                  </SearchItem>
                </li>
                <li>
                  <SearchItem
                    data-sort-term='surname_descending'
                    onClick={this.handleSortClick}
                  >
                    <span>Surname descending</span>
                  </SearchItem>
                </li>
              </SearchDropdownOptions>
            </SearchDropdown>
            <SearchResize>
              <GridSlider callback={this.gridSizeCallback} />
            </SearchResize>
            <SearchContainer onClick={this.toggleSearchDropdown}>
              <SearchTextInput
                value={value}
                innerRef={el => (this.searchInputRef = el)}
                onChange={this.handleChange}
                onFocus={this.handleChange}
                placeholder='Search'
                type='search'
              />
              <SearchIcon />
            </SearchContainer>
          </SearchParent>
        </GridContainer>

        {children}
        {LoadMoreButton}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filterState: state.filterState
});

const mapDispatchToProps = dispatch => ({
  handleFilterChange(value) {
    dispatch(setFilterState(value));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBarPeople);
