import React from 'react';
import styled from 'styled-components';
import { Grid, GridItem } from 'styled-grid-responsive';
import GridContainer from '../Common/GridContainer';
import media from '../utils/style-utils';

const Pullquote = styled.blockquote`
  margin: 20px 0 20px;
  font-size: 26px;
  width: 100%;
  font-family: ${props => props.theme.FFDINWebProMedium};

  ${media.tablet`
      margin: 40px 0 40px;
      font-size: 45px;
    `};
`;

const PullQuote = ({ PrismicReact, slice, PrismicConfig }) => (
  <GridContainer>
    <Grid>
      <GridItem
        media={{
          smallPhone: 0,
          mediumPhone: 1 / 12
        }}
        col={1 / 12}
      />
      <GridItem
        media={{
          smallPhone: 1,
          mediumPhone: 10 / 12
        }}
        col={10 / 12}
      >
        <aside>
          <Pullquote>
            {PrismicReact.RichText.asText(
              slice.primary.pull_quote_contents,
              PrismicConfig.linkResolver
            )}
          </Pullquote>
        </aside>
      </GridItem>
    </Grid>
  </GridContainer>
);

export default PullQuote;
