// @flow

import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import PrismicReact from "prismic-reactjs";
import "../Vendor/carousel-styles.css";
import BackgroundImage from "react-background-image-loader";
import styled from "styled-components";
import Loadable from "react-loadable";
import Slider from "react-slick";
import SlickCarouselArrow from "../Common/SlickCarouselArrow";
import media from "../utils/style-utils";

const CarouselWrapper = styled.div`
  position: relative;
  padding-top: 30px;
  display: block;
  margin-bottom: 40px;
  overflow: hidden;

  ${media.tablet`
      padding-top: 60px;
      margin-bottom: 80px;
    `}

  .slick-slider {
    width: 100vw;
  }
`;

const Slide = styled.div`
  max-height: 800px;
  min-height: 250px;
  height: 55vw;
  width: 100%;

  &:focus {
    outline: 0;
  }

  > div {
    background-position: center center;
    background-size: cover;
    min-width: 100%;
    height: 100%;
    min-height: auto;
    max-height: 800px;
  }
`;

type Props = {
  content: Array<Object>,
  prismic: () => mixed
};

type State = {
  domLoaded: boolean
};

class FullWidthCarousel extends Component<Props, State> {
  keyCounter: number;

  constructor(props: Props) {
    super();

    this.state = {
      domLoaded: false
    };

    (this: any).keyCounter = 0;
    (this: any).getKey = this.getKey.bind(this);
  }

  getKey() {
    return this.keyCounter++;
  }

  render() {
    const { domLoaded } = this.state;
    const { content, prismic } = this.props;
    let slides = [];
    let carousel = null;
    const ctx = this;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SlickCarouselArrow type="next" />,
      prevArrow: <SlickCarouselArrow type="prev" />
    };

    slides = content.map(img => {
      if ("dimensions" in img.image) {
        return (
          <Slide key={this.getKey()}>
            <BackgroundImage
              key={this.getKey()}
              placeholder="./images/grey-placeholder.png"
              className="slide"
              src={img.image.url}
            />
          </Slide>
        );
      }
      return null;
    });

    if (slides.length > 1) {
      carousel = <Slider {...settings}>{slides}</Slider>;
    } else {
      carousel = slides;
    }

    return <CarouselWrapper>{carousel}</CarouselWrapper>;
  }
}

export default FullWidthCarousel;
