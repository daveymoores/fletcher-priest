import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import * as PrismicReact from "prismic-reactjs";
import * as DarkBackgroundImageLeftRightAnimation from "../../Animations/DarkBackgroundImageLeftRightAnimation";
import DarkBackgroundImageLeftRight from "../DarkBackgroundImageLeftRight";

configure({ adapter: new Adapter() });

jest.mock("prismic-reactjs", () => ({
    RichText: {
        render: jest.fn()
    }
}));

const mockAnimationFile = jest.fn();

jest.mock("../../Animations/DarkBackgroundImageLeftRightAnimation", () =>
    jest.fn().mockImplementation(() => ({
        show: mockAnimationFile
    }))
);

test("check DarkBackgroundImageRightLeft renders", () => {
    const fakeContent = {
        items: [
            {
                image: {
                    url: "www.fakeurl.com",
                    dimensions: "500"
                }
            }
        ],
        primary: {
            image_alignment: "left",
            section_title: "title",
            section_text: "text"
        }
    };

    const component = shallow(
        <DarkBackgroundImageLeftRight content={fakeContent} />
    );

    component.domRefs = {
        wrapper: component.carouselRef,
        mask: component.maskRef,
        text: component.textWrapperRef
    };

    it("The consumer should be able to call new() on animation file", () => {
        const animation = new DarkBackgroundImageLeftRightAnimation(
            component.domRefs
        );
        animation.show();
    });

    expect(PrismicReact.RichText.render).toHaveBeenCalledTimes(4);
    expect(component).toMatchSnapshot();
});
