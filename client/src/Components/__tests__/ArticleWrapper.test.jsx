import React from "react";
import renderer from "react-test-renderer";
import ArticleWrapper from "../ArticleWrapper";

test("ArticleWrapper renders", () => {
    const component = renderer.create(<ArticleWrapper />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
