import React from "react";
import renderer from "react-test-renderer";
import * as PrismicReact from "prismic-reactjs";
import CareerGrid from "../CareerGrid";

jest.mock("prismic-reactjs", () => ({
    RichText: {
        render: jest.fn()
    }
}));

test("CareerGrid renders", () => {
    const fakeItem = {
        title: "title",
        description: "description",
        reason_background_image: {
            url: "www.url.com"
        }
    };

    const component = renderer.create(<CareerGrid item={fakeItem} index={1} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
