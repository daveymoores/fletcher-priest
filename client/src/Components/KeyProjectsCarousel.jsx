// @flow

import React, { Component } from 'react';
import Prismic from 'prismic-javascript';
import { Link } from 'react-router-dom';
import PrismicReact from 'prismic-reactjs';
import '../Vendor/carousel-styles.css';
import styled from 'styled-components';
import Loadable from 'react-loadable';
import Slider from 'react-slick';
import media from '../utils/style-utils';

import SlickKeyProjectsCarouselArrow from '../Common/SlickKeyProjectsCarouselArrow';
import InsetHeading from './InsetHeading';
import { FluidGrid, FluidRow, FluidGridParent } from '../Common/FluidGrid';
import Spots from '../Common/textures/spots.png';
import LoaderGridItem from '../Loaders/LoaderGridItem';
import LoaderGridItemCarousel from '../Loaders/LoaderGridItemCarousel';
import ProjectTeaser from './ProjectTeaser';

const CarouselWrapper = styled.div`
  position: relative;
  margin-bottom: 60px;
  display: block;

  div[type='prev'],
  div[type='next'] {
    top: 0;
    margin-top: -60px;

    ${media.tablet`
          margin-top: -70px;
        `};
  }

  .slick-track {
    background-image: url(${Spots});
  }

  .slick-slide {
    display: block;
    border: 1px solid ${props => props.theme.darkGrey};
    border-right: none;
    background-color: white;
    position: relative;

    &.slick-active {
      .animating {
        border-right: 1px solid #868989;
        width: calc(100% + 1px);
      }
    }

    > div {
      h3 {
        font-size: 21px;
        position: relative;

        &:: after {
          content: '';
          width: 40px;
          height: 4px;
          position: absolute;
          bottom: -0.8em;
          left: 0;
          background-color: ${props => props.theme.primary};
        }
      }

      h4 {
        font-size: 14px;
        margin: 25px 0 10px;
      }
    }

    h5 {
      font-size: 15px;
    }

    a > img {
      position: absolute;
      max-width: calc(100% - 40px);
      width: auto;
      max-height: 65%;
      top: 50%;
      left: 50%;
      transform: translate3d(-50%, -50%, 0);
      margin-top: 25px;
    }
  }
`;

type State = {
  results: ?Array<Object>,
  gridSize: number,
  domLoaded: boolean
};

type Props = {
  prismicCtx: () => void,
  location: string,
  headingText: string
};

export default class KeyProjectsCarousel extends Component<Props, State> {
  mounted: boolean;

  constructor() {
    super();

    this.state = {
      results: null,
      gridSize: 100,
      domLoaded: false
    };
  }

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillReceiveProps(props: Props) {
    this.fetchPage(props);
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  fetchPage(props: Props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(
          [
            Prismic.Predicates.at('document.type', 'single_project_page'),
            Prismic.Predicates.at(
              'my.single_project_page.featured_project',
              'Featured'
            )
          ],
          {
            orderings: '[my.single_project_page.publication_date desc]'
          }
        )
        .then(response => {
          const { results } = response;
          if (this.mounted) {
            this.setState({ results });
          }
        });
    }
    return null;
  }

  render() {
    const { results, gridSize, domLoaded } = this.state;
    const { prismicCtx, location, headingText } = this.props;
    let slides = [];
    let carousel = null;
    const ctx = this;

    const settings = {
      dots: false,
      infinite: true,
      draggable: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      nextArrow: <SlickKeyProjectsCarouselArrow type="next" />,
      prevArrow: <SlickKeyProjectsCarouselArrow type="prev" />,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            draggable: true
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            draggable: true
          }
        }
      ]
    };

    if (results) {
      const text = headingText || `Selected Projects`;

      slides = results
        .filter(project => {
          if (project.data.featured_project === 'Featured') {
            return true;
          }
          return false;
        })
        .map((project, index, array) => {
          if (array.length > 3) {
            return (
              <ProjectTeaser
                key={project.id}
                project={project}
                gridSize={gridSize}
                prismicCtx={prismicCtx}
                PrismicReact={PrismicReact}
                location={location}
              />
            );
          }
          return (
            <FluidGrid key={project.id}>
              <ProjectTeaser
                project={project}
                gridSize={gridSize}
                prismicCtx={prismicCtx}
                PrismicReact={PrismicReact}
                location={location}
              />
            </FluidGrid>
          );
        });

      if (slides.length > 3) {
        carousel = (
          <CarouselWrapper>
            <InsetHeading text={text} />
            <Slider {...settings}>{slides}</Slider>
          </CarouselWrapper>
        );
      } else {
        carousel = (
          <CarouselWrapper>
            <InsetHeading text={text} />
            <FluidGridParent>
              <FluidRow>{slides}</FluidRow>
            </FluidGridParent>
          </CarouselWrapper>
        );
      }

      return <section>{carousel}</section>;
    }
    return <LoaderGridItemCarousel />;
  }
}
