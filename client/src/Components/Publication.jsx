import React, { Component } from "react";
import { Link } from "react-router-dom";
import { GridItem } from "styled-grid-responsive";
import styled from "styled-components";
import { Date } from "prismic-reactjs";
import Moment from "moment";
import { connect } from "react-redux";
import download from "downloadjs";
import LazyLoad from "react-lazyload";
import {
  setOffCanvasState,
  setDownloadState,
  setHardCopyTitle
} from "../redux/actionCreators";

const PublicationWrapper = styled.div`
  padding: 20px;
  height: auto;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  position: relative;

  img {
    height: auto;
    width: 70%;
    display: block;
    margin: 20px auto 40px;
    position: relative;
    transform: none;
    left: inherit;
    top: inherit;
  }

  h3,
  h4,
  h5 {
    margin: 0px;
  }
`;

const Heading = styled.h3`
  font-family: ${props => props.theme.FFDINWebProMedium};
  font-size: 21px;
  margin-top: 0px;
`;

const DateWrap = styled.dl`
  margin: 0;

  dt {
    visibility: hidden;
    text-indent: -999px;
  }

  dd {
    margin: 10px 0 0;
    font-size: 14px;
    font-family: ${props => props.theme.FFDINWebProMedium};
  }
`;

const Downloads = styled.ul`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: space-between;
  list-style: none;
  font-family: ${props => props.theme.FFDINWebProMedium};
  padding: 0px;
  margin: 0px;

  a {
    font-family: ${props => props.theme.FFDINWebProMedium};
  }

  li {
    &:first-child > a {
      color: ${props => props.theme.primary};
    }
  }
`;

const RequestButton = styled.a`
  background-color: transparent;
  outline: none;
  border: none;
`;

class Publication extends Component {
  handleClickEvent = this.handleClickEvent.bind(this);

  handleClickEvent(e) {
    e.preventDefault();

    if (e.target.hasAttribute("download")) {
      const url = e.target.getAttribute("href");
      const name = e.target.getAttribute("data-name");
      const x = new XMLHttpRequest();
      x.open("GET", url, true);
      x.responseType = "blob";
      x.onload = event => {
        download(event.target.response, name);
      };
      x.send();
    }

    const {
      offCanvas,
      handleOffCanvasChange,
      downloadMessage,
      handleDownloadMessage,
      handleHardCopyTitle,
      publication
    } = this.props;

    if (offCanvas === "closed") handleOffCanvasChange("open");
    const y = e.currentTarget.getAttribute("data-type");

    handleDownloadMessage(y);
    if (y === "hardCopy")
      handleHardCopyTitle(publication.data.publication_title[0].text);
  }

  render() {
    const { publication, prismicCtx, PrismicReact, offCanvas } = this.props;
    const date = Date(publication.data.date_of_publication);
    const formattedDate = Moment(date).format("YYYY");

    const img = publication.data.publication_thumbnail.url;
    const thumbnail = img || `./images/pdf-placeholder.jpg`;

    return (
      <PublicationWrapper>
        <div>
          <Heading>
            <Link to="/news-and-events/">
              {PrismicReact.RichText.asText(publication.data.publication_title)}
            </Link>
          </Heading>
          <DateWrap>
            <dt>Date</dt>
            <dd>{formattedDate}</dd>
          </DateWrap>
        </div>

        <LazyLoad height={300} offset={400}>
          <img src={thumbnail} alt="" />
        </LazyLoad>
        <Downloads>
          <li>
            <a
              href={publication.data.publication.url}
              data-type="download"
              data-name={publication.data.publication.name}
              onClick={this.handleClickEvent}
              download
            >
              &darr; Download
            </a>
          </li>
          <li>
            <RequestButton
              href="#"
              role="button"
              data-type="hardCopy"
              onClick={this.handleClickEvent}
            >
              Request a hard copy
            </RequestButton>
          </li>
        </Downloads>
      </PublicationWrapper>
    );
  }
}

const mapStateToProps = state => ({
  offCanvas: state.offCanvas,
  downloadMessage: state.downloadMessage,
  hardCopyTitle: state.hardcopyTitle
});

const mapDispatchToProps = dispatch => ({
  handleOffCanvasChange(value) {
    dispatch(setOffCanvasState(value));
  },
  handleDownloadMessage(value) {
    dispatch(setDownloadState(value));
  },
  handleHardCopyTitle(value) {
    dispatch(setHardCopyTitle(value));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Publication);
