import React from "react";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import uniqid from "uniqid";
import media from "../utils/style-utils";

import PrismicConfig from "../prismic-configuration";
import Map from "./Map";
import Button from "./Button";
import ForwardButton from "./ForwardButton";
import InternalLink from "./InternalLink";

let order = [0, 1];
const focus = type => {
    if (type === "right") {
        return (order = [0, 1]);
    }
    return (order = [1, 0]);
};

let padding = [0, 0, 0, 0];
const containerPadding = type => {
    if (type === "right") {
        return (padding = 0);
    }
    return (padding = "0 0 0 100px");
};

const LocationWrapper = styled.div`
    width: 100%;
    margin-bottom: 60px;

    ${media.mobile`
      margin-bottom: 100px;
    `};
`;

const LocationList = styled.div`
    margin-bottom: 30px;

    p {
        margin: 0;
        color: black;
        font-size: 14px;
        line-height: 18px;
        font-family: ${props => props.theme.FFDINWebProMedium};
    }
`;

const LocationHeading = styled.h3`
    font-size: 36px;
    margin: 0 0 40px;
`;

const LocationDetails = styled.div`
    display: flex;
    position: relative;
    min-height: 420px;
    flex-direction: column;
    padding-left: ${props => (props.type === "left" ? "15px" : "0px")};

    ${media.mobile`
      flex-direction: row;
      padding-left: 0px;
    `} > div {
        &:first-child {
            order: 0;

            ${media.mobile`
              order: ${props => focus(props.type)[0]};
            `} ${media.mobile`
              padding: ${props => containerPadding(props.type)};
            `};
        }

        &:last-child {
            order: 1;

            ${media.mobile`
              order: ${props => focus(props.type)[1]};
            `};
        }
    }
`;

const LocationText = styled.div`
    min-width: 100%;
    max-width: 100%;

    ${media.mobile`
      min-width: 40%;
      max-width: 40%;
    `} > div {
        display: flex;
        justify-content: flex-start;
        flex-direction: row;
        margin-bottom: 40px;

        ${media.mobile`
          flex-direction: column;
          margin-bottom: 0px;
        `} ${media.desktop`
          flex-direction: row;
        `} > div:first-child {
            margin-right: 5vw;
        }
    }
`;

const LocationMap = styled.div`
    position: relative;
    min-width: calc(
        100% + ${props => (props.type === "left" ? "30px" : "15px")}
    );
    max-width: calc(100% + 15px);
    height: 70vw;
    margin: 0 0 0 -15px;

    ${media.mobile`
      min-width: 60%;
      max-width: 60%;
      margin: 0;
      height: auto;
    `};
`;

const ContactDetails = styled.div`
    margin-bottom: 15px;

    ${media.desktop`
      margin-bottom: 30px;
    `} h4 {
        color: ${props => props.theme.primary};
        font-size: 21px;
        margin: 0 0 15px;
    }

    p {
        font-size: 14px;
        line-height: 18px;
        font-family: ${props => props.theme.FFDINWebProMedium};
        margin: 0;
    }
`;

const OfficeLocation = props => {
    const { content, side } = props;

    const venue = `${PrismicReact.RichText.asText(
        content.primary.address_line_1
    )}
    ${PrismicReact.RichText.asText(content.primary.address_line_2)}
    ${PrismicReact.RichText.asText(content.primary.city)}
    ${PrismicReact.RichText.asText(content.primary.country)}
    ${PrismicReact.RichText.asText(content.primary.postcode)}`;

    return (
        <LocationWrapper>
            <LocationDetails type={side}>
                <LocationText>
                    <LocationHeading>
                        {PrismicReact.RichText.asText(
                            content.primary.office_city
                        )}
                    </LocationHeading>
                    <div>
                        <div>
                            <LocationList>
                                {PrismicReact.RichText.render(
                                    content.primary.address_line_1,
                                    PrismicConfig.linkResolver
                                )}
                                {PrismicReact.RichText.render(
                                    content.primary.address_line_2,
                                    PrismicConfig.linkResolver
                                )}
                                {PrismicReact.RichText.render(
                                    content.primary.city,
                                    PrismicConfig.linkResolver
                                )}
                                {PrismicReact.RichText.render(
                                    content.primary.country,
                                    PrismicConfig.linkResolver
                                )}
                                {PrismicReact.RichText.render(
                                    content.primary.postcode,
                                    PrismicConfig.linkResolver
                                )}
                            </LocationList>
                        </div>
                        <div>
                            {content.items.map(item => (
                                <ContactDetails key={uniqid()}>
                                    <h4>
                                        {PrismicReact.RichText.asText(
                                            item.contact_detail
                                        )}
                                    </h4>
                                    <p>
                                        <a
                                            href={`mailTo:${PrismicReact.RichText.asText(
                                                item.contact_email
                                            )}`}
                                        >
                                            {PrismicReact.RichText.asText(
                                                item.contact_email
                                            )}
                                        </a>
                                    </p>
                                    <p>
                                        <a
                                            href={`tel:${PrismicReact.RichText.asText(
                                                item.contact_number
                                            )}`}
                                        >
                                            {PrismicReact.RichText.asText(
                                                item.contact_number
                                            )}
                                        </a>
                                    </p>
                                </ContactDetails>
                            ))}
                            <InternalLink
                                PrismicReact={PrismicReact}
                                link={content.primary.internal_link}
                            />
                        </div>
                    </div>
                </LocationText>
                <LocationMap type={side}>
                    <Map
                        location={[
                            content.primary.location.latitude,
                            content.primary.location.longitude
                        ]}
                        venue={venue}
                    />
                </LocationMap>
            </LocationDetails>
        </LocationWrapper>
    );
};

export default OfficeLocation;
