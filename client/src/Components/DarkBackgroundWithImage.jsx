// @flow

import React, { Component } from 'react';
import Prismic from 'prismic-javascript';
import { Link } from 'react-router-dom';
import PrismicReact from 'prismic-reactjs';
import '../Vendor/carousel-styles.css';
import styled from 'styled-components';
import Loadable from 'react-loadable';
import { Grid, GridItem } from 'styled-grid-responsive';
import BackgroundImage from 'react-background-image-loader';
import Slider from 'react-slick';
import media from '../utils/style-utils';

import PrismicConfig from '../prismic-configuration';
import SlickCarouselArrow from '../Common/SlickCarouselArrow';
import GridContainer from '../Common/GridContainer';
import InternalLink from './InternalLink';

const CarouselWrapper = styled.div`
  position: relative;
  display: block;

  .BrainhubCarousel__customArrows > div {
    margin-top: 40px;
  }
`;

const Slide = styled.div`
  height: 42vw;
  max-height: 75vh;
  min-height: 350px;
  position: relative;
  z-index: 1;
  width: 100%;

  > div {
    background-position: center center;
    background-size: cover;
    height: 100vh;
    min-height: auto;
    height: 42vw;
    max-height: 75vh;
    min-height: 350px;
  }
`;

const CarouselTypeWrapper = styled.div`
  margin: 80px 0 20px;
`;

const CarouselText = styled.div`
  padding: 80px 0 20px;
  background-color: ${props => props.theme.darkGrey3};
  margin-top: -80px;

  ${media.tablet`
    padding: 100px 0 80px;
  `};

  h3 {
    font-size: 28px;
    line-height: 28px;
    color: white;

    ${media.tablet`
        font-size: 45px;
        line-height: 45px;
      `};
  }

  p {
    color: white;
  }
`;

type Props = {
  content: {
    items: Array<Object>,
    primary: {
      section_title: Array<Object>,
      section_text: Array<Object>,
      internal_link: Array<Object>,
      internal_link_2: Array<Object>
    }
  },
  prismic: () => {}
};

type State = {
  domLoaded: boolean
};

class DarkBackgroundWithImage extends Component<Props, State> {
  keyCount: number;

  animation: () => {};

  constructor(props: Props) {
    super();

    this.state = {
      domLoaded: false
    };

    (this: any).settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SlickCarouselArrow type='next' />,
      prevArrow: <SlickCarouselArrow type='prev' />
    };

    (this: any).keyCount = 0;
    (this: any).getKey = this.getKey.bind(this);
  }

  getKey() {
    return this.keyCount++;
  }

  render() {
    const { domLoaded } = this.state;
    const { content, prismic } = this.props;
    let slides = [];
    let carousel = null;

    slides = content.items.map(img => {
      if ('dimensions' in img.image) {
        return (
          <Slide key={this.getKey()}>
            <BackgroundImage
              key={this.getKey()}
              placeholder='./images/grey-placeholder.png'
              className='slide'
              src={img.image.url}
            />
          </Slide>
        );
      }
      return null;
    });

    if (slides.length > 1) {
      carousel = (
        <Slider key={this.getKey()} {...(this: any).settings}>
          {slides}
        </Slider>
      );
    } else {
      carousel = slides;
    }

    return (
      <CarouselTypeWrapper>
        <GridContainer>
          <CarouselWrapper>{carousel}</CarouselWrapper>
        </GridContainer>
        <CarouselText>
          <GridContainer>
            <Grid>
              <GridItem
                media={{
                  smallPhone: 0,
                  mediumPhone: 1 / 12
                }}
                col={1 / 6}
              />
              <GridItem
                media={{
                  smallPhone: 1,
                  mediumPhone: 10 / 12
                }}
                col={2 / 3}
              >
                <div>
                  {PrismicReact.RichText.render(
                    content.primary.section_title,
                    PrismicConfig.linkResolver
                  )}
                  {PrismicReact.RichText.render(
                    content.primary.section_text,
                    PrismicConfig.linkResolver
                  )}
                  <InternalLink
                    PrismicReact={PrismicReact}
                    link={content.primary.internal_link}
                  />
                  <InternalLink
                    PrismicReact={PrismicReact}
                    link={content.primary.internal_link_2}
                  />
                </div>
              </GridItem>
            </Grid>
          </GridContainer>
        </CarouselText>
      </CarouselTypeWrapper>
    );
  }
}

export default DarkBackgroundWithImage;
