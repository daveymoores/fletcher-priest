import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import PrismicReact from 'prismic-reactjs';
import GridContainer from '../Common/GridContainer';
import media from '../utils/style-utils';
import InternalLink from './InternalLink';
import PrismicConfig from '../prismic-configuration';

const Banner = styled.section`
  padding: 20px 0;
  background-color: ${props => props.theme.darkGrey3};
  margin-bottom: 50px;

  ${media.mobile`
      padding: 30px 0;
    `};

  ${media.desktop`
      padding: 50px 0;
    `};

  a {
    margin: 55px 0 0px;

    ${media.mobile`
          margin: 75px 0 0px;
        `};
  }
`;

const CalloutText = styled.h3`
  font-size: 28px;
  color: white;
  margin: 0px;
  width: 20vw;

  ${media.mobile`
      font-size: 10vw;
      line-height: 9.5vw;
    `};

  ${media.desktop`
      font-size: 100px;
      line-height: 95px;
    `};
`;

const GreyBanner = ({ content }) => (
  <Banner>
    <GridContainer>
      <CalloutText>
        {PrismicReact.RichText.asText(
          content.callout_text,
          PrismicConfig.linkResolver
        )}
      </CalloutText>

      <div>
        <InternalLink
          link={content.internal_link}
          PrismicReact={PrismicReact}
        />
      </div>
    </GridContainer>
  </Banner>
);

export default GreyBanner;
