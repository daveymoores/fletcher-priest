// @flow

import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import PrismicReact from "prismic-reactjs";
import styled, { css } from "styled-components";
import BackgroundImage from "react-background-image-loader";
import Loadable from "react-loadable";
import Slider from "react-slick";
import media from "../utils/style-utils";

import PrismicConfig from "../prismic-configuration";
import SlickCarouselArrow from "../Common/SlickCarouselArrow";
import GridContainer from "../Common/GridContainer";
import InternalLink from "./InternalLink";

const CarouselWrapper = styled.div`
  position: relative;
  display: block;
  overflow: hidden;
`;

const Slide = styled.div`
  height: 500px;
  width: 100%;

  ${media.tablet`
  height: 650px;
    `};

  > div {
    background-position: center center;
    background-size: cover;
    height: 100vh;
    min-height: auto;
    max-height: 105%;
  }
`;

const CarouselTypeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: stretch;
  positon: relative;
  margin: 40px 0 40px;

  ${media.tablet`
      flex-direction: row;
      margin: 60px 0 60px;
    `};

  > div {
    &:first-child {
      order: 0;
      width: 100%;
      min-width: 100%;
      height: 100%;

      ${media.tablet`
              width: 50%;
              min-width: 50%;
            `};
    }

    &:last-child {
      order: 1;
      display: flex;
      align-items: center;
    }
  }

  ${props =>
    props.type === "Right" &&
    css`
      > div {
        &:first-child {
          order: 0;

          ${media.tablet`
                      order: 1;
                    `};
        }

        &:last-child {
          order: 1;

          ${media.tablet`
                      order: 0;
                    `};
        }
      }
    `};
`;

const CarouselText = styled.div`
  background-color: ${props => props.theme.secondary};
  width: 100%;
  padding: 20px;
  max-height: 100%;
  min-height: 100%;
  position: relative;
  display: flex;

  ${media.mobile`
      padding: 40px;
    `};

  ${media.tablet`
      width: 50%;
      padding: 60px;
    `};

  > div {
    max-width: 500px;
    margin: auto;
  }

  h3 {
    font-size: 28px;
    line-height: 28px;
    color: white;
    margin-top: 0;

    ${media.tablet`
          font-size: 45px;
          line-height: 45px;
        `};
  }

  p {
    color: white;
  }
`;

type Props = {
  content: {
    items: Array<Object>,
    primary: {
      image_alignment: string,
      section_title: Array<Object>,
      section_text: Array<Object>,
      internal_link: Array<Object>
    }
  },
  prismic: () => {}
};

type State = {
  domLoaded: boolean
};

class DarkBackgroundImageLeftright extends Component<Props, State> {
  keyCount: number;

  constructor(props: Props) {
    super();

    this.state = {
      domLoaded: false
    };

    (this: any).settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SlickCarouselArrow type="next" />,
      prevArrow: <SlickCarouselArrow type="prev" />
    };

    (this: any).keyCount = 0;
    (this: any).getKey = this.getKey.bind(this);
  }

  getKey() {
    return this.keyCount++;
  }

  render() {
    const { domLoaded } = this.state;
    const { content, prismic } = this.props;
    let slides = [];
    let carousel = null;

    slides = content.items.map(img => {
      if ("dimensions" in img.image) {
        return (
          <Slide key={this.getKey()}>
            <BackgroundImage
              key={this.getKey()}
              placeholder="./images/grey-placeholder.png"
              className="slide"
              src={img.image.url}
            />
          </Slide>
        );
      }

      return null;
    });

    if (slides.length > 1) {
      carousel = <Slider {...(this: any).settings}>{slides}</Slider>;
    } else {
      carousel = slides;
    }

    return (
      <CarouselTypeWrapper type={content.primary.image_alignment}>
        <CarouselWrapper>{carousel}</CarouselWrapper>
        <CarouselText>
          <div>
            <div>
              {PrismicReact.RichText.render(
                content.primary.section_title,
                PrismicConfig.linkResolver
              )}
              {PrismicReact.RichText.render(
                content.primary.section_text,
                PrismicConfig.linkResolver
              )}
              <InternalLink
                PrismicReact={PrismicReact}
                link={content.primary.internal_link}
              />
            </div>
          </div>
        </CarouselText>
      </CarouselTypeWrapper>
    );
  }
}

export default DarkBackgroundImageLeftright;
