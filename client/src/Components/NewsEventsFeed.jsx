import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import { Grid } from "styled-grid-responsive";
import Loadable from "react-loadable";
import debounce from "lodash/debounce";

import EventTeaser from "./EventTeaser";
import NewsTeaser from "./NewsTeaser";
import GridContainer from "../Common/GridContainer";
import InsetHeading from "./InsetHeading";
import LoaderGridItem from "../Loaders/LoaderGridItem";

const LoadableTextOnlyPageTitle = Loadable({
    loader: () => import("../Components/TextOnlyPageTitle"),
    loading: () => null
});

const LoadableEventTeaser = Loadable({
    loader: () => import("../Components/EventTeaser"),
    loading: () => <LoaderGridItem />
});

const LoadableNewsTeaser = Loadable({
    loader: () => import("../Components/NewsTeaser"),
    loading: () => <LoaderGridItem />
});

const SectionHeading = styled.h3`
    font-size: 35px;
    margin: 0 0 60px;
`;

const NewsEventsFeedWrapper = styled.div`
    border-top: 1px solid ${props => props.theme.darkGrey};
    padding-top: 0px;
    margin-top: 60px;
`;

class NewsEventsFeed extends Component {
    constructor() {
        super();

        this.state = {
            results: null
        };
        this.handleResize = debounce(this.handleResize, 100);
    }

    componentWillMount() {
        this.fetchPage(this.props);
        this.handleResize();
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleResize, false);
    }

    componentWillReceiveProps(props) {
        this.fetchPage(props);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize, false);
    }

    handleResize = () => {
        this.setState(() => ({
            viewport: { x: window.innerWidth, y: window.innerHeight }
        }));
    };

    fetchPage(props) {
        const { exclude } = props;
        if (props.prismicCtx) {
            return props.prismicCtx.api
                .query(
                    [
                        Prismic.Predicates.any("document.type", [
                            "single_news_item",
                            "single_events_page",
                            "news__events_landing"
                        ]),
                        Prismic.Predicates.not("document.id", exclude)
                    ],
                    {
                        fetchLinks: [
                            "single_events_page.start_time_date",
                            "single_events_page.venue_name"
                        ],
                        orderings:
                            "[my.single_news_item.publication_date desc]",
                        pageSize: 12
                    }
                )
                .then(response => {
                    const { results } = response;
                    this.setState({ results });
                });
        }
        return null;
    }

    updateState(data) {
        this.setState(prevState => {
            if (prevState.results !== data[0]) {
                return { results: data[0], textSearch: data[1] };
            }
            return null;
        });
    }

    render() {
        const { results, viewport } = this.state;
        const { prismicCtx, headingText } = this.props;

        if (results) {
            const text = headingText || `More News & Events`;

            return (
                <NewsEventsFeedWrapper>
                    <GridContainer>
                        <Grid>
                            <InsetHeading text={text} />
                            {results
                                .filter(item => {
                                    const values = Object.values(item);
                                    return (
                                        values.indexOf("single_news_item") >=
                                            0 ||
                                        values.indexOf("single_events_page") >=
                                            0
                                    );
                                })
                                .map((item, index) => {
                                    if (item.type === `single_events_page`) {
                                        return (
                                            <LoadableEventTeaser
                                                key={item.id}
                                                event={item}
                                                windowSize={viewport}
                                                prismicCtx={prismicCtx}
                                                PrismicReact={PrismicReact}
                                            />
                                        );
                                    }
                                    return (
                                        <LoadableNewsTeaser
                                            key={item.id}
                                            news={item}
                                            windowSize={viewport}
                                            prismicCtx={prismicCtx}
                                            PrismicReact={PrismicReact}
                                        />
                                    );
                                })}
                        </Grid>
                    </GridContainer>
                </NewsEventsFeedWrapper>
            );
        }
        return <h1>Loading</h1>;
    }
}

export default NewsEventsFeed;
