// @flow

import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Forward = styled(Link)`
    font-family: ${props => props.theme.FFDINWebProBlack};
    font-size: ${props => (props.type === "small" ? "16px" : "20px")};
    margin-top: 40px;
    color: ${props => props.theme.primary};
    display: block;
`;

type Props = {
    slug: string,
    destination: string,
    size: string
};

const ForwardButton = (props: Props) => {
    const { slug, destination, size } = props;
    return (
        <Forward type={size} to={slug}>
            {destination} &rarr;
        </Forward>
    );
};

export default ForwardButton;
