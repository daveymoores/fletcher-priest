// @flow

import React, { Component } from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import MetaTags from "react-meta-tags";
import { Grid, GridItem } from "styled-grid-responsive";

import {
    MobileSwapGrid,
    ShowForMobile,
    HideForMobile
} from "../SharedStyles/utils-styles";
import media from "../utils/style-utils";

const FileUpload = styled.div`
    background-color: ${props => props.theme.darkGrey3};
    color: white;
    border: none;
    padding: 19px 20px 21px;
    font-family: ${props => props.theme.FFDINWebProMedium};
    margin: 20px 0 0;
    font-size: 18px;
    cursor: pointer;
    display: table;
    justify-content: flex-start;
    text-align: left;
    transition: background-color 0.1s ease;

    .box__dragndrop,
    .box__uploading,
    .box__success,
    .box__error {
        display: none;
    }

    .box__dragndrop {
        display: none;

        ${media.tablet`
            display: ${props => (props.type ? "inline" : "none")};
        `};
    }

    &.is-uploading .box__input {
        visibility: none;
    }

    &.is-uploading .box__uploading {
        display: block;
    }

    &.is-dragover {
        background-color: ${props => props.theme.darkGrey4};
    }

    &.file-selected {
        background-color: ${props => props.theme.darkGrey4};

        .mob-filename {
            display: block;

            ${media.tablet`
              display: none;
            `};
        }
    }

    ${media.tablet`
      padding: 22px 20px 25px;
      margin: 20px 0;
      display: flex;
      width: 100%;
      color: ${props => props.theme.tertiary};
      background-color: ${props => props.theme.lightGrey};
      justify-content: center;
      align-items: center;
      text-align: center;
      height: 200px;
    `} span {
        font-size: 18px;
        text-align: center;
        color: ${props => props.theme.tertiary};
    }

    input[type="file"] {
        display: none;
    }
`;

const Label = styled.label`
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 18px;
    color: black;
    margin: 10px 0 30px;
    display: block;
    position: relative;

    ${media.tablet`
      margin: 30px 0;
    `};
`;

const FileNameForMobile = styled.span`
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 18px;
    color: black;
    margin: 10px 0 30px;
    display: none;
    top: 50%;
    position: absolute;
    left: 135px;
    text-align: left !important;
    transform: translateY(-50%);
    padding-top: 15px;
`;

type State = {
    dragging: boolean,
    droppedFiles: Array<any>
};

type Props = {
    results: Array<Object>,
    labelText: string,
    name: string,
    onFileUpload: (a: ?Array<Object>, b: ?string, c: ?string) => {}
};

declare type SyntheticInputEventElement<E> = {
    currentTarget: { parentNode: { parentNode: E } },
    target: E
} & SyntheticInputEvent<EventTarget>;

declare type DomElement<E> = {
    target: E
} & HTMLElement;

declare type CallbackEvent<E> = {
    preventDefault: () => {},
    stopPropagation: () => {},
    currentTarget: E,
    target: { files: Array<Object> },
    dataTransfer: any
};

class FormDragInput extends Component<Props, State> {
    formRef: ?HTMLElement;

    static isAdvancedUpload() {
        const div = document.createElement("div");
        const advanced =
            ("draggable" in div || ("ondragstart" in div && "ondrop" in div)) &&
            "FormData" in window &&
            "FileReader" in window
                ? 1
                : 0;

        return advanced;
    }

    static addListenerMulti(
        el: Element,
        s: string,
        fn: (x: CallbackEvent<any>) => void
    ) {
        // $FlowFixMe
        s.split(" ").forEach(e => el.addEventListener(e, fn, false));
    }

    constructor(props: Props) {
        super();

        this.state = {
            dragging: false,
            droppedFiles: []
        };

        this.showFiles = this.showFiles.bind(this);
    }

    componentDidMount() {
        const { results } = this.props;
        const { dragging } = this.state;
        let boxes: ?HTMLCollection<HTMLElement>;

        if (results) {
            if (this.formRef) {
                boxes = this.formRef.getElementsByClassName("box__input");
            }

            if (boxes !== null) {
                [].forEach.call(boxes, e => {
                    this.attachDragListeners(e);
                });
            }
        }
    }

    showFiles = (
        element: SyntheticInputEventElement<HTMLInputElement> | any
    ) => {
        const domElement =
            element instanceof Element
                ? element
                : element.currentTarget.parentNode.parentNode;

        const target =
            domElement.target instanceof Element ? domElement.target : null;
        const { droppedFiles } = this.state;
        const textCont: ?HTMLElement = domElement.querySelector(
            ".box__dragndrop"
        );
        const input: ?HTMLElement = domElement.querySelector(".box__file");
        const mobFilename: ?HTMLElement = domElement.querySelector(
            ".mob-filename"
        );
        domElement.classList.add("file-selected");

        if (textCont && input && mobFilename) {
            if (droppedFiles.length > 0) {
                mobFilename.innerText = droppedFiles[0][0].name;
                textCont.innerHTML =
                    droppedFiles.length > 0
                        ? (
                              input.getAttribute("data-multiple-caption") || ""
                          ).replace(
                              "{count}",
                              `${
                                  droppedFiles.length
                              } file selected for upload <br /> <small>(${
                                  droppedFiles[0][0].name
                              })</small>`
                          )
                        : droppedFiles[0][0].name;
            } else if (target) {
                mobFilename.innerText = target.files[0].name;
                textCont.innerHTML =
                    target.files.length > 0
                        ? (
                              input.getAttribute("data-multiple-caption") || ""
                          ).replace(
                              "{count}",
                              `${
                                  target.files.length
                              } file selected for upload <br /> <small>(${
                                  target.files[0].name
                              })</small>`
                          )
                        : target.files[0].name;
            }
        }
    };

    attachDragListeners(element: Element) {
        const { droppedFiles } = this.state;
        const { onFileUpload } = this.props;

        if (FormDragInput.isAdvancedUpload() && FormDragInput) {
            FormDragInput.addListenerMulti(
                element,
                "drag dragstart dragend dragover dragenter dragleave drop",
                e => {
                    e.preventDefault();
                    e.stopPropagation();
                }
            );

            FormDragInput.addListenerMulti(element, "dragover dragenter", e => {
                element.classList.add("is-dragover");
            });

            FormDragInput.addListenerMulti(
                element,
                "dragleave dragend drop",
                e => {
                    element.classList.remove("is-dragover");
                }
            );

            FormDragInput.addListenerMulti(element, "drop", e => {
                this.setState(
                    {
                        droppedFiles: [...droppedFiles, e.dataTransfer.files]
                    },
                    () => {
                        const { droppedFiles: files } = this.state;
                        onFileUpload(
                            files,
                            e.currentTarget.getAttribute("data-name"),
                            e.dataTransfer.files[0].size
                        );
                        this.showFiles(element);
                    }
                );
            });

            FormDragInput.addListenerMulti(element, "change", e => {
                this.setState(
                    {
                        droppedFiles: [...droppedFiles, e.target.files]
                    },
                    () => {
                        const { droppedFiles: files } = this.state;
                        onFileUpload(
                            files,
                            e.currentTarget.getAttribute("data-name"),
                            e.target.files[0].size
                        );
                        this.showFiles(e);
                    }
                );
            });
        }
    }

    render() {
        const { labelText, name } = this.props;
        return (
            <Label
                innerRef={el => {
                    this.formRef = el;
                }}
            >
                {labelText}
                <FileUpload
                    className="box__input"
                    type={FormDragInput.isAdvancedUpload()}
                    data-name={name}
                >
                    <div>
                        <HideForMobile className="box__dragndrop">
                            Click here to upload
                            <br />
                            or drag and drop
                        </HideForMobile>
                        <ShowForMobile>Upload &rarr;</ShowForMobile>
                        <input
                            className="box__file"
                            type="file"
                            id="file_resume"
                            name={name}
                            data-multiple-caption="{count}"
                            placeholder="Attachments (optional)"
                            onChange={this.showFiles}
                        />
                    </div>
                    <div className="box__uploading">Uploading&hellip;</div>
                    <div className="box__success">Done!</div>
                    <div className="box__error">
                        Error! <span />.
                    </div>
                    <FileNameForMobile className="mob-filename">
                        Filename
                    </FileNameForMobile>
                </FileUpload>
            </Label>
        );
    }
}

export default FormDragInput;
