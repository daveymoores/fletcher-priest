import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled, { css } from "styled-components";
import { connect } from "react-redux";

import GridContainer from "../Common/GridContainer";
import Search from "./icons/search.svg";
import Small from "./icons/small.svg";
import Large from "./icons/large.svg";
import {
    OpenDropdown,
    OpenSearchDropdown
} from "../Animations/SearchAnimation";
import GridSlider from "./GridSlider";
import {
    SearchParent,
    SearchDropdown,
    SearchDropdownOptions,
    SearchItem,
    SearchResize,
    SearchTextInput,
    SearchContainer,
    SearchIcon,
    DownChevron,
    LoadMore
} from "./search-bar-styles";
import { setFilterState } from "../redux/actionCreators";
import SearchBarAnimation from "../Animations/SearchBarAnimation";

function setSelectState(element) {
    const parent = element.parentNode.parentNode;
    const currentSelected = parent.querySelectorAll(".selected");

    // does target element contain selected
    function checkChildrenForSelected() {
        if (element.nextSibling) {
            const selected =
                element.nextSibling.querySelector(".selected") || false;
            return selected;
        }
        return false;
    }

    if (element.classList.contains("selected")) {
        element.classList.remove("selected");
    } else if (currentSelected && !checkChildrenForSelected()) {
        [].forEach.call(currentSelected, e => {
            e.classList.remove("selected");
        });
        element.classList.add("selected");
    } else {
        element.classList.add("selected");
    }
    return true;
}

class SearchBarPublications extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: null,
            sortTerm: "[my.publication.date_of_publication]",
            textSearch: "",
            response: null,
            totalRendered: null,
            resultsSize: null,
            increment: 6,
            revealed: false
        };
    }

    componentWillMount() {
        this.buildPredicate(this.props);
    }

    componentWillReceiveProps(props) {
        this.buildPredicate(props);
    }

    componentDidUpdate(prevProps, prevState) {
        const { results, textSearch, sortTerm, revealed } = this.state;
        const { callback } = this.props;

        if (this.searchParentRef && !revealed) {
            this.setState({ revealed: true }, () => {
                this.animations = new SearchBarAnimation(this.searchParentRef);
            });
        }

        if (prevState.results !== results) {
            callback([results, textSearch, sortTerm]);
        }
    }

    handleChange(event) {
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "complete") handleFilterChange("loading");
        this.searchParentRef.style.marginBottom = "60px";
        this.setState(
            { textSearch: event.target.value, totalRendered: null },
            () => {
                this.buildPredicate();
            }
        );
    }

    toggleDropdown(e) {
        const target = e.currentTarget;
        setSelectState(target);
        const dropdown = target.parentNode.querySelector("ul");
        const dropdownItems = dropdown.querySelectorAll("li");

        OpenDropdown(dropdownItems);
        this.searchParentRef.classList.add("dropdown-open");
        const currentActiveDropdown = this.searchParentRef.querySelector(
            ".visible"
        );

        if (currentActiveDropdown) {
            this.searchInputRef.style = "";
            currentActiveDropdown.classList.remove("visible");
        }

        dropdown.classList.add("visible");
    }

    toggleSearchDropdown(e) {
        e.preventDefault();
        const target = e.currentTarget;
        setSelectState(target);
        const input = target.querySelector("input");
        const currentActiveDropdown = this.searchParentRef.querySelector(
            ".visible"
        );
        if (currentActiveDropdown) {
            currentActiveDropdown.classList.remove("filter-open");
            currentActiveDropdown.classList.remove("visible");
        }
        target.classList.add("visible");

        if (window.innerWidth < 576) {
            OpenSearchDropdown(input);
        }
    }

    handleSortClick(e) {
        const target = e.currentTarget;
        const { sortTerm } = this.state;
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "complete") handleFilterChange("loading");
        setSelectState(target);
        let s;

        function findSortParam(value) {
            let x;
            switch (value) {
                case "ascending":
                    x = "[my.publication.date_of_publication]";
                    break;
                case "descending":
                    x = "[my.publication.date_of_publication desc]";
                    break;
                default:
                    x = "[my.publication.date_of_publication]";
            }

            return x;
        }

        if (!target.classList.contains("selected")) {
            s = "[my.publication.date_of_publication]";
        } else {
            s = target.getAttribute("data-sort-term")
                ? findSortParam(target.getAttribute("data-sort-term"))
                : sortTerm;
        }

        this.setState({ sortTerm: s, totalRendered: null }, () => {
            this.buildPredicate();
        });
    }

    loadMore(e) {
        e.preventDefault();
        const { response, totalRendered, increment, resultsSize } = this.state;
        const { filterState, handleFilterChange } = this.props;
        const newPageSize = totalRendered + increment;
        const newTotal = newPageSize < resultsSize ? newPageSize : resultsSize;
        if (filterState === "complete") handleFilterChange("loading");
        this.setState({ totalRendered: newTotal }, () => {
            this.buildPredicate();
        });
    }

    buildPredicate(props) {
        const { prismicCtx, filterState, handleFilterChange } =
            props || this.props;
        const { sortTerm, textSearch, totalRendered } = this.state;
        const paginationTotal = totalRendered || 12;

        if (prismicCtx) {
            return prismicCtx.api
                .query(
                    [
                        Prismic.Predicates.at("document.type", "publication"),
                        Prismic.Predicates.fulltext("document", textSearch)
                    ],
                    {
                        orderings: sortTerm,
                        pageSize: paginationTotal
                    }
                )
                .then(response => {
                    const { results } = response;

                    this.setState(
                        {
                            results,
                            response,
                            totalRendered: paginationTotal,
                            resultsSize: response.total_results_size
                        },
                        () => {
                            if (filterState === "loading")
                                handleFilterChange("complete");
                        }
                    );
                });
        }
        return null;
    }

    render() {
        const { results, value, totalRendered, resultsSize } = this.state;
        const { children } = this.props;
        const ctx = this;

        function renderLoadMore() {
            if (totalRendered < resultsSize) {
                return (
                    <LoadMore href="#" onClick={ctx.loadMore.bind(ctx)}>
                        Load More
                    </LoadMore>
                );
            }

            return null;
        }

        const LoadMoreButton = renderLoadMore();

        return (
            <div>
                <GridContainer type="pullRight">
                    <SearchParent
                        innerRef={e => {
                            this.searchParentRef = e;
                        }}
                    >
                        <SearchDropdown>
                            <SearchItem
                                onClick={this.toggleDropdown.bind(this)}
                            >
                                <span>Date</span>
                                <DownChevron />
                            </SearchItem>
                            <SearchDropdownOptions data-search-predicate="sort">
                                <li>
                                    <SearchItem
                                        data-sort-term="ascending"
                                        onClick={this.handleSortClick.bind(
                                            this
                                        )}
                                    >
                                        <span>Ascending</span>
                                    </SearchItem>
                                </li>
                                <li>
                                    <SearchItem
                                        data-sort-term="descending"
                                        onClick={this.handleSortClick.bind(
                                            this
                                        )}
                                    >
                                        <span>Descending</span>
                                    </SearchItem>
                                </li>
                            </SearchDropdownOptions>
                        </SearchDropdown>
                        <SearchContainer
                            onClick={this.toggleSearchDropdown.bind(this)}
                        >
                            <SearchTextInput
                                value={value}
                                innerRef={el => (this.searchInputRef = el)}
                                onChange={this.handleChange.bind(this)}
                                onFocus={this.handleChange.bind(this)}
                                placeholder="Search"
                                type="search"
                            />
                            <SearchIcon />
                        </SearchContainer>
                    </SearchParent>
                </GridContainer>

                <GridContainer type="pullRight">
                    {children}
                    {LoadMoreButton}
                </GridContainer>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    filterState: state.filterState
});

const mapDispatchToProps = dispatch => ({
    handleFilterChange(value) {
        dispatch(setFilterState(value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchBarPublications);
