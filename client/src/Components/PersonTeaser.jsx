import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled, { withTheme } from 'styled-components';
import LazyLoad from 'react-lazyload';
import LoaderPerson from '../Loaders/LoaderPerson';

import AwardArrow from '../Common/icons/award-arrow';

const AwardRow = styled.div`
  display: table;
  width: 100%;
  font-size: 12px;
  font-family: ${props => props.theme.FFDINWebProMedium};
  border: 1px solid ${props => props.theme.tertiary};
  margin-bottom: 5px;
  position: relative;
  background-color: white;

  > div {
    display: table-cell;
    text-align: left;
    padding-left: 10px;
    width: 25%;
  }
`;

const ProjectArrow = styled(Link)`
  position: absolute;
  right: 15px;
  top: 50%;
  transform: translateY(-50%);
  width: 18px;
  height: 18px;

  > svg {
    width: 100%;
    height: 100%;
  }
`;

const PersonTeaserCont = styled.div`
  padding: ${props => (props.medium ? '0px' : '20px')};
  height: ${props => (props.medium ? '75px' : '450px')};
  display: flex;
  justify-content: ${props =>
    props.medium ? 'justify-content' : 'space-between'};
  flex-direction: ${props => (props.medium ? 'row' : 'column')};
  border: ${props =>
    props.medium
      ? `${`1px solid ${
          props.searchResult ? props.theme.darkGrey7 : props.theme.tertiary
        }`}`
      : 'none'};
  margin-bottom: ${props => (props.medium ? '20px' : 0)};

  img {
    width: 100%;
  }

  h3,
  h4,
  h5 {
    margin: 0px;
    color: ${props => (props.searchResult ? 'white' : 'black')};
  }
`;

const Thumbnail = styled(Link)`
  height: 100%;
  width: 120px;
  min-width: 120px;
  max-width: 120px;
  display: block;
  margin-right: 10px;

  > div {
    width: 100%;
    height: 100%;
    background-size: cover;
    background-position: center 20%;
  }
`;

const ThumbnailText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 0;
`;

const Name = styled.h3`
  font-size: 12px !important;

  > a {
    ${props => (props.searchResult ? `color: inherit;` : ``)}
  }

  &::after {
    display: none;
  }
`;

const Title = styled.h4`
  font-size: 8px !important;
  margin: 0px !important;
`;

function chooseImg(person) {
  let thumbnail = null;

  if ('thumbnail' in person.data.headshot) {
    thumbnail =
      person.data.headshot.thumbnail.url || `./images/placeholder-person.png`;
  } else {
    thumbnail = person.data.headshot.url || `./images/placeholder-person.png`;
  }

  return thumbnail;
}

const PersonTeaser = ({ person, searchResult, location, gridSize, theme }) => {
  const loc = location
    ? !location.pathname
      ? '/people'
      : location.pathname
    : '/people';

  if (gridSize === 100) {
    return (
      <PersonTeaserCont searchResult={searchResult}>
        <div>
          <h3>
            {Object.prototype.hasOwnProperty.call(person.data, 'first_name') &&
              person.data.first_name}{' '}
            {Object.prototype.hasOwnProperty.call(person.data, 'middle_name') &&
              person.data.middle_name}{' '}
            {Object.prototype.hasOwnProperty.call(person.data, 'last_name') &&
              person.data.last_name}
          </h3>
          <h4>{person.data.job_title}</h4>
        </div>
        <Link to={`${loc}/${person.uid}`}>
          <LazyLoad
            height={300}
            throttle={200}
            placeholder={<LoaderPerson />}
            offset={400}
          >
            <img alt="cover" src={chooseImg(person)} />
          </LazyLoad>
        </Link>
      </PersonTeaserCont>
    );
  }

  if (gridSize === 50) {
    return (
      <PersonTeaserCont medium searchResult={searchResult}>
        <Thumbnail to={`${loc}/${person.uid}`}>
          <LazyLoad offset={100}>
            <div
              style={{
                backgroundImage: `url(${person.data.headshot.url})`
              }}
            />
          </LazyLoad>
        </Thumbnail>
        <ThumbnailText>
          <Name searchResult={searchResult}>
            <Link to={`${loc}/${person.uid}`}>
              {Object.prototype.hasOwnProperty.call(
                person.data,
                'first_name'
              ) && person.data.first_name}{' '}
              {Object.prototype.hasOwnProperty.call(
                person.data,
                'middle_name'
              ) && person.data.middle_name}{' '}
              {Object.prototype.hasOwnProperty.call(person.data, 'last_name') &&
                person.data.last_name}
            </Link>
          </Name>
          <Title>{person.data.job_title}</Title>
        </ThumbnailText>
      </PersonTeaserCont>
    );
  }

  if (gridSize === 0) {
    return (
      <AwardRow>
        <div>
          <Name>
            <Link to={`${loc}/${person.uid}`}>
              {Object.prototype.hasOwnProperty.call(
                person.data,
                'first_name'
              ) && person.data.first_name}{' '}
              {Object.prototype.hasOwnProperty.call(
                person.data,
                'middle_name'
              ) && person.data.middle_name}{' '}
              {Object.prototype.hasOwnProperty.call(person.data, 'last_name') &&
                person.data.last_name}
            </Link>
          </Name>
        </div>
        <div>
          <Title>{person.data.job_title}</Title>
        </div>
        <ProjectArrow to={`${loc}/${person.uid}`}>
          <AwardArrow color={theme.primary} />
        </ProjectArrow>
      </AwardRow>
    );
  }

  return null;
};

export default withTheme(PersonTeaser);
