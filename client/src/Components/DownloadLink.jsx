// @flow

import React from "react";
import styled from "styled-components";
import { Link } from "prismic-reactjs";
import download from "downloadjs";
import PrismicConfig from "../prismic-configuration";

const LinkWrapper = styled.a`
    font-family: ${props => props.theme.FFDINWebProMedium};
    font-size: 20px;
    margin-top: 5px;
    color: ${props => props.theme.primary};
    display: inline-block;
    padding-right: 10px;
    margin-bottom: 20px;
`;

type Props = {
    url: {
        url: string,
        name: string
    }
};

const DownloadButton = (props: Props) => {
    const { url } = props;

    function handleClickEvent(e) {
        e.preventDefault();
        const link = e.target.getAttribute("href");
        const name = e.target.getAttribute("data-name");
        const x = new XMLHttpRequest();
        x.open("GET", link, true);
        x.responseType = "blob";
        x.onload = (event: Object) => {
            download(event.target.response, name);
        };
        x.send();
    }

    if (url.url) {
        return (
            <LinkWrapper
                href={Link.url(url, PrismicConfig.linkResolver)}
                onClick={handleClickEvent}
                data-name={url.name}
                download
            >
                &darr; Download the press release
            </LinkWrapper>
        );
    }

    return null;
};

export default DownloadButton;
