import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { ThemeProvider, injectGlobal } from 'styled-components';
import styledNormalize from 'styled-normalize';

import PrismicApp from './PrismicApp';

injectGlobal([
  `
  ${styledNormalize}
  body {
    margin: 0;
    font-family: 'Roboto', sans-serif;
    font-weight: normal;
    font-style: normal;
    height: 100%;
    min-height: 100vh;
  }

  iframe[allowfullscreen] {
    width: 100%;
  }

  #hardCopyForm {
    .validation-message {
      margin-top: -5px;
      margin-bottom: 10px;
    }
  }

  .validation-message {
    font-family: FFDINWebProMedium;
    color: red;
    margin-top: -15px;
    margin-bottom: 20px;
  }

  .wio-link {
    position: fixed;
    bottom: 0;
    right: 0;
    z-index: 999;
    background-color: #ADFFC1;
    height: 30px;
    width: 130px;
    padding: 8px;
    -webkit-border-top-left-radius: 3px;
    -moz-border-radius-topleft: 3px;
    border-top-left-radius: 3px;

    &::after {
      content: "Edit page";
      font-family: FFDINWebProBold;
      position: absolute;
      top: 7px;
      left: 33px;
    }

    img {
      display: block;
    }
  }

  #app {
    overflow-x: hidden;
    display: flex;
    flex-direction: column;
    height: 100%;
    min-height: 100vh;
  }

  .push-footer {
    flex: 1 0 auto;
  }

  h1 {
    font-family: FFDINWebProBold;
    font-weight: normal;
    font-style: normal;
    letter-spacing:-0.05em;
  }

  h2, h3, h4, h5 {
    font-family: FFDINWebProMedium;
    font-weight: normal;
    font-style: normal;
  }

  a {
    text-decoration: none;
    font-weight: normal;
    font-style: normal;
    color: #2A2F38;
  }

  ul {
    padding: 0 15px 0;

    li {
      font-family: 'Roboto', sans-serif;
      font-weight: normal;
      font-style: normal;
      line-height: 25px;
      font-size: 15px;
      color: #868989;
    }
  }

  ol {
    padding: 0 15px 0;

    li {
      font-family: 'Roboto', sans-serif;
      font-weight: normal;
      font-style: normal;
      line-height: 25px;
      font-size: 15px;
      color: #868989;
    }
  }

  p {
    font-family: 'Roboto', sans-serif;
    font-weight: normal;
    font-style: normal;
    line-height: 20px;
    font-size: 16px;
    color: #868989;

    @media all and (min-width: 576px) {
      line-height: 25px;
      font-size: 15px;
    }

    b, strong {
      font-weight: bold;
      color: black;
    }

    i, em {
      font-style: italic;
    }
  }

  input {
    -moz-appearance: none;
    -webkit-appearance: none;
    border-radius: 0px;
  }

  *, *:before, *:after {
   box-sizing: border-box;
 }

 @keyframes pulse {
   0% {
     transform: scale(0.6);
     opacity: 0;
   }
   33% {
     transform: scale(1);
     opacity: 1;
   }
   100% {
     transform: scale(1.4);
     opacity: 0;
   }
 }
`
]);

const breakpoints = {
  smallPhone: {
    max: 576
  },
  mediumPhone: {
    min: 576,
    max: 768
  },
  tablet: {
    min: 768,
    max: 992
  },
  desktop: {
    min: 992,
    max: 1200
  },
  reallyLarge: {
    min: 1200
  }
};

const theme = {
  primary: '#FDD927',
  secondary: '#2A2F38',
  tertiary: '#DEDEDE',
  darkGrey: '#868989',
  darkGrey2: '#979797',
  darkGrey3: '#2A2F38',
  darkGrey4: '#585D67',
  darkGrey5: '#20242C',
  darkGrey6: '#D8D8D8',
  darkGrey7: '#42464D',
  darkGrey8: '#181C22',
  darkGrey9: '#8A8C8C',
  lightGrey: '#F4F4F4',
  FFDINWebProMedium: 'FFDINWebProMedium, sans-serif',
  FFDINWebProBold: 'FFDINWebProBold, sans-serif',
  FFDINWebProBlack: 'FFDINWebProBlack, sans-serif',
  UniversLTPro55Roman: "'Roboto', sans-serif",
  breakpoints
};

const renderApp = () => {
  const app = document.getElementById('app');
  const newPrimary = app.getAttribute('data-color');
  theme.primary = newPrimary || theme.primary;
  render(
    <AppContainer>
      <ThemeProvider theme={theme}>
        <PrismicApp />
      </ThemeProvider>
    </AppContainer>,
    app
  );
};

renderApp();

if (module.hot) {
  module.hot.accept();
}
