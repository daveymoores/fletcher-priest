// @Flow

// these sizes are arbitrary and you can set them to whatever you wish
import { css, styled } from 'styled-components';

const sizes = {
  mobile: 576,
  tablet: 769,
  tabletLrg: 850,
  desktop: 992,
  giant: 1170
};

// iterate through the sizes and create a media template
const media = Object.keys(sizes).reduce((accumulator, label) => {
  // use em in breakpoints to work properly cross-browser and support users
  // changing their browsers font-size: https://zellwk.com/blog/media-query-units/
  const emSize = sizes[label] / 16;
  accumulator[label] = (...args) => css`
    @media (min-width: ${emSize}em) {
      ${css(...args)};
    }
  `;
  return accumulator;
}, {});

export default media;
