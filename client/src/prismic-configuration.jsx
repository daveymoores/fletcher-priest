export default {
    apiEndpoint: "https://fletcher-repo.prismic.io/api/v2",

    // -- Access token if the Master is not open
    accessToken:
        "MC5XOGRNRnhJQUFDb0FoWXBQ.VRUs77-977-9K3fvv71F77-9D--_ve-_ve-_vSZ977-977-9b--_ve-_vWrvv707BQfvv73vv73vv73vv70OTQ",

    // OAuth
    // clientId: 'xxxxxx',
    // clientSecret: 'xxxxxx',

    // -- Links resolution rules
    // This function will be used to generate links to Prismic.io documents
    // As your project grows, you should update this function according to your routes
    linkResolver(doc) {
        if (doc.type === "single_project_page") return `/projects/${doc.uid}`;
        if (doc.type === "project_landing") return `/projects`;
        if (doc.type === "single_events_page")
            return `/news-and-events/${doc.uid}`;
        if (doc.type === "single_news_item")
            return `/news-and-events/${doc.uid}`;
        if (doc.type === "service") return `/services/${doc.uid}`;
        if (doc.type === "services_landing") return `/services`;
        if (doc.type === "sector") return `/sectors/${doc.uid}`;
        if (doc.type === "sectors_landing") return `/sectors`;
        if (doc.type === "person") return `/people/${doc.uid}`;
        if (doc.type === "people") return `/people`;
        if (doc.type === "job_listing") return `/careers/${doc.uid}`;
        if (doc.type === "practice") return `/practice`;
        if (doc.type === "awards") return `/practice/awards`;
        if (doc.type === "careers") return `/careers`;
        if (doc.type === "contact_page") return `/contact`;
        if (doc.type === "news__events_landing") return `/news-and-events`;
        if (doc.type === "publications_landing") return `/publications`;
        if (doc.type === "publication") return `/publications`;
        if (doc.type === "practice") return `/practice`;
        if (doc.type === "community") return `/practice/community`;
        if (doc.type === "approach") return `/practice/approach`;
        return "/";
    }
};
