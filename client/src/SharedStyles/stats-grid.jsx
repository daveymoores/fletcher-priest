import styled, { css } from 'styled-components';
import media from '../utils/style-utils';
import Spots from '../Common/textures/spots.png';

export const StatsGrid = styled.ul`
  background-image: url(${Spots});
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  list-style: none;
  padding: 0;
  margin: 0;
`;

export const StatsGridItem = styled.li`
  border: 1px solid ${props => props.theme.darkGrey};
  background-color: white;
  padding: 10px 15px;
  width: ${props => (props.fullWidth ? `calc(100% + 1px)` : `calc(50% + 1px)`)};
  font-family: ${props => props.theme.FFDINWebProMedium};
  margin-left: -1px;
  margin-top: -1px;
  color: black;
  position: relative;
  line-height: 18px;

  h1,
  h2,
  h3 {
    font-size: inherit !important;
    font-family: inherit !important;
    line-height: inherit !important;
    margin: 0;
  }

  ${props =>
    props.number
      ? `

      p {
        font-family: ${props.theme.FFDINWebProMedium};
        color: black;
        position: relative;
        display: inline-block;
        font-size: 60px;
        line-height: 34px;
        margin: 0 0 40px;
      }

      ${media.tablet`
        width: calc(25% + 1px);
        padding: 20px 25px;
        font-size: 60px;
        min-height: 100px;
      `}

      p::after {
          content: "";
          font-size: 18px;
          background-color: ${props.theme.primary};
          position: absolute;
          width: 36px;
          height: 4px;
          left: 0;
          bottom: -32px;
      }
    `
      : `
      font-size: 15px;
    `} ${media.tablet`
      width: ${props =>
        props.fullWidth ? `calc(100% + 1px)` : `calc(25% + 1px)`};
      padding: 20px 25px;
      min-height: 100px;
    `}
    span {
    font-family: ${props => props.theme.FFDINWebProBold};
    color: black;
    display: block;
    font-size: 18px;
    margin-bottom: 5px;
  }
`;
