import styled, { css } from "styled-components";

const LinkContainer = styled.div`
    padding: 20px;
    min-height: 190px;
    height: 190px;
    display: flex;
    align-items: center;
    display: -ms-flexbox;

    a {
        font-size: 27px;
        font-family: ${props => props.theme.FFDINWebProMedium};
        position: relative;

        &::before {
            content: "";
            position: absolute;
            left: 0;
            bottom: -5px;
            height: 3px;
            width: 0;
            background-color: ${props => props.theme.primary};
            display: block;
            transition: width 0.3s cubic-bezier(0.77, 0, 0.175, 1);
        }

        &:hover {
            &::before {
                width: 100%;
            }
        }
    }
`;

export default LinkContainer;
