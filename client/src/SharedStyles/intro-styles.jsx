import styled, { css } from "styled-components";
import media from "../utils/style-utils";

const Intro = styled.h2`
    font-size: 26px;

    ${media.tablet`
      font-size: 45px;
    `};
`;

export default Intro;
