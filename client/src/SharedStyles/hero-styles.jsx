import styled, { css } from "styled-components";
import media from "../utils/style-utils";

export const HeroWrapper = styled.section`
  width: 100%;
  height: 42vw;
  max-height: 75vh;
  min-height: 450px;
  margin-top: ${props => (props.outerHeader ? "40px" : "66px")};
  background: transparent url(${props => props.content}) center center no-repeat;
  background-size: cover;
  position: relative;

  ${media.tabletLrg`
      margin-top: ${props => (props.outerHeader ? "50px" : "85px")};
    `};
`;

export const Description = styled.div`
  padding: 20px 0 40px;
  position: relative;
  background-color: white;
  width: 100%;

  h2 {
    font-size: 28px;
    margin: 0px;
    font-family: ${props => props.theme.FFDINWebProMedium};

    ${media.mobile`
          font-size: 5vw;
        `};

    ${media.tablet`
          font-size: 50px;
          line-height: 55px;
        `};
  }
`;
