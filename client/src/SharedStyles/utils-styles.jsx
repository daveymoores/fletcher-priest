import styled, { css } from "styled-components";
import { Grid } from "styled-grid-responsive";
import media from "../utils/style-utils";

export const MobileSwapGrid = styled(Grid)`
    display: flex;
    flex-direction: column;

    > div {
        &:first-child {
            order: 1;
        }

        &:last-child {
            order: 0;
        }
    }

    ${media.tablet`
      display: block;
    `};
`;

export const ShowForMobile = styled.div`
    display: block;

    ${media.tablet`
      display: none;
    `};
`;

export const HideForMobile = styled.div`
    display: none;

    ${media.tablet`
      display: block;
    `};
`;
