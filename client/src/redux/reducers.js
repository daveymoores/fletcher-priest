import objectAssign from "object-assign";

import {
    SET_OFFCANVAS_STATE,
    SET_DOWNLOAD_STATE,
    SET_FILTER_STATE,
    SET_SCROLL_STATE,
    SET_SCROLL_NAV_STATE,
    SET_HARD_COPY_TITLE
} from "./actions";

const DEFAULT_STATE = {
    offCanvas: "closed",
    scrollNavState: "visible",
    downloadMessage: "download",
    filterState: "complete",
    scrollState: "noScroll",
    hardCopyTitle: ""
};

const setOffCanvasState = (state, action) =>
    objectAssign({}, state, { offCanvas: action.payload });

const setScrollNavState = (state, action) =>
    objectAssign({}, state, { scrollNavState: action.payload });

const setDownloadMessage = (state, action) =>
    objectAssign({}, state, { downloadMessage: action.payload });

const setFilterState = (state, action) =>
    objectAssign({}, state, { filterState: action.payload });

const setScrollState = (state, action) =>
    objectAssign({}, state, { scrollState: action.payload });

const setHardCopyTitle = (state, action) =>
    objectAssign({}, state, { hardCopyTitle: action.payload });

const rootReducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case SET_OFFCANVAS_STATE:
            return setOffCanvasState(state, action);
        case SET_SCROLL_NAV_STATE:
            return setScrollNavState(state, action);
        case SET_DOWNLOAD_STATE:
            return setDownloadMessage(state, action);
        case SET_FILTER_STATE:
            return setFilterState(state, action);
        case SET_SCROLL_STATE:
            return setScrollState(state, action);
        case SET_HARD_COPY_TITLE:
            return setHardCopyTitle(state, action);
        default:
            return state;
    }
};

export default rootReducer;
