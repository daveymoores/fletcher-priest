import {
    SET_OFFCANVAS_STATE,
    SET_DOWNLOAD_STATE,
    SET_FILTER_STATE,
    SET_SCROLL_STATE,
    SET_SCROLL_NAV_STATE,
    SET_HARD_COPY_TITLE
} from "./actions";

export function setOffCanvasState(state) {
    return { type: SET_OFFCANVAS_STATE, payload: state };
}

export function setScrollNavState(state) {
    return { type: SET_SCROLL_NAV_STATE, payload: state };
}

export function setFilterState(state) {
    return { type: SET_FILTER_STATE, payload: state };
}

export function setDownloadState(state) {
    return { type: SET_DOWNLOAD_STATE, payload: state };
}

export function setScrollState(state) {
    return { type: SET_SCROLL_STATE, payload: state };
}

export function setHardCopyTitle(title) {
    return { type: SET_HARD_COPY_TITLE, payload: title };
}
