import React from "react";
import ContentLoader from "react-content-loader";
import GridContainer from "../Common/GridContainer";

const LoaderNav = props => {
    const h = window.innerWidth < 768 ? "66" : "85";
    return (
        <ContentLoader
            height={h}
            width={window.innerWidth}
            speed={1}
            primaryColor="#f3f3f3"
            secondaryColor="#ecebeb"
            {...props}
        >
            <rect x="0" y="0" width="100%" height={h} />
        </ContentLoader>
    );
};

export default LoaderNav;
