import React from "react";
import ContentLoader from "react-content-loader";

const LoaderGridItem = props => (
    <ContentLoader
        height="450"
        width={window.innerWidth / 3}
        speed={1}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
        {...props}
    >
        <rect x="0" y="0" width={window.innerWidth / 3} height="450" />
    </ContentLoader>
);

export default LoaderGridItem;
