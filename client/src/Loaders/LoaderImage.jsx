import React from 'react';
import ContentLoader from 'react-content-loader';
import styled from 'styled-components';

const StyledContentLoader = styled(ContentLoader)`
  width: 100%;
`;

const LoaderImage = props => (
  <StyledContentLoader
    height='450'
    width='500'
    speed={1}
    primaryColor='#f3f3f3'
    secondaryColor='#ecebeb'
    {...props}
  >
    <rect x='0' y='0' width='500' height='450' />
  </StyledContentLoader>
);

export default LoaderImage;
