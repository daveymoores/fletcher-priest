import React from "react";
import ContentLoader from "react-content-loader";

const LoaderPageContent = props => (
    <ContentLoader
        height={window.innerHeight}
        width={window.innerWidth}
        speed={1}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
        {...props}
    >
        <rect
            x="0"
            y="0"
            width={window.innerWidth}
            height={window.innerHeight}
        />
    </ContentLoader>
);

export default LoaderPageContent;
