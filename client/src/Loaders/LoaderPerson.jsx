import React from 'react';
import ContentLoader from 'react-content-loader';
import styled from 'styled-components';

const StyledContentLoader = styled(ContentLoader)`
  position: absolute;
  max-width: calc(100% - 40px);
  width: auto;
  max-height: 65%;
  top: 50%;
  left: 50%;
  -webkit-transform: translate3d(-50%, -50%, 0);
  -ms-transform: translate3d(-50%, -50%, 0);
  transform: translate3d(-50%, -50%, 0);
  margin-top: 25px;
`;

const LoaderPerson = props => (
  <StyledContentLoader
    height='450'
    width='500'
    speed={1}
    primaryColor='#f3f3f3'
    secondaryColor='#ecebeb'
    {...props}
  >
    <rect x='0' y='0' width='500' height='450' />
  </StyledContentLoader>
);

export default LoaderPerson;
