import React from "react";
import ContentLoader from "react-content-loader";
import styled from "styled-components";
import GridContainer from "../Common/GridContainer";

const LoaderGridItemCarousel = props => (
    <ContentLoader
        height={450}
        width={window.innerWidth}
        speed={1}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
        {...props}
    >
        <rect x="0" y="0" width="100%" height={450} />
    </ContentLoader>
);

export default LoaderGridItemCarousel;
