import React from "react";
import ContentLoader from "react-content-loader";
import styled from "styled-components";
import GridContainer from "../Common/GridContainer";

const LoaderWrapper = styled.div`
    flex-shrink: 0;
`;

const LoaderFooter = props => {
    const h = window.innerWidth < 768 ? "564" : "336";
    return (
        <LoaderWrapper>
            <ContentLoader
                height={h}
                width={window.innerWidth}
                speed={1}
                primaryColor="#f3f3f3"
                secondaryColor="#ecebeb"
                {...props}
            >
                <rect x="0" y="0" width="100%" height={h} />
            </ContentLoader>
        </LoaderWrapper>
    );
};

export default LoaderFooter;
