import React from "react";
import ContentLoader from "react-content-loader";
import GridContainer from "../Common/GridContainer";

const LoaderSearchBar = props => (
    <GridContainer type="pullRight">
        <ContentLoader
            height="60"
            width={window.innerWidth}
            speed={1}
            primaryColor="#f3f3f3"
            secondaryColor="#ecebeb"
            {...props}
        >
            <rect x="0" y="0" width="100%" height="60" />
        </ContentLoader>
    </GridContainer>
);

export default LoaderSearchBar;
