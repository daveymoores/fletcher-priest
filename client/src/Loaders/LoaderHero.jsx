import React from "react";
import ContentLoader from "react-content-loader";
import GridContainer from "../Common/GridContainer";

const LoaderHero = props => {
    const h = window.innerWidth * 0.42;
    const y = h <= 350 ? 350 : h;

    return (
        <ContentLoader
            height={y}
            width={window.innerWidth}
            speed={1}
            primaryColor="#f3f3f3"
            secondaryColor="#ecebeb"
            {...props}
        >
            <rect x="0" y="0" width="100%" height={y} />
        </ContentLoader>
    );
};

export default LoaderHero;
