import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2,
  Power3,
  Power4,
  Expo
} from 'gsap/TweenMax';

class SearchBarAnimation {
  constructor(nodes) {
    this.nodes = nodes;
    this.filters = this.nodes.children;

    function setInitialState(e) {
      [].forEach.call(e, element => {
        Tween.set(element, {
          autoAlpha: 0
        });
      });
    }

    setInitialState(this.filters);

    this.show = () => {
      const tl = new Timeline({ dalay: 1 });

      tl.staggerTo(
        this.filters,
        0.8,
        {
          autoAlpha: 1,
          ease: Power3.easeInOut
        },
        0.15
      );
      return tl;
    };

    this.show();
  }
}

export default SearchBarAnimation;
