import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class GridAnimation {
    constructor(nodes) {
        this.nodes = nodes;

        const carouselTitle = this.nodes.wrapper.querySelector("h3");
        const carouselWrapper = this.nodes.wrapper.childNodes[1];
        const slides = carouselWrapper.childNodes[0].children;
        const name = carouselWrapper.querySelectorAll("h3");
        const client = carouselWrapper.querySelectorAll("h4");
        const location = carouselWrapper.querySelectorAll("h5");
        const slideDims = carouselWrapper.getBoundingClientRect().width;

        function setInitialState() {
            Tween.set(carouselTitle, { opacity: 0, y: 30 });

            function setTweenState(e) {
                Tween.set(e, { opacity: 0, y: -10 });
            }
            [].forEach.call(name, element => setTweenState(element));
            [].forEach.call(client, element => setTweenState(element));
            [].forEach.call(location, element => setTweenState(element));
            [].forEach.call(slides, element => {
                const mask = element.querySelector(".mask");
                element.childNodes[0].classList.add("animating");
                Tween.set(mask, {
                    height: "100%",
                    width: "100%",
                    position: "absolute",
                    zIndex: 2,
                    backgroundColor: "white",
                    top: 0,
                    left: 0
                });
                Tween.set(element, {
                    xPercent: 30,
                    opacity: 0
                });
            });
        }

        setInitialState();

        this.show = () => {
            const tl = new Timeline();

            tl.to(carouselTitle, 0.6, {
                opacity: 1,
                y: 0,
                ease: Power3.easeInOut
            });
            tl.staggerTo(
                slides,
                0.8,
                {
                    opacity: 1,
                    xPercent: 0,
                    ease: Power3.easeInOut,
                    onStart() {
                        const mask = this.target.querySelector(".mask");
                        Tween.to(mask, 0.8, {
                            width: 0,
                            ease: Power3.easeInOut
                        });
                    }
                },
                0.15,
                "-=0.5"
            );
            tl.staggerTo(
                name,
                0.5,
                {
                    opacity: 1,
                    y: 0,
                    ease: Power3.easeInOut,
                    onComplete() {
                        this.target.classList.add("completed");
                    }
                },
                0.15,
                "-=0.5"
            );
            tl.staggerTo(
                client,
                0.5,
                {
                    opacity: 1,
                    y: 0,
                    ease: Power3.easeInOut
                },
                0.15,
                "-=0.5"
            );
            tl.staggerTo(
                location,
                0.5,
                {
                    opacity: 1,
                    y: 0,
                    ease: Power3.easeInOut
                },
                0.15,
                "-=0.5"
            );
            return tl;
        };
    }
}

export default GridAnimation;
