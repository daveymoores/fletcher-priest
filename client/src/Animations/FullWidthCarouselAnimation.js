import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class FullWidthCarouselAnimation {
    constructor(nodes) {
        const { mask, wrapper } = nodes;

        const imgs = wrapper.querySelectorAll(".slide");

        Tween.set(mask, { backgroundColor: "#FFFFFF" });
        [].forEach.call(imgs, i => {
            Tween.set(i, { scale: 1.4, xPercent: 10 });
        });

        this.show = () => {
            const tl = new Timeline();
            const tl2 = new Timeline();

            tl.to(mask, 0.6, {
                xPercent: 100,
                backgroundColor: "#2A2F38",
                ease: Power3.easeInOut
            });

            tl2.staggerTo(
                imgs,
                0.6,
                {
                    scale: 1,
                    xPercent: 0,
                    ease: Power3.easeInOut
                },
                0.1
            );

            tl.progress(1).progress(0);
            tl2.progress(1).progress(0);
            return [tl, tl2];
        };
    }
}

export default FullWidthCarouselAnimation;
