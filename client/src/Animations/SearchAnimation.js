import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4
} from "gsap/TweenMax";

export function OpenDropdown(nodes) {
    const tl = new Timeline();
    const parent = nodes[0].parentNode.parentNode.parentNode;
    const filterParent = nodes[0].parentNode.parentNode.querySelector(
        ".selected"
    );
    const openParent = parent.querySelector(".filter-open");
    const num = nodes.length;
    let n;

    function closeOpenFilter() {
        const openChildren = openParent.querySelectorAll("li");
        [].forEach.call(openChildren, e => {
            Tween.set(e, { autoAlpha: 0, y: -8 });
        });
        openParent.classList.remove("filter-open");
    }

    function checkToClose() {
        closeOpenFilter();
        Tween.to(parent, 0.3, {
            marginBottom: `60px`,
            ease: Power3.easeInOut
        });
    }

    if (nodes[0].parentNode === openParent) {
        checkToClose();
        return null;
    }

    // find the open dropdown
    if (openParent) {
        closeOpenFilter();
    }

    const dropdownParentDims = nodes[0].parentNode.getBoundingClientRect()
        .height;

    if (window.innerWidth >= 992) {
        n = 1;
    } else if (window.innerWidth < 992 && window.innerWidth >= 576) {
        n = Math.ceil(num / 2);
    } else {
        n = num;
    }

    [].forEach.call(nodes, e => {
        Tween.set(e, { autoAlpha: 0, y: -8 });
    });

    Tween.to(parent, 0.3, {
        marginBottom: `${dropdownParentDims + 60}px`,
        ease: Power3.easeInOut
    });

    tl.staggerTo(
        nodes,
        0.4,
        {
            autoAlpha: 1,
            y: 0,
            ease: Power3.easeInOut,
            onComplete: () => {
                filterParent.nextSibling.classList.add("filter-open");
            }
        },
        0.05
    );
}

export function OpenSearchDropdown(node) {
    const parent = node.parentNode.parentNode;
    Tween.set(node, { autoAlpha: 0, y: -8 });
    Tween.to(node, 0.3, { autoAlpha: 1, y: 0, ease: Power3.easeInOut });
    Tween.to(parent, 0.3, {
        marginBottom: `120px`,
        ease: Power3.easeInOut
    });
}
