import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2,
  Power3,
  Power4
} from "gsap/TweenMax";

class OffCanvasAnimation {
  constructor(nodes) {
    this.nodes = nodes;

    this.openCanvas = () => {
      const tl = new Timeline();
      const offset = window.innerWidth >= 576 ? 450 : 325;
      tl.to(this.nodes.wrapper, 0.3, {
        x: -offset,
        ease: Power3.easeInOut
      });
      return tl;
    };

    this.closeCanvas = () => {
      const tl = new Timeline();
      tl.to(this.nodes.wrapper, 0.3, { x: 0, ease: Power3.easeInOut });
      return tl;
    };
  }
}

export default OffCanvasAnimation;
