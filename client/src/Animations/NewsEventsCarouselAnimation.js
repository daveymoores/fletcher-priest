import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class NewsEventsCarouselAnimation {
    constructor(nodes) {
        this.nodes = nodes;

        const carouselTitle = this.nodes.wrapper.childNodes[0];
        const carouselArrows = this.nodes.wrapper.querySelectorAll("svg");
        const carouselWrapper = this.nodes.wrapper.querySelector(
            ".slick-slider"
        );
        const slides = carouselWrapper.querySelectorAll(".slick-slide");

        function setInitialState() {
            Tween.set(carouselTitle, { opacity: 0, y: 30 });

            [].forEach.call(carouselArrows, element => {
                Tween.set(element, { opacity: 0, scale: 1.3 });
            });

            [].forEach.call(slides, element => {
                Tween.set(element, {
                    x: 20,
                    opacity: 0
                });
            });
        }

        setInitialState();

        this.show = () => {
            const tl = new Timeline();

            tl.to(carouselTitle, 0.6, {
                opacity: 1,
                y: 0,
                ease: Power3.easeInOut
            });
            tl.staggerTo(
                carouselArrows,
                0.6,
                {
                    opacity: 1,
                    x: 0,
                    scale: 1,
                    ease: Power3.easeInOut
                },
                0.15,
                "-=0.5"
            );
            tl.staggerTo(
                slides,
                0.8,
                {
                    opacity: 1,
                    x: 0,
                    ease: Power3.easeInOut
                },
                0.15,
                "-=1"
            );

            return tl;
        };
    }
}

export default NewsEventsCarouselAnimation;
