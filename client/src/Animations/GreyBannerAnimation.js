import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2,
  Power3,
  Power4,
  Expo
} from 'gsap/TweenMax';

class GreyBannerAnimation {
  constructor(nodes) {
    this.nodes = nodes;

    Tween.set(this.nodes.wrapper, { opacity: 0 });
    Tween.set(this.nodes.callout, { y: 0, opacity: 0 });
    Tween.set(this.nodes.link, { y: 0, opacity: 0 });

    this.expand = () => {
      const tl = new Timeline();
      Tween.set(this.nodes.wrapper, { width: '100%', opacity: 1 });
      tl.to(
        this.nodes.callout,
        0.6,
        {
          y: 0,
          opacity: 1,
          ease: Power3.easeInOut
        },
        '-=0.65'
      );
      tl.to(
        this.nodes.link,
        0.6,
        {
          y: 0,
          opacity: 1,
          ease: Power4.easeInOut
        },
        '-=0.5'
      );

      tl.progress(1).progress(0);
      return tl;
    };
  }
}

export default GreyBannerAnimation;
