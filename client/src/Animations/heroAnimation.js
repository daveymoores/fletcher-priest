import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class heroAnimation {
    constructor(nodes) {
        this.nodes = nodes;

        Tween.set(this.nodes.wrapper, { width: "0" });
        Tween.set(this.nodes.description, { yPercent: 50 });
        Tween.set(this.nodes.description.childNodes[0], {
            yPercent: 10,
            opacity: 0
        });
        Tween.set(this.nodes.title, {
            opacity: 0,
            x: 5
        });

        this.expand = () => {
            const tl = new Timeline();
            tl.to(this.nodes.wrapper, 1, {
                width: "100%",
                ease: Expo.easeInOut
            });
            tl.to(
                this.nodes.title,
                0.8,
                {
                    opacity: 1,
                    x: 0,
                    ease: Expo.easeInOut
                },
                "-=0.6"
            );
            tl.to(
                this.nodes.description,
                0.6,
                {
                    yPercent: 0,
                    opacity: 1,
                    ease: Power3.easeInOut
                },
                "-=0.75"
            );
            tl.to(
                this.nodes.description.childNodes[0],
                0.6,
                {
                    yPercent: 0,
                    opacity: 1,
                    ease: Power4.easeInOut
                },
                "-=0.55"
            );

            tl.progress(1).progress(0);
            return tl.timeScale(1.2);
        };
    }
}

export default heroAnimation;
