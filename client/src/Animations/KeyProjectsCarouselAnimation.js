import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class KeyProjectsCarouselAnimation {
    constructor(nodes) {
        this.nodes = nodes;

        let slides = null;
        let carouselArrows = null;
        const carouselTitle = this.nodes.wrapper.querySelector("h3");
        const carouselWrapper = this.nodes.wrapper.childNodes[1];
        const slideDims = carouselWrapper.getBoundingClientRect().width;

        if (carouselWrapper.classList.contains("slick-slider")) {
            slides = carouselWrapper.querySelectorAll(".slick-active");
            carouselArrows = this.nodes.wrapper.querySelectorAll("svg");

            [].forEach.call(carouselArrows, element => {
                Tween.set(element, { opacity: 0, scale: 1.3 });
            });
        } else {
            slides = carouselWrapper.childNodes[0].children;
        }

        function setInitialState() {
            Tween.set(carouselTitle, { opacity: 0, y: 30 });

            function setTweenState(e) {
                Tween.set(e, { opacity: 0, y: -10 });
            }
            [].forEach.call(slides, element => {
                const mask = element.querySelector(".mask");
                element.childNodes[0].classList.add("animating");

                setTweenState(element.querySelector("h3"));
                setTweenState(element.querySelector("h4"));
                setTweenState(element.querySelector("h5"));

                Tween.set(mask, {
                    height: "100%",
                    width: "100%",
                    position: "absolute",
                    zIndex: 2,
                    backgroundColor: "white",
                    top: 0,
                    left: 0
                });
                Tween.set(element, {
                    xPercent: 30,
                    opacity: 0
                });
            });
        }

        setInitialState();

        this.show = () => {
            const tl = new Timeline();

            tl.to(carouselTitle, 0.6, {
                opacity: 1,
                y: 0,
                ease: Power3.easeInOut
            });
            if (carouselArrows) {
                tl.staggerTo(
                    carouselArrows,
                    0.6,
                    {
                        opacity: 1,
                        x: 0,
                        scale: 1,
                        ease: Power3.easeInOut
                    },
                    0.15,
                    "-=0.5"
                );
            }
            tl.staggerTo(
                slides,
                0.8,
                {
                    opacity: 1,
                    xPercent: 0,
                    ease: Power3.easeInOut,
                    onStart() {
                        const tl2 = new Timeline();
                        const m = this.target.querySelector(".mask");
                        const n = this.target.querySelectorAll("h3");
                        const c = this.target.querySelectorAll("h4");
                        const l = this.target.querySelectorAll("h5");
                        tl2.to(m, 0.8, {
                            width: 0,
                            ease: Power3.easeInOut
                        });
                        tl2.staggerTo(
                            n,
                            0.5,
                            {
                                opacity: 1,
                                y: 0,
                                ease: Power3.easeInOut,
                                onComplete() {
                                    this.target.classList.add("completed");
                                }
                            },
                            0.15
                        );
                        tl2.staggerTo(
                            c,
                            0.5,
                            {
                                opacity: 1,
                                y: 0,
                                ease: Power3.easeInOut
                            },
                            0.15,
                            "+=0.2"
                        );
                        tl2.staggerTo(
                            l,
                            0.5,
                            {
                                opacity: 1,
                                y: 0,
                                ease: Power3.easeInOut
                            },
                            0.15,
                            "-=0.2"
                        );
                    }
                },
                0.15,
                "-=0.5"
            );
            return tl;
        };
    }
}

export default KeyProjectsCarouselAnimation;
