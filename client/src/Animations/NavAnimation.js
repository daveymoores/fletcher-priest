import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2,
  Power3,
  Power4
} from 'gsap/TweenMax';

class NavAnimation {
  constructor(nodes, theme) {
    this.nodes = nodes;
    this.theme = theme.primary;

    const listItems = this.nodes.parentNav.querySelectorAll('li > a');
    const logoPaths = this.nodes.logo.querySelectorAll('path');
    const searchPaths = this.nodes.search.querySelectorAll('path');
    const mobileSearchPaths = this.nodes.mobileSearch.querySelectorAll('path');

    this.resetStyles();

    this.state = {
      about: false,
      whatwedo: false
    };

    this.timeout = null;
    this.timeout2 = null;
    this.timeout3 = null;
    this.timeout4 = null;
    this.timelineCheckNavUp = null;
    this.timelineCheckNavDown = null;

    this.navDownAbout = target => {
      const panelListItems = target.querySelectorAll('li');
      const targetDims = target.getBoundingClientRect();
      const h = targetDims.height + targetDims.top;

      const TIME = 0.3;

      [].forEach.call(panelListItems, e => {
        Tween.set(e, { autoAlpha: 0, y: 5 });
      });

      const tl = new Timeline({
        paused: true,
        onStart: () => {
          this.resetStyles();
        }
      });

      tl.call(() => {
        this.state.about = true;
        this.state.whatwedo = false;
        this.nodes.wrapper.classList.add('open');
        return this.state;
      });

      tl.to(this.nodes.bg, 0.4, {
        backgroundColor: '#2A2F38',
        height: h,
        ease: Power3.easeInOut
      });
      tl.staggerTo(
        listItems,
        0.3,
        {
          color: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        searchPaths,
        0.3,
        {
          stroke: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        logoPaths,
        0.3,
        {
          fill: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.to(
        this.nodes.wwdPanel,
        TIME,
        {
          autoAlpha: 0,
          ease: Power2.easeInOut
        },
        '-=0.6'
      );
      tl.to(
        this.nodes.wwdPanel.parentNode.querySelector('path'),
        0.2,
        {
          fill: '#2a2f38',
          ease: Power2.easeInOut
        },
        '-=0.6'
      );
      tl.to(
        this.nodes.aboutPanel,
        TIME,
        {
          autoAlpha: 1,
          ease: Power2.easeInOut
        },
        '-=0.3'
      );
      tl.to(
        this.nodes.aboutPanel.parentNode.querySelector('path'),
        TIME,
        {
          fill: '#FFF',
          ease: Power2.easeInOut
        },
        `-=${TIME}`
      );
      tl.staggerTo(
        panelListItems,
        0.35,
        { autoAlpha: 1, y: 0, ease: Power4.easeInOut },
        0.03,
        '-=0.3'
      );
      tl.to(
        nodes.mask,
        0.5,
        {
          autoAlpha: 1,
          ease: Power2.easeInOut
        },
        '-=0.9'
      );

      return tl;
    };

    this.navDownWwd = target => {
      const panelListItems = target.querySelectorAll('li');
      const targetDims = target.getBoundingClientRect();
      const h = targetDims.height + targetDims.top;

      const TIME = 0.3;

      [].forEach.call(panelListItems, e => {
        Tween.set(e, { autoAlpha: 0, y: 5 });
      });

      const tl = new Timeline({
        paused: true,
        onStart: () => {
          this.resetStyles();
        }
      });

      tl.call(() => {
        this.state.about = false;
        this.state.whatwedo = true;
        this.nodes.wrapper.classList.add('open');
        return this.state;
      });

      tl.to(this.nodes.bg, 0.4, {
        backgroundColor: '#2A2F38',
        height: h,
        ease: Power3.easeInOut
      });
      tl.staggerTo(
        listItems,
        0.3,
        {
          color: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        searchPaths,
        0.3,
        {
          stroke: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        logoPaths,
        0.3,
        {
          fill: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.to(
        this.nodes.aboutPanel,
        TIME,
        {
          autoAlpha: 0,
          ease: Power2.easeInOut
        },
        '-=0.6'
      );
      tl.to(
        this.nodes.aboutPanel.parentNode.querySelector('path'),
        0.2,
        {
          fill: '#2a2f38',
          ease: Power2.easeInOut
        },
        '-=0.6'
      );
      tl.to(
        this.nodes.wwdPanel,
        TIME,
        {
          autoAlpha: 1,
          ease: Power2.easeInOut
        },
        '-=0.3'
      );
      tl.to(
        this.nodes.wwdPanel.parentNode.querySelector('path'),
        TIME,
        {
          fill: '#FFF',
          ease: Power2.easeInOut
        },
        `-=${TIME}`
      );
      tl.staggerTo(
        panelListItems,
        0.35,
        { autoAlpha: 1, y: 0, ease: Power4.easeInOut },
        0.03,
        '-=0.3'
      );
      tl.to(
        nodes.mask,
        0.5,
        {
          autoAlpha: 1,
          ease: Power2.easeInOut
        },
        '-=0.9'
      );

      return tl;
    };

    this.navUp = target => {
      const tl = new Timeline({
        onStart: () => {
          this.resetStyles();
        },
        onComplete: () => {
          this.state.about = false;
          this.state.whatwedo = false;
          this.nodes.wrapper.classList.remove('open');
          this.setInitialState([nodes.aboutPanel, nodes.wwdPanel]);
          return this.state;
        }
      });

      tl.to(target, 0.3, {
        autoAlpha: 0,
        ease: Power2.easeInOut
      });
      tl.to(
        this.nodes.bg,
        0.4,
        {
          backgroundColor: this.theme,
          height: '85px',
          ease: Power3.easeInOut
        },
        '-=0.2'
      );
      tl.to(
        target.parentNode.querySelector('path'),
        0.3,
        {
          fill: '#2a2f38',
          ease: Power2.easeInOut
        },
        '-=0.5'
      );
      tl.staggerTo(
        logoPaths,
        0.3,
        {
          fill: '#2a2f38',
          ease: Power2.easeInOut
        },
        0,
        '-=0.5'
      );
      tl.staggerTo(
        searchPaths,
        0.3,
        {
          stroke: '#2a2f38',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        listItems,
        0.3,
        {
          color: '#2a2f38',
          ease: Power2.easeInOut
        },
        0,
        '-=0.5'
      );
      tl.to(
        nodes.mask,
        0.3,
        {
          autoAlpha: 0,
          ease: Power2.easeInOut
        },
        '-=0.5'
      );

      return tl;
    };
  }

  animateMobileElements(bool) {
    const listItems = this.nodes.parentNav.querySelectorAll('li');
    const divItems = this.nodes.parentNav.querySelectorAll('div');
    const logoPaths = this.nodes.logo.querySelectorAll('path');
    const mobileSearchPaths = this.nodes.mobileSearch.querySelectorAll('path');

    this.navDown = () => {
      const TIME = 0.3;

      [].forEach.call(listItems, e => {
        Tween.set(e, { autoAlpha: 0, y: 5 });
      });

      const tl = new Timeline({
        paused: true,
        onComplete: () => {
          this.nodes.wrapper.classList.remove('open');
        }
      });

      tl.staggerTo(
        logoPaths,
        0.3,
        {
          fill: '#FFF',
          ease: Power2.easeInOut
        },
        0
      );
      tl.staggerTo(
        divItems,
        0,
        {
          autoAlpha: 1,
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        mobileSearchPaths,
        0.3,
        {
          stroke: '#FFF',
          ease: Power2.easeInOut
        },
        0,
        '-=0.3'
      );
      tl.staggerTo(
        listItems,
        0.25,
        { autoAlpha: 1, y: 0, ease: Power4.easeInOut },
        0.015,
        '-=0.2'
      );

      return tl;
    };

    if (bool) {
      this.navDown().play();
    } else {
      this.resetStyles();
    }
  }

  resetStyles() {
    const listItems = this.nodes.parentNav.querySelectorAll('li');
    const divItems = this.nodes.parentNav.querySelectorAll('div');
    const listItemAnchors = this.nodes.parentNav.querySelectorAll('li > a');
    const logoPaths = this.nodes.logo.querySelectorAll('path');
    const mobileSearchPaths = this.nodes.mobileSearch.querySelectorAll('path');

    [].forEach.call(listItems, element => {
      element.setAttribute('style', '');
    });

    [].forEach.call(divItems, element => {
      element.setAttribute('style', '');
    });

    [].forEach.call(listItemAnchors, element => {
      element.setAttribute('style', '');
    });

    [].forEach.call(mobileSearchPaths, e => {
      Tween.set(e, { stroke: '#2a2f38' });
    });

    [].forEach.call(logoPaths, e => {
      Tween.set(e, { fill: '#000' });
    });


    Tween.set(this.nodes.aboutPanel, {
      autoAlpha: 0
    });

    Tween.set(this.nodes.wwdPanel, {
      autoAlpha: 0
    });

    this.nodes.wrapper.classList.remove('expand');
    this.nodes.parentNav.classList.remove('expand');
    this.nodes.wrapper.classList.remove('open');
    this.nodes.hamburger.classList.remove('is-active');
  }

  setInitialState(targets) {
    this.resetStyles();

    if (window.innerWidth > 850) {
      Tween.set(this.nodes.mask, { autoAlpha: 0 });

      targets.forEach(element => {
        Tween.set(element, {
          autoAlpha: 0
        });
      });
    } else {
      Tween.set(this.nodes.mask, { autoAlpha: 1 });

      targets.forEach(element => {
        Tween.set(element, {
          autoAlpha: 1
        });
      });
    }
  }

  toggleDropdown(targetEventCurrent, targetEventRelated, targetEventType) {
    const eventType = targetEventType;
    const target = targetEventCurrent;
    const nextElement = targetEventRelated || null;
    const str = target.innerText.replace(/\s/g, '').toLowerCase();
    const animationTarget =
      target.innerText === 'About'
        ? this.nodes.aboutPanel
        : this.nodes.wwdPanel;

    clearInterval(this.timeout);

    if (eventType === 'mouseout') {
      clearInterval(this.timeout2);
      clearInterval(this.timeout4);
    }

    if (eventType === 'mouseover') {
      if (!this.state[str]) {
        if (animationTarget === this.nodes.aboutPanel) {
          this.timeout2 = setTimeout(() => {
            this.navDownAbout(animationTarget)
              .play()
              .eventCallback('onComplete', () => {});
          }, 250);
        } else {
          this.timeout4 = setTimeout(() => {
            this.navDownWwd(animationTarget)
              .play()
              .eventCallback('onComplete', () => {});
          }, 250);
        }
      }
    }

    function isDescendant(parent, child) {
      let node = child.parentNode;
      while (node != null) {
        if (node === parent) {
          return true;
        }
        node = node.parentNode;
      }
      return false;
    }

    if (eventType === 'mouseout') {
      // conditional for moving cursor on to submenu
      if (isDescendant(target.parentNode, nextElement)) {
        this.timeout = setTimeout(() => {
          this.collapseMenu();
        }, 250);
      }
    }
  }

  collapseMenu() {
    function findTarget(obj, ctx) {
      const keys = Object.keys(obj);
      const filtered = keys.filter(key => obj[key]);
      const value =
        filtered[0] === 'about' ? ctx.nodes.aboutPanel : ctx.nodes.wwdPanel;
      return value;
    }

    this.navUp(findTarget(this.state, this))
      .timeScale(1.3)
      .play();
  }
}

export default NavAnimation;
