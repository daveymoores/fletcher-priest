import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class VariableWidthCarouselAnimation {
    constructor(nodes) {
        const { wrapper } = nodes;

        const slides = wrapper.querySelectorAll(".mask");
        const imgs = wrapper.querySelectorAll("img");

        [].forEach.call(imgs, i => {
            Tween.set(i, { scale: 1.4 });
        });

        [].forEach.call(slides, i => {
            Tween.set(i, { backgroundColor: "#FFFFFF" });
        });

        this.show = () => {
            const tl = new Timeline();
            const tl2 = new Timeline();

            tl.staggerTo(
                slides,
                0.6,
                {
                    xPercent: 100,
                    backgroundColor: "#2A2F38",
                    ease: Power3.easeInOut
                },
                0.1
            );

            tl2.staggerTo(
                imgs,
                0.6,
                {
                    scale: 1,
                    ease: Power3.easeInOut
                },
                0.1
            );

            tl.progress(1).progress(0);
            tl2.progress(1).progress(0);
            return [tl, tl2];
        };
    }
}

export default VariableWidthCarouselAnimation;
