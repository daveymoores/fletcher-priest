import {
    TimelineMax as Timeline,
    TweenMax as Tween,
    Power2,
    Power3,
    Power4,
    Expo
} from "gsap/TweenMax";

class LightBackgroundImageLeftRightAnimation {
    constructor(nodes) {
        const { mask, wrapper, text } = nodes;

        const imgs = wrapper.querySelectorAll(".slide");

        Tween.set(mask, { backgroundColor: "#FFFFFF" });
        [].forEach.call(imgs, i => {
            Tween.set(i, { scale: 1.4, xPercent: 10 });
        });

        [].forEach.call(text.childNodes, element => {
            Tween.set(element, { y: 20, opacity: 0 });
        });

        this.show = () => {
            const tl = new Timeline();
            const tl2 = new Timeline();
            const tl3 = new Timeline();

            tl.to(mask, 0.6, {
                xPercent: 100,
                backgroundColor: "#2A2F38",
                ease: Power3.easeInOut
            });

            tl2.staggerTo(
                imgs,
                0.6,
                {
                    scale: 1,
                    xPercent: 0,
                    ease: Power3.easeInOut
                },
                0.1
            );

            tl3.staggerTo(
                text.childNodes,
                0.6,
                {
                    delay: 0.2,
                    y: 0,
                    opacity: 1,
                    ease: Power2.easeInOut
                },
                0.08
            );

            tl.progress(1).progress(0);
            tl2.progress(1).progress(0);
            tl3.progress(1).progress(0);
            return [tl, tl2, tl3];
        };
    }
}

export default LightBackgroundImageLeftRightAnimation;
