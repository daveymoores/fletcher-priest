import React, { Component } from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import { Grid, GridItem } from "styled-grid-responsive";
import { Link } from "react-router-dom";
import uniqid from "uniqid";
import ReactGA from "react-ga";

import media from "../utils/style-utils";
import InternalLink from "../Components/InternalLink";
import { StatsGrid, StatsGridItem } from "../SharedStyles/stats-grid";
import PrismicConfig from "../prismic-configuration";
import GridContainer from "../Common/GridContainer";
import BorderTopWrapper from "../Common/BorderWrapper";
import Meta from "../Common/Meta";
import LoaderHero from "../Loaders/LoaderHero";
import Credits from "../Common/Credits";

const LoadableTwoImagePlusText = Loadable({
  loader: () => import("../Components/TwoImagePlusText"),
  loading: () => null
});

const LoadableHero = Loadable({
  loader: () => import("../Common/Hero"),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import("../Common/VideoHero"),
  loading: () => <LoaderHero />
});

const SecondIntro = styled.div`
  padding: 40px 0 60px;

  h3 {
    font-size: 36px;
  }

  p {
    font-size: 18px;
    font-family: ${props => props.theme.FFDINWebProMedium};
  }
`;

const PracticeIntro = styled.div`
  h2 {
    font-size: 28px;
    margin: 30px 0 0;
    font-family: ${props => props.theme.FFDINWebProMedium};

    ${media.mobile`
          font-size: 5vw;
        `};

    ${media.tablet`
          font-size: 50px;
          line-height: 55px;
          margin: 60px 0px 30px;
        `};
  }
`;

export default class Practice extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at("document.type", "practice"))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;
    let color = null;

    function returnStatsGrid(item) {
      const k = Object.keys(item.data);
      const v = Object.values(item.data);

      const statsGrid = (
        <StatsGrid>
          {k.map((element, index) => {
            switch (element) {
              case "years_established":
                return (
                  <StatsGridItem key={uniqid()} number>
                    <p>{v[index]}</p>
                    <span>Years Established</span>
                  </StatsGridItem>
                );

              case "free_thinkers":
                return (
                  <StatsGridItem key={uniqid()} number>
                    <p>{v[index]}</p>
                    <span>Freethinkers</span>
                  </StatsGridItem>
                );

              case "awards_won":
                return (
                  <StatsGridItem key={uniqid()} number>
                    <p>{v[index]}</p>
                    <span>Awards Won</span>
                  </StatsGridItem>
                );

              case "projects_undertaken":
                return (
                  <StatsGridItem key={uniqid()} number>
                    <p>{v[index]}</p>
                    <span>Projects Undertaken</span>
                  </StatsGridItem>
                );

              default:
            }
            return null;
          })}
        </StatsGrid>
      );

      return statsGrid;
    }

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results[0].data} />

          {results.map(item => {
            landingPageIntro = PrismicReact.RichText.render(
              item.data.landing_page_intro,
              PrismicConfig.linkResolver
            );
            pageTitle = PrismicReact.RichText.render(
              item.data.page_title,
              PrismicConfig.linkResolver
            );

            color = item.data.title_colour;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "masthead_image") {
              return (
                <div key={uniqid()}>
                  <LoadableHero
                    type="stats"
                    content={slice.primary.image}
                    color={color}
                    intro={returnStatsGrid(results[0])}
                    title={pageTitle}
                  />
                </div>
              );
            }
            if (slice.slice_type === "masthead_video") {
              return (
                <div key={uniqid()}>
                  <LoadableVideoHero
                    type="stats"
                    content={slice}
                    color={color}
                    intro={returnStatsGrid(results[0])}
                    title={pageTitle}
                  />
                </div>
              );
            }
            return null;
          })}

          <GridContainer>
            <Grid>
              <GridItem
                media={{
                  smallPhone: 0,
                  mediumPhone: 1,
                  tablet: 1 / 12
                }}
                col={1 / 12}
              />
              <GridItem
                media={{
                  smallPhone: 1,
                  mediumPhone: 1,
                  tablet: 10 / 12
                }}
                col={10 / 12}
              >
                <PracticeIntro>{landingPageIntro}</PracticeIntro>
              </GridItem>
            </Grid>
          </GridContainer>

          <BorderTopWrapper>
            <GridContainer>
              <Grid>
                <GridItem
                  media={{
                    smallPhone: 0,
                    mediumPhone: 1,
                    tablet: 1 / 12
                  }}
                  col={1 / 12}
                />
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1,
                    tablet: 10 / 12
                  }}
                  col={10 / 12}
                >
                  <SecondIntro>
                    {results[0].data.rich_text_block_with_link.map(item => (
                      <div key={uniqid()}>
                        {PrismicReact.RichText.render(
                          item.text_block_title,
                          PrismicConfig.linkResolver
                        )}
                        {PrismicReact.RichText.render(
                          item.rich_text_block,
                          PrismicConfig.linkResolver
                        )}
                        <InternalLink
                          PrismicReact={PrismicReact}
                          link={item.internal_link}
                        />
                      </div>
                    ))}
                  </SecondIntro>
                </GridItem>
              </Grid>
            </GridContainer>
          </BorderTopWrapper>

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "2_image_with_text") {
              return (
                <LoadableTwoImagePlusText
                  key={uniqid()}
                  slice={slice}
                  prismicCtx={prismicCtx}
                  PrismicReact={PrismicReact}
                  location={location}
                />
              );
            }
            return null;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return <h1>Loading</h1>;
  }
}
