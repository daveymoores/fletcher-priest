import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { Link, withRouter } from "react-router-dom";
import styled from "styled-components";
import PrismicReact from "prismic-reactjs";
import { Grid, GridItem } from "styled-grid-responsive";
import Loadable from "react-loadable";
import ReactGA from "react-ga";
import media from "../utils/style-utils";

import PrismicConfig from "../prismic-configuration";
import BorderTopWrapper from "../Common/BorderWrapper";
import Meta from "../Common/Meta";
import LoaderHero from "../Loaders/LoaderHero";
import LoaderPageContent from "../Loaders/LoaderPageContent";
import Credits from "../Common/Credits";
import GridContainer from "../Common/GridContainer";

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import("../Components/TextOnlyPageTitle"),
  loading: () => null
});

const LoadablePeopleGrid = Loadable({
  loader: () => import("../Components/PeopleGrid"),
  loading: () => <LoaderPageContent />
});

const LoadablePageHeading = Loadable({
  loader: () => import("../Components/PageHeading"),
  loading: () => null
});

const LoadableHero = Loadable({
  loader: () => import("../Common/Hero"),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import("../Common/VideoHero"),
  loading: () => <LoaderHero />
});

const TeamMemberName = styled.p`
  font-size: 16px;
  font-family: ${props => props.theme.FFDINWebProMedium};
  margin-bottom: 0;
  margin-top: 0;
  color: ${props => props.theme.secondary};
`;

const TeamMemberContainer = styled.div`
  margin-top: 60px;
`;

const TeamColumns = styled.div`
  column-count: 1;
  column-gap: 20px;

  ${media.tablet`
    column-count: 2;
  `};

  ${media.desktop`
    column-count: 3;
  `};
`;

class PeopleLanding extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { results } = this.state;
    if (nextState.results !== results) {
      return true;
    }
    return false;
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.any("document.type", ["people"]))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  collectTeamMembers = data => {
    const arr = [];
    data.map(item => {
      arr.push(PrismicReact.RichText.asText(item.team_member));
    });

    return arr.sort();
  };

  render() {
    const { results } = this.state;
    const { prismicCtx, location, history } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;
    let landingPageContent = null;
    let color = null;
    let id = null;

    if (results) {
      return (
        <div>
          {results
            .filter(item => item.type === "people")
            .map(result => (
              <Meta key={result.id} metaContent={result.data} />
            ))}

          {results
            .filter(item => item.type === "people")
            .map(item => {
              id = item.uid;
              landingPageIntro = PrismicReact.RichText.render(
                item.data.landing_page_intro,
                PrismicConfig.linkResolver
              );
              pageTitle = PrismicReact.RichText.render(
                item.data.page_title,
                PrismicConfig.linkResolver
              );

              color = item.data.title_colour;

              landingPageContent = item;
            })}

          {landingPageContent.data.body.map(slice => {
            if (slice.slice_type === "masthead_image") {
              return (
                <LoadableHero
                  key={id}
                  content={slice.primary.image}
                  color={color}
                  intro={landingPageIntro}
                  title={pageTitle}
                />
              );
            }
            if (slice.slice_type === "masthead_video") {
              return (
                <LoadableVideoHero
                  key={id}
                  content={slice}
                  color={color}
                  intro={landingPageIntro}
                  title={pageTitle}
                />
              );
            }
            return true;
          })}

          <BorderTopWrapper>
            <LoadablePeopleGrid
              history={history}
              prismicCtx={prismicCtx}
              PrismicReact={PrismicReact}
              location={location}
            />
          </BorderTopWrapper>

          <TeamMemberContainer>
            <GridContainer>
              <Grid>
                <GridItem col={1 / 1}>
                  <TeamColumns>
                    {this.collectTeamMembers(results[0].data.team_members).map(
                      person => (
                        <TeamMemberName>{person}</TeamMemberName>
                      )
                    )}
                  </TeamColumns>
                </GridItem>
              </Grid>
            </GridContainer>
          </TeamMemberContainer>

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits key={slice.slice_type} data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return null;
  }
}

export default withRouter(PeopleLanding);
