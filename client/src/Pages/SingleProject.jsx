import React from "react";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import Loadable from "react-loadable";
import { Grid, GridItem } from "styled-grid-responsive";
import debounce from "lodash/debounce";
import ReactGA from "react-ga";
import LazyLoad from "react-lazyload";

import {
  AwardRow,
  YearHeading,
  YearGroup
} from "../Components/award-row-styles";
import { StatsGrid, StatsGridItem } from "../SharedStyles/stats-grid";
import Intro from "../SharedStyles/intro-styles";
import PrismicConfig from "../prismic-configuration";
import GridContainer from "../Common/GridContainer";
import { FluidGrid, FluidRow, FluidGridParent } from "../Common/FluidGrid";
import Meta from "../Common/Meta";
import LoaderHero from "../Loaders/LoaderHero";
import LoaderPageContent from "../Loaders/LoaderPageContent";
import LoaderGridItem from "../Loaders/LoaderGridItem";
import LoaderSearchBar from "../Loaders/LoaderSearchBar";
import LoaderGridItemCarousel from "../Loaders/LoaderGridItemCarousel";
import RichText from "../Components/RichText";
import FullWidthVideo from "../Components/FullWidthVideo";
import Credits from "../Common/Credits";
import PullQuote from "../Components/PullQuote";

const LoadablePersonTeaser = Loadable({
  loader: () => import("../Components/PersonTeaser"),
  loading: () => <LoaderGridItem />
});

const LoadableNotFound = Loadable({
  loader: () => import("./NotFound"),
  loading: () => null
});

const LoadableInsetHeading = Loadable({
  loader: () => import("../Components/InsetHeading"),
  loading: () => <LoaderSearchBar />
});

const LoadableProjectsQuoteCarousel = Loadable({
  loader: () => import("../Components/ProjectsQuoteCarousel"),
  loading: () => null
});

const LoadableHero = Loadable({
  loader: () => import("../Common/Hero"),
  loading: () => <LoaderHero />
});

const LoadableSimilarProjectsCarousel = Loadable({
  loader: () => import("../Components/SimilarProjectsCarousel"),
  loading: () => <LoaderGridItemCarousel />
});

const LoadablePressCarousel = Loadable({
  loader: () => import("../Components/PressCarousel"),
  loading: () => <LoaderGridItemCarousel />
});

const LoadableDarkBackgroundImageLeftRight = Loadable({
  loader: () => import("../Components/DarkBackgroundImageLeftRight"),
  loading: () => <LoaderPageContent />
});

const LoadableFullWidthCarousel = Loadable({
  loader: () => import("../Components/FullWidthCarousel"),
  loading: () => <LoaderPageContent />
});

const LoadableVariableWidthCarousel = Loadable({
  loader: () => import("../Components/VariableWidthCarousel"),
  loading: () => <LoaderPageContent />
});

const LoadableTwoColumnTextured = Loadable({
  loader: () => import("../Components/TwoColumnTextured"),
  loading: () => <LoaderPageContent />
});

const RichTextWrapper = styled.section`
  p {
    margin-bottom: 30px;
  }

  h2,
  h3,
  h4,
  h5 {
    line-height: 1.4em;
    margin-bottom: 2em;
  }

  img {
    width: 100%;
    max-width: 100%;
    margin: 30px 0;
  }
`;

const SliceBorder = styled.div`
  border-top: 1px solid ${props => props.theme.darkGrey2};
  border-bottom: ${props =>
    props.last ? "none" : `1px solid ${props.theme.darkGrey2}`};
  padding: ${props => (props.NoBtmPadding ? "20px 0 20px" : "20px 0 80px")};
  margin-top: -1px;
`;

const SlicesContainer = styled.div`
  .slice:last-child {
    border-bottom: none;
  }
`;

export default class SingleProject extends React.Component {
  constructor() {
    super();

    this.state = {
      results: null,
      notFound: false
    };

    this.keyCount = 0;
    this.getKey = this.getKey.bind(this);

    this.handleResize = debounce(this.handleResize, 100);
  }

  componentWillMount() {
    this.fetchPage(this.props);
    this.handleResize();
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
    window.addEventListener("resize", this.handleResize, false);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize, false);
  }

  getKey() {
    return this.keyCount++;
  }

  handleResize = () => {
    const ww = window.innerWidth;
    let rowSize;

    this.setState(() => ({
      viewport: { x: window.innerWidth, y: window.innerHeight }
    }));
  };

  fetchPage(props) {
    if (props.prismicCtx) {
      // We are using the function to get a document by its uid
      return props.prismicCtx.api.getByUID(
        "single_project_page",
        props.match.params.uid,
        {
          fetchLinks: [
            "person.first_name",
            "person.middle_name",
            "person.last_name",
            "person.job_title",
            "person.headshot",
            "single_project_page.project_name",
            "single_project_page.client",
            "single_project_page.location",
            "single_project_page.image",
            "awards.award_date",
            "awards.award_name",
            "awards.award_place",
            "awards.award_category",
            "single_news_item.title",
            "single_news_item.category",
            "single_news_item.publication_date",
            "single_news_item.text_contents",
            "single_news_item.featured_image"
          ]
        },
        (err, results) => {
          if (results) {
            // We put the retrieved content in the state as a doc variable
            this.setState({ results });
          } else {
            // We changed the state to display error not found if no matched doc
            this.setState({ notFound: !results });
          }
        }
      );
    }
    return null;
  }

  sortByKey = array =>
    array.sort((a, b) => {
      const x = a.award_link.data.award_date.split("-")[0];
      const y = b.award_link.data.award_date.split("-")[0];
      return x < y ? -1 : x > y ? 1 : 0;
    });

  generateRows = (i, totalRows, remainder, element) => {
    let rowArray = [];

    if (i < totalRows * 3) {
      rowArray.push(element);

      if (rowArray.length === 3) {
        const newRow = rowArray;
        rowArray = [];
        return newRow;
      }

      return null;
    }

    rowArray.push(element);

    if (rowArray.length === remainder) {
      const newRow = rowArray;
      rowArray = [];
      return newRow;
    }

    return null;
  };

  returnBoxStyle = (item, i, arr, location, prismicCtx) => {
    const remainder = arr.length % 3;
    const totalRows = Math.floor(arr.length / 3);

    const TeaserComponent = (
      <LoadablePersonTeaser
        gridSize={100}
        key={item.id}
        person={item}
        prismicCtx={prismicCtx}
        PrismicReact={PrismicReact}
        location={location}
      />
    );

    const element = <FluidGrid key={item.id}>{TeaserComponent}</FluidGrid>;
    return this.generateRows(i, totalRows, remainder, element);
  };

  checkLast = (i, a) => {
    if (i + 1 === a.length) {
      return true;
    }
    return false;
  };

  returnStatsGrid = item => {
    const k = Object.keys(item.data);
    const v = Object.values(item.data);
    const sectorArray = [];
    const serviceArray = [];
    const titleArray = [];

    function caseCorrection(str) {
      const originalStr = str;
      const newStr =
        originalStr.charAt(0).toUpperCase() + originalStr.substr(1);

      return newStr;
    }

    function formatArray(arr) {
      let str = "";
      arr.forEach((i, j) => {
        if (i === "") return null;
        const spacer = j === arr.length - 1 ? "" : ",";
        const x = i.indexOf("-") > -1 ? i.replace("-", " ") : i;
        str += `${x}${spacer} `;
        return true;
      });

      return str;
    }

    const statsGrid = (
      <StatsGrid>
        <StatsGridItem fullWidth key={this.getKey()}>
          <h1>
            <span>
              {item.data.project_name &&
                PrismicReact.RichText.asText(item.data.project_name)}
            </span>
          </h1>
          <h2>
            {item.data.client && PrismicReact.RichText.asText(item.data.client)}
          </h2>
          <h3>
            {item.data.location &&
              PrismicReact.RichText.asText(item.data.location)}
          </h3>
        </StatsGridItem>
        {k.map((element, index) => {
          function returnElement(string) {
            const i = string.replace("ft2", `ft<sup>2</sup>`);
            const j = i.replace("m2", `m<sup>2</sup>`);
            return { __html: j };
          }

          switch (element) {
            case "sector_group":
              if (v[index] && v[index].length > 0) {
                v[index].map(ele => {
                  const caseCorrected = caseCorrection(ele.sector.uid);

                  switch (caseCorrected) {
                    case "Retail-hospitality":
                      sectorArray.push("Retail and Hospitality");
                      break;
                    case "Culture-sport-leisure":
                      sectorArray.push("Culture, Sport and Leisure");
                      break;
                    default:
                      sectorArray.push(caseCorrected);
                  }
                });

                if (item.data.custom_sector.length > 0) {
                  sectorArray.unshift(
                    PrismicReact.RichText.asText(item.data.custom_sector)
                  );
                }

                return (
                  <StatsGridItem key={this.getKey()}>
                    <span>Sector</span>
                    {formatArray(sectorArray)}
                  </StatsGridItem>
                );
              }
              break;
            case "service_group":
              if (v[index] && v[index].length > 0) {
                v[index].map(ele => {
                  const caseCorrected = caseCorrection(ele.service.uid);
                  serviceArray.push(caseCorrected);
                });

                return (
                  <StatsGridItem
                    style={{
                      textTransform: "capitalize"
                    }}
                    key={this.getKey()}
                  >
                    <span>Our Role</span>
                    {formatArray(serviceArray)}
                  </StatsGridItem>
                );
              }
              break;
            case "status":
              if (v[index] && v[index].length > 0) {
                return (
                  <StatsGridItem key={this.getKey()}>
                    <span>Status</span>
                    {v[index]}
                  </StatsGridItem>
                );
              }
              break;
            case "size":
              if (v[index] && v[index].length > 0) {
                return (
                  <StatsGridItem key={this.getKey()}>
                    <span>Size</span>
                    <React.Fragment>
                      <div
                        dangerouslySetInnerHTML={returnElement(
                          PrismicReact.RichText.asText(v[index])
                        )}
                      />
                    </React.Fragment>
                  </StatsGridItem>
                );
              }
              break;
            default:
          }
          return null;
        })}
      </StatsGrid>
    );

    return statsGrid;
  };

  render() {
    const { results, notFound, viewport } = this.state;
    const { prismicCtx, location } = this.props;
    let yearGroupSortArray = [];
    const yearArray = [];
    let newYearArray = [];
    let yearGroup = [];

    const buildAwardGroupByYear = (award, index, array, yArray) => {
      const prevYear =
        index > 0
          ? array[index - 1].award_link.data.award_date.split("-")[0]
          : award.data.award_date.split("-")[0];

      const newYear = award ? award.data.award_date.split("-")[0] : false;
      if (!newYear) return null;

      const awardRow = (
        <AwardRow key={award.id}>
          <div>
            {Object.prototype.hasOwnProperty.call(award.data, "award_name") &&
              PrismicReact.RichText.asText(award.data.award_name)}
          </div>
          <div>
            {Object.prototype.hasOwnProperty.call(
              award.data,
              "award_category"
            ) && PrismicReact.RichText.asText(award.data.award_category)}
          </div>
          <div>
            {Object.prototype.hasOwnProperty.call(award.data, "award_place") &&
              award.data.award_place}
          </div>
        </AwardRow>
      );

      yearGroup.push(awardRow);
      newYearArray.push(award.data.award_date.split("-")[0]);

      // if the new year group has the same number as the test group
      if (
        newYearArray.filter(year => year === newYear).length ===
        yArray.filter(year => year === newYear).length
      ) {
        const y = (
          <YearHeading key={this.getKey()} inset>
            {award.data.award_date.split("-")[0]}
          </YearHeading>
        );
        yearGroup.unshift(y);
        const g = yearGroup;
        yearGroup = [];
        newYearArray = [];
        return g;
      }

      return null;
    };

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results.data} />
          <LoadableHero
            type="stats"
            content={results.data.image}
            intro={this.returnStatsGrid(results)}
            color={results.data.title_colour}
            title={PrismicReact.RichText.render(
              results.data.project_name,
              PrismicConfig.linkResolver
            )}
          />

          <GridContainer>
            <Grid>
              <GridItem
                media={{
                  smallPhone: 0,
                  mediumPhone: 1,
                  tablet: 1 / 12
                }}
                col={1 / 12}
              />
              <GridItem
                media={{
                  smallPhone: 1,
                  mediumPhone: 1,
                  tablet: 10 / 12
                }}
                col={10 / 12}
              >
                <Intro>
                  {PrismicReact.RichText.asText(
                    results.data.intro_text,
                    PrismicConfig.linkResolver
                  )}
                </Intro>
              </GridItem>
            </Grid>
          </GridContainer>

          <SlicesContainer>
            {results.data.body.map((slice, i, arr) => {
              if (slice.slice_type === "rich_text") {
                return (
                  <RichText
                    key={this.getKey()}
                    PrismicReact={PrismicReact}
                    slice={slice}
                    PrismicConfig={PrismicConfig}
                  />
                );
              }
              if (slice.slice_type === "full_width_video") {
                return <FullWidthVideo slice={slice} key={this.getKey()} />;
              }
              if (slice.slice_type === "pull_quote") {
                return (
                  <PullQuote
                    PrismicReact={PrismicReact}
                    slice={slice}
                    PrismicConfig={PrismicConfig}
                    key={this.getKey()}
                  />
                );
              }
              if (slice.slice_type === "full_width_image_image_carousel") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <LoadableFullWidthCarousel
                      key={this.getKey()}
                      prismic={prismicCtx}
                      content={slice.items}
                    />
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "variable_width_carousel") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <LoadableVariableWidthCarousel
                      key={this.getKey()}
                      prismic={prismicCtx}
                      content={slice.items}
                    />
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "two_column_textured") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <LoadableTwoColumnTextured
                      key={this.getKey()}
                      content={slice}
                      prismic={prismicCtx}
                    />
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "dark_background_image_left_right") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <LoadableDarkBackgroundImageLeftRight
                      key={this.getKey()}
                      content={slice}
                      prismic={prismicCtx}
                    />
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "quote_carousel") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <GridContainer key={this.getKey()}>
                      <Grid>
                        <GridItem
                          media={{
                            smallPhone: 0 / 12,
                            mediumPhone: 0 / 12,
                            tablet: 1 / 12
                          }}
                          col={1 / 6}
                        />
                        <GridItem
                          media={{
                            smallPhone: 1,
                            mediumPhone: 1,
                            tablet: 10 / 12
                          }}
                          col={2 / 3}
                        >
                          <LoadableProjectsQuoteCarousel
                            content={slice.items}
                            prismic={prismicCtx}
                          />
                        </GridItem>
                      </Grid>
                    </GridContainer>
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "project_leaders") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <SliceBorder
                      key={this.getKey()}
                      last={this.checkLast(i, arr)}
                      className="slice"
                    >
                      <GridContainer type="pullRight">
                        <LoadableInsetHeading text="Project Leaders" />
                        <FluidGridParent>
                          {slice.items
                            .filter(item => {
                              if (item.project_leader.id) return true;
                              return false;
                            })
                            .map((item, index, array) => {
                              const boxStyle = this.returnBoxStyle(
                                item.project_leader,
                                index,
                                array,
                                location,
                                prismicCtx
                              );

                              if (boxStyle !== null) {
                                return (
                                  <FluidRow key={this.getKey()}>
                                    {boxStyle}
                                  </FluidRow>
                                );
                              }
                              return null;
                            })}
                        </FluidGridParent>
                      </GridContainer>
                    </SliceBorder>
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "awards") {
                slice.items
                  .filter(item => {
                    if (item.award_link.id) return true;
                    return false;
                  })
                  .map((award, index, array) => {
                    if (award.award_link.isBroken) return null;
                    const AwardYear = award.award_link.data.award_date.split(
                      "-"
                    )[0];
                    yearArray.push(
                      award.award_link.data.award_date.split("-")[0]
                    );

                    yearGroupSortArray.push(award);

                    return true;
                  });

                yearGroupSortArray = this.sortByKey(
                  yearGroupSortArray
                ).reverse();

                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <SliceBorder
                      key={this.getKey()}
                      last={this.checkLast(i, arr)}
                      className="slice"
                    >
                      <GridContainer>
                        <LoadableInsetHeading text="Awards" />
                        <Grid>
                          <GridItem col={12 / 12}>
                            {yearGroupSortArray
                              .filter(item => {
                                if (item.award_link.id) return true;
                                return false;
                              })
                              .map((award, index, array) => (
                                <YearGroup key={this.getKey()}>
                                  {buildAwardGroupByYear(
                                    award.award_link,
                                    index,
                                    array,
                                    yearArray.sort().reverse(),
                                    newYearArray,
                                    yearGroup
                                  )}
                                </YearGroup>
                              ))}
                          </GridItem>
                        </Grid>
                      </GridContainer>
                    </SliceBorder>
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "in_the_press") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <SliceBorder
                      key={this.getKey()}
                      last={this.checkLast(i, arr)}
                      className="slice"
                    >
                      <GridContainer>
                        <LoadablePressCarousel
                          headingText="In the press"
                          windowSize={viewport}
                          prismicCtx={prismicCtx}
                          results={slice}
                        />
                      </GridContainer>
                    </SliceBorder>
                  </LazyLoad>
                );
              }
              if (slice.slice_type === "similar_projects") {
                return (
                  <LazyLoad
                    once
                    height={window.innerHeight}
                    offset={window.innerHeight / 2}
                    key={this.getKey()}
                  >
                    <SliceBorder
                      key={this.getKey()}
                      NoBtmPadding
                      last={this.checkLast(i, arr)}
                      className="slice"
                    >
                      <GridContainer type="pullRight">
                        <LoadableSimilarProjectsCarousel
                          prismicCtx={prismicCtx}
                          location={location}
                          headingText="Explore similar projects"
                          results={slice}
                        />
                      </GridContainer>
                    </SliceBorder>
                  </LazyLoad>
                );
              }
              return null;
            })}
          </SlicesContainer>

          {results.data.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    if (notFound) {
      return <LoadableNotFound />;
    }
    return null;
  }
}
