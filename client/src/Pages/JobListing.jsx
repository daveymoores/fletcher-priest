import React, { Component } from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import { Grid, GridItem } from "styled-grid-responsive";
import Moment from "moment";
import ReactGA from "react-ga";
import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2
} from "gsap/TweenMax";

import { MobileSwapGrid, HideForMobile } from "../SharedStyles/utils-styles";
import PrismicConfig from "../prismic-configuration";
import GridContainer from "../Common/GridContainer";
import Meta from "../Common/Meta";
import Credits from "../Common/Credits";

const LoadablePageTitle = Loadable({
  loader: () => import("../Components/PageTitle"),
  loading: () => null
});

const LoadableSocial = Loadable({
  loader: () => import("../Components/Social"),
  loading: () => null
});

const LoadableArticleWrapper = Loadable({
  loader: () => import("../Components/ArticleWrapper"),
  loading: () => null
});

const LoadableForwardButton = Loadable({
  loader: () => import("../Components/ForwardButton"),
  loading: () => null
});

const LoadableNotFound = Loadable({
  loader: () => import("./NotFound"),
  loading: () => null
});

const NewsEventsFeedWrapper = styled.div`
  border-top: 1px solid ${props => props.theme.darkGrey};
  padding-top: 60px;
  margin-top: 60px;
`;

const RichTextWrapper = styled.section`
  p {
    margin-bottom: 30px;
  }

  img {
    width: 100%;
    max-width: 100%;
    margin: 30px 0;
  }
`;

const Tag = styled.p`
  color: ${props => props.theme.primary};
  margin: 0px;
`;

const ReadLength = styled.p`
  color: ${props => props.theme.primary};
  margin: 0;
`;

const PubDate = styled.p`
  margin: 0;
`;

const PullQuote = styled.blockquote`
  margin: 40px 0 40px;
  font-size: 45px;
  width: 100%;
  font-family: ${props => props.theme.FFDINWebProMedium};
`;

const FeaturedImg = styled.img`
  width: 100%;
  max-width: 100%;
  margin: 30px 0;
`;

const ListingIntro = styled.div`
  line-height: 25px;
  font-size: 15px;
`;

const ListingAnimationWrapper = styled.div`
  opacity: 0;
`;

class JobListing extends Component {
  state = {
    results: null,
    notFound: false
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  componentDidUpdate() {
    if (this.listingRef) {
      const nodes = this.listingRef.getElementsByTagName("*");
      [].forEach.call(nodes, element => {
        Tween.set(element, { opacity: 0 });
      });
      Tween.set(this.listingRef, { opacity: 1 });
      Tween.staggerTo(nodes, 0.25, { opacity: 1, ease: Power2.ease }, 0.007);
    }
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      // We are using the function to get a document by its uid
      return props.prismicCtx.api.getByUID(
        "job_listing",
        props.match.params.uid,
        {},
        (err, results) => {
          if (results) {
            // We put the retrieved content in the state as a doc variable
            this.setState({ results });
          } else {
            // We changed the state to display error not found if no matched doc
            this.setState({ notFound: !results });
          }
        }
      );
    }
    return null;
  }

  render() {
    const { results, notFound } = this.state;
    const { prismicCtx } = this.props;

    if (results) {
      let formattedDate;

      if (results.first_publication_date) {
        const date = PrismicReact.Date(
          results.first_publication_date
        ).toString();
        formattedDate = Moment(date, "ddd MMM DD YYYY HH:mm:ss ZZ").format(
          "LL"
        );
      }

      return (
        <div data-wio-id={results.id}>
          <ListingAnimationWrapper innerRef={el => (this.listingRef = el)}>
            <LoadableArticleWrapper>
              <Meta metaContent={results.data} />
              <GridContainer>
                <MobileSwapGrid>
                  <GridItem
                    media={{
                      smallPhone: 1,
                      mediumPhone: 1 / 12
                    }}
                    col={1 / 6}
                  >
                    <HideForMobile>
                      <LoadableSocial
                        title={PrismicReact.RichText.asText(
                          results.data.job_listing_title
                        )}
                      />
                    </HideForMobile>
                  </GridItem>
                  <GridItem
                    media={{
                      smallPhone: 1,
                      mediumPhone: 12 / 12
                    }}
                    col={2 / 3}
                  >
                    <LoadablePageTitle
                      content={PrismicReact.RichText.asText(
                        results.data.job_listing_title
                      )}
                    />
                    <PubDate>{formattedDate}</PubDate>

                    <ListingIntro>
                      {PrismicReact.RichText.render(
                        results.data.listing_intro,
                        PrismicConfig.linkResolver
                      )}
                    </ListingIntro>
                  </GridItem>
                </MobileSwapGrid>
              </GridContainer>
              <GridContainer>
                <Grid>
                  <GridItem
                    media={{
                      smallPhone: 1,
                      mediumPhone: 1 / 12
                    }}
                    col={1 / 6}
                  />
                  <GridItem
                    media={{
                      smallPhone: 1,
                      mediumPhone: 12 / 12
                    }}
                    col={2 / 3}
                  >
                    <RichTextWrapper>
                      {PrismicReact.RichText.render(
                        results.data.job_description_and_requirements,
                        PrismicConfig.linkResolver
                      )}
                    </RichTextWrapper>

                    {PrismicReact.RichText.render(
                      results.data.salary,
                      PrismicConfig.linkResolver
                    )}

                    <LoadableForwardButton
                      slug={`/careers/${results.uid}/application`}
                      size="small"
                      destination="Apply Now"
                    />
                  </GridItem>
                </Grid>
              </GridContainer>
            </LoadableArticleWrapper>
          </ListingAnimationWrapper>

          {results.data.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    if (notFound) {
      return <LoadableNotFound prismicCtx={prismicCtx} />;
    }
    return null;
  }
}

export default JobListing;
