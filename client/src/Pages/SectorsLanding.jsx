import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Loadable from 'react-loadable';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import { Grid } from 'styled-grid-responsive';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import BorderTopWrapper from '../Common/BorderWrapper';
import Meta from '../Common/Meta';
import LoaderHero from '../Loaders/LoaderHero';
import Credits from '../Common/Credits';

const LoadableSimilarProjectsCarousel = Loadable({
  loader: () => import('../Components/SimilarProjectsCarousel'),
  loading: () => null
});

const LoadableHero = Loadable({
  loader: () => import('../Common/Hero'),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import('../Common/VideoHero'),
  loading: () => <LoaderHero />
});

const LoadableTwoImagePlusText = Loadable({
  loader: () => import('../Components/TwoImagePlusText'),
  loading: () => null
});

const LoadableLightBackgroundImageLeftRight = Loadable({
  loader: () => import('../Components/LightBackgroundImageLeftRight'),
  loading: () => null
});

const LoadableDarkBackgroundWithImage = Loadable({
  loader: () => import('../Components/DarkBackgroundWithImage'),
  loading: () => null
});

export default class SectorsLanding extends Component {
  constructor(props) {
    super();

    this.state = {
      results: null
    };

    this.keyCount = 0;
    this.getKey = this.getKey.bind(this);
  }

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  getKey() {
    return this.keyCount++;
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at('document.type', 'sectors_landing'), {
          fetchLinks: [
            'single_project_page.project_name',
            'single_project_page.client',
            'single_project_page.location',
            'single_project_page.image'
          ]
        })
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results[0].data} />

          {results.map(item => {
            pageTitle = PrismicReact.RichText.render(
              item.data.page_title,
              PrismicConfig.linkResolver
            );
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'masthead_image') {
              landingPageIntro = slice.primary.masthead_intro_text.length
                ? PrismicReact.RichText.render(
                    slice.primary.masthead_intro_text,
                    PrismicConfig.linkResolver
                  )
                : null;
              return (
                <LoadableHero
                  key={this.getKey()}
                  content={slice.primary.image}
                  color={slice.primary.title_colour}
                  intro={landingPageIntro}
                  title={pageTitle}
                  projectLink={slice.primary.project_link}
                />
              );
            }
            if (slice.slice_type === 'masthead_video') {
              landingPageIntro = slice.primary.masthead_intro_text.length
                ? PrismicReact.RichText.render(
                    slice.primary.masthead_intro_text,
                    PrismicConfig.linkResolver
                  )
                : null;
              return (
                <LoadableVideoHero
                  key={this.getKey()}
                  content={slice}
                  color={slice.primary.title_colour}
                  intro={landingPageIntro}
                  title={pageTitle}
                  projectLink={slice.primary.project_link}
                />
              );
            }

            if (slice.slice_type === 'dark_background_with_image') {
              return (
                <LoadableDarkBackgroundWithImage
                  key={this.getKey()}
                  content={slice}
                  prismic={prismicCtx}
                />
              );
            }
            if (slice.slice_type === 'light_background_image_left_right') {
              return (
                <LoadableLightBackgroundImageLeftRight
                  key={this.getKey()}
                  content={slice}
                  prismic={prismicCtx}
                />
              );
            }
            return true;
          })}
          {results[0].data.body.map((slice, i, arr) => {
            if (slice.slice_type === 'similar_projects') {
              return (
                <BorderTopWrapper>
                  <GridContainer key={this.getKey()} type="pullRight">
                    <LoadableSimilarProjectsCarousel
                      prismicCtx={prismicCtx}
                      location={location}
                      headingText="Selected Projects"
                      results={slice}
                    />
                  </GridContainer>
                </BorderTopWrapper>
              );
            }
            return null;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return <h1>Loading</h1>;
  }
}
