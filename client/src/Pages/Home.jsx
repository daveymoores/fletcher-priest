import React, { Component } from "react";
import Loadable from "react-loadable";
import LoadableVisibility from "react-loadable-visibility/react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import { Grid } from "styled-grid-responsive";
import uniqid from "uniqid";
import ReactGA from "react-ga";
import Credits from "../Common/Credits";

import PrismicConfig from "../prismic-configuration";
import GridContainer from "../Common/GridContainer";
import Meta from "../Common/Meta";
import LoaderHero from "../Loaders/LoaderHero";
import LoaderGridItemCarousel from "../Loaders/LoaderGridItemCarousel";
import LoaderPageContent from "../Loaders/LoaderPageContent";

const LoadableHero = Loadable({
  loader: () => import("../Common/Hero"),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import("../Common/VideoHero"),
  loading: () => <LoaderHero />
});

const LoadableKeyProjectsCarousel = LoadableVisibility({
  loader: () => import("../Components/KeyProjectsCarousel"),
  loading: () => <LoaderGridItemCarousel />
});

const LoadableNewsEventsCarousel = LoadableVisibility({
  loader: () => import("../Components/NewsEventsCarousel"),
  loading: () => <LoaderGridItemCarousel />
});

const LoadableGreyBanner = LoadableVisibility({
  loader: () => import("../Components/GreyBanner"),
  loading: () => <LoaderGridItemCarousel />
});

export default class Homepage extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at("document.type", "homepage"))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;
    let mastheadId = null;

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results[0].data} />

          {results.map(item => {
            mastheadId = item.id;
            landingPageIntro = PrismicReact.RichText.render(
              item.data.landing_page_intro,
              PrismicConfig.linkResolver
            );
            pageTitle = PrismicReact.RichText.render(
              item.data.page_title,
              PrismicConfig.linkResolver
            );
          })}
          {results[0].data.body.map(slice => {
            if (slice.slice_type === "masthead_image") {
              return (
                <LoadableHero
                  key={mastheadId}
                  content={slice.primary.image}
                  intro={landingPageIntro}
                />
              );
            }
            if (slice.slice_type === "masthead_video") {
              return (
                <LoadableVideoHero
                  key={mastheadId}
                  content={slice}
                  intro={landingPageIntro}
                />
              );
            }
            return true;
          })}

          <GridContainer type="pullRight">
            <LoadableKeyProjectsCarousel
              prismicCtx={prismicCtx}
              location={location}
            />
          </GridContainer>

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "grey_banner") {
              return (
                <LoadableGreyBanner key={uniqid()} content={slice.primary} />
              );
            }
            return true;
          })}

          <GridContainer>
            <LoadableNewsEventsCarousel
              headingText="Latest news"
              prismicCtx={prismicCtx}
            />
          </GridContainer>

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits key={0} data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return <LoaderPageContent />;
  }
}
