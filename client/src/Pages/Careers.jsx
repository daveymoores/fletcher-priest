import React, { Component } from 'react';
import Loadable from 'react-loadable';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import { Grid } from 'styled-grid-responsive';
import { Link } from 'react-router-dom';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';
import { connect } from 'react-redux';
import scrollToComponent from 'react-scroll-to-component';
import media from '../utils/style-utils';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import Meta from '../Common/Meta';
import LoaderHero from '../Loaders/LoaderHero';
import Credits from '../Common/Credits';

const LoadableHero = Loadable({
  loader: () => import('../Common/Hero'),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import('../Common/VideoHero'),
  loading: () => <LoaderHero />
});

const LoadableFullWidthLink = Loadable({
  loader: () => import('../Components/FullWidthLink'),
  loading: () => null
});

const LoadableCareerGrid = Loadable({
  loader: () => import('../Components/CareerGrid'),
  loading: () => null
});

const LoadableInsetHeading = Loadable({
  loader: () => import('../Components/InsetHeading'),
  loading: () => null
});

const LoadableQuoteCarousel = Loadable({
  loader: () => import('../Components/QuoteCarousel'),
  loading: () => null
});

const LoadableArrow = Loadable({
  loader: () => import('../Components/icons/vacancy-arrow.svg'),
  loading: () => null
});

const CareerGridParent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding-bottom: 40px;

  ${media.tablet`
      padding-bottom: 100px;
    `};
`;

const VacanciesWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const Vacancy = styled(Link)`
  width: 100%;
  max-width: 100%;
  min-width: 100%;
  background-color: ${props => props.theme.lightGrey};
  padding: 8px 20px;
  margin-bottom: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  ${media.tablet`
      width: calc(50% - 10px);
      max-width: calc(50% - 10px);
      min-width: calc(50% - 10px);
    `};

  h4 {
    margin-bottom: 0;
    margin-top: 15px;
    font-size: 21px;
    line-height: 20px;
  }

  p {
    margin-top: 10px;
    font-size: 16px;
    line-height: 18px;
  }
`;

const ArrowWrapper = styled.span`
  width: 40px;
  height: 40px;
  min-width: 40px;
  min-height: 40px;
  display: block;
  border-radius: 50%;
  overflow: hidden;

  svg {
    width: 100%;
    height: 100%;
  }
`;

class Careers extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location, scrollState } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  componentDidUpdate() {
    const { location, scrollState } = this.props;

    if (location.hash === '#vacancies') {
      setTimeout(() => {
        scrollToComponent(this.vacancyRef, {
          duration: 800,
          ease: 'inOutQuad'
        });
      }, 500);
    }
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at('document.type', 'careers'))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;
    let color = null;

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results[0].data} />
          {results.map(item => {
            landingPageIntro = PrismicReact.RichText.render(
              item.data.landing_page_intro,
              PrismicConfig.linkResolver
            );
            pageTitle = PrismicReact.RichText.render(
              item.data.page_title,
              PrismicConfig.linkResolver
            );

            color = item.data.title_colour;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'masthead_image') {
              return (
                <div key={uniqid()}>
                  <LoadableHero
                    content={slice.primary.image}
                    color={color}
                    intro={landingPageIntro}
                    title={pageTitle}
                  />
                  <LoadableFullWidthLink
                    linktext="View open positions"
                    destination="vacancies"
                    type="internal"
                  />
                </div>
              );
            }
            if (slice.slice_type === 'masthead_video') {
              return (
                <div key={uniqid()}>
                  <LoadableVideoHero
                    content={slice}
                    color={color}
                    intro={landingPageIntro}
                    title={pageTitle}
                  />
                  <LoadableFullWidthLink
                    linktext="View open positions"
                    destination="vacancies"
                    type="internal"
                  />
                </div>
              );
            }
            if (slice.slice_type === 'quote_carousel') {
              return (
                <GridContainer key={uniqid()} type="pullRight">
                  <LoadableInsetHeading text="What the team says" />
                  <LoadableQuoteCarousel
                    content={slice.items}
                    prismic={prismicCtx}
                  />
                </GridContainer>
              );
            }
            if (slice.slice_type === 'image_grid') {
              return (
                <CareerGridParent key={uniqid()}>
                  {slice.items.map((content, i) => (
                    <LoadableCareerGrid
                      key={uniqid()}
                      item={content}
                      index={i}
                    />
                  ))}
                </CareerGridParent>
              );
            }
            if (slice.slice_type === 'vacancies') {
              return (
                <GridContainer key={uniqid()}>
                  <div
                    id="vacancies"
                    ref={e => {
                      this.vacancyRef = e;
                    }}
                  >
                    <LoadableInsetHeading text="Vacancies" />
                    <VacanciesWrapper>
                      {slice.items.map(item => (
                        <Vacancy
                          key={uniqid()}
                          to={`/careers/${item.link_to_job_vacancies.uid}`}
                        >
                          <span>
                            {PrismicReact.RichText.render(
                              item.job_title,
                              PrismicConfig.linkResolver
                            )}
                            {PrismicReact.RichText.render(
                              item.location,
                              PrismicConfig.linkResolver
                            )}
                          </span>
                          <ArrowWrapper>
                            <LoadableArrow />
                          </ArrowWrapper>
                        </Vacancy>
                      ))}
                    </VacanciesWrapper>
                  </div>
                </GridContainer>
              );
            }
            return null;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({ scrollState: state.scrollState });

export default connect(mapStateToProps)(Careers);
