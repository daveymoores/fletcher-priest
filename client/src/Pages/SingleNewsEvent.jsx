import React from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import ReactGA from "react-ga";

const LoadableNews = Loadable({
    loader: () => import("./News"),
    loading: () => null
});

const LoadableEvent = Loadable({
    loader: () => import("./Event"),
    loading: () => null
});

const LoadableNotFound = Loadable({
    loader: () => import("./NotFound"),
    loading: () => null
});

export default class SingleNewsEvent extends React.Component {
    static findTemplate(results, prismicCtx, location, notFound) {
        const pagePath = location.pathname.replace("/news-and-events/", "");
        let template;
        results.map(result => {
            if (pagePath === result.uid) {
                if (result.type === "single_news_item") {
                    template = (
                        <LoadableNews content={result} prismic={prismicCtx} />
                    );
                } else if (result.type === "single_events_page") {
                    template = (
                        <LoadableEvent content={result} prismic={prismicCtx} />
                    );
                } else if (notFound) {
                    template = <LoadableNotFound />;
                }
            }
        });

        return template;
    }

    state = {
        results: null,
        notFound: false
    };

    componentWillMount() {
        this.fetchPage(this.props);
    }

    componentDidMount() {
        const { location } = this.props;
        ReactGA.pageview(location.pathname);
    }

    componentWillReceiveProps(props) {
        this.fetchPage(props);
    }

    componentDidUpdate() {
        const { prismicCtx } = this.props;
        prismicCtx.toolbar();
    }

    fetchPage(props) {
        const { prismicCtx, location } = props;
        const pagePath = location.pathname.replace("/news-and-events/", "");

        if (props.prismicCtx) {
            return props.prismicCtx.api
                .query([
                    Prismic.Predicates.at("my.single_events_page.uid", pagePath)
                ])
                .then(response => {
                    const { results } = response;

                    if (!results.length) {
                        this.fetchNextPage(props);
                    } else {
                        this.setState({ results });
                    }
                });
        }
        return null;
    }

    fetchNextPage(props) {
        const { prismicCtx, location } = props;
        const pagePath = location.pathname.replace("/news-and-events/", "");

        if (props.prismicCtx) {
            return props.prismicCtx.api
                .query([
                    Prismic.Predicates.at("my.single_news_item.uid", pagePath)
                ])
                .then(response => {
                    const { results } = response;
                    this.setState({ results });
                });
        }
        return null;
    }

    render() {
        const { results, notFound } = this.state;
        const { prismicCtx, location } = this.props;

        if (results) {
            const template = SingleNewsEvent.findTemplate(
                results,
                prismicCtx,
                location,
                notFound
            );

            return <div data-wio-id={results.id}>{template}</div>;
        }
        return <h1>Loading</h1>;
    }
}
