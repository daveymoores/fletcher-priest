import React, { Component } from 'react';
import Loadable from 'react-loadable';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import styled, { withTheme } from 'styled-components';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';
import { Link } from 'react-router-dom';

import Facebook from '../Common/icons/facebook.svg';
import Linkedin from '../Common/icons/linkedin.svg';
import Instagram from '../Common/icons/instagram.svg';
import Twitter from '../Common/icons/twitter.svg';
import { FluidGrid, FluidRow, FluidGridParent } from '../Common/FluidGrid';
import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import Meta from '../Common/Meta';
import LoaderPageContent from '../Loaders/LoaderPageContent';
import Credits from '../Common/Credits';
import LinkContainer from '../SharedStyles/link-container-styles';

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const LoadableOfficeLocation = Loadable({
  loader: () => import('../Components/OfficeLocation'),
  loading: () => <LoaderPageContent />
});

const StyledLink = styled.a`
  display: flex;
  flex-direction: column;
  font-size: 18px;
  font-family: ${props => props.theme.FFDINWebProBold};
  cursor: pointer;

  span {
    display: block;
    margin-top: 30px;
    font-size: 22px;
  }
`;

const SocialLinks = styled.div`
  margin-bottom: 60px;
`;

const IconContainer = styled.div`
  position: relative;

  svg {
    height: 60px;
    width: 60px;
  }

  &::after {
    content: '';
    font-size: 18px;
    background-color: ${props => props.theme.primary};
    position: absolute;
    width: 36px;
    height: 4px;
    left: 0;
    bottom: -20px;
  }
`;

class Contact extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at('document.type', 'contact_page'))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results[0].data} />

          {results.map((item, index) => (
            <LoadableTextOnlyPageTitle
              key={item.id}
              pageTitle={PrismicReact.RichText.render(
                item.data.page_title,
                PrismicConfig.linkResolver
              )}
            />
          ))}

          <SocialLinks>
            <GridContainer type='pullRight'>
              <FluidGridParent>
                <FluidRow>
                  <FluidGrid type='3-col'>
                    <LinkContainer>
                      <StyledLink
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://www.facebook.com/Fletcher-Priest-Architects-211471152365334/'
                      >
                        <IconContainer>
                          <Facebook />
                        </IconContainer>
                        <span>Facebook</span>
                      </StyledLink>
                    </LinkContainer>
                  </FluidGrid>
                  <FluidGrid type='3-col'>
                    <LinkContainer>
                      <StyledLink
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://www.instagram.com/explore/locations/703799273094014/fletcher-priest-architects/?hl=en'
                      >
                        <IconContainer>
                          <Instagram />
                        </IconContainer>
                        <span>Instagram</span>
                      </StyledLink>
                    </LinkContainer>
                  </FluidGrid>
                  <FluidGrid type='3-col'>
                    <LinkContainer>
                      <StyledLink
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://twitter.com/FletcherPriest?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor'
                      >
                        <IconContainer>
                          <Twitter />
                        </IconContainer>
                        <span>Twitter</span>
                      </StyledLink>
                    </LinkContainer>
                  </FluidGrid>
                  <FluidGrid type='3-col'>
                    <LinkContainer>
                      <StyledLink
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://www.linkedin.com/company/fletcher-priest-architects'
                      >
                        <IconContainer>
                          <Linkedin />
                        </IconContainer>
                        <span>Linkedin</span>
                      </StyledLink>
                    </LinkContainer>
                  </FluidGrid>
                </FluidRow>
              </FluidGridParent>
            </GridContainer>
          </SocialLinks>

          {results[0].data.body.map((slice, index) => {
            if (slice.slice_type === 'office_location' && index % 2 === 0) {
              return (
                <GridContainer key={uniqid()} type='pullRight'>
                  <LoadableOfficeLocation side='right' content={slice} />
                </GridContainer>
              );
            }
            if (
              slice.slice_type === 'office_location' &&
              Math.abs(index % 2) === 1
            ) {
              return (
                <GridContainer key={uniqid()} type='pullLeft'>
                  <LoadableOfficeLocation side='left' content={slice} />
                </GridContainer>
              );
            }
            return true;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return <h1>Loading</h1>;
  }
}

export default withTheme(Contact);
