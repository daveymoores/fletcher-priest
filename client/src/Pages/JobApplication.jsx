import React, { Component } from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import styled from "styled-components";
import MetaTags from "react-meta-tags";
import { Grid, GridItem } from "styled-grid-responsive";
import ReactGA from "react-ga";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import SimpleReactValidator from "simple-react-validator";
import scrollToComponent from "react-scroll-to-component";

import {
  MobileSwapGrid,
  ShowForMobile,
  HideForMobile
} from "../SharedStyles/utils-styles";
import media from "../utils/style-utils";
import FormDragInput from "../Components/FormDragInput";
import GridContainer from "../Common/GridContainer";
import { setFilterState } from "../redux/actionCreators";
import LoaderPageContent from "../Loaders/LoaderPageContent";

const LoadableSocial = Loadable({
  loader: () => import("../Components/Social"),
  loading: () => null
});

const LoadablePageTitle = Loadable({
  loader: () => import("../Components/PageTitle"),
  loading: () => null
});

const LoadableArticleWrapper = Loadable({
  loader: () => import("../Components/ArticleWrapper"),
  loading: () => null
});

const LoadableSubmitButton = Loadable({
  loader: () => import("../Components/SubmitButton"),
  loading: () => null
});

const LoadableLoadingStateWrapper = Loadable({
  loader: () => import("../Components/LoadingStateWrapper"),
  loading: () => <LoaderPageContent />
});

const FormInput = styled.input`
  background-color: ${props => props.theme.lightGrey};
  border: none;
  font-family: ${props => props.theme.FFDINWebProMedium};
  width: 100%;
  margin-bottom: 20px;
  color: black;
  font-size: 18px;
  padding: 19px 20px 21px;

  ${media.tablet`
      padding: 22px 20px 25px;
    `};

  &::placeholder {
    color: black;
  }
`;

const Label = styled.label`
  font-family: ${props => props.theme.FFDINWebProMedium};
  font-size: 18px;
  color: black;
  margin: 10px 0 30px;
  display: block;

  ${media.tablet`
      margin: 30px 0;
    `};
`;

const ApplicationForm = styled.form`
  margin-top: 60px;
`;

class JobApplication extends Component {
  static isAdvancedUpload() {
    const div = document.createElement("div");
    const advanced =
      ("draggable" in div || ("ondragstart" in div && "ondrop" in div)) &&
      "FormData" in window &&
      "FileReader" in window
        ? 1
        : 0;

    return advanced;
  }

  constructor() {
    super();

    this.state = {
      results: null,
      droppedFiles: [],
      firstname: "",
      secondname: "",
      email: "",
      number: "",
      resume: 0,
      portfolio: 0,
      coverletter: 0
    };

    this.validator = new SimpleReactValidator({
      formSize5mb: {
        message: "The file size must not be greater than 5mb",
        rule: (val, options) => {
          if (val > 5000000) {
            return false;
          }
          return true;
        }
      },
      formSize15mb: {
        message: "The file size must not be greater than 15mb",
        rule: (val, options) => {
          if (val > 15000000) {
            return false;
          }
          return true;
        }
      }
    });

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.setFormRef = element => {
      this.formRef = element;
    };
  }

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  handleChange = (value, name, fileSize) => {
    const { droppedFiles } = this.state;
    value.forEach(e => {
      const reader = new FileReader();
      reader.readAsDataURL(e[0]);
      reader.onloadend = () => {
        this.setState({
          droppedFiles: [...droppedFiles, reader.result],
          [name]: fileSize
        });
      };
    });
  };

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(
          Prismic.Predicates.at("my.job_listing.uid", props.match.params.uid)
        )
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  async handleSubmit(e) {
    e.preventDefault();
    const { history, filterState, handleFilterChange } = this.props;
    const {
      droppedFiles,
      firstname,
      secondname,
      email,
      number,
      results
    } = this.state;

    if (this.validator.allValid()) {
      if (this.formRef.classList.contains("is-uploading")) return false;
      if (filterState === "complete") handleFilterChange("loading");

      this.formRef.classList.add("is-uploading");
      this.formRef.classList.remove("is-error");
      if (JobApplication.isAdvancedUpload) {
        const formPost = axios
          .post("/api/application", {
            applicationName: PrismicReact.RichText.asText(
              results[0].data.job_listing_title
            ),
            droppedFiles,
            firstname,
            secondname,
            email,
            number
          })
          .then(response => {
            if (response.status === 200) {
              if (filterState === "loading") handleFilterChange("complete");
              history.push("/confirmation");
            }
          })
          .catch(error => {
            if (filterState === "loading") handleFilterChange("complete");
            history.push("/error");
            console.log(error); // eslint-disable-line no-console
          });
      }
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      this.forceUpdate();
      scrollToComponent(this.formWrapperRef, {
        duration: 800,
        ease: "inOutQuad",
        align: "top"
      });
    }

    return null;
  }

  handleInput(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { location } = this.props;
    const {
      prismicCtx,
      results,
      firstname,
      secondname,
      email,
      number,
      resume,
      portfolio,
      coverletter
    } = this.state;

    if (results) {
      return (
        <div ref={node => (this.formWrapperRef = node)}>
          <LoadableArticleWrapper>
            <GridContainer>
              <MobileSwapGrid>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1 / 12
                  }}
                  col={1 / 6}
                >
                  <HideForMobile>
                    <LoadableSocial
                      title={`${PrismicReact.RichText.asText(
                        results[0].data.job_listing_title
                      )} Application`}
                    />
                  </HideForMobile>
                </GridItem>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 12 / 12
                  }}
                  col={2 / 3}
                >
                  <LoadablePageTitle
                    content={`${PrismicReact.RichText.asText(
                      results[0].data.job_listing_title
                    )} Application`}
                  />

                  <LoadableLoadingStateWrapper>
                    <ApplicationForm
                      encType="multipart/form-data"
                      onSubmit={this.handleSubmit}
                      innerRef={node => (this.formRef = node)}
                    >
                      <input type="hidden" name="_honeypot" value="" />

                      <Grid>
                        <GridItem
                          media={{
                            smallPhone: 1
                          }}
                          col={1 / 2}
                        >
                          <FormInput
                            type="text"
                            name="firstname"
                            placeholder="First Name"
                            className="form-control"
                            value={firstname}
                            onChange={this.handleInput}
                          />
                          {this.validator.message(
                            "first name",
                            firstname,
                            "required|alpha"
                          )}
                        </GridItem>
                        <GridItem
                          media={{
                            smallPhone: 1
                          }}
                          col={1 / 2}
                        >
                          <FormInput
                            type="text"
                            name="secondname"
                            placeholder="Second Name"
                            className="form-control"
                            value={secondname}
                            onChange={this.handleInput}
                          />

                          {this.validator.message(
                            "second name",
                            secondname,
                            "required|alpha"
                          )}
                        </GridItem>
                      </Grid>
                      <Grid>
                        <GridItem
                          media={{
                            smallPhone: 1
                          }}
                          col={2 / 3}
                        >
                          <FormInput
                            type="email"
                            name="email"
                            placeholder="Email address"
                            value={email}
                            onChange={this.handleInput}
                          />
                          {this.validator.message(
                            "email",
                            email,
                            "required|email"
                          )}
                        </GridItem>
                      </Grid>
                      <Grid>
                        <GridItem
                          media={{
                            smallPhone: 1
                          }}
                          col={1 / 2}
                        >
                          <FormInput
                            type="tel"
                            name="number"
                            value={number}
                            placeholder="Phone Number"
                            onChange={this.handleInput}
                          />
                          {this.validator.message("number", number, "phone")}
                        </GridItem>
                      </Grid>
                      <Grid>
                        <GridItem
                          media={{
                            smallPhone: 1,
                            mediumPhone: 1,
                            tablet: 1
                          }}
                          col={1 / 2}
                        >
                          <FormDragInput
                            results={results}
                            value={resume}
                            name="resume"
                            labelText="Resume (PDF max 5MB)"
                            onFileUpload={this.handleChange}
                          />
                          {this.validator.message(
                            "resume",
                            resume,
                            "formSize5mb"
                          )}
                        </GridItem>
                        <GridItem
                          media={{
                            smallPhone: 1,
                            mediumPhone: 1,
                            tablet: 1
                          }}
                          col={1 / 2}
                        >
                          <FormDragInput
                            results={results}
                            value={portfolio}
                            name="portfolio"
                            labelText="Portfolio (PDF max 15MB)"
                            onFileUpload={this.handleChange}
                          />
                          {this.validator.message(
                            "portfolio",
                            portfolio,
                            "formSize15mb"
                          )}
                        </GridItem>
                      </Grid>
                      <Grid>
                        <GridItem
                          media={{
                            smallPhone: 1,
                            mediumPhone: 1,
                            tablet: 1
                          }}
                          col={1 / 2}
                        >
                          <FormDragInput
                            results={results}
                            value={coverletter}
                            name="coverletter"
                            labelText="Cover letter (PDF max 5MB)"
                            onFileUpload={this.handleChange}
                          />
                          {this.validator.message(
                            "resume",
                            coverletter,
                            "formSize5mb"
                          )}
                        </GridItem>
                      </Grid>
                      <LoadableSubmitButton content="Submit application" />
                    </ApplicationForm>
                  </LoadableLoadingStateWrapper>
                </GridItem>
              </MobileSwapGrid>
            </GridContainer>
          </LoadableArticleWrapper>
        </div>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  filterState: state.filterState
});

const mapDispatchToProps = dispatch => ({
  handleFilterChange(value) {
    dispatch(setFilterState(value));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(JobApplication));
