import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Loadable from 'react-loadable';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import { Grid, GridItem } from 'styled-grid-responsive';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import BorderTopWrapper from '../Common/BorderWrapper';
import Meta from '../Common/Meta';
import LoaderHero from '../Loaders/LoaderHero';
import Credits from '../Common/Credits';

const LoadableSimilarProjectsCarousel = Loadable({
  loader: () => import('../Components/SimilarProjectsCarousel'),
  loading: () => null
});

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const RichTextWrapper = styled.section`
  p {
    margin-bottom: 30px;
  }

  h2,
  h3,
  h4,
  h5 {
    line-height: 1.4em;
    margin-bottom: 2em;
  }

  img {
    width: 100%;
    max-width: 100%;
    margin: 30px 0;
  }
`;

export default class PrivacyPolicy extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at('document.type', 'privacy_policy'), {
          fetchLinks: [
            'single_project_page.project_name',
            'single_project_page.client',
            'single_project_page.location',
            'single_project_page.image'
          ]
        })
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;

    if (results) {
      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results[0].data} />

          {results.map(item => (
            <React.Fragment key={item.id}>
              <Meta metaContent={item.data} />
              <LoadableTextOnlyPageTitle
                key={item.id}
                pageTitle={PrismicReact.RichText.render(
                  item.data.page_title,
                  PrismicConfig.linkResolver
                )}
              />
            </React.Fragment>
          ))}

          {results[0].data.body.map((slice, i, arr) => {
            if (slice.slice_type === 'rich_text') {
              return (
                <GridContainer key={uniqid()}>
                  <Grid>
                    <GridItem
                      media={{
                        smallPhone: 0,
                        mediumPhone: 1 / 12
                      }}
                      col={1 / 6}
                    />
                    <GridItem
                      media={{
                        smallPhone: 1,
                        mediumPhone: 10 / 12
                      }}
                      col={2 / 3}
                    >
                      <RichTextWrapper>
                        {PrismicReact.RichText.render(
                          slice.primary.rich_text,
                          PrismicConfig.linkResolver
                        )}
                      </RichTextWrapper>
                    </GridItem>
                  </Grid>
                </GridContainer>
              );
            }
            return null;
          })}

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return null;
  }
}
