import React, { Component } from 'react';
import Prismic from 'prismic-javascript';
import { Link, withRouter } from 'react-router-dom';
import PrismicReact from 'prismic-reactjs';
import Loadable from 'react-loadable';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import { FluidGrid, FluidRow, FluidGridParent } from '../Common/FluidGrid';
import Meta from '../Common/Meta';
import Credits from '../Common/Credits';

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const LoadableProjectGrid = Loadable({
  loader: () => import('../Components/ProjectGrid'),
  loading: () => null
});

class ProjectLanding extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.any('document.type', ['project_landing']))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location, history } = this.props;
    const rowArray = [];

    if (results) {
      return (
        <div data-wio-id={results.id}>
          {results
            .filter(item => item.type === 'project_landing')
            .map((item, index) => (
              <React.Fragment key={uniqid()}>
                <Meta metaContent={item.data} />
                <LoadableTextOnlyPageTitle
                  key={item.id}
                  pageTitle={PrismicReact.RichText.render(
                    item.data.page_title,
                    PrismicConfig.linkResolver
                  )}
                />
              </React.Fragment>
            ))}

          <LoadableProjectGrid
            history={history}
            prismicCtx={prismicCtx}
            PrismicReact={PrismicReact}
            location={location}
          />

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return null;
  }
}

export default withRouter(ProjectLanding);
