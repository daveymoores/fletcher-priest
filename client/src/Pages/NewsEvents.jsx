import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { Grid } from 'styled-grid-responsive';
import Loadable from 'react-loadable';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import Meta from '../Common/Meta';
import Credits from '../Common/Credits';

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const LoadableNewsEventsGrid = Loadable({
  loader: () => import('../Components/NewsEventsGrid'),
  loading: () => null
});

class NewsEvents extends Component {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.at('document.type', 'news__events_landing'))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;
    const { prismicCtx, location, history } = this.props;

    if (results) {
      return (
        <div>
          {results
            .filter(item => item.type === 'news__events_landing')
            .map((item, index) => (
              <React.Fragment key={item.id}>
                <Meta metaContent={item.data} />
                <LoadableTextOnlyPageTitle
                  key={item.id}
                  pageTitle={PrismicReact.RichText.render(
                    item.data.page_title,
                    PrismicConfig.linkResolver
                  )}
                />
              </React.Fragment>
            ))}

          <LoadableNewsEventsGrid
            history={history}
            prismicCtx={prismicCtx}
            PrismicReact={PrismicReact}
            location={location}
          />

          {results[0].data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return null;
  }
}

export default withRouter(NewsEvents);
