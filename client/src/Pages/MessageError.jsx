import React, { Component } from "react";
import styled from "styled-components";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import GridContainer from "../Common/GridContainer";
import { setFilterState } from "../redux/actionCreators";

import { FluidGrid, FluidRow, FluidGridParent } from "../Common/FluidGrid";
import LinkContainer from "../SharedStyles/link-container-styles";

const LoadableTextOnlyPageTitle = Loadable({
    loader: () => import("../Components/TextOnlyPageTitle"),
    loading: () => <div>Loading...</div>
});

const LoadableKeyProjectsCarousel = Loadable({
    loader: () => import("../Components/KeyProjectsCarousel"),
    loading: () => <div>Loading...</div>
});

const LoadableInsetHeading = Loadable({
    loader: () => import("../Components/InsetHeading"),
    loading: () => <div>Loading...</div>
});

const Header = styled.header`
    background-color: ${props => props.theme.primary};
    width: 100%;
    padding-bottom: 40px;
`;

const MailLink = styled.a`
    color: white;
`;

class MessageError extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: null
        };
    }

    componentWillMount() {
        this.fetchPage(this.props);
    }

    componentDidMount() {
        const { filterState, handleFilterChange } = this.props;
        if (filterState === "loading") handleFilterChange("complete");
    }

    componentWillReceiveProps(props) {
        this.fetchPage(props);
    }

    fetchPage(props) {
        if (props.prismicCtx) {
            return props.prismicCtx.api
                .query(
                    Prismic.Predicates.any("document.type", [
                        "sector",
                        "service"
                    ])
                )
                .then(response => {
                    const { results } = response;
                    this.setState({ results });
                });
        }
        return null;
    }

    render() {
        const { results } = this.state;
        const { prismicCtx } = this.props;

        if (results) {
            return (
                <div>
                    <Header>
                        <LoadableTextOnlyPageTitle
                            pageTitle={
                                <h1>
                                    {"Whoops"}
                                    <br />
                                    {"We've hit a snag"}
                                </h1>
                            }
                        />
                        <GridContainer>
                            <h2>
                                There was an error sending your application.
                                Please try again. If you continue to have
                                problems
                                <br />
                                please email{" "}
                                <MailLink href="mailTo:london@fletcherpriest.com">
                                    london@fletcherpriest.com
                                </MailLink>
                            </h2>
                        </GridContainer>
                    </Header>

                    <GridContainer type="pullRight">
                        <LoadableKeyProjectsCarousel
                            prismicCtx={prismicCtx}
                            location="/news-and-events"
                        />
                    </GridContainer>

                    <GridContainer type="pullRight">
                        <LoadableInsetHeading text="Services" />

                        <FluidGridParent>
                            <FluidRow>
                                {results.map(link => {
                                    if (link.type === "service") {
                                        return (
                                            <FluidGrid
                                                key={link.id}
                                                type="4-col"
                                            >
                                                <LinkContainer>
                                                    <Link
                                                        to={`/services/${
                                                            link.uid
                                                        }`}
                                                    >
                                                        {PrismicReact.RichText.asText(
                                                            link.data.page_title
                                                        )}
                                                    </Link>
                                                </LinkContainer>
                                            </FluidGrid>
                                        );
                                    }
                                    return null;
                                })}
                            </FluidRow>
                        </FluidGridParent>

                        <LoadableInsetHeading text="Sectors" />

                        <FluidGridParent>
                            <FluidRow>
                                {results.map(link => {
                                    if (link.type === "sector") {
                                        return (
                                            <FluidGrid
                                                key={link.id}
                                                type="4-col"
                                            >
                                                <LinkContainer>
                                                    <Link
                                                        to={`/sectors/${
                                                            link.uid
                                                        }`}
                                                    >
                                                        {PrismicReact.RichText.asText(
                                                            link.data.page_title
                                                        )}
                                                    </Link>
                                                </LinkContainer>
                                            </FluidGrid>
                                        );
                                    }
                                    return null;
                                })}
                            </FluidRow>
                        </FluidGridParent>

                        <LoadableInsetHeading text="Practice" />

                        <FluidGridParent>
                            <FluidRow>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/practice/approach">
                                            Approach
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/people">People</Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/practice/community">
                                            Community
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                            </FluidRow>
                            <FluidRow>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/practice/awards">
                                            Awards
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/publications">
                                            Publications
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/careers">Careers</Link>
                                    </LinkContainer>
                                </FluidGrid>
                            </FluidRow>
                        </FluidGridParent>
                    </GridContainer>
                </div>
            );
        }
        return <h1>Loading</h1>;
    }
}

const mapStateToProps = state => ({ filterState: state.filterState });

const mapDispatchToProps = dispatch => ({
    handleFilterChange(value) {
        dispatch(setFilterState(value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessageError);
