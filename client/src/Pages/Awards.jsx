// @flow

import React, { Component } from 'react';
import Loadable from 'react-loadable';
import Prismic from 'prismic-javascript';
import PrismicReact from 'prismic-reactjs';
import { withTheme } from 'styled-components';
import ReactGA from 'react-ga';

import { Grid, GridItem } from 'styled-grid-responsive';
import GridContainer from '../Common/GridContainer';
import {
  AwardRow,
  ProjectArrow,
  YearHeading,
  YearGroup,
  AwardWrapper
} from '../Components/award-row-styles';
import Meta from '../Common/Meta';
import LoaderSearchBar from '../Loaders/LoaderSearchBar';
import LoaderPageContent from '../Loaders/LoaderPageContent';
import Credits from '../Common/Credits';

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const LoadableSearchBarAwards = Loadable({
  loader: () => import('../Components/SearchBarAwards'),
  loading: () => <LoaderSearchBar />
});

const LoadableAwardArrow = Loadable({
  // $FlowFixMe
  loader: () => import('../Common/icons/award-arrow'),
  loading: () => null
});

const LoadableLoadingStateWrapper = Loadable({
  loader: () => import('../Components/LoadingStateWrapper'),
  loading: () => <LoaderPageContent />
});

type State = {
  results: any,
  landingResults: any,
  sortTerm: string
};

type Props = {
  location: Object,
  theme: Object,
  prismicCtx: () => {}
};

class Awards extends Component<Props, State> {
  state = {
    results: null,
    landingResults: null,
    sortTerm: `[my.awards.award_date desc]`
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props: Props) {
    const { results } = this.state;

    if (!results) {
      this.fetchPage(props);
    }
  }

  counter = 0;

  generateKey = () => this.counter++;

  searchCallback = (data: Object) => {
    this.updateState(data);
  };

  updateState(data: Object) {
    this.setState(prevState => {
      if (prevState.results !== data[0]) {
        return {
          results: data[0],
          sortTerm: data[1]
        };
      }
      return null;
    });
  }

  fetchPage(props: Props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.any('document.type', ['award_landing']), {
          fetchLinks: 'single_project_page.project_name',
          orderings: '[my.awards.award_date desc]'
        })
        .then(response => {
          const { results } = response;
          this.setState({ results, landingResults: results });
        });
    }
    return null;
  }

  fetchProject(uid: string) {
    const { prismicCtx } = this.props;
    let name;
    if (prismicCtx) {
      return prismicCtx.api
        .query(Prismic.Predicates.at('my.single_project_page.uid', uid))
        .then(response => {
          const { results } = response;
          name = PrismicReact.RichText.asText(results[0].data.project_name);
          return name;
        });
    }
    return null;
  }

  render() {
    const { results, landingResults, sortTerm } = this.state;
    const { prismicCtx, location, theme } = this.props;
    let combinedResults;
    const ctx = this;
    const yearArray = [];
    const awardNameArray = [];
    const awardProjectArray = [];
    let newYearArray = [];
    let newNameArray = [];
    let yearGroup = [];
    let nameGroup = [];
    let prevYear;
    let newYear;
    let prevName;
    let newName;

    function buildAwardGroupByYear(award, index, array, yArray) {
      prevYear =
        index > 0
          ? array[index - 1].data.award_date.split('-')[0]
          : award.data.award_date.split('-')[0];
      newYear = award ? award.data.award_date.split('-')[0] : false;

      if (!newYear) return null;

      function returnAwardRow(aw) {
        if ('data' in aw.data.project) {
          return (
            <AwardRow key={award.id}>
              <div>{PrismicReact.RichText.asText(aw.data.award_name)}</div>
              <div>{PrismicReact.RichText.asText(aw.data.award_category)}</div>
              <div>{award.data.award_place}</div>
              <div>
                {PrismicReact.RichText.asText(
                  aw.data.project.data.project_name
                )}
              </div>
              <ProjectArrow to={`/projects/${aw.data.project.uid}`}>
                <LoadableAwardArrow color={theme.primary} />
              </ProjectArrow>
            </AwardRow>
          );
        }

        return (
          <AwardRow key={award.id}>
            <div>{PrismicReact.RichText.asText(aw.data.award_name)}</div>
            <div>{PrismicReact.RichText.asText(aw.data.award_category)}</div>
            <div>{award.data.award_place}</div>
            <div />
            <ProjectArrow type='disabled' to='/projects/'>
              <LoadableAwardArrow color={theme.primary} />
            </ProjectArrow>
          </AwardRow>
        );
      }

      const awardRow = returnAwardRow(award);

      yearGroup.push(awardRow);
      newYearArray.push(award.data.award_date.split('-')[0]);
      // if the new year group has the same number as the test group
      if (
        newYearArray.filter(year => year === newYear).length ===
        yArray.filter(year => year === newYear).length
      ) {
        const y = (
          <YearHeading key={ctx.generateKey()}>
            {award.data.award_date.split('-')[0]}
          </YearHeading>
        );
        yearGroup.unshift(y);
        const g = yearGroup;
        yearGroup = [];
        newYearArray = [];
        return g;
      }

      return null;
    }

    function buildAwardGroupByName(award, index, array, nArray) {
      const n = PrismicReact.RichText.asText(award.data.award_name);

      prevName =
        index > 0
          ? PrismicReact.RichText.asText(array[index - 1].data.award_name)
          : n;
      newName = award ? n : false;

      if (!newName) return null;

      function returnAwardRow(aw) {
        if ('data' in aw.data.project) {
          return (
            <AwardRow key={award.id}>
              <div>{PrismicReact.RichText.asText(aw.data.award_name)}</div>
              <div>{PrismicReact.RichText.asText(aw.data.award_category)}</div>
              <div>{award.data.award_place}</div>
              <div>
                {PrismicReact.RichText.asText(
                  aw.data.project.data.project_name
                )}
              </div>
              <ProjectArrow to={`/projects/${aw.data.project.slug}`}>
                <LoadableAwardArrow color={theme.primary} />
              </ProjectArrow>
            </AwardRow>
          );
        }
        return (
          <AwardRow key={award.id}>
            <div>{PrismicReact.RichText.asText(aw.data.award_name)}</div>
            <div>{PrismicReact.RichText.asText(aw.data.award_category)}</div>
            <div>{award.data.award_place}</div>
            <div />
            <ProjectArrow type='disabled' to='/projects/'>
              <LoadableAwardArrow color={theme.primary} />
            </ProjectArrow>
          </AwardRow>
        );
      }

      const awardRow = returnAwardRow(award);

      nameGroup.push(awardRow);
      newNameArray.push(n);

      // if the new year group has the same number as the test group
      if (
        newNameArray.filter(name => name === newName).length ===
        nArray.filter(name => name === newName).length
      ) {
        const y = <YearHeading key={ctx.generateKey()}>{n}</YearHeading>;
        nameGroup.unshift(y);
        const g = nameGroup;
        nameGroup = [];
        newNameArray = [];
        return g;
      }

      return null;
    }

    function buildAwardGroupByProject(award, index, array, nArray) {
      const n = PrismicReact.RichText.asText(
        award.data.project.data.project_name
      );

      prevName =
        index > 0
          ? PrismicReact.RichText.asText(
              array[index - 1].data.project.data.project_name
            )
          : n;
      newName = award ? n : false;

      if (!newName) return null;

      function returnAwardRow(aw) {
        if ('data' in aw.data.project) {
          return (
            <AwardRow key={award.id}>
              <div>{PrismicReact.RichText.asText(aw.data.award_name)}</div>
              <div>{PrismicReact.RichText.asText(aw.data.award_category)}</div>
              <div>{award.data.award_place}</div>
              <div>
                {PrismicReact.RichText.asText(
                  aw.data.project.data.project_name
                )}
              </div>
              <ProjectArrow to={`/projects/${aw.data.project.slug}`}>
                <LoadableAwardArrow color={theme.primary} />
              </ProjectArrow>
            </AwardRow>
          );
        }
        return (
          <AwardRow key={award.id}>
            <div>{PrismicReact.RichText.asText(aw.data.award_name)}</div>
            <div>{PrismicReact.RichText.asText(aw.data.award_category)}</div>
            <div>{award.data.award_place}</div>
            <div />
            <ProjectArrow type='disabled' to='/projects/'>
              <LoadableAwardArrow color={theme.primary} />
            </ProjectArrow>
          </AwardRow>
        );
      }

      const awardRow = returnAwardRow(award);

      nameGroup.push(awardRow);
      newNameArray.push(n);

      // if the new year group has the same number as the test group
      if (
        newNameArray.filter(name => name === newName).length ===
        nArray.filter(name => name === newName).length
      ) {
        const y = <YearHeading key={ctx.generateKey()}>{n}</YearHeading>;
        nameGroup.unshift(y);
        const g = nameGroup;
        nameGroup = [];
        newNameArray = [];
        return g;
      }

      return null;
    }

    function checkResults(res) {
      return (
        <GridContainer>
          <Grid>
            <GridItem col={12 / 12}>
              <AwardWrapper>
                {res
                  .filter(
                    item =>
                      item.type === 'awards' && !item.data.project.isBroken
                  )
                  .map((award, index, array) => {
                    if (sortTerm === `[my.awards.award_date desc]`) {
                      const x = buildAwardGroupByYear(
                        award,
                        index,
                        array,
                        yearArray
                      );

                      if (x !== null) {
                        return <YearGroup key={award.id}>{x}</YearGroup>;
                      }
                    }

                    if (sortTerm === `[my.awards.award_name desc]`) {
                      const x = buildAwardGroupByName(
                        award,
                        index,
                        array,
                        awardNameArray
                      );

                      if (x !== null) {
                        return <YearGroup key={award.id}>{x}</YearGroup>;
                      }
                    }

                    if (
                      sortTerm ===
                      `[my.awards.single_project_page.project_name]`
                    ) {
                      return (
                        <YearGroup key={award.id}>
                          {buildAwardGroupByProject(
                            award,
                            index,
                            array,
                            awardProjectArray
                          )}
                        </YearGroup>
                      );
                    }
                    return null;
                  })}
              </AwardWrapper>
            </GridItem>
          </Grid>
        </GridContainer>
      );
    }

    if (results) {
      // if the search bar has rendered...
      combinedResults =
        results.length > 1 ? results.concat(landingResults) : results;

      combinedResults
        .filter(item => item.type === 'awards' && !item.data.project.isBroken)
        .map(award => {
          const AwardYear = award.data.award_date.split('-')[0];
          const AwardName = PrismicReact.RichText.asText(award.data.award_name);
          // const AwardProject = function returnProjectName() {
          //     if ("project_name" in award.data.project.data) {
          //         return PrismicReact.RichText.asText(
          //             award.data.project.data.project_name
          //         );
          //     }
          //     return null;
          // };

          yearArray.push(award.data.award_date.split('-')[0]);
          awardNameArray.push(AwardName);
          // awardProjectArray.push(AwardProject());
        });

      return (
        <div data-wio-id={results.id}>
          {combinedResults
            .filter(item => item.type === 'award_landing')
            .map(result => (
              <React.Fragment key={this.generateKey()}>
                <Meta metaContent={result.data} />
              </React.Fragment>
            ))}
          <LoadableTextOnlyPageTitle pageTitle={<h1>Awards</h1>} />
          <LoadableSearchBarAwards
            prismicCtx={prismicCtx}
            location={location}
            callback={this.searchCallback}
          >
            <LoadableLoadingStateWrapper>
              {checkResults(combinedResults)}
            </LoadableLoadingStateWrapper>
          </LoadableSearchBarAwards>

          {landingResults &&
            landingResults.length &&
            landingResults[0].data.body.map(slice => {
              if (slice.slice_type === 'photo_credits') {
                return <Credits key={this.generateKey()} data={slice} />;
              }
              return true;
            })}
        </div>
      );
    }
    return null;
  }
}

export default withTheme(Awards);
