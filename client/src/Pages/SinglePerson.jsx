import React from 'react';
import PrismicReact from 'prismic-reactjs';
import Loadable from 'react-loadable';
import MetaTags from 'react-meta-tags';
import styled from 'styled-components';
import { Grid, GridItem } from 'styled-grid-responsive';
import ReactGA from 'react-ga';
import media from '../utils/style-utils';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import Meta from '../Common/Meta';
import Credits from '../Common/Credits';

const LoadableKeyProjectsCarousel = Loadable({
  loader: () => import('../Components/KeyProjectsCarousel'),
  loading: () => null
});

const LoadableArticleWrapper = Loadable({
  loader: () => import('../Components/ArticleWrapper'),
  loading: () => null
});

const LoadableBackButton = Loadable({
  loader: () => import('../Components/BackButton'),
  loading: () => null
});

const LoadableNotFound = Loadable({
  loader: () => import('./NotFound'),
  loading: () => null
});

const ImageFormat = styled.div`
  min-height: 500px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  position: relative;
  padding: 25px;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  width: calc(100% + 40px);
  margin-left: -20px;
  margin-top: -25px;

  ${media.tablet`
        min-height: 800px;
        margin-top: 0px;
        width: 100%;
    `};
`;

const Name = styled.h1`
  color: ${props => props.theme.secondary};
  font-size: 45px;
  line-height: 45px;
  font-family: ${props => props.theme.FFDINWebProMedium};
  margin-top: 0;
  position: relative;

  &::after {
    content: '';
    width: 40px;
    height: 4px;
    position: absolute;
    bottom: -0.4em;
    left: 0;
    background-color: ${props => props.theme.primary};
  }

  ${media.tablet`
      font-size: 70px;
      line-height: 60px;
    `};
`;

const OtherInfo = styled.h4`
  color: ${props => props.theme.secondary};
  font-size: 18px;
  width: 50%;
`;

const TextContainer = styled.div`
  padding-top: 20px;

  ${media.tablet`
        padding: 40px;
    `};

  p {
    font-family: ${props => props.theme.FFDINWebProMedium};
  }

  h2 {
    font-size: 26px;
    margin-bottom: 40px;

    ${media.tablet`
            font-size: 38px;
        `};
  }
`;

const BorderWrapper = styled.div`
  border-top: 1px solid ${props => props.theme.darkGrey2};
  margin-top: 60px;
`;

export default class SingleProject extends React.Component {
  state = {
    doc: null,
    notFound: false
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  componentDidUpdate() {
    const { prismicCtx } = this.props;
    prismicCtx.toolbar();
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api.getByUID(
        'person',
        props.match.params.uid,
        {},
        (err, doc) => {
          if (doc) {
            this.setState({ doc });
          } else {
            this.setState({ notFound: !doc });
          }
        }
      );
    }
    return null;
  }

  render() {
    const { doc, notFound } = this.state;
    const { prismicCtx, location } = this.props;

    if (doc) {
      return (
        <div data-wio-id={doc.id}>
          <LoadableArticleWrapper>
            <Meta metaContent={doc.data} />

            <GridContainer>
              <Grid>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1,
                    tablet: 1
                  }}
                  col={1 / 2}
                >
                  <img
                    src={doc.data.headshot.url}
                    style={{ width: `100%` }}
                    alt=''
                  />
                </GridItem>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1,
                    tablet: 1
                  }}
                  col={1 / 2}
                >
                  <TextContainer>
                    <Name>
                      {doc.data.first_name} {doc.data.middle_name}{' '}
                      {doc.data.last_name}
                    </Name>
                    <OtherInfo>{doc.data.job_title}</OtherInfo>
                    <OtherInfo>
                      {PrismicReact.RichText.asText(
                        doc.data.degree___education
                      )}
                    </OtherInfo>

                    {PrismicReact.RichText.render(
                      doc.data.person_intro,
                      PrismicConfig.linkResolver
                    )}

                    {PrismicReact.RichText.render(
                      doc.data.description,
                      PrismicConfig.linkResolver
                    )}

                    <LoadableBackButton slug='/people' destination='People' />
                  </TextContainer>
                </GridItem>
              </Grid>
            </GridContainer>

            <BorderWrapper>
              <GridContainer type='pullRight'>
                <LoadableKeyProjectsCarousel
                  prismicCtx={prismicCtx}
                  location={location}
                />
              </GridContainer>
            </BorderWrapper>
          </LoadableArticleWrapper>
          {doc.data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    if (notFound) {
      return <LoadableNotFound />;
    }
    return <h1>Loading</h1>;
  }
}
