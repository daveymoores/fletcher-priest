import React, { Component } from "react";
import styled from "styled-components";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import GridContainer from "../Common/GridContainer";

import { FluidGrid, FluidRow, FluidGridParent } from "../Common/FluidGrid";
import LinkContainer from "../SharedStyles/link-container-styles";

const LoadableTextOnlyPageTitle = Loadable({
    loader: () => import("../Components/TextOnlyPageTitle"),
    loading: () => <div>Loading...</div>
});

const LoadableKeyProjectsCarousel = Loadable({
    loader: () => import("../Components/KeyProjectsCarousel"),
    loading: () => <div>Loading...</div>
});

const LoadableInsetHeading = Loadable({
    loader: () => import("../Components/InsetHeading"),
    loading: () => <div>Loading...</div>
});

const Header = styled.header`
    background-color: ${props => props.theme.primary};
    width: 100%;
    padding-bottom: 40px;
`;

const MailLink = styled.a`
    color: white;
`;

export default class NotFound extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: null
        };
    }

    componentWillMount() {
        this.fetchPage(this.props);
    }

    componentWillReceiveProps(props) {
        this.fetchPage(props);
    }

    fetchPage(props) {
        if (props.prismicCtx) {
            return props.prismicCtx.api
                .query(
                    Prismic.Predicates.any("document.type", [
                        "sector",
                        "service"
                    ])
                )
                .then(response => {
                    const { results } = response;
                    this.setState({ results });
                });
        }
        return null;
    }

    render() {
        const { results } = this.state;
        const { prismicCtx } = this.props;

        if (results) {
            return (
                <div>
                    <Header>
                        <LoadableTextOnlyPageTitle
                            pageTitle={
                                <h1>
                                    404-
                                    <br />
                                    Page not found
                                </h1>
                            }
                        />
                        <GridContainer>
                            <h2>
                                You may find what you’re looking for below,
                                otherwise
                                <br />
                                please email{" "}
                                <MailLink href="mailTo:london@fletcherpriest.com">
                                    london@fletcherpriest.com
                                </MailLink>
                            </h2>
                        </GridContainer>
                    </Header>

                    <GridContainer type="pullRight">
                        <LoadableKeyProjectsCarousel
                            prismicCtx={prismicCtx}
                            location="/news-and-events"
                        />
                    </GridContainer>

                    <GridContainer type="pullRight">
                        <LoadableInsetHeading text="Services" />

                        <FluidGridParent>
                            <FluidRow>
                                {results.map(link => {
                                    if (link.type === "service") {
                                        return (
                                            <FluidGrid
                                                key={link.id}
                                                type="3-col"
                                            >
                                                <LinkContainer>
                                                    <Link
                                                        to={`/services/${
                                                            link.uid
                                                        }`}
                                                    >
                                                        {PrismicReact.RichText.asText(
                                                            link.data.page_title
                                                        )}
                                                    </Link>
                                                </LinkContainer>
                                            </FluidGrid>
                                        );
                                    }
                                    return null;
                                })}
                            </FluidRow>
                        </FluidGridParent>

                        <LoadableInsetHeading text="Sectors" />

                        <FluidGridParent>
                            <FluidRow>
                                {results.map(link => {
                                    if (link.type === "sector") {
                                        return (
                                            <FluidGrid
                                                key={link.id}
                                                type="3-col"
                                            >
                                                <LinkContainer>
                                                    <Link
                                                        to={`/sectors/${
                                                            link.uid
                                                        }`}
                                                    >
                                                        {PrismicReact.RichText.asText(
                                                            link.data.page_title
                                                        )}
                                                    </Link>
                                                </LinkContainer>
                                            </FluidGrid>
                                        );
                                    }
                                    return null;
                                })}
                            </FluidRow>
                        </FluidGridParent>

                        <LoadableInsetHeading text="Practice" />

                        <FluidGridParent>
                            <FluidRow>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/practice/approach">
                                            Approach
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/people">People</Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/practice/community">
                                            Community
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/practice/awards">
                                            Awards
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/publications">
                                            Publications
                                        </Link>
                                    </LinkContainer>
                                </FluidGrid>
                                <FluidGrid type="4-col">
                                    <LinkContainer>
                                        <Link to="/careers">Careers</Link>
                                    </LinkContainer>
                                </FluidGrid>
                            </FluidRow>
                        </FluidGridParent>
                    </GridContainer>
                </div>
            );
        }
        return <h1>Loading</h1>;
    }
}
