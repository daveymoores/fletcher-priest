// @flow

import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Loadable from "react-loadable";
import Prismic from "prismic-javascript";
import { Link } from "react-router-dom";
import ReactGA from "react-ga";

import PrismicConfig from "../prismic-configuration";
import GridContainer from "../Common/GridContainer";
import { FluidGrid, FluidRow, FluidGridParent } from "../Common/FluidGrid";
import LinkContainer from "../SharedStyles/link-container-styles";
import Meta from "../Common/Meta";
import LoaderPageContent from "../Loaders/LoaderPageContent";
import LoaderHero from "../Loaders/LoaderHero";
import Credits from "../Common/Credits";

const LoadableHero = Loadable({
  loader: () => import("../Common/Hero"),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import("../Common/VideoHero"),
  loading: () => <LoaderHero />
});

const LoadableInsetHeading = Loadable({
  loader: () => import("../Components/InsetHeading"),
  loading: () => null
});

const LoadableTwoImagePlusText = Loadable({
  loader: () => import("../Components/TwoImagePlusText"),
  loading: () => <LoaderPageContent />
});

const LoadableLightBackgroundImageLeftRight = Loadable({
  loader: () => import("../Components/LightBackgroundImageLeftRight"),
  loading: () => <LoaderPageContent />
});

const LoadableDarkBackgroundWithImage = Loadable({
  loader: () => import("../Components/DarkBackgroundWithImage"),
  loading: () => <LoaderPageContent />
});

type Props = {
  location: any,
  prismicCtx: () => {}
};

type State = {
  results: ?Object
};

export default class Approach extends Component<Props, State> {
  state = {
    results: null
  };

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props: Props) {
    this.fetchPage(props);
  }

  counter = 0;

  fetchPage(props: Props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.any("document.type", ["approach", "service"]))
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  generateKey = () => this.counter++;

  render() {
    const { results } = this.state;
    const { prismicCtx, location } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;

    if (results) {
      return (
        <div data-wio-id={results.id}>
          {results
            .filter(item => item.type === "approach")
            .map(result => (
              <Meta key={this.generateKey()} metaContent={result.data} />
            ))}

          {results
            .filter(item => item.type === "approach")
            .map(item => {
              pageTitle = PrismicReact.RichText.render(
                item.data.page_title,
                PrismicConfig.linkResolver
              );

              return (
                <React.Fragment key={this.generateKey()}>
                  {item.data.body.map(slice => {
                    if (slice.slice_type === "masthead_image") {
                      landingPageIntro = PrismicReact.RichText.render(
                        slice.primary.masthead_intro_text,
                        PrismicConfig.linkResolver
                      );
                      return (
                        <LoadableHero
                          key={this.generateKey()}
                          content={slice.primary.image}
                          color={slice.primary.title_colour}
                          intro={landingPageIntro}
                          title={pageTitle}
                        />
                      );
                    }
                    if (slice.slice_type === "masthead_video") {
                      landingPageIntro = PrismicReact.RichText.render(
                        slice.primary.masthead_intro_text,
                        PrismicConfig.linkResolver
                      );
                      return (
                        <LoadableVideoHero
                          key={this.generateKey()}
                          content={slice}
                          color={slice.primary.title_colour}
                          intro={landingPageIntro}
                          title={pageTitle}
                        />
                      );
                    }
                    if (slice.slice_type === "dark_background_with_image") {
                      return (
                        <LoadableDarkBackgroundWithImage
                          key={this.generateKey()}
                          content={slice}
                          prismic={prismicCtx}
                        />
                      );
                    }
                    if (
                      slice.slice_type === "light_background_image_left_right"
                    ) {
                      return (
                        <LoadableLightBackgroundImageLeftRight
                          key={this.generateKey()}
                          content={slice}
                          prismic={prismicCtx}
                        />
                      );
                    }
                    return true;
                  })}
                </React.Fragment>
              );
            })}

          <GridContainer type="pullRight">
            <LoadableInsetHeading text="Find out more about each service" />

            <FluidGridParent>
              <FluidRow>
                {results
                  .filter(item => item.type === "service")
                  .map(link => (
                    <FluidGrid key={link.id} type="3-col">
                      <LinkContainer>
                        <Link to={`/services/${link.uid}`}>
                          {PrismicReact.RichText.asText(link.data.page_title)}
                        </Link>
                      </LinkContainer>
                    </FluidGrid>
                  ))}
              </FluidRow>
            </FluidGridParent>
          </GridContainer>

          {results[0].data.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits key={this.generateKey()} data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    return <LoaderPageContent />;
  }
}
