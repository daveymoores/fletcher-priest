import React, { Component } from "react";
import PrismicReact from "prismic-reactjs";
import Prismic from "prismic-javascript";
import Loadable from "react-loadable";
import { Grid } from "styled-grid-responsive";
import styled from "styled-components";
import debounce from "lodash/debounce";
import ReactGA from "react-ga";

import GridContainer from "../Common/GridContainer";
import { FluidGrid, FluidRow, FluidGridParent } from "../Common/FluidGrid";
import LoaderGridItem from "../Loaders/LoaderGridItem";
import LoaderSearchBar from "../Loaders/LoaderSearchBar";
import LoaderPageContent from "../Loaders/LoaderPageContent";

const LoadableTextOnlyPageTitle = Loadable({
    loader: () => import("../Components/TextOnlyPageTitle"),
    loading: () => null
});

const LoadablePublication = Loadable({
    loader: () => import("../Components/Publication"),
    loading: () => <LoaderGridItem />
});

const LoadableOffCanvas = Loadable({
    loader: () => import("../Components/OffCanvas"),
    loading: () => null
});

const LoadableSearchBarPublications = Loadable({
    loader: () => import("../Components/SearchBarPublications"),
    loading: () => <LoaderSearchBar />
});

const LoadableLoadingStateWrapper = Loadable({
    loader: () => import("../Components/LoadingStateWrapper"),
    loading: () => <LoaderPageContent />
});

const NoResults = styled.h3`
    text-align: center;
    display: block;
    margin: 60px auto 200px;
    max-width: 100%;
    background-color: white;

    strong {
        font-family: ${props => props.theme.FFDINWebProBold};
    }
`;

export default class Publications extends Component {
    constructor() {
        super();

        this.state = {
            results: null,
            textSearch: "",
            offCanvas: "closed",
            viewport: { x: window.innerWidth, y: window.innerHeight },
            rowSize: 3
        };

        this.handleResize = debounce(this.handleResize, 100);
    }

    componentWillMount() {
        this.handleResize();
    }

    componentDidMount() {
        const { location } = this.props;
        ReactGA.pageview(location.pathname);
        window.addEventListener("resize", this.handleResize, false);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize, false);
    }

    handleResize = () => {
        const { viewport } = this.state;
        let rowSize;

        if (viewport.x > 768) {
            rowSize = 3;
        } else if (viewport.x <= 768 && viewport.x >= 576) {
            rowSize = 2;
        } else {
            rowSize = 1;
        }

        this.setState(() => ({
            viewport: { x: window.innerWidth, y: window.innerHeight },
            rowSize
        }));
    };

    searchCallback = data => {
        this.updateState(data);
    };

    updateState(data) {
        this.setState(prevState => {
            if (prevState.results !== data[0]) {
                return {
                    results: data[0],
                    textSearch: data[1]
                };
            }
            return null;
        });
    }

    toggleOffCanvas(x) {
        const { offCanvas } = this.state;
        this.setState({
            offCanvas: x
        });
    }

    render() {
        const {
            results,
            textSearch,
            offCanvas,
            location,
            rowSize
        } = this.state;
        const { prismicCtx, history } = this.props;
        let rowArray = [];
        const ctx = this;

        function returnBoxStyle(item, i, arr) {
            const remainder = arr.length % rowSize;
            const totalRows = Math.floor(arr.length / rowSize);

            if (i < totalRows * rowSize) {
                rowArray.push(
                    <FluidGrid key={item.id}>
                        <LoadablePublication
                            key={item.id}
                            publication={item}
                            prismicCtx={prismicCtx}
                            PrismicReact={PrismicReact}
                            setOffCanvas={ctx.toggleOffCanvas.bind(ctx)}
                        />
                    </FluidGrid>
                );

                if (rowArray.length === rowSize) {
                    const newRow = rowArray;
                    rowArray = [];
                    return newRow;
                }

                return null;
            }

            rowArray.push(
                <FluidGrid key={item.id}>
                    <LoadablePublication
                        key={item.id}
                        publication={item}
                        prismicCtx={prismicCtx}
                        PrismicReact={PrismicReact}
                        setOffCanvas={ctx.toggleOffCanvas.bind(ctx)}
                    />
                </FluidGrid>
            );

            if (rowArray.length === remainder) {
                const newRow = rowArray;
                rowArray = [];
                return newRow;
            }

            return <LoaderPageContent />;
        }

        function checkResults(res) {
            if (res) {
                if (res.length > 0) {
                    return res.map((item, index, array) => {
                        const boxStyle = returnBoxStyle(item, index, array);

                        if (boxStyle !== null && Array.isArray(boxStyle)) {
                            return (
                                <FluidRow key={item.id}>{boxStyle}</FluidRow>
                            );
                        }
                        return null;
                    });
                }
                return (
                    <NoResults>
                        No results found for
                        <br /> search term <strong>{textSearch}</strong>
                    </NoResults>
                );
            }
            return null;
        }

        return (
            <div>
                <LoadableOffCanvas history={history} behaviour={offCanvas}>
                    <LoadableTextOnlyPageTitle
                        pageTitle={<h1>Publications</h1>}
                    />

                    <LoadableSearchBarPublications
                        prismicCtx={prismicCtx}
                        location={location}
                        callback={this.searchCallback}
                    >
                        <FluidGridParent>
                            <LoadableLoadingStateWrapper>
                                {checkResults(results)}
                            </LoadableLoadingStateWrapper>
                        </FluidGridParent>
                    </LoadableSearchBarPublications>
                </LoadableOffCanvas>
            </div>
        );
    }
}
