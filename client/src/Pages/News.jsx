import React, { Component } from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import { Grid, GridItem } from "styled-grid-responsive";
import MetaTags from "react-meta-tags";
import Moment from "moment";
import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2
} from "gsap/TweenMax";

import GridContainer from "../Common/GridContainer";
import PrismicConfig from "../prismic-configuration";
import media from "../utils/style-utils";
import {
  MobileSwapGrid,
  HideForMobile,
  ShowForMobile
} from "../SharedStyles/utils-styles";
import Meta from "../Common/Meta";
import DownloadLink from "../Components/DownloadLink";
import RichText from "../Components/RichText";
import PullQuote from "../Components/PullQuote";
import Credits from "../Common/Credits";

const LoadableFullWidthCarousel = Loadable({
  loader: () => import("../Components/FullWidthCarousel"),
  loading: () => null
});

const LoadableArticleWrapper = Loadable({
  loader: () => import("../Components/ArticleWrapper"),
  loading: () => null
});

const LoadableNewsEventsFeed = Loadable({
  loader: () => import("../Components/NewsEventsFeed"),
  loading: () => null
});

const LoadablePageTitle = Loadable({
  loader: () => import("../Components/PageTitle"),
  loading: () => null
});

const LoadableSocial = Loadable({
  loader: () => import("../Components/Social"),
  loading: () => null
});

const LoadableBackButton = Loadable({
  loader: () => import("../Components/BackButton"),
  loading: () => null
});

const Tag = styled.p`
  color: ${props => props.theme.primary};
  margin: 0px;
`;

const ReadLength = styled.p`
  color: ${props => props.theme.primary};
  margin: 0;
`;

const PubDate = styled.p`
  margin: 0;
`;

const FeaturedImg = styled.img`
  width: 100%;
  max-width: 100%;
  margin: 30px 0;
`;

const SocialWrapper = styled.div`
  position: relative;

  > div {
    position: relative;

    ${media.tablet`
          position: absolute;
          top: 0;
          left: 0;

        `};
  }
`;

const ListingAnimationWrapper = styled.div`
  opacity: 0;
`;

class News extends Component {
  constructor(props) {
    super();

    this.keyCounter = 0;
    this.getKey = this.getKey.bind(this);
  }

  componentDidMount() {
    if (this.listingRef) {
      const nodes = this.listingRef.getElementsByTagName("*");
      [].forEach.call(nodes, element => {
        Tween.set(element, { opacity: 0 });
      });
      Tween.set(this.listingRef, { opacity: 1 });
      Tween.staggerTo(nodes, 0.25, { opacity: 1, ease: Power2.ease }, 0.007);
    }
  }

  getKey() {
    return this.keyCounter++;
  }

  returnFeaturedImg = content => {
    if (content.data.featured_image.url) {
      return <FeaturedImg src={content.data.featured_image.url} alt="" />;
    }
    return null;
  };

  returnReadLength = time => {
    let t;

    if (time < 1) {
      return `${1} minute read`;
    }

    return `${Math.round(time)} minute read`;
  };

  returnCanonicalMeta = data => {
    if (data.canonical_url) {
      return (
        <MetaTags>
          <link rel="canonical" href={data.canonical_url.url} />
        </MetaTags>
      );
    }

    return null;
  };

  render() {
    const { content, prismic } = this.props;
    let formattedDate = "";

    if (content.data.publication_date) {
      const date = PrismicReact.Date(content.data.publication_date).toString();
      formattedDate = Moment(date, "ddd MMM DD YYYY HH:mm:ss ZZ").format("LL");
    }

    return (
      <div data-wio-id={content.id}>
        <ListingAnimationWrapper innerRef={el => (this.listingRef = el)}>
          <LoadableArticleWrapper>
            <Meta metaContent={content.data} />
            {this.returnCanonicalMeta(content.data)}
            <GridContainer>
              <MobileSwapGrid>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1 / 12
                  }}
                  col={1 / 6}
                >
                  <HideForMobile>
                    <SocialWrapper>
                      <LoadableSocial
                        title={PrismicReact.RichText.asText(content.data.title)}
                      />
                    </SocialWrapper>
                  </HideForMobile>
                </GridItem>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 12 / 12
                  }}
                  col={2 / 3}
                >
                  <Tag>{content.data.category}</Tag>
                  <LoadablePageTitle
                    content={PrismicReact.RichText.asText(content.data.title)}
                  />
                  <PubDate>{formattedDate}</PubDate>
                  <ReadLength>
                    {this.returnReadLength(content.data.read_length)}
                  </ReadLength>
                  {this.returnFeaturedImg(content)}
                  <DownloadLink url={content.data.press_release} />
                </GridItem>
              </MobileSwapGrid>
            </GridContainer>

            {content.data.body.map(slice => {
              if (slice.slice_type === "pull_quote") {
                return (
                  <PullQuote
                    PrismicReact={PrismicReact}
                    slice={slice}
                    PrismicConfig={PrismicConfig}
                    key={this.getKey()}
                  />
                );
              }
              if (slice.slice_type === "full_width_image_image_carousel") {
                return (
                  <LoadableFullWidthCarousel
                    key={this.getKey()}
                    prismic={prismic}
                    content={slice.items}
                  />
                );
              }
              if (slice.slice_type === "rich_text") {
                return (
                  <RichText
                    key={this.getKey()}
                    PrismicReact={PrismicReact}
                    slice={slice}
                    PrismicConfig={PrismicConfig}
                  />
                );
              }
              return true;
            })}

            <GridContainer>
              <Grid>
                <GridItem
                  media={{
                    smallPhone: 0,
                    mediumPhone: 1 / 12
                  }}
                  col={1 / 6}
                />
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 10 / 12
                  }}
                  col={2 / 3}
                >
                  <ShowForMobile>
                    <SocialWrapper>
                      <LoadableSocial
                        title={PrismicReact.RichText.asText(content.data.title)}
                      />
                    </SocialWrapper>
                  </ShowForMobile>
                  <LoadableBackButton
                    slug="/news-and-events"
                    destination="News &amp; Events"
                  />
                </GridItem>
              </Grid>
            </GridContainer>
            <LoadableNewsEventsFeed
              exclude={content.id}
              content={content}
              prismicCtx={prismic}
            />
          </LoadableArticleWrapper>
        </ListingAnimationWrapper>

        {content.data.body.map(slice => {
          if (slice.slice_type === "photo_credits") {
            return <Credits data={slice} />;
          }
          return true;
        })}
      </div>
    );
  }
}

export default News;
