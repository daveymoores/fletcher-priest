import React, { Component } from "react";
import Loadable from "react-loadable";
import PrismicReact from "prismic-reactjs";

import styled from "styled-components";
import { Grid, GridItem } from "styled-grid-responsive";
import { Link } from "react-router-dom";
import Moment from "moment";
import {
  TimelineMax as Timeline,
  TweenMax as Tween,
  Power2,
  Power3,
  Power4,
  Expo
} from "gsap/TweenMax";
import Meta from "../Common/Meta";
import media from "../utils/style-utils";

import {
  MobileSwapGrid,
  ShowForMobile,
  HideForMobile
} from "../SharedStyles/utils-styles";
import PrismicConfig from "../prismic-configuration";
import GridContainer from "../Common/GridContainer";
import Credits from "../Common/Credits";

const LoadableNewsEventsFeed = Loadable({
  loader: () => import("../Components/NewsEventsFeed"),
  loading: () => null
});

const LoadableArticleWrapper = Loadable({
  loader: () => import("../Components/ArticleWrapper"),
  loading: () => null
});

const LoadablePageTitle = Loadable({
  loader: () => import("../Components/PageTitle"),
  loading: () => null
});

const LoadableMap = Loadable({
  loader: () => import("../Components/Map"),
  loading: () => null
});

const LoadableSocial = Loadable({
  loader: () => import("../Components/Social"),
  loading: () => null
});

const LoadableBackButton = Loadable({
  loader: () => import("../Components/BackButton"),
  loading: () => null
});

const LoadableButton = Loadable({
  loader: () => import("../Components/Button"),
  loading: () => null
});

const Tag = styled.p`
  color: ${props => props.theme.primary};
  font-size: 16px;
  margin: 0px;
`;

const EventDetails = styled.dl`
  margin: 0px;
  padding: 0px;
  border-bottom: 1px solid ${props => props.theme.darkGrey};

  dt {
    color: ${props => props.theme.primary};
    font-size: 16px;
    margin-bottom: 10px;
  }
`;

const EventRow = styled.dd`
  font-family: ${props => props.theme.FFDINWebProMedium};
  font-size: 48px;
  color: black;
  margin: ${props => (props.type === "date" ? "0 0 15px" : "0 0 40px")};

  ${media.tablet`
      font-size: 38px;
    `};
`;

const VenueDetails = styled.div`
  position: relative;
  min-height: 200px;
  height: 20vw;
  margin-bottom: 80px;
`;

const ListingAnimationWrapper = styled.div`
  opacity: 0;
`;

const Location = styled.p`
  font-size: 14px;
  display: block;
  margin: 30px 0 20px;
`;

class Event extends Component {
  constructor(props) {
    super();

    this.keyCounter = 0;
    this.getKey = this.getKey.bind(this);
  }

  componentDidMount() {
    if (this.listingRef) {
      const nodes = this.listingRef.getElementsByTagName("*");
      [].forEach.call(nodes, element => {
        Tween.set(element, { opacity: 0 });
      });
      Tween.set(this.listingRef, { opacity: 1 });
      Tween.staggerTo(nodes, 0.25, { opacity: 1, ease: Power2.ease }, 0.007);
    }
  }

  getKey() {
    return this.keyCounter++;
  }

  render() {
    function splitDate(date) {
      const dd = date;
      return `${dd[0]}${dd[1]}:${dd[2]}${dd[3]}`;
    }

    const { content, prismic } = this.props;

    const FormatPrice = !content.data.event_price
      ? "Free"
      : `£${content.data.event_price}`;

    function returnFormattedTime(event) {
      const startDate = PrismicReact.Date(event.start_time_date);
      const endDate = PrismicReact.Date(event.end_time_date);
      const formattedStartTime = splitDate(Moment(startDate).format("kkmm"));
      const formattedEndTime = splitDate(Moment(endDate).format("kkmm"));

      const FormatTime = formattedEndTime
        ? `${formattedStartTime} - ${formattedEndTime}`
        : formattedStartTime;

      return FormatTime;
    }

    function returnFormattedDate(event) {
      const startDate = PrismicReact.Date(event.start_time_date);
      const formattedStartDate = Moment(startDate)
        .format("LL")
        .replace(",", "");

      return formattedStartDate;
    }

    const { location } = content.data;
    const venueName = content.data.venue_name;

    function clickToMap(data) {
      const url = `http://www.google.com/maps/place/${data[0]},${data[1]}`;
      const win = window.open(url, "_blank");
      win.focus();
    }

    const type = "url" in content.data.eventbright_link ? "active" : "inactive";

    return (
      <div data-wio-id={content.id}>
        <ListingAnimationWrapper innerRef={el => (this.listingRef = el)}>
          <LoadableArticleWrapper>
            <GridContainer>
              <Meta metaContent={content.data} />

              <MobileSwapGrid>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1 / 12
                  }}
                  col={1 / 6}
                >
                  <LoadableSocial
                    title={PrismicReact.RichText.asText(
                      content.data.event_name
                    )}
                  />
                </GridItem>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 12 / 12
                  }}
                  col={5 / 12}
                >
                  <Tag>Event</Tag>
                  <LoadablePageTitle
                    content={PrismicReact.RichText.asText(
                      content.data.event_name
                    )}
                  />
                  {PrismicReact.RichText.render(
                    content.data.event_description,
                    PrismicConfig.linkResolver
                  )}

                  <HideForMobile>
                    <LoadableBackButton
                      slug="/news-and-events"
                      destination="News &amp; Events"
                    />
                  </HideForMobile>
                </GridItem>
                <GridItem
                  media={{
                    smallPhone: 0,
                    mediumPhone: 1 / 12
                  }}
                  col={1 / 12}
                />
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 12 / 12
                  }}
                  col={1 / 3}
                >
                  <EventDetails>
                    {content.data.event_dates_and_times.map(event => (
                      <React.Fragment key={this.getKey()}>
                        <dt>Date & time</dt>
                        <EventRow type="date">
                          {returnFormattedDate(event)}
                        </EventRow>
                        <EventRow>{returnFormattedTime(event)}</EventRow>
                      </React.Fragment>
                    ))}
                    <dt>Price</dt>
                    <EventRow>{FormatPrice}</EventRow>
                  </EventDetails>

                  <VenueDetails
                    onClick={() => {
                      clickToMap([location.latitude, location.longitude]);
                    }}
                  >
                    <Location>{venueName[0].text}</Location>
                    <LoadableMap
                      location={[location.latitude, location.longitude]}
                      venue={venueName[0].text}
                    />
                  </VenueDetails>
                  <LoadableButton
                    type={type}
                    content="Register now"
                    destination={PrismicReact.Link.url(
                      content.data.eventbright_link
                    )}
                  />
                </GridItem>
              </MobileSwapGrid>

              <ShowForMobile>
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 1 / 12
                  }}
                  col={1 / 6}
                />
                <GridItem
                  media={{
                    smallPhone: 1,
                    mediumPhone: 12 / 12
                  }}
                  col={5 / 12}
                >
                  <LoadableBackButton
                    slug="/news-and-events"
                    destination="News &amp; Events"
                  />
                </GridItem>
              </ShowForMobile>
            </GridContainer>

            <LoadableNewsEventsFeed
              exclude={content.id}
              content={content}
              prismicCtx={prismic}
            />
          </LoadableArticleWrapper>
        </ListingAnimationWrapper>

        {content.body &&
          content.body.map(slice => {
            if (slice.slice_type === "photo_credits") {
              return <Credits data={slice} />;
            }
            return true;
          })}
      </div>
    );
  }
}

export default Event;
