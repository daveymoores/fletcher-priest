import React, { Component } from 'react';
import PrismicReact from 'prismic-reactjs';
import Loadable from 'react-loadable';
import Prismic from 'prismic-javascript';
import styled from 'styled-components';
import { Grid } from 'styled-grid-responsive';
import uniqid from 'uniqid';
import ReactGA from 'react-ga';

import PrismicConfig from '../prismic-configuration';
import GridContainer from '../Common/GridContainer';
import BorderTopWrapper from '../Common/BorderWrapper';
import BackButton from '../Components/BackButton';
import Meta from '../Common/Meta';
import LoaderHero from '../Loaders/LoaderHero';
import Credits from '../Common/Credits';

const LoadableSimilarProjectsCarousel = Loadable({
  loader: () => import('../Components/SimilarProjectsCarousel'),
  loading: () => null
});

const LoadableTextOnlyPageTitle = Loadable({
  loader: () => import('../Components/TextOnlyPageTitle'),
  loading: () => null
});

const LoadableHero = Loadable({
  loader: () => import('../Common/Hero'),
  loading: () => <LoaderHero />
});

const LoadableVideoHero = Loadable({
  loader: () => import('../Common/VideoHero'),
  loading: () => <LoaderHero />
});

const LoadableTwoImagePlusText = Loadable({
  loader: () => import('../Components/TwoImagePlusText'),
  loading: () => null
});

const LoadableLightBackgroundImageLeftRight = Loadable({
  loader: () => import('../Components/LightBackgroundImageLeftRight'),
  loading: () => null
});

const LoadableDarkBackgroundWithImage = Loadable({
  loader: () => import('../Components/DarkBackgroundWithImage'),
  loading: () => null
});

const LoadableNotFound = Loadable({
  loader: () => import('./NotFound'),
  loading: () => null
});

export default class SingleSector extends Component {
  constructor(props) {
    super();

    this.state = {
      results: null,
      notFound: false
    };

    this.keyCount = 0;
    this.getKey = this.getKey.bind(this);
  }

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    const { location } = this.props;
    ReactGA.pageview(location.pathname);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  getKey() {
    return this.keyCount++;
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      // We are using the function to get a document by its uid
      return props.prismicCtx.api.getByUID(
        'sector',
        props.match.params.uid,
        {
          fetchLinks: [
            'single_project_page.project_name',
            'single_project_page.client',
            'single_project_page.location',
            'single_project_page.image'
          ]
        },
        (err, results) => {
          if (results) {
            // We put the retrieved content in the state as a doc variable
            this.setState({ results });
          } else {
            // We changed the state to display error not found if no matched doc
            this.setState({ notFound: !results });
          }
        }
      );
    }
    return null;
  }

  render() {
    const { results, notFound } = this.state;
    const { prismicCtx, location } = this.props;
    let landingPageIntro = null;
    let pageTitle = null;
    let mastheadCheck = null;

    function returnSliceContent(slice) {
      if (slice.slice_type === 'dark_background_with_image') {
        return (
          <LoadableDarkBackgroundWithImage
            content={slice}
            prismic={prismicCtx}
          />
        );
      }
      if (slice.slice_type === 'light_background_image_left_right') {
        return (
          <LoadableLightBackgroundImageLeftRight
            content={slice}
            prismic={prismicCtx}
          />
        );
      }
      return null;
    }

    function mastHeadReplacement(item, bool) {
      if (!bool && item.data.page_title) {
        return (
          <LoadableTextOnlyPageTitle
            pageTitle={PrismicReact.RichText.render(
              item.data.page_title,
              PrismicConfig.linkResolver
            )}
          />
        );
      }
      return null;
    }

    if (results) {
      pageTitle = PrismicReact.RichText.render(
        results.data.page_title,
        PrismicConfig.linkResolver
      );

      const mastheadSliceCheck = Object.values(results.data.body);
      mastheadCheck = mastheadSliceCheck.map(item => {
        if (item.slice_type === 'masthead_image') {
          return true;
        }
        return false;
      });

      return (
        <div data-wio-id={results.id}>
          <Meta metaContent={results.data} />

          {mastHeadReplacement(results, mastheadCheck.indexOf(true) > -1)}

          {results.data.body.map(slice => {
            if (mastheadCheck.indexOf(true) > -1) {
              if (slice.slice_type === 'masthead_image') {
                landingPageIntro = slice.primary.masthead_intro_text.length
                  ? PrismicReact.RichText.render(
                      slice.primary.masthead_intro_text,
                      PrismicConfig.linkResolver
                    )
                  : null;
                return (
                  <LoadableHero
                    key={this.getKey()}
                    content={slice.primary.image}
                    color={slice.primary.title_colour}
                    intro={landingPageIntro}
                    title={pageTitle}
                    projectLink={slice.primary.project_link}
                  />
                );
              }
              if (slice.slice_type === 'masthead_video') {
                landingPageIntro = slice.primary.masthead_intro_text.length
                  ? PrismicReact.RichText.render(
                      slice.primary.masthead_intro_text,
                      PrismicConfig.linkResolver
                    )
                  : null;
                return (
                  <LoadableVideoHero
                    key={this.getKey()}
                    content={slice}
                    color={slice.primary.title_colour}
                    intro={landingPageIntro}
                    title={pageTitle}
                    projectLink={slice.primary.project_link}
                  />
                );
              }

              if (slice.slice_type === 'dark_background_with_image') {
                return (
                  <LoadableDarkBackgroundWithImage
                    key={this.getKey()}
                    content={slice}
                    prismic={prismicCtx}
                  />
                );
              }
              if (slice.slice_type === 'light_background_image_left_right') {
                return (
                  <LoadableLightBackgroundImageLeftRight
                    key={this.getKey()}
                    content={slice}
                    prismic={prismicCtx}
                  />
                );
              }
              return null;
            }
            if (slice.slice_type === 'dark_background_with_image') {
              return (
                <LoadableDarkBackgroundWithImage
                  key={this.getKey()}
                  content={slice}
                  prismic={prismicCtx}
                />
              );
            }
            if (slice.slice_type === 'light_background_image_left_right') {
              return (
                <LoadableLightBackgroundImageLeftRight
                  key={this.getKey()}
                  content={slice}
                  prismic={prismicCtx}
                />
              );
            }
            return null;
          })}
          <GridContainer>
            <BackButton slug="/sectors" destination="sectors" />
          </GridContainer>

          {results.data.body.map((slice, i, arr) => {
            if (slice.slice_type === 'similar_projects') {
              return (
                <BorderTopWrapper key={this.getKey()}>
                  <GridContainer key={this.getKey()} type="pullRight">
                    <LoadableSimilarProjectsCarousel
                      prismicCtx={prismicCtx}
                      location={location}
                      headingText="Selected Projects"
                      results={slice}
                    />
                  </GridContainer>
                </BorderTopWrapper>
              );
            }
            return null;
          })}

          {results.data.body.map(slice => {
            if (slice.slice_type === 'photo_credits') {
              return <Credits data={slice} />;
            }
            return true;
          })}
        </div>
      );
    }
    if (notFound) {
      return <LoadableNotFound prismicCtx={prismicCtx} />;
    }
    return <h1>Loading</h1>;
  }
}
