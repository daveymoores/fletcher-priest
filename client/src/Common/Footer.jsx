import React, { Component } from 'react';
import { Grid, GridItem } from 'styled-grid-responsive';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Prismic from 'prismic-javascript';
import GridContainer from './GridContainer';
import media from '../utils/style-utils';
import LoaderFooter from '../Loaders/LoaderFooter';
import Facebook from './icons/facebook.svg';
import Linkedin from './icons/linkedin.svg';
import Instagram from './icons/instagram.svg';
import MailChimp from './icons/mailchimp.svg';
import Twitter from './icons/twitter.svg';

const SiteFooter = styled.footer`
  margin-top: 40px;
  background-color: ${props => props.theme.tertiary};
  flex-shrink: 0;
`;

const SiteFooterList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;

  > li {
    padding-bottom: 5px;
    font-size: 12px;
    line-height: 20px;

    a {
      text-decoration: none;
      font-size: 18px;
      font-family: ${props => props.theme.FFDINWebProMedium};
      color: ${props => props.theme.secondary};

      ${media.tablet`
              font-size: 14px;
            `};
    }
  }
`;

const SiteFooterListContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding: 0;

  ul {
    width: 50%;
    box-sizing: content-box;

    ${media.tablet`
          width: auto;
        `};
  }

  > ul:last-child {
    padding-left: 20px;

    ${media.tablet`
          padding-left: 40px;
        `};
  }
`;

const SmallPrint = styled.div`
  width: 100%;
  background-color: black;
  text-align: center;
  display: flex;
  justify-content: center;
  height: 45px;

  p {
    font-family: ${props => props.theme.FFDINWebProBlack};
    font-size: 11px;
    color: white;
  }
`;

const Title = styled.h4`
  font-size: 18px;
  font-family: ${props => props.theme.FFDINWebProBlack};
  margin-top: 0px;
`;

const FooterGroup = styled.div`
  margin-bottom: 30px;

  ${media.desktop`
      margin-bottom: 60px;
    `};
`;

const SocialLink = styled.a`
  display: flex;
  align-items: center;

  svg {
    height: 14px;
    width: 14px;
    margin-right: 10px;
  }
`;

const oggi = new Date();
const year = oggi.getFullYear();

class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: null
    };
  }

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  componentDidUpdate() {
    const { offCanvas } = this.props;
    const { results } = this.state;

    if (results) {
      this.setupAnimations();
      if (offCanvas === 'open') {
        import('../Animations/OffCanvasAnimation').then(
          ({ default: Animation }) =>
            new Animation({
              wrapper: this.footer
            }).openCanvas()
        );
      }

      if (offCanvas === 'closed') {
        import('../Animations/OffCanvasAnimation').then(
          ({ default: Animation }) =>
            new Animation({
              wrapper: this.footer
            }).closeCanvas()
        );
      }
    }

    return null;
  }

  setupAnimations() {
    this.domRef = {
      wrapper: this.footer
    };

    import('../Animations/OffCanvasAnimation').then(
      ({ default: Animation }) => {
        this.animation = new Animation(this.domRefs);
      }
    );
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.any('document.type', ['sector', 'service']), {
          orderings: '[my.service.published_date]',
          fetch: []
        })
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  render() {
    const { results } = this.state;

    function returnPageTitle(uid) {
      const x = uid.replace('-', ' ');
      return x.charAt(0).toUpperCase() + x.slice(1);
    }

    if (results) {
      return (
        <SiteFooter
          innerRef={el => {
            this.footer = el;
          }}
        >
          <GridContainer type="footer">
            <Grid>
              <GridItem
                media={{
                  smallPhone: 1,
                  mediumPhone: 2 / 3
                }}
                col={1 / 4}
              >
                <FooterGroup>
                  <Title>
                    <Link to="/practice">Practice &rarr;</Link>
                  </Title>

                  <SiteFooterListContainer>
                    <SiteFooterList>
                      <li>
                        <Link to="/practice/approach">Approach</Link>
                      </li>
                      <li>
                        <Link to="/people">People</Link>
                      </li>
                      <li>
                        <Link to="/practice/community">Community</Link>
                      </li>
                    </SiteFooterList>

                    <SiteFooterList>
                      <li>
                        <Link to="/practice/awards">Awards</Link>
                      </li>
                      <li>
                        <Link to="/publications">Publications</Link>
                      </li>
                      <li>
                        <Link to="/careers">Careers</Link>
                      </li>
                    </SiteFooterList>
                  </SiteFooterListContainer>
                </FooterGroup>
              </GridItem>

              <GridItem
                media={{
                  smallPhone: 1 / 2,
                  mediumPhone: 1 / 3
                }}
                col={1 / 6}
              >
                <FooterGroup>
                  <Title>
                    <Link to="/services">Services &rarr;</Link>
                  </Title>

                  <SiteFooterList>
                    {results.map(link => {
                      if (link.type === 'service') {
                        return (
                          <li key={link.id}>
                            <Link to={`/services/${link.uid}`}>
                              {returnPageTitle(link.uid)}
                            </Link>
                          </li>
                        );
                      }
                      return null;
                    })}
                  </SiteFooterList>
                </FooterGroup>
              </GridItem>

              <GridItem
                media={{
                  smallPhone: 1 / 2,
                  mediumPhone: 1 / 3
                }}
                col={1 / 6}
              >
                <FooterGroup>
                  <Title>
                    <Link to="/sectors">Sectors &rarr;</Link>
                  </Title>

                  <SiteFooterList>
                    {results.map(link => {
                      if (link.type === 'sector') {
                        return (
                          <li key={link.id}>
                            <Link to={`/sectors/${link.uid}`}>
                              {returnPageTitle(link.uid)}
                            </Link>
                          </li>
                        );
                      }
                      return null;
                    })}
                  </SiteFooterList>
                </FooterGroup>
              </GridItem>

              <GridItem
                media={{
                  smallPhone: 1 / 2,
                  mediumPhone: 1 / 3
                }}
                col={1 / 6}
              >
                <Title>
                  <Link to="/contact">Contact &rarr;</Link>
                </Title>
              </GridItem>

              <GridItem
                media={{
                  smallPhone: 1 / 2,
                  mediumPhone: 1 / 3
                }}
                col={1 / 4}
              >
                <FooterGroup>
                  <Title>Social</Title>

                  <SiteFooterList>
                    <li>
                      <SocialLink
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://fletcherpriest.us13.list-manage.com/subscribe/post?u=0f797f2a2950b4bd1c97f0d27&amp;id=b22beb9ffb"
                      >
                        <MailChimp />
                        <span>MailChimp</span>
                      </SocialLink>
                    </li>
                    <li>
                      <SocialLink
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://www.linkedin.com/company/fletcher-priest-architects"
                      >
                        <Linkedin />
                        <span>Linkedin</span>
                      </SocialLink>
                    </li>
                    <li>
                      <SocialLink
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://twitter.com/FletcherPriest?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"
                      >
                        <Twitter />
                        <span>Twitter</span>
                      </SocialLink>
                    </li>
                    <li>
                      <SocialLink
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://www.instagram.com/fletcherpriestarchitects/"
                      >
                        <Instagram />
                        <span>Instagram</span>
                      </SocialLink>
                    </li>
                    <li>
                      <SocialLink
                        target="_blank"
                        rel="noopener noreferrer"
                        href="https://www.facebook.com/Fletcher-Priest-Architects-211471152365334/"
                      >
                        <Facebook />
                        <span>Facebook</span>
                      </SocialLink>
                    </li>
                  </SiteFooterList>
                </FooterGroup>
              </GridItem>
            </Grid>
          </GridContainer>
          <SmallPrint>
            <p>Copyright &copy; fletcher priest architects {year}</p>
          </SmallPrint>
        </SiteFooter>
      );
    }
    return <LoaderFooter />;
  }
}

const mapStateToProps = state => ({ offCanvas: state.offCanvas });

export default connect(mapStateToProps)(Footer);
