import React from 'react';

export default ({ color }) => (
  <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 98 98'>
    <title>carousel-arrow</title>
    <g id='8eba043b-b4ad-4f0a-99e7-9e1c9c0b027b' data-name='Layer 2'>
      <g id='4751d909-3783-41f4-9e4c-3174088798b2' data-name='Layer 1'>
        <g id='ffdc9ffc-28ae-43a3-97bf-0f1c783144ea' data-name='Group-3'>
          <circle
            id='89fd0ad5-0635-4bbb-a7d7-6f2235dd40f6'
            data-name='Oval'
            cx='49'
            cy='49'
            r='49'
            fill={color}
          />
          <g id='1c3e17c2-614b-49cb-bfdf-d2743aed267d' data-name='larr'>
            <polygon
              id='593c2d86-a736-4fcb-99b4-150851e0cde9'
              data-name='path-1'
              points='43.03 70 22 49 43.03 28 54.47 28 37.51 44.71 74 44.71 74 53.29 37.51 53.29 54.47 70 43.03 70'
              fill='#fff'
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);
