import React from 'react';

export default ({ color }) => (
  <svg width='36px' height='36px' viewBox='0 0 36 36'>
    <defs>
      <circle id='path-1' cx='18' cy='18' r='18' />
      <circle id='path-3' cx='18' cy='18' r='18' />
      <polygon
        id='path-5'
        points='12.85 17.19 20.98 9.05 12.85 0.91 8.42 0.91 14.98 7.39 0.87 7.39 0.87 10.71 14.98 10.71 8.42 17.19'
      />
    </defs>
    <g id='Page-1' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
      <g id='award-arrow'>
        <g
          id='0795e77a-3b31-43ff-b6aa-90e22b9d98f5'
          fill='#F4E218'
          fillRule='nonzero'
        >
          <circle
            id='f8b87518-1a69-48a0-822c-058a3038ab33'
            cx='18'
            cy='18'
            r='18'
          />
        </g>
        <g id='Clipped'>
          <mask id='mask-2' fill='white'>
            <use xlinkHref='#path-1' />
          </mask>
          <g id='997d5969-cd08-4bec-af05-52730e63a02a' />
          <g
            id='c6eeea63-ae6f-40f9-950f-237f1e92ebeb'
            mask='url(#mask-2)'
            fill={color}
            fillRule='nonzero'
          >
            <g id='80278486-f9e3-4e36-af70-43f746e6cea1'>
              <rect id='Rectangle-path' x='0' y='0' width='36' height='37.48' />
            </g>
          </g>
        </g>
        <g id='Clipped'>
          <mask id='mask-4' fill='white'>
            <use xlinkHref='#path-3' />
          </mask>
          <g id='3d3a930c-06d4-4d93-b24f-aaa3bb91b6e4' />
          <g id='1065957a-c7dc-4e84-8615-91ecbc14bc66' mask='url(#mask-4)'>
            <g transform='translate(7.000000, 9.000000)'>
              <g id='de77bf71-148f-418b-8edb-1f4b4cf48c4e'>
                <polygon
                  id='7d42ca50-5349-4b38-b2b0-848fef24380c'
                  fill='#FFFFFF'
                  fillRule='nonzero'
                  points='12.85 17.19 20.98 9.05 12.85 0.91 8.42 0.91 14.98 7.39 0.87 7.39 0.87 10.71 14.98 10.71 8.42 17.19'
                />
              </g>
              <g id='Clipped'>
                <mask id='mask-6' fill='white'>
                  <use xlinkHref='#path-5' />
                </mask>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);
