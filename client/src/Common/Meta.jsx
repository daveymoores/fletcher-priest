import React from "react";
import MetaTags from "react-meta-tags";
import PrismicReact from "prismic-reactjs";
import uniqid from "uniqid";

const Meta = props => {
    const { metaContent } = props;

    if (metaContent.body1.length > 0) {
        return (
            <MetaTags>
                {metaContent.body1.map(slice => {
                    if (slice.slice_type === "general_card") {
                        return (
                            <React.Fragment key={uniqid()}>
                                <title>
                                    {PrismicReact.RichText.asText(
                                        slice.primary.title
                                    )}
                                </title>
                                <meta
                                    name="description"
                                    content={PrismicReact.RichText.asText(
                                        slice.primary.description
                                    )}
                                />
                                <meta
                                    property="og:title"
                                    content={PrismicReact.RichText.asText(
                                        slice.primary.title
                                    )}
                                />
                                <meta property="og:type" content="article" />
                                <meta property="og:url" content="" />
                                <meta property="og:image" content="" />
                                <meta
                                    property="og:description"
                                    content={PrismicReact.RichText.asText(
                                        slice.primary.description
                                    )}
                                />
                                <meta
                                    property="og:site_name"
                                    content="Fletcher Priest Architects"
                                />
                                <meta property="fb:admins" content="" />
                            </React.Fragment>
                        );
                    }
                    if (slice.slice_type === "twitter_card") {
                        return (
                            <React.Fragment key={uniqid()}>
                                <meta
                                    name="twitter:card"
                                    content={slice.primary.card_type}
                                />
                                <meta
                                    name="twitter:site"
                                    content="Fletcher Priest Architects"
                                />
                                <meta
                                    name="twitter:title"
                                    content={PrismicReact.RichText.asText(
                                        slice.primary.title
                                    )}
                                />
                                <meta
                                    name="twitter:description"
                                    content={PrismicReact.RichText.asText(
                                        slice.primary.description
                                    )}
                                />
                                <meta
                                    name="twitter:image"
                                    content={slice.primary.image.url}
                                />
                            </React.Fragment>
                        );
                    }
                    return null;
                })}
            </MetaTags>
        );
    }

    if (metaContent.page_title) {
        return (
            <MetaTags key={uniqid()}>
                <meta
                    property="og:title"
                    content={PrismicReact.RichText.asText(
                        metaContent.page_title
                    )}
                />
            </MetaTags>
        );
    }

    if (metaContent.title) {
        return (
            <MetaTags key={uniqid()}>
                <meta
                    name="description"
                    content={PrismicReact.RichText.asText(
                        metaContent.text_contents
                    )}
                />
                <meta
                    property="og:title"
                    content={PrismicReact.RichText.asText(metaContent.title)}
                />
            </MetaTags>
        );
    }

    if (metaContent.project_name) {
        return (
            <MetaTags key={uniqid()}>
                <meta
                    name="description"
                    content={PrismicReact.RichText.asText(
                        metaContent.intro_text
                    )}
                />
                <meta
                    property="og:title"
                    content={PrismicReact.RichText.asText(
                        metaContent.project_name
                    )}
                />
            </MetaTags>
        );
    }

    if (metaContent.event_name) {
        return (
            <MetaTags key={uniqid()}>
                <meta
                    name="description"
                    content={PrismicReact.RichText.asText(
                        metaContent.event_description
                    )}
                />
                <meta
                    property="og:title"
                    content={PrismicReact.RichText.asText(
                        metaContent.event_name
                    )}
                />
            </MetaTags>
        );
    }

    if (metaContent.job_listing_title) {
        return (
            <MetaTags key={uniqid()}>
                <meta
                    name="description"
                    content={PrismicReact.RichText.asText(
                        metaContent.listing_intro
                    )}
                />
                <meta
                    property="og:title"
                    content={PrismicReact.RichText.asText(
                        metaContent.job_listing_title
                    )}
                />
            </MetaTags>
        );
    }

    if (metaContent.first_name) {
        const middleName = metaContent.middle_name
            ? `${metaContent.middle_name} `
            : "";
        const fullName = `${metaContent.first_name} ${middleName}${
            metaContent.last_name
        }`;

        return (
            <MetaTags key={uniqid()}>
                <meta
                    name="description"
                    content={PrismicReact.RichText.asText(
                        metaContent.person_intro
                    )}
                />
                <meta property="og:title" content={fullName} />
            </MetaTags>
        );
    }

    return (
        <MetaTags>
            <meta name="description" content="" />
            <meta name="keywords" content="" />

            <meta property="og:locale" content="en_GB" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="" />
            <meta property="og:url" content="" />
            <meta property="og:image" content="" />

            <meta property="og:site_name" content="" />
            <meta property="og:description" content="" />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:description" content="" />
            <meta name="twitter:title" content="" />
            <meta name="twitter:image:src" content="" />
            <meta name="twitter:creator" content="Fist of Fury" />
        </MetaTags>
    );
};

export default Meta;
