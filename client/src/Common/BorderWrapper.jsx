import React from "react";
import styled from "styled-components";

const BorderWrapper = styled.div`
    border-top: 1px solid ${props => props.theme.darkGrey2};
    margin-top: 60px;
`;

const BorderTopWrapper = props => {
    const { children } = props;
    return <BorderWrapper>{children}</BorderWrapper>;
};

export default BorderTopWrapper;
