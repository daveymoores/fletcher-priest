import React from 'react';
import styled, { withTheme } from 'styled-components';
import ArrowIcon from './icons/carousel-arrow';
import media from '../utils/style-utils';

const CarArrow = styled.div`
  height: 40px;
  width: 40px;
  display: block;
  position: absolute;
  top: 30px;
  transform: ${props => (props.type === 'next' ? 'rotate(180deg)' : 'inherit')};
  cursor: pointer;
  z-index: 1;

  ${media.tablet`
      height: 44px;
      width: 44px;
      top: 60px;
    `} > svg {
    height: 100%;
    width: 100%;
  }

  right: ${props => (props.type === 'next' ? '15px' : '68px')};

  ${media.desktop`right: ${props =>
    props.type === 'next' ? '40px' : '95px'}`} ${media.giant`right: ${props =>
    props.type === 'next' ? '60px' : '115px'}`};
`;

const SlickCarouselArrow = props => {
  const { onClick, type, theme } = props;
  return (
    <CarArrow type={type} onClick={onClick}>
      <ArrowIcon color={theme.primary} />
    </CarArrow>
  );
};

export default withTheme(SlickCarouselArrow);
