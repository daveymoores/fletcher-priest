import React, { Component } from "react";
import Prismic from "prismic-javascript";
import { NavLink, Link, withRouter } from "react-router-dom";
import styled, { css, withTheme } from "styled-components";
import { connect } from "react-redux";
import media from "../utils/style-utils";
import LogoSvg from "./fp-logo.svg";
import DropdownIndicator from "./icons/dropdown-indicator.svg";
import Search from "./icons/search.svg";
import SearchScreen from "./SearchScreen";

import LoaderNav from "../Loaders/LoaderNav";
import "./css/burger.css";
import { setScrollNavState } from "../redux/actionCreators";

const StyledSearch = styled(Search)`
  width: 21px;
  height: 21px;
  cursor: pointer;
  display: block;
  margin-top: 12px;
`;

const StyledMobileButton = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  top: 15px;
  z-index: 9;
  right: 60px;
  width: 25px;
  height: 25px;

  ${media.tabletLrg`
    display: none;
  `};

  > svg {
    width: 25px;
    height: 25px;
    margin-top: 6px;
    position: absolute;
    top: 0;
    left: 0;
  }
`;

const StyledNavLink = styled(NavLink)`
  font-size: 16px;
  color: white;
  text-decoration: none;
  margin-left: 0;
  font-size: 15px;
  font-family: ${props => props.theme.FFDINWebProBold};
  font-weight: normal;
  position: relative;

  &.selected {
    pointer-events: none;
    opacity: 0.7;
  }

  ${media.tabletLrg`
      margin-left: ${props => (props.logolink ? "0" : "25px")}
      color: ${props => props.theme.secondary};

      > span {
        display: none;
      }
    `};

  ${media.desktop`
      margin-left: ${props => (props.logolink ? "0" : "45px")};
    `};
`;

const StyledNavButton = styled.button`
  font-size: 16px;
  color: white;
  text-decoration: none;
  margin-left: 0;
  font-size: 15px;
  font-family: ${props => props.theme.FFDINWebProBold};
  font-weight: normal;
  position: relative;
  background-color: transparent;
  border: none;
  outline: none;

  &.selected {
    pointer-events: none;
    opacity: 0.7;
  }

  ${media.tabletLrg`
    margin-left: ${props => (props.logolink ? "0" : "25px")}
    color: ${props => props.theme.secondary};

    > span {
      display: none;
    }
  `};

  ${media.desktop`
    margin-left: ${props => (props.logolink ? "0" : "45px")};
  `};
`;

const NavLinkAnchor = styled.a`
  color: ${props => props.theme.darkGrey4};
  text-decoration: none;
  margin-left: 0;
  font-size: 14px;
  font-family: ${props => props.theme.FFDINWebProBold};
  position: relative;
  cursor: pointer;

  ${media.tabletLrg`
      font-size: 15px;
      margin-left: ${props => (props.logolink ? "0" : "45px")}
      color: ${props => props.theme.secondary};
    `};
`;

const NavList = styled.ul`
  margin: 0;
  list-style: none;
  padding: 0px;
  display: block;
  flex-direction: column;
  height: ${props => (props.column ? "auto" : "50px")};
  align-items: flex-start;

  ${media.tabletLrg`
      flex-direction: row;
      align-items: center;
      display: ${props => (props.column ? "block" : "flex")};
    `};

  > li {
    height: auto;
    display: block;
    align-items: center;
    padding-bottom: 5px;
    margin-left: -15px;
    width: calc(100% + 30px);
    padding: 10px 15px;
    border-top: 2px solid ${props => props.theme.darkGrey4};

    &.desktop-search {
      display: none;
    }

    ${media.tabletLrg`
          display: flex;
          height: 100%;
          margin-left: auto;
          width: auto;
          padding: 0;
          border-top: none;

          &.desktop-search {
            display: block;
          }
        `};
  }

  li:first-child > a {
    margin-left: 0px;
  }
`;

const Logo = styled(LogoSvg)`
  height: 32px;
  margin: 17px 0;
  display: block;

  ${media.tabletLrg`height: 42px; margin: 0;`};
`;

const NavBar = styled.nav`
  background-color: ${props => props.theme.primary};
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  height: 100%;
  max-height: 66px;
  position: fixed;
  z-index: 9999;
  top: 0;
  width: 100%;
  box-sizing: border-box;
  padding: 0 15px;
  overflow: hidden;

  &.expand {
    background-color: ${props => props.theme.darkGrey5};
    max-height: 100%;
    transition: max-height 0.4s cubic-bezier(0.77, 0, 0.175, 1),
      background-color 0.4s cubic-bezier(0.77, 0, 0.175, 1);
    overflow: scroll;
  }

  ${media.tabletLrg`
      background-color: transparent;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      max-height: 85px;
      overflow: visible;
    `} ${media.desktop`
      padding: 0 30px;
    `};
`;

const NavListDropDownContainer = styled.div`
  display: block;
  position: relative;
  height: 100%;
  width: 100%;
  z-index: 1;

  > ul > li {
    &:nth-child(3),
    &:nth-child(4),
    &:nth-child(5) {
      > a {
        @media all and (max-width: 767px) {
          font-size: 18px;
          margin-bottom: 20px;
          color: ${props => props.theme.primary};
          font-family: ${props => props.theme.FFDINWebProBlack};
        }
      }
    }
  }

  ${media.tabletLrg`
      height: auto;
      width: auto;
    `};
`;

const NavListDropDownContent = styled.div`
  display: flex;
  flex-direction: ${props => (props.primary ? "column" : "row")};
  align-items: flex-start;
  padding-top: 0;
  position: relative;
  width: 100%;
  opacity: 0;

  ${media.tabletLrg`
      display: ${props => (props.hidden ? "none" : "flex")};
      position: absolute;
      padding: 40px 0;
      top: 100%;
      left: 0;
    `};

  > div {
    width: 50%;

    li {
      border-top: none;
      padding: 3px 15px;

      ${media.tabletLrg`
              padding: 0;
            `};
    }
  }

  ${props =>
    props.primary &&
    css`
      > div {
        display: flex;
        width: 100%;

        > div {
          width: 50%;
        }
      }
    `} ${StyledNavLink} {
    margin: 0 0 10px 0;
    display: block;
    color: white;
  }
`;

const NavListDropDownTitle = styled.h3`
  margin-top: 5px;

  ${media.tabletLrg`
      margin-top: 20px;
    `};

  a {
    font-size: 18px;
    margin-bottom: 20px;
    color: ${props => props.theme.primary};
    font-family: ${props => props.theme.FFDINWebProBlack};
  }
`;

const DropdownIcon = styled.span`
  left: 50%;
  bottom: -15px;
  transform: translate3d(-50%, 0, 0);
  width: 11px;
  position: absolute;
  pointer-events: none;
  display: none;

  ${media.tabletLrg`
      display: block;
    `};
`;

const DropdownBgMask = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${props => props.theme.secondary};
  position: absolute;
  display: none;
  top: 0;
  left: 0;

  ${media.tabletLrg`
      display: block;
      height: 85px;
      background-color: ${props => props.theme.primary};
    `};
`;

const SiteMask = styled.div`
  display: none;

  ${media.tabletLrg`
      position: fixed;
      z-index: 3;
      background-color: rgba(42, 47, 56, 0.8);
      width: 100%;
      height: 100vh;
      top: 0;
      left: 0;
      display: block;
      opacity: 0;
      visibility: hidden;
    `};
`;

const HideNavOnScroll = styled.div`
  position: fixed;
  transition: transform 0.3s cubic-bezier(0.77, 0, 0.175, 1);
  width: 100%;
  height: 66px;
  z-index: 99;

  &.expand {
    height: 100%;
  }

  ${media.tabletLrg`
        height: 85px;
    `};
`;

const Hamburger = styled.button`
  position: absolute;
  top: 22px;
  right: 15px;
  background: transparent;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 0;

  .hamburger-inner,
  .hamburger-inner::before,
  .hamburger-inner::after {
    background-color: black;
  }

  &.is-active {
    .hamburger-inner,
    .hamburger-inner::before,
    .hamburger-inner::after {
      background-color: white;
    }
  }

  ${media.tabletLrg`
      display: none;
    `};
`;

const MobileLogoBurgerContainer = styled.div`
  margin-right: auto;

  a > span {
    display: block;
  }
`;

class Navigation extends Component {
  static isParent(parent, child) {
    if (child) {
      let node = child.parentNode;
      while (node != null) {
        if (node === parent) {
          return true;
        }
        node = node.parentNode;
      }
    }

    return false;
  }

  static returnPageTitle(uid) {
    let x;
    switch (uid) {
      case `culture-sport-leisure`:
        x = `Culture, Sport and Leisure`;
        break;
      case `retail-hospitality`:
        x = `Retail and Hospitality`;
        break;
      case `Our Approach`:
        x = `Our Approach`;
        break;
      default:
        x = uid.replace("-", " ");
    }

    return x.charAt(0).toUpperCase() + x.slice(1);
  }

  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
      results: null,
      desktop: false,
      prevScrollY: window.pageYOffset,
      searchScreen: false
    };

    this.timer = null;
    this.collapseMenu = this.collapseMenu.bind(this);
    this.hideMenuAfterClick = this.hideMenuAfterClick.bind(this);
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.handleMobileClick = this.handleMobileClick.bind(this);
    this.toggleSearchScreen = this.toggleSearchScreen.bind(this);
  }

  componentWillMount() {
    this.fetchPage(this.props);
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
    window.addEventListener("scroll", this.hideNav.bind(this));
  }

  componentWillReceiveProps(props) {
    this.fetchPage(props);
  }

  componentDidUpdate(prevProps, prevState) {
    const { offCanvas } = this.props;
    const { results, desktop } = this.state;

    if (prevState.desktop !== desktop) {
      this.setupAnimations();
    }

    if (!prevState.results) {
      this.setupAnimations();
    }

    if (results) {
      if (offCanvas === "open") {
        this.hideNavOnScrollRef.style.pointerEvents = "none";
        import("../Animations/OffCanvasAnimation").then(
          ({ default: Animation }) => new Animation(this.domRefs).openCanvas()
        );
      }

      if (offCanvas === "closed") {
        this.hideNavOnScrollRef.style.pointerEvents = "auto";
        import("../Animations/OffCanvasAnimation").then(
          ({ default: Animation }) => new Animation(this.domRefs).closeCanvas()
        );
      }
    }

    return null;
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
    window.removeEventListener("scroll", this.hideNav.bind(this));
  }

  setupAnimations() {
    const { results } = this.state;
    const { theme } = this.props;
    if (results) {
      this.domRefs = {
        about: this.aboutLinkRef,
        aboutChevron: this.aboutChevronRef,
        aboutPanel: this.aboutPanelRef,
        wwd: this.wwdLinkRef,
        wwdChevron: this.wwdChevronRef,
        wwdPanel: this.wwdPanelRef,
        bg: this.bgPanelRef,
        parentNav: this.parentNavRef,
        logo: this.logoRef,
        mask: this.siteMaskRef,
        wrapper: this.offCanvasWrapperRef,
        hamburger: this.hamburgerRef,
        search: this.searchIconRef,
        mobileSearch: this.searchMobileIconRef
      };

      import("../Animations/NavAnimation").then(({ default: Animation }) => {
        this.animation = new Animation(this.domRefs, theme);
      });

      import("../Animations/OffCanvasAnimation").then(
        ({ default: Animation }) => {
          this.offCanvasAnimation = new Animation(this.domRefs);
        }
      );
    }
  }

  updateDimensions() {
    const { desktop } = this.state;
    if (window.innerWidth > 850) {
      this.setState({ desktop: true });
    } else {
      this.setState({ desktop: false });
    }
  }

  hideNav() {
    const { prevScrollY, expanded, desktop } = this.state;
    const { handleScrollNavStateChange, scrollNavState } = this.props;

    // show the nav on route change
    if (scrollNavState === "hidden" && !window.pageYOffset) {
      handleScrollNavStateChange("visible");
      this.hideNavOnScrollRef.style.transform = "translateY(0)";
    }

    const currentScrollPos = window.pageYOffset;

    if (!this.domRefs.wrapper.classList.contains("open")) {
      if (!expanded && currentScrollPos > 100) {
        if (prevScrollY > currentScrollPos) {
          handleScrollNavStateChange("visible");
          this.hideNavOnScrollRef.style.transform = "translateY(0)";
        } else {
          handleScrollNavStateChange("hidden");
          this.hideNavOnScrollRef.style.transform = desktop
            ? "translateY(-85px)"
            : "translateY(-66px)";
        }
      }
    }

    this.setState({ prevScrollY: currentScrollPos });
  }

  fetchPage(props) {
    if (props.prismicCtx) {
      return props.prismicCtx.api
        .query(Prismic.Predicates.any("document.type", ["sector", "service"]), {
          orderings: "[my.service.published_date]",
          fetch: []
        })
        .then(response => {
          const { results } = response;
          this.setState({ results });
        });
    }
    return null;
  }

  toggleDropdown(e) {
    const { results, desktop } = this.state;
    const targetEvent = e;
    const targetEventType = e.type;
    const targetEventTarget = e.target;
    const targetEventCurrent = e.currentTarget;
    const targetEventRelated = e.relatedTarget;
    if (targetEventType === "mouseout") clearInterval(this.timer);

    if (results) {
      e.stopPropagation();
      if (
        !Navigation.isParent(targetEventCurrent, targetEventRelated) &&
        targetEventTarget === targetEventCurrent &&
        desktop
      ) {
        this.timer = setTimeout(() => {
          this.animation.toggleDropdown(
            targetEventCurrent,
            targetEventRelated,
            targetEventType
          );
        }, 100);
      }
    }
  }

  collapseMenu(e) {
    const { desktop } = this.state;
    const targetEvent = e;
    const targetEventType = e.type;
    const targetEventTarget = e.target;
    const targetEventCurrent = e.currentTarget;
    const targetEventRelated = e.relatedTarget;
    e.stopPropagation();
    if (
      !Navigation.isParent(targetEventCurrent, targetEventRelated) &&
      targetEventTarget === targetEventCurrent &&
      desktop
    ) {
      this.animation.collapseMenu();
    }
  }

  hideMenuAfterClick(e) {
    const { desktop, expanded } = this.state;

    if (e.currentTarget.getAttribute("logolink") && !expanded) return false;

    if (desktop) {
      return this.animation.collapseMenu();
    }
    return this.handleMobileClick();
  }

  handleMobileClick(e) {
    const target = this.hamburgerRef;
    const { expanded } = this.state;

    this.setState({ expanded: !expanded }, () => {
      const { expanded: bool } = this.state;

      if (bool) {
        target.classList.add("is-active");
        this.offCanvasWrapperRef.classList.add("expand");
        this.offCanvasWrapperRef.parentNode.classList.add("expand");
      } else {
        target.classList.remove("is-active");
        this.offCanvasWrapperRef.classList.remove("expand");
        this.offCanvasWrapperRef.parentNode.classList.remove("expand");
        this.offCanvasWrapperRef.scrollTop = 0;
      }

      this.animation.animateMobileElements(bool);
    });
  }

  toggleSearchScreen(e) {
    e.preventDefault();
    this.setState(({ searchScreen }) => ({ searchScreen: !searchScreen }));
  }

  searchScreenCallback = close => {
    if (close) this.setState({ searchScreen: false });
  };

  render() {
    const { results, searchScreen } = this.state;
    const { prismicCtx } = this.props;

    if (results) {
      return (
        <React.Fragment>
          <HideNavOnScroll
            innerRef={el => {
              this.hideNavOnScrollRef = el;
            }}
          >
            <SiteMask
              innerRef={el => {
                this.siteMaskRef = el;
              }}
              onMouseOver={this.collapseMenu}
              onFocus={this.collapseMenu}
            />

            <NavBar
              innerRef={el => {
                this.offCanvasWrapperRef = el;
              }}
            >
              <DropdownBgMask
                innerRef={el => {
                  this.bgPanelRef = el;
                }}
              />
              <MobileLogoBurgerContainer>
                <StyledNavLink
                  activeClassName="selected"
                  exact
                  onClick={this.hideMenuAfterClick}
                  logolink="true"
                  to="/"
                >
                  <span
                    ref={el => {
                      this.logoRef = el;
                    }}
                  >
                    <Logo />
                  </span>
                </StyledNavLink>

                <StyledMobileButton
                  innerRef={el => {
                    this.searchMobileIconRef = el;
                  }}
                  type="button"
                  onClick={this.toggleSearchScreen}
                >
                  <StyledSearch />
                </StyledMobileButton>

                <Hamburger
                  className="hamburger--squeeze js-hamburger"
                  type="button"
                  innerRef={el => {
                    this.hamburgerRef = el;
                  }}
                  onClick={this.handleMobileClick}
                >
                  <span className="hamburger-box">
                    <span className="hamburger-inner" />
                  </span>
                </Hamburger>
              </MobileLogoBurgerContainer>
              <NavListDropDownContainer>
                <NavList
                  innerRef={el => {
                    this.parentNavRef = el;
                  }}
                >
                  <li
                    ref={el => {
                      this.aboutLinkRef = el;
                    }}
                  >
                    <NavLinkAnchor
                      onMouseOver={this.toggleDropdown}
                      onMouseOut={this.toggleDropdown}
                      onBlur={this.toggleDropdown}
                      onFocus={this.toggleDropdown}
                    >
                      About
                      <DropdownIcon
                        innerRef={el => {
                          this.aboutChevronRef = el;
                        }}
                      >
                        <DropdownIndicator />
                      </DropdownIcon>
                    </NavLinkAnchor>

                    <NavListDropDownContent
                      primary
                      innerRef={el => {
                        this.aboutPanelRef = el;
                      }}
                      onMouseOut={this.collapseMenu}
                      onBlur={this.collapseMenu}
                    >
                      <NavListDropDownTitle onClick={this.hideMenuAfterClick}>
                        <NavLink
                          activeClassName="selected"
                          exact
                          to="/practice"
                        >
                          About the practice &rarr;
                        </NavLink>
                      </NavListDropDownTitle>
                      <div>
                        <div>
                          <NavList column>
                            <li>
                              <StyledNavLink
                                activeClassName="selected"
                                exact
                                onClick={this.hideMenuAfterClick}
                                to="/practice/approach"
                              >
                                Our Approach &rarr;
                              </StyledNavLink>
                            </li>
                            <li>
                              <StyledNavLink
                                activeClassName="selected"
                                exact
                                onClick={this.hideMenuAfterClick}
                                to="/people"
                              >
                                People &rarr;
                              </StyledNavLink>
                            </li>
                            <li>
                              <StyledNavLink
                                activeClassName="selected"
                                exact
                                onClick={this.hideMenuAfterClick}
                                to="/practice/community"
                              >
                                Community &rarr;
                              </StyledNavLink>
                            </li>
                          </NavList>
                        </div>
                        <div>
                          <NavList column>
                            <li>
                              <StyledNavLink
                                activeClassName="selected"
                                exact
                                onClick={this.hideMenuAfterClick}
                                to="/practice/awards"
                              >
                                Awards &rarr;
                              </StyledNavLink>
                            </li>
                            <li>
                              <StyledNavLink
                                activeClassName="selected"
                                exact
                                onClick={this.hideMenuAfterClick}
                                to="/publications"
                              >
                                Publications &rarr;
                              </StyledNavLink>
                            </li>
                            <li>
                              <StyledNavLink
                                activeClassName="selected"
                                exact
                                onClick={this.hideMenuAfterClick}
                                to="/careers"
                              >
                                Careers &rarr;
                              </StyledNavLink>
                            </li>
                          </NavList>
                        </div>
                      </div>
                    </NavListDropDownContent>
                  </li>
                  <li
                    ref={el => {
                      this.wwdLinkRef = el;
                    }}
                  >
                    <NavLinkAnchor
                      activeClassName="selected"
                      exact
                      onMouseOver={this.toggleDropdown}
                      onMouseOut={this.toggleDropdown}
                      onBlur={this.toggleDropdown}
                      onFocus={this.toggleDropdown}
                    >
                      What we do
                      <DropdownIcon
                        innerRef={el => {
                          this.wwdChevronRef = el;
                        }}
                      >
                        <DropdownIndicator />
                      </DropdownIcon>
                    </NavLinkAnchor>

                    <NavListDropDownContent
                      innerRef={el => {
                        this.wwdPanelRef = el;
                      }}
                      onMouseOut={this.collapseMenu}
                      onBlur={this.collapseMenu}
                    >
                      <div>
                        <NavListDropDownTitle>
                          <NavLink
                            activeClassName="selected"
                            exact
                            onClick={this.hideMenuAfterClick}
                            to="/services"
                          >
                            Services &rarr;
                          </NavLink>
                        </NavListDropDownTitle>
                        <NavList column>
                          {results.map(link => {
                            if (link.type === "service") {
                              return (
                                <li key={link.id}>
                                  <StyledNavLink
                                    activeClassName="selected"
                                    exact
                                    onClick={this.hideMenuAfterClick}
                                    to={`/services/${link.uid}`}
                                  >
                                    {Navigation.returnPageTitle(link.uid)}{" "}
                                    &rarr;
                                  </StyledNavLink>
                                </li>
                              );
                            }
                            return null;
                          })}
                        </NavList>
                      </div>
                      <div>
                        <NavListDropDownTitle>
                          <NavLink
                            activeClassName="selected"
                            exact
                            onClick={this.hideMenuAfterClick}
                            to="/sectors"
                          >
                            Sectors &rarr;
                          </NavLink>
                        </NavListDropDownTitle>
                        <NavList column>
                          {results.map(link => {
                            if (link.type === "sector") {
                              return (
                                <li key={link.id}>
                                  <StyledNavLink
                                    activeClassName="selected"
                                    exact
                                    onClick={this.hideMenuAfterClick}
                                    to={`/sectors/${link.uid}`}
                                  >
                                    {Navigation.returnPageTitle(link.uid)}{" "}
                                    &rarr;
                                  </StyledNavLink>
                                </li>
                              );
                            }
                            return null;
                          })}
                        </NavList>
                      </div>
                    </NavListDropDownContent>
                  </li>
                  <li>
                    <StyledNavLink
                      activeClassName="selected"
                      exact
                      onClick={this.hideMenuAfterClick}
                      to="/projects"
                    >
                      Projects <span>&rarr;</span>
                    </StyledNavLink>
                  </li>
                  <li>
                    <StyledNavLink
                      activeClassName="selected"
                      exact
                      onClick={this.hideMenuAfterClick}
                      to="/news-and-events"
                    >
                      News &amp; Events <span>&rarr;</span>
                    </StyledNavLink>
                  </li>
                  <li>
                    <StyledNavLink
                      activeClassName="selected"
                      exact
                      onClick={this.hideMenuAfterClick}
                      to="/contact"
                    >
                      Contact <span>&rarr;</span>
                    </StyledNavLink>
                  </li>
                  <li className="desktop-search">
                    <span
                      ref={el => {
                        this.searchIconRef = el;
                      }}
                    >
                      <StyledNavButton
                        type="button"
                        onClick={this.toggleSearchScreen}
                      >
                        <StyledSearch />
                      </StyledNavButton>
                    </span>
                  </li>
                </NavList>
              </NavListDropDownContainer>
            </NavBar>
          </HideNavOnScroll>
          <SearchScreen
            closeCallback={this.searchScreenCallback}
            prismic={prismicCtx}
            searchScreenOpen={searchScreen}
          />
        </React.Fragment>
      );
    }
    return <LoaderNav />;
  }
}

const mapStateToProps = state => ({
  offCanvas: state.offCanvas,
  scrollNavState: state.scrollNavState
});

const mapDispatchToProps = dispatch => ({
  handleScrollNavStateChange(value) {
    dispatch(setScrollNavState(value));
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withTheme(Navigation))
);
