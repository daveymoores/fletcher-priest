import React, { Component } from "react";
import styled from "styled-components";
import debounce from "lodash/debounce";
import PageHeading from "../Components/PageHeading";
import GridContainer from "./GridContainer";
import media from "../utils/style-utils";
import { HeroWrapper, Description } from "../SharedStyles/hero-styles";
import Spots from "./textures/spots.png";

const LoadingWrapper = styled.div`
  position: relative;
`;

const VideoContainer = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  overflow: hidden;

  > video {
    min-height: 100%;
    min-width: 100vw;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate3d(-50%, -50%, 0);
    z-index: 0;
  }

  .vimeo-wrapper {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
    pointer-events: none;
    overflow: hidden;
  }
  .vimeo-wrapper iframe {
    width: 100vw;
    height: 56.25vw; /* Given a 16:9 aspect ratio, 9/16*100 = 56.25 */
    min-height: 100vh;
    min-width: 177.77vh; /* Given a 16:9 aspect ratio, 16/9*100 = 177.77 */
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

class VideoHero extends Component {
  constructor() {
    super();
    this.state = {
      viewport: { x: window.innerWidth, y: window.innerHeight }
    };

    this.handleResize = debounce(this.handleResize, 100);
  }

  componentWillMount() {
    this.handleResize();
  }

  componentDidMount() {
    this.mounted = true;
    window.addEventListener("resize", this.handleResize, false);
  }

  componentWillUnmount() {
    this.mounted = false;
    window.removeEventListener("resize", this.handleResize, false);
  }

  handleResize = () => {
    if (this.mounted) {
      this.setState(
        {
          viewport: { x: window.innerWidth, y: window.innerHeight }
        },
        () => {
          this.calcOffset();
        }
      );
    }
  };

  calcOffset() {
    const dims = this.descRef.getBoundingClientRect();
    const { type } = this.props;
    const x = type === `stats` ? 3 : 2;
    this.descRef.style.marginTop = `-${dims.height / x}px`;
  }

  returnColor = c => {
    switch (c) {
      case "Black":
        return "#000000";
      case "Grey":
        return "#868989";
      default:
        return "#FFFFFF";
    }
  };

  checkPadding = ({ viewport }) => {
    const padding = viewport.x > 768 ? "30px 40px" : "15px 20px 30px";
    return padding;
  };

  render() {
    const { title, content, intro, type, color, projectLink } = this.props;
    const { viewport } = this.state;

    const titleColor = this.returnColor(color);
    const defaultStyles = {
      backgroundImage: `url(${Spots})`,
      backgroundRepeat: "repeat"
    };

    const linkToProject =
      projectLink && Object.prototype.hasOwnProperty.call(projectLink, "uid")
        ? projectLink
        : null;

    const paddingType =
      type === `stats`
        ? { padding: 0 }
        : { padding: this.checkPadding(this.state) };
    return (
      <div>
        {title && (
          <div ref={node => (this.titleRef = node)}>
            <PageHeading color={titleColor} text={title} />
          </div>
        )}
        <div style={defaultStyles}>
          {linkToProject ? (
            <a href={`/projects/${linkToProject.uid}`}>
              <HeroWrapper
                innerRef={node => (this.heroRef = node)}
                style={{
                  backgroundImage: `url(${
                    content.primary.video_placeholder_image.url
                  })`
                }}
              >
                <VideoContainer>
                  <div className="vimeo-wrapper">
                    <iframe
                      title="background video"
                      src={`${content.primary.video.url}?background=1`}
                      width="500"
                      height="281"
                      frameBorder="0"
                    />
                  </div>
                </VideoContainer>
              </HeroWrapper>
            </a>
          ) : (
            <HeroWrapper
              innerRef={node => (this.heroRef = node)}
              style={{
                backgroundImage: `url(${
                  content.primary.video_placeholder_image.url
                })`
              }}
            >
              <VideoContainer>
                <div className="vimeo-wrapper">
                  <iframe
                    title="background video"
                    src={`${content.primary.video.url}?background=1`}
                    width="500"
                    height="281"
                    frameBorder="0"
                  />
                </div>
              </VideoContainer>
            </HeroWrapper>
          )}
        </div>

        <GridContainer type="pullRight">
          <Description style={paddingType} innerRef={e => (this.descRef = e)}>
            {intro}
          </Description>
        </GridContainer>
      </div>
    );
  }
}

export default VideoHero;
