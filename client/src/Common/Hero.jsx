import React, { Component } from "react";
import styled from "styled-components";
import debounce from "lodash/debounce";
import PageHeading from "../Components/PageHeading";
import GridContainer from "./GridContainer";
import { HeroWrapper, Description } from "../SharedStyles/hero-styles";
import Spots from "./textures/spots.png";

const LoadingWrapper = styled.div`
  position: relative;
`;

class Hero extends Component {
  constructor() {
    super();
    this.state = {
      viewport: { x: window.innerWidth, y: window.innerHeight }
    };
    this.handleResize = debounce(this.handleResize, 100);
  }

  componentWillMount() {
    this.handleResize();
  }

  componentDidMount() {
    this.mounted = true;
    window.addEventListener("resize", this.handleResize, false);
  }

  componentWillUnmount() {
    this.mounted = false;
    window.removeEventListener("resize", this.handleResize, false);
  }

  handleResize = () => {
    if (this.mounted) {
      this.setState(
        {
          viewport: { x: window.innerWidth, y: window.innerHeight }
        },
        () => {
          this.calcOffset();
        }
      );
    }
  };

  calcOffset() {
    const { intro } = this.props;
    if (!intro) return;

    const dims = this.descRef.getBoundingClientRect();
    const { type } = this.props;
    const x = type === `stats` ? 3 : 2;
    this.descRef.style.marginTop = `-${dims.height / x}px`;
  }

  checkPadding = ({ viewport }) => {
    const padding = viewport.x > 768 ? "30px 40px" : "15px 20px 30px";
    return padding;
  };

  render() {
    const { title, content, intro, type, projectLink } = this.props;

    const defaultStyles = {
      backgroundImage: `url(${Spots})`,
      backgroundRepeat: "repeat",
      backgroundColor: "transparent"
    };

    const linkToProject =
      projectLink && Object.prototype.hasOwnProperty.call(projectLink, "uid")
        ? projectLink
        : null;

    let bgImg = null;

    if (Object.prototype.hasOwnProperty.call(content, "mobile")) {
      bgImg =
        window.innerWidth <= 700
          ? Object.prototype.hasOwnProperty.call(content, "mobile")
            ? content.mobile.url
            : content.url
          : Object.prototype.hasOwnProperty.call(content, "url")
          ? content.url
          : null;
    } else {
      bgImg =
        window.innerWidth <= 700
          ? Object.prototype.hasOwnProperty.call(content, "tablet/mobile")
            ? content["tablet/mobile"].url
            : content.url
          : Object.prototype.hasOwnProperty.call(content, "url")
          ? content.url
          : null;
    }

    const paddingType =
      type === `stats`
        ? { padding: 0 }
        : { padding: this.checkPadding(this.state) };
    return (
      <div>
        {title && (
          <div ref={node => (this.titleRef = node)}>
            <PageHeading type={type} text={title} />
          </div>
        )}
        <div style={defaultStyles}>
          {linkToProject ? (
            <a href={`/projects/${linkToProject.uid}`}>
              <HeroWrapper
                outerHeader={title}
                innerRef={node => (this.heroRef = node)}
                style={{
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundImage: `url(${bgImg})`
                }}
              />
            </a>
          ) : (
            <HeroWrapper
              outerHeader={title}
              innerRef={node => (this.heroRef = node)}
              style={{
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundImage: `url(${bgImg})`
              }}
            />
          )}
        </div>

        <GridContainer type="pullRight">
          {intro && (
            <Description style={paddingType} innerRef={e => (this.descRef = e)}>
              {intro}
            </Description>
          )}
        </GridContainer>
      </div>
    );
  }
}

export default Hero;
