import React, { Component } from 'react';
import Prismic from 'prismic-javascript';
import PrismicReact from 'prismic-reactjs';
import debounce from 'lodash/debounce';
import styled from 'styled-components';

import { Grid, GridItem } from 'styled-grid-responsive';
import ProjectTeaser from '../Components/ProjectTeaser';
import PersonTeaser from '../Components/PersonTeaser';
import NewsSearchTeaser from '../Components/NewsSearchTeaser';
import GenericTeaser from '../Components/GenericTeaser';
import Cross from './icons/search-cross.svg';
import GridContainer from './GridContainer';
import media from '../utils/style-utils';

const SearchContent = styled.div`
  width: 100%;
`;

const SearchContainer = styled.div`
  visibility: ${props => (props.searchScreenOpen ? 'visible' : 'hidden')};
  pointer-events: ${props => (props.searchScreenOpen ? 'auto' : 'none')};
  opacity: ${props => (props.searchScreenOpen ? '1' : '0')};
  transition: all 0.3s ease-in-out;
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 9999;
`;

const SearchHeading = styled.div`
  height: 66px;
  background-color: rgba(42, 47, 56, 1);
  border-bottom: 1px solid ${props => props.theme.darkGrey7};
  width: 100%;
  position: fixed;
  top: 0;

  ${media.tabletLrg`
    height: 85px;
  `};

  > div {
    height: 100%;
  }
`;

const SearchInput = styled.input`
  background-color: rgba(42, 47, 56, 0.8);
  font-size: 40px;
  border: none;
  outline: none;
  height: 100%;
  width: calc(100vw - 60px);
  color: white;
  font-size: 24px;
  letter-spacing: 1px;
  font-family: ${props => props.theme.FFDINWebProMedium};
  ::-webkit-input-placeholder,
  ::-moz-placeholder,
  :-ms-input-placeholder,
  :-moz-placeholder {
    color: ${props => props.theme.darkGrey7};
  }
`;

const SearchClose = styled.button`
  position: absolute;
  background-color: transparent;
  border: none;
  color: white;
  right: 15px;
  outline: none;
  cursor: pointer;
  width: 22px;
  height: 22px;
  top: 50%;
  transform: translate3d(-50%, -50%, 0);

  ${media.tablet`
    right: 18px;
  `};

  > svg {
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 9999;
    left: 0;
    top: 0;
  }
`;

const SearchResults = styled.div`
  background-color: ${props =>
    props.searchResultsVisible
      ? 'rgba(42, 47, 56, 1)'
      : 'rgba(42, 47, 56, 0.8)'};
  padding: 85px 0 20px;
  position: fixed;
  height: 100%;
  width: 100%;
  z-index: 0;
  overflow-x: scroll;
`;

const SearchFilters = styled.div`
  font-family: ${props => props.theme.FFDINWebProMedium};
  margin-top: 30px;
  padding-bottom: 30px;
  display: flex;
  flex-wrap: nowrap;
  width: 100%;
  overflow: scroll;
`;

const Filter = styled.button`
  background: ${props => props.theme.darkGrey8};
  border: 1px solid
    ${props => (props.active ? props.theme.darkGrey7 : 'rgba(42, 47, 56, 1)')};
  transition: all 0.2s ease;
  margin-right: 20px;
  margin-bottom: 10px;
  color: white;
  padding: 6px 10px;
  font-size: 13px;
  line-height: 13px;
  width: 140px;
  min-width: 140px;
  max-width: 140px;
  text-align: left;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 2px;
  transition: all 0.2s ease;
  cursor: pointer;

  > span {
    font-family: ${props => props.theme.FFDINWebProBold};
    background-color: ${props =>
      props.active ? props.theme.primary : 'rgba(42, 47, 56, 1)'};
    border-radius: 50%;
    font-size: 11px;
    color: ${props => (props.active ? props.theme.secondary : 'white')};
    display: flex;
    text-align: center;
    height: 20px;
    width: 20px;
    align-items: center;
    justify-content: center;
    transition: all 0.2s ease;
  }
`;

class SearchScreen extends Component {
  constructor(props) {
    super(props);

    this.counter = 0;
    this.getKey = this.getKey.bind(this);
    this.state = {
      searchTerm: '',
      searchResults: '',
      filterResults: '',
      rowSize: 3
    };

    this.handleResize = debounce(this.handleResize, 100);
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize, false);
  }

  componentDidUpdate(prevProps, prevState) {
    const { searchScreenOpen } = this.props;
    if (searchScreenOpen)
      setTimeout(() => {
        this.inputRef.focus();
      }, 500);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize, false);
  }

  handleResize = () => {
    const ww = window.innerWidth;
    let rowSize;

    if (ww > 768) {
      rowSize = 3;
    } else if (ww <= 768 && ww >= 576) {
      rowSize = 2;
    } else {
      rowSize = 1;
    }

    this.setState(() => ({
      rowSize
    }));
  };

  fetchSearchData = (currentPage, term, currentContent, callback) => {
    const { prismic } = this.props;

    prismic.api
      .query([Prismic.Predicates.fulltext('document', term)], {
        pageSize: 100,
        page: currentPage
      })
      .then(response => {
        const oldContent = currentContent.concat(response.results);
        if (response.page < response.total_pages) {
          this.fetchSearchData(
            response.page + 1,
            term,
            oldContent,
            newContent => {
              callback(newContent);
            }
          );
        } else {
          callback(oldContent);
        }
      });
  };

  getKey() {
    return this.counter++;
  }

  handleInput = e => {
    this.setState({ searchTerm: e.target.value }, () => {
      const { searchTerm } = this.state;

      if (searchTerm.length) {
        this.fetchSearchData(1, searchTerm, [], res => {
          this.setState(
            {
              searchResults: res
            },
            () => {
              this.filterResults();
            }
          );
        });
      }

      this.setState({ searchResults: [], filterResults: [] });
    });
  };

  filterResults = () => {
    const { searchResults } = this.state;
    const typeArray = [];
    const uniquesArray = [];
    searchResults.map(res => {
      if (!(uniquesArray.indexOf(res.type) >= 0)) {
        switch (res.type) {
          case 'job_listing':
            uniquesArray.push(res.type);
            typeArray.push({
              type: res.type,
              name: 'Job Listings',
              quantity: this.returnQuantity(res.type)
            });
            break;
          case 'single_news_item':
            uniquesArray.push(res.type);
            typeArray.push({
              type: res.type,
              name: 'News',
              quantity: this.returnQuantity(res.type)
            });
            break;
          case 'single_project_page':
            uniquesArray.push(res.type);
            typeArray.push({
              type: res.type,
              name: 'Project',
              quantity: this.returnQuantity(res.type)
            });
            break;
          case 'person':
            uniquesArray.push(res.type);
            typeArray.push({
              type: res.type,
              name: 'People',
              quantity: this.returnQuantity(res.type)
            });
            break;
          case 'project_landing':
            uniquesArray.push(res.type);
            typeArray.push({
              type: res.type,
              name: 'Pages',
              quantity: this.returnQuantity(res.type)
            });
            break;
          default:
            return null;
        }
      }
      return null;
    });

    this.setState({
      filterResults: typeArray,
      filterTerm: typeArray.length > 0 ? typeArray[0].type : null
    });
  };

  returnQuantity(type) {
    const { searchResults } = this.state;
    let x = 0;
    searchResults.map(res => {
      if (res.type === type) x++;
    });
    return x;
  }

  closeSearch = () => {
    const { closeCallback } = this.props;
    closeCallback(true);
  };

  returnIfTrue = (item, filterTerm, type) => {
    if (item === type && filterTerm === type) {
      return true;
    }

    return false;
  };

  handleFilterClick = e => {
    const type = e.currentTarget.getAttribute('data-type');
    this.setState({ filterTerm: type });
  };

  render() {
    const {
      searchTerm,
      searchResults,
      filterResults,
      rowSize,
      filterTerm
    } = this.state;
    const { prismic, searchScreenOpen } = this.props;

    return (
      <SearchContainer searchScreenOpen={searchScreenOpen}>
        <SearchResults searchResultsVisible={searchResults.length > 0}>
          <GridContainer>
            <SearchFilters>
              {filterResults &&
                filterResults.map(result => (
                  <Filter
                    onClick={this.handleFilterClick}
                    data-type={result.type}
                    active={result.type === filterTerm ? 1 : 0}
                    key={this.getKey()}
                  >
                    {result.name}{' '}
                    <span active={result.type === filterTerm ? 1 : 0}>
                      {result.quantity}
                    </span>
                  </Filter>
                ))}
            </SearchFilters>
          </GridContainer>

          <SearchContent>
            <GridContainer>
              <Grid>
                {searchResults &&
                  searchResults.map(item => {
                    if (this.returnIfTrue(item.type, filterTerm, 'person')) {
                      return (
                        <GridItem
                          onClick={this.closeSearch}
                          col={1 / rowSize}
                          key={item.id}
                        >
                          <PersonTeaser
                            gridSize={50}
                            key={item.id}
                            person={item}
                            prismicCtx={prismic}
                            PrismicReact={PrismicReact}
                            location={false}
                            searchResult
                          />
                        </GridItem>
                      );
                    }

                    if (
                      this.returnIfTrue(
                        item.type,
                        filterTerm,
                        'single_project_page'
                      )
                    ) {
                      return (
                        <GridItem
                          onClick={this.closeSearch}
                          col={1 / rowSize}
                          key={item.id}
                        >
                          <ProjectTeaser
                            gridSize={50}
                            key={item.id}
                            project={item}
                            prismicCtx={prismic}
                            PrismicReact={PrismicReact}
                            searchResult
                          />
                        </GridItem>
                      );
                    }

                    if (
                      this.returnIfTrue(
                        item.type,
                        filterTerm,
                        'single_news_item'
                      )
                    ) {
                      return (
                        <GridItem
                          onClick={this.closeSearch}
                          col={1 / rowSize}
                          key={item.id}
                        >
                          <NewsSearchTeaser
                            key={item.id}
                            result={item}
                            PrismicReact={PrismicReact}
                            searchResult
                          />
                        </GridItem>
                      );
                    }

                    if (
                      this.returnIfTrue(
                        item.type,
                        filterTerm,
                        'project_landing'
                      )
                    ) {
                      return (
                        <GridItem
                          onClick={this.closeSearch}
                          col={1 / rowSize}
                          key={item.id}
                        >
                          <GenericTeaser
                            key={item.id}
                            result={item}
                            PrismicReact={PrismicReact}
                            searchResult
                          />
                        </GridItem>
                      );
                    }

                    if (
                      this.returnIfTrue(item.type, filterTerm, 'job_listing')
                    ) {
                      return (
                        <GridItem
                          onClick={this.closeSearch}
                          col={1 / rowSize}
                          key={item.id}
                        >
                          <GenericTeaser
                            key={item.id}
                            result={item}
                            PrismicReact={PrismicReact}
                            searchResult
                          />
                        </GridItem>
                      );
                    }

                    return null;
                  })}
              </Grid>
            </GridContainer>
          </SearchContent>
        </SearchResults>
        <SearchHeading>
          <GridContainer>
            <SearchInput
              type="text"
              value={searchTerm}
              placeholder="Search here"
              onChange={this.handleInput}
              autoFocus
              innerRef={el => (this.inputRef = el)}
            />
            <SearchClose onClick={this.closeSearch}>
              <Cross />
            </SearchClose>
          </GridContainer>
        </SearchHeading>
      </SearchContainer>
    );
  }
}

export default SearchScreen;
