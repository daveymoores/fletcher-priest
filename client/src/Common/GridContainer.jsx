import React from "react";
import styled from "styled-components";
import media from "../utils/style-utils";

let padding = [0, 0];
const containerType = (type, horzPadding) => {
    if (type === "footer") {
        return (padding = ["30px", horzPadding, "30px", horzPadding]);
    }
    if (type === "pullRight") {
        return (padding = [0, 0, 0, horzPadding]);
    }
    if (type === "pullLeft") {
        return (padding = [0, horzPadding, 0, 0]);
    }
    return [0, horzPadding, 0, horzPadding];
};

const Container = styled.div`
    padding: ${props => containerType(props.type, "15px")[0]}
        ${props => containerType(props.type, "15px")[1]}
        ${props => containerType(props.type, "15px")[2]}
        ${props => containerType(props.type, "15px")[3]};

    ${media.desktop`padding: ${props =>
        containerType(props.type, "40px")[0]} ${props =>
        containerType(props.type, "40px")[1]}
        ${props => containerType(props.type, "40px")[2]} ${props =>
        containerType(
            props.type,
            "40px"
        )[3]}`} ${media.giant`padding: ${props =>
        containerType(props.type, "60px")[0]} ${props =>
        containerType(props.type, "60px")[1]}
            ${props => containerType(props.type, "60px")[2]} ${props =>
        containerType(props.type, "60px")[3]}`};
`;

const GridContainer = ({ children, type }) => (
    <Container type={type}>{children}</Container>
);

export default GridContainer;
