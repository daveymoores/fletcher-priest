import React from "react";
import styled from "styled-components";
import media from "../utils/style-utils";
import Spots from "./textures/spots.png";

const FlGrid = styled.div`
    min-width: calc(100% / 1 + 1px);
    max-width: calc(100% / 1 + 1px);
    width: calc(100% / 1 + 1px);
    display: block;
    box-sizing: border-box;
    border: 1px solid ${props => props.theme.darkGrey};
    background-color: white;
    position: relative;
    margin-right: -1px;
    margin-top: -1px;

    ${media.mobile`
      min-width: calc(100% / 2 + 1px);
      max-width: calc(100% / 2 + 1px);
      width: calc(100% / 2 + 1px);
    `};

    ${media.tablet`
      min-width: ${props =>
          props.type === `3-col`
              ? `calc(100% / 4 + 1px)`
              : `calc(100% / 3 + 1px)`};
      max-width: ${props =>
          props.type === `3-col`
              ? `calc(100% / 4 + 1px)`
              : `calc(100% / 3 + 1px)`};
      width: ${props =>
          props.type === `3-col`
              ? `calc(100% / 4 + 1px)`
              : `calc(100% / 3 + 1px)`};
    `};

    > div {
        h3 {
            font-size: 21px;
            position: relative;

            &:: after {
                content: "";
                width: 40px;
                height: 4px;
                position: absolute;
                bottom: -0.8em;
                left: 0;
                background-color: ${props => props.theme.primary};
            }
        }

        h4 {
            font-size: 14px;
            margin: 25px 0 10px;
        }
    }

    h5 {
        font-size: 15px;
    }

    a > img {
        position: absolute;
        max-width: calc(100% - 40px);
        width: auto;
        max-height: 65%;
        top: 50%;
        left: 50%;
        transform: translate3d(-50%, -50%, 0);
        margin-top: 25px;
    }
`;

const FlGridParent = styled.div`
    display: block;
    border-collapse: collapse;
    width: 100%;
    background-image: url(${Spots});
`;

const FlGridRow = styled.div`
    display: flex;
    flex-wrap: wrap;

    @media all and (-ms-high-contrast: none) {
        width: calc(100% + 4px);
    }
`;

export const FluidGrid = ({ children, type }) => (
    <FlGrid type={type}>{children}</FlGrid>
);
export const FluidRow = ({ children }) => <FlGridRow>{children}</FlGridRow>;

export const FluidGridParent = ({ children }) => (
    <FlGridParent>{children}</FlGridParent>
);
