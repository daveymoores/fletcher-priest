import React, { Fragment } from "react";
import { Grid, GridItem } from "styled-grid-responsive";
import PrismicReact from "prismic-reactjs";
import styled from "styled-components";
import GridContainer from "./GridContainer";

const CreditContainer = styled.section`
  border-top: 1px solid grey;
  padding-top: 40px;
  margin-top: 40px;
`;

const StyledHeader = styled.header`
  font-size: 16px;
  font-family: ${props => props.theme.FFDINWebProBlack};
  margin-bottom: 20px;
`;

const StyledList = styled.ul`
  margin-top: 0;
  margin-bottom: 0;
  padding: 0;
`;

const ListItem = styled.li`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  font-size: 10px;
  line-height: 14px;
  color: ${props => props.theme.secondary};

  .divider {
    padding: 0 3px;
    color: inherit;
  }

  .project {
    font-family: ${props => props.theme.FFDINWebProBlack};
    color: inherit;
  }

  .name {
    font-family: ${props => props.theme.FFDINWebProMedium};
    color: inherit;
  }
`;

const Credits = ({ data }) => {
  const { items, primary } = data;
  let arr = [];

  const returnGridSegments = (item, index, array) => {
    const i = index + 1;
    const maxLength = array.length;

    arr.push(
      <ListItem key={i.toString()}>
        {PrismicReact.RichText.asText(item.content___project) !== "" && (
          <Fragment>
            <span className="project">
              {PrismicReact.RichText.asText(item.content___project)}
            </span>
            <span className="divider">/</span>
          </Fragment>
        )}
        <span className="name">
          {item.full_name && PrismicReact.RichText.asText(item.full_name)}
        </span>
      </ListItem>
    );

    if (arr.length === 4 || i === maxLength) {
      const newArr = arr;
      arr = [];
      return (
        <GridItem
          key={i.toString()}
          media={{ smallPhone: 1, mediumPhone: 1 / 2 }}
          col={1 / 4}
        >
          <StyledList key={i.toString()}>{newArr}</StyledList>
        </GridItem>
      );
    }

    return null;
  };

  return (
    <CreditContainer>
      <GridContainer>
        <StyledHeader>
          {primary.photography_credits_main_title.length
            ? PrismicReact.RichText.asText(
                primary.photography_credits_main_title
              )
            : `Photography Credits`}
        </StyledHeader>
        <Grid>
          {items.map((item, index, array) =>
            returnGridSegments(item, index, array)
          )}
        </Grid>
      </GridContainer>
    </CreditContainer>
  );
};

export default Credits;
