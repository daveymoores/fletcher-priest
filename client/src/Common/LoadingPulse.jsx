import React from "react";
import styled from "styled-components";

const StyledSpinner = styled.div`
    border-radius: 50px;
    line-height: 100px;
    text-align: center;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-transform: uppercase;
    letter-spacing: 0.05em;

    &:before,
    &:after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(253, 217, 39, 0.5);
        border-radius: 50px;
        opacity: 0;
    }

    &:before {
        transform: scale(1);
        animation: pulse 1s infinite linear;
    }

    &:after {
        animation: pulse 1s 1s infinite linear;
    }

    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 3;
    transform: translate3d(0, -50%, 0);
    display: ${props =>
        props.type === "visible" || props.type === "loading"
            ? "block"
            : "none"};
`;

const LoadingPulse = props => {
    const { loaderVisibility } = props;
    return <StyledSpinner type={loaderVisibility} />;
};

export default LoadingPulse;
