import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { setOffCanvasState } from "../redux/actionCreators";

class ScrollToTop extends Component {
    componentDidUpdate(prevProps) {
        const { location, offCanvas, handleOffCanvasChange } = this.props;

        // if its a different location, scroll to the top
        // if the location is the same but the search params have changed, don't
        if (location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0);
            if (offCanvas === "open") handleOffCanvasChange("closed");
        }
    }

    render() {
        const { children } = this.props;
        return children;
    }
}

const mapStateToProps = state => ({
    offCanvas: state.offCanvas
});

const mapDispatchToProps = dispatch => ({
    handleOffCanvasChange(value) {
        dispatch(setOffCanvasState(value));
    }
});

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(ScrollToTop)
);
