import React, { Component } from 'react';
import { withTheme } from 'styled-components';
import Loadable from 'react-loadable';
import { TweenMax as Tween, Power4 } from 'gsap/TweenMax';
import ReactGA from 'react-ga';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  Link
} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import CookieConsent from 'react-cookie-consent';
import store from './redux/store';
import Preview from './Preview';
import Footer from './Common/Footer';
import Navigation from './Common/Navigation';
import ScrollToTop from './Common/ScrollToTop';
import LoaderPageContent from './Loaders/LoaderPageContent';

const LoadableNotFound = Loadable({
  loader: () => import('./Pages/NotFound'),
  loading: () => <LoaderPageContent />
});

const LoadableSingleProject = Loadable({
  loader: () => import('./Pages/SingleProject'),
  loading: () => <LoaderPageContent />
});

const LoadableSingleNewsEvent = Loadable({
  loader: () => import('./Pages/SingleNewsEvent'),
  loading: () => <LoaderPageContent />
});

const LoadableProjectLanding = Loadable({
  loader: () => import('./Pages/ProjectLanding'),
  loading: () => <LoaderPageContent />
});

const LoadableHome = Loadable({
  loader: () => import('./Pages/Home'),
  loading: () => <LoaderPageContent />
});

const LoadableContact = Loadable({
  loader: () => import('./Pages/Contact'),
  loading: () => <LoaderPageContent />
});

const LoadablePractice = Loadable({
  loader: () => import('./Pages/Practice'),
  loading: () => <LoaderPageContent />
});

const LoadableNewsEvents = Loadable({
  loader: () => import('./Pages/NewsEvents'),
  loading: () => <LoaderPageContent />
});

const LoadableServicesLanding = Loadable({
  loader: () => import('./Pages/ServicesLanding'),
  loading: () => <LoaderPageContent />
});

const LoadableSingleService = Loadable({
  loader: () => import('./Pages/SingleService'),
  loading: () => <LoaderPageContent />
});

const LoadablePublications = Loadable({
  loader: () => import('./Pages/Publications'),
  loading: () => <LoaderPageContent />
});

const LoadableCareers = Loadable({
  loader: () => import('./Pages/Careers'),
  loading: () => <LoaderPageContent />
});

const LoadableJobListing = Loadable({
  loader: () => import('./Pages/JobListing'),
  loading: () => <LoaderPageContent />
});

const LoadableJobApplication = Loadable({
  loader: () => import('./Pages/JobApplication'),
  loading: () => <LoaderPageContent />
});

const LoadableAwards = Loadable({
  loader: () => import('./Pages/Awards'),
  loading: () => <LoaderPageContent />
});

const LoadableSectorsLanding = Loadable({
  loader: () => import('./Pages/SectorsLanding'),
  loading: () => <LoaderPageContent />
});

const LoadableSingleSector = Loadable({
  loader: () => import('./Pages/SingleSector'),
  loading: () => <LoaderPageContent />
});

const LoadablePeopleLanding = Loadable({
  loader: () => import('./Pages/PeopleLanding'),
  loading: () => <LoaderPageContent />
});

const LoadableSinglePerson = Loadable({
  loader: () => import('./Pages/SinglePerson'),
  loading: () => <LoaderPageContent />
});

const LoadableApproach = Loadable({
  loader: () => import('./Pages/Approach'),
  loading: () => <LoaderPageContent />
});

const LoadableCommunity = Loadable({
  loader: () => import('./Pages/Community'),
  loading: () => <LoaderPageContent />
});

const LoadableMessageSent = Loadable({
  loader: () => import('./Pages/MessageSent'),
  loading: () => <LoaderPageContent />
});

const LoadableMessageError = Loadable({
  loader: () => import('./Pages/MessageError'),
  loading: () => <LoaderPageContent />
});

const LoadablePrivacyPolicy = Loadable({
  loader: () => import('./Pages/PrivacyPolicy'),
  loading: () => <LoaderPageContent />
});

class App extends Component {
  componentDidMount() {
    ReactGA.initialize('UA-98797820-1');
  }

  render() {
    const { prismicCtx, theme } = this.props;
    const history = createHistory();

    return (
      <Router>
        <Provider store={store}>
          <ScrollToTop>
            <div className='push-footer'>
              <Navigation prismicCtx={prismicCtx} />
              <Switch>
                <Route
                  exact
                  path='/'
                  render={routeProps => (
                    <LoadableHome {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  path='/preview'
                  render={routeProps => (
                    <Preview {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  path='/practice'
                  render={routeProps => (
                    <LoadablePractice {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  path='/practice/community'
                  render={routeProps => (
                    <LoadableCommunity
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/practice/approach'
                  render={routeProps => (
                    <LoadableApproach {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  path='/projects/:uid'
                  render={routeProps => (
                    <LoadableSingleProject
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  history={history}
                  path='/projects'
                  render={routeProps => (
                    <LoadableProjectLanding
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/news-and-events/:uid'
                  render={routeProps => (
                    <LoadableSingleNewsEvent
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  history={history}
                  path='/news-and-events'
                  render={routeProps => (
                    <LoadableNewsEvents
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/services/:uid'
                  render={routeProps => (
                    <LoadableSingleService
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/services'
                  render={routeProps => (
                    <LoadableServicesLanding
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/sectors/:uid'
                  render={routeProps => (
                    <LoadableSingleSector
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/sectors'
                  render={routeProps => (
                    <LoadableSectorsLanding
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/contact'
                  render={routeProps => (
                    <LoadableContact {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  history={history}
                  path='/publications'
                  render={routeProps => (
                    <LoadablePublications
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/people'
                  history={history}
                  render={routeProps => (
                    <LoadablePeopleLanding
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/people/:uid'
                  render={routeProps => (
                    <LoadableSinglePerson
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/careers'
                  render={routeProps => (
                    <LoadableCareers {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  path='/careers/:uid'
                  render={routeProps => (
                    <LoadableJobListing
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/careers/:uid/application'
                  render={routeProps => (
                    <LoadableJobApplication
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/practice/awards'
                  render={routeProps => (
                    <LoadableAwards {...routeProps} prismicCtx={prismicCtx} />
                  )}
                />
                <Route
                  exact
                  path='/confirmation'
                  render={routeProps => (
                    <LoadableMessageSent
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/error'
                  render={routeProps => (
                    <LoadableMessageError
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  exact
                  path='/privacy-policy'
                  render={routeProps => (
                    <LoadablePrivacyPolicy
                      {...routeProps}
                      prismicCtx={prismicCtx}
                    />
                  )}
                />
                <Route
                  render={routeProps => (
                    <LoadableNotFound prismicCtx={prismicCtx} />
                  )}
                />
              </Switch>
            </div>
            <Footer prismicCtx={prismicCtx} />
            <CookieConsent
              location='bottom'
              buttonText='I accept'
              cookieName='CookieConsentFinal1'
              hideOnAccept={false}
              onAccept={() => {
                const ctx = document.querySelector('.cookieConsent');
                Tween.to(ctx, 0.4, {
                  yPercent: 100,
                  ease: Power4.easeInOut,
                  onComplete: () => {
                    ctx.style.display = 'none';
                  }
                });
              }}
              style={{
                background: '#2A2F38',
                fontFamily: 'FFDINWebProMedium, sans-serif',
                alignItems: 'center'
              }}
              buttonStyle={{
                padding: '15px 20px',
                fontSize: '16px',
                color: '#000000',
                background: theme.primary,
                cursor: 'pointer'
              }}
              expires={150}
            >
              We use cookies to help us improve your experience on our website.
              By closing this notification or interacting with the website you
              agree to our use of cookies.{' '}
              <Link
                to='/privacy-policy'
                style={{ display: 'inline', color: theme.primary }}
              >
                Find out more →
              </Link>
            </CookieConsent>
          </ScrollToTop>
        </Provider>
      </Router>
    );
  }
}

export default withTheme(App);
