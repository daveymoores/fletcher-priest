# Fletcher Priest

This project is split between the frontend in the `client` directory, and the root which contains the server code used for redirects, handling mail and some small templating features.

## Packages of note

1.  Webpack 4
2.  Node 8.10
3.  react
4.  Prismic

### Frontend Usage

project is served through port 8080

`cd client/`

`yarn install`

`yarn dev` - runs development build

`yarn prod` - runs production build

`yarn prod:analyze` - runs bundle analysis

### Run server

Project is served through port 3001

`npm run dev` - runs concurrently nodemon and runs a prod build in client.

or

`npm run server`

if the frontend code is already built and changes are just being made to the server code.
