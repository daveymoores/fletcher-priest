const readApacheRedirectFile = require('../redirect');
const getUrl = require('../getUrl');

describe('Redirects', () => {
  test('getUrl takes url and returns null', async () => {
    getUrl('www.google.com').then(value => {
      expect(value).toBeFalsy();
    });
  });

  test('getUrl takes url in nginx file and returns redirect url', () => {
    getUrl('/get-in-touch/london/').then(value => {
      expect(value).toEqual('https://www.fletcherpriest.com/contact');
    });
  });

  test('readApacheRedirectFile takes params and calls redirect', () => {
    const req = {
      url: '/get-in-touch/london/index.html'
    };

    const res = {
      redirect: jest.fn(),
      end: jest.fn()
    };

    const next = jest.fn();

    jest.mock('../getUrl', () => Promise.resolve(true));

    readApacheRedirectFile(req, res, next);
    getUrl()
      .then(value => {
        expect(value).toBeTruthy();
      })
      .catch(err => {
        expect(err).toBeThrown();
      });
  });
});
