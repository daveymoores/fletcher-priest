const getUrl = require('./getUrl');

async function readApacheRedirectFile(req, res, next) {
  const url = req.url.replace('index.html', '');
  const currentUrl = `${req.protocol}://${req.get('host')}`;

  try {
    const redirectUrl = await getUrl(url);
    const fullUrl = redirectUrl.replace(
      'https://www.fletcherpriest.com',
      currentUrl
    );
    if (fullUrl) {
      res.redirect(fullUrl);
      res.end();
    }
  } catch (err) {
    next();
  }
}

module.exports = readApacheRedirectFile;
